# Teaser

## Étape 1

Avant l'ouverture du challenge, un teaser est annoncé. Dans le code source de la page d'accueil:

```html
    <p>
      En attendant l'ouverture, un premier flag est à trouver sur ce site. 🔥
      <!-- /teasing -->
    </p>
```

On se rend donc à l'adresse `/teasing`, qui annonce un point de départ quelque part sur le site. On regarde de nouveau le code et:
```html
<center>  
   <img src="/files/bdfb24976c411e1611ff2859e59ccfc3/stegano.png" style="width: 40%;" alt="stegano.png" />
</center>
```

## Étape 2

Voyons donc cette image de plus près:
![stegano](teaser.png)

Les données Exif cachent une URL:
```shell
$ strings -n 10 stegano.png 
https://www.youtube.com/watch?v=dQw4w9WgXcQ
RY7*"5).#-
@S}2rHVJLB
-11Q9;9:;9>
wTrJjpXtPX
<|"9%5 0"d
W^~AxdLxdL
r#6.16>)&.
t+=#6.qw\bl\
(,,J9s>6.1jW
IHMu>o	}TQ
```

Mais c'est une fausse piste (je vous laisse découvrir pourquoi).

On va plutôt regarder le contenu de l'image : il semble s'agit du logo du FCSC, mais on remarque deux nuances de blanc différentes. En les isolant, on obtient :

![stegano](teaser2.jpg)

Ça ressemble à un texte chiffré avec des caractères représentant des dinosaures. On peut essayer de la résoudre comme une mono-substitution, car on l'analyse des fréquences des caractères réfute un poly-susbtitution (j'ai déjà remplacé quelque caractères spéciaux pour faciliter la chose) :

`ABCDEDEFGDHIHKLHMCGGHBNCMBHOPHBHHUCMHLHQHQRCNNHISHDEFGUBEFDHBHKNCTPICQHUUHCLBHGGHRUUMG://TBCIQH-QWAHBGHQFBPUW-QRCNNHISH.TB/AMAQOBMNTSKOKUSSSMAQ`

Après une analyse et quelques hypothèses, on arrive à reconstituer le texte :

`bravo vous venez de passer la premiere etape de ce challenge vous trouverez la fin a cette adresse https://france-cybersecurity-challenge.fr/bpbcmrplfgzmztgggpbc`

## Étape 3

La page nous donne alors un fichier [fcsc.8xp](fcsc.8xp) à analyser. C'est un code assembleur pour une Texas Instrument TI-83 (`TI-83+ Graphing Calculator (assembly program)`). Pour la suite, il va seulement nous falloir deux éléments :

- un désassembleur correct. Après quelques tâtonnements, c'est finalement le [z80disassembler de Dan Weiss](https://ticalc.org/archives/files/authors/22/2284.html) qui m'a servi car il a le mérite de résoudre les interruptions TI et les pointeurs de chaînes !.
- un bon bouquin sur l'assembleur Z80 : j'ai ressorti ma version originale du [Z80 de Rodney Zacks](https://archive.org/details/Programming_the_Z-80_2nd_Edition_1980_Rodnay_Zaks) ;-) .

On désassemble le binaire, ce qui donne [ce résultat](fcsc.z80). L'analyse se fait relativement simplement, les processeurs 8 bits ne débordant pas de subtilités (en dehors peut-être des registres implicites). 

On repère donc le fonctionnement global :
- le programme demande la saisie de 32 caractères.
- ceux-ci sont vérifiés un à un, non pas littéralement, mais à partir de XOR successifs.
- si tous les caractères sont corrects, on génère le flag FCSC{} à partir de ceux-ci, avec un petit décalage pour chacun.

Voici un bout de code python qui fait le boulot:
```python
xors = [
    0xc2, 0x09, 0x0d, 0x0a, 0x07, 0x0c, 0x09, 0x10, 0x14, 0x07, 0x06, 0x0e, 0x0a, 0x0f, 0x0b, 0x0a, 
    0x0a, 0x01, 0x08, 0x0c, 0x0a, 0x08, 0x07, 0x0e, 0x1a, 0x1c, 0x07, 0x0b, 0x10, 0x1f, 0x02, 0x03]

result = list()
acc = 0x50
for xor in xors:
    acc ^= xor
    result.append(acc)

output = [ r-0x5E if r<0x9a else r-0x59 for r in result ]
print( 'FCSC{' + ''.join([ f'{r:c}' for r in  output ]) + '}' )
```

Le flag est : `FCSC{4B8CB9E0ADB7F2B3BA4E6CB7156F1243}`
