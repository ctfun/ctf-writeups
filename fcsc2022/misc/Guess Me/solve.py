#!/usr/bin/env python3

# pip install pwntools
from pwn import *

HOST = args.HOST or "challenges.france-cybersecurity-challenge.fr"
PORT = args.PORT or 2001

max = 256 ** 8
min = 0
count = 0
c = remote(HOST, PORT)
c.recvuntil(b">>> ")
while True:
    count += 1
    guess = (max+min)//2
    c.sendline(str(guess).encode('utf-8'))
    answer = c.recvuntil( [b'>>> ', b'}'] ).rstrip(b'\n>>> ').decode('utf-8')
    print(f'\r{guess} => {answer} ({count=})', end='')
    if 'FCSC' in answer:
        print()
        exit(0)
    elif '+1' in answer:
        min = guess
    elif '-1' in answer:
        max = guess
    elif '0' in answer:
        print()
        max = 256 ** 8
        min = 0
        count = 0