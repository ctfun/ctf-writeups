# Guess Me

Devine le secret !

Fichier: [guessme.py](guessme.py)

`nc challenges.france-cybersecurity-challenge.fr 2001`

**Solution**

Un grand classique, dans lequel il faut deviner un nombre et à chaque tentative, on nous indique si on est trop haut ou trop bas. [Un petit algorithme dichotomique](solve.py), et on n'en parle plus :

```console
$ python3 solve.py 
[+] Opening connection to challenges.france-cybersecurity-challenge.fr on port 2001: Done
543246170695408901 => 0
1 found, 15 more to go (count=64)
[...]
11171363940108613513 => 0
16 found, 0 more to go
FCSC{7b20416c4f019ea4486e1e5c13d2d1667eebac732268b46268a9b64035ab294d} (count=64)
[*] Closed connection to challenges.france-cybersecurity-challenge.fr port 2001
```