Cette épreuve est en plusieurs parties :

[[_TOC_]]

# Color Plant 1/2

Description du contexte : [ici](contexte.md)

Dans cette première partie du challenge `Color Plant`, vous manipulez une usine en production dans un système industriel via le protocole industriel Modbus. La première étape de ce challenge est de récupérer un token présent dans les registres de l'automate afin de pouvoir acceder à l'interface web SCADA. Pour cela, vous devez lire dans les registres de l'automate à l'aide du protocole Modbus et tenter de réassembler un token valide. Vous pourrez alors continuer la deuxième partie de l'épreuve.

Fichier: [docker-compose.yml](https://hackropole.fr/challenges/fcsc2022-misc-color-plant/docker-compose.public.yml)

**Solution**

C'est la petite entrée dans le monde modbus. On cherche le token, et le [contexte](contexte.md) nous indique que les registres 0 à 31 nous donne le token du joueur.

On choisit de le résoudre en python, et la doc de `pyModbusTCP` indique une ouverture de la communication par `ModbusClient()` et une lecture des registres par `read_holding_registers()`. On n'oublie pas d'ouvrir le navigateur sur l'interface avec le token **avant** de couper la communication. Le code est donc :

```python
#!/usr/bin/env python3

import os

# pip install pyModbusTCP
from pyModbusTCP.client import ModbusClient
c = ModbusClient(host="localhost", port=4502, unit_id=1, auto_open=True)

regs = c.read_holding_registers(0, 32)
if regs:
    print(regs)
    token = ''.join([ chr(r) for r in regs])
    os.system(f'firefox http://localhost:8000/{token}')
    input('Press any key to stop')
else:
    print("read error")
```

Et le navigateur affiche:

![flag1](flag1.png)

Premier flag : `FCSC{266350f412840c932b29bb095394d318c17c844f70c05f49c9998a8e614be531}`

# Color Plant 2/2

Description du contexte : [ici](contexte.md)

Dans cette deuxième partie du challenge `Color Plant`, votre objectif sera de manipuler l'usine en production afin de remplir une cuve avec la couleur RGB(32, 126, 42) et ce dans un temps limité à 90 secondes. Pour cela, vous devrez ouvrir et fermer des vannes au bon moment, ajuster des débits en manipulant les différents registres de l'automate via le protocole Modbus.

Fichier: [docker-compose.yml](https://hackropole.fr/challenges/fcsc2022-misc-color-plant/docker-compose.public.yml)

**Solution**

Même méthode que précédemment, on reprend la doc et on essaie d'interagir avec le système pour bien comprendre :
- on ouvre/ferme une vanne par `write_single_coil()` (0 ou 1).
- on règle son débit via un registre : `write_single_register()`. Quelques essais montre un point important : on ne peut pas dépasser un débit de 5 unités par secondes.
- on peut récupérer les quantités présentes dans les cuves avec `read_input_registers()`.

L'objectif est de faire un mix de 32, 126 et 42, soit une somme de 200, et évidemment, la cuve de mixage n'est pas suffisante. Heureusement, on se rend compte qu'en deux passes de 16, 64 et 21, on est tout bon.

On notera aussi que notre travail est facilité par les systèmes automatiques. Par exemple, lorsque la cuve de mixage est pleine (ce qui va être le cas pendant nos deux passes), elle va se vidanger automatiquement sans action de notre part.

Pour terminer, il y a plusieurs design possibles, par exemple ouvrir les vannes à un certain débit et de temporiser jusqu'à obtenir la bonne quantité (ou celle que l'on pense). J'ai choisi plutôt de me baser sur les valeurs réelles retournées par les capteurs, ce qui me semble plus en phase avec ce qui se fait dans l'industrie.

L'un dans l'autre, [ma solution](solve.py) a des améliorations possibles, notamment au niveau des débits, ou encore en temporisant les boucles de lecture des valeurs. Mais elle fonctionne bien, dans les limites de temps imparties. 

Pour une raison que je comprends pas, c'est extrêmement satisfaisant à regarder.

![](solve.gif)

Et le résultat affiché est le suivant ;

![flag2](flag2.png)

Second flag: FCSC{3518dcd9579b4f2bf4ab32f126aa746e00767d6b130ef6c2e79ae6a313d0ba07}
