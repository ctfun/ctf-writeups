Cette épreuve est en plusieurs parties :

[[_TOC_]]

# Échec OP 0/3

Quel est l'identifiant unique (UUID) de la table de partition de ce disque ? Une fois que vous l'aurez trouvé, encadrez le dans FCSC{} pour obtenir le flag. Par exemple FCSC{1111-2222-3333-4444}.

SHA256(fcsc.7z) = fe23478be033fb87db95313650619d95a3756d90d272e82887d70936c7700f5c (5.4GB).

SHA256(fcsc.raw) = 18b33658c9fc8e81666f04999bd38cb6709c6a7399d8a43a72028caa278067bf (10GB).

> Note : le fichier fcsc.7z est le même pour tous les challenges Echec OP.

**Solution**

Après décompression de l'archive, on jette un coup d'œil aux partitions :

```console
$ fdisk -l  fcsc.raw
Disque fcsc.raw : 10 GiB, 10737418240 octets, 20971520 secteurs
Unités : secteur de 1 × 512 = 512 octets
Taille de secteur (logique / physique) : 512 octets / 512 octets
taille d'E/S (minimale / optimale) : 512 octets / 512 octets
Type d'étiquette de disque : gpt
Identifiant de disque : 60DA4A85-6F6F-4043-8A38-0AB83853E6DC

Périphérique   Début      Fin Secteurs Taille Type
fcsc.raw1       2048     4095     2048     1M Amorçage BIOS
fcsc.raw2       4096  1861631  1857536   907M Système de fichiers Linux
fcsc.raw3    1861632 20969471 19107840   9,1G Système de fichiers Linux
```

Le flag 0 est donc `FCSC{60DA4A85-6F6F-4043-8A38-0AB83853E6DC}`.

# Échec OP 1/3

L'administrateur de ce serveur a chiffré son disque, le mot de passe est fcsc2022.

Quelle est la date de la création du système de fichiers en UTC ?

Le flag est au format ISO 8601, tel que dans l'exemple suivant : FCSC{2022-04-22T06:59:59Z}.

SHA256(fcsc.7z) = fe23478be033fb87db95313650619d95a3756d90d272e82887d70936c7700f5c (5.4GB).

SHA256(fcsc.raw) = 18b33658c9fc8e81666f04999bd38cb6709c6a7399d8a43a72028caa278067bf (10GB).

> Note : le fichier fcsc.7z est le même pour tous les challenges Echec OP.

**Solution**

Cette fois-ci, on va monter l'image :

```console
$ sudo mount -o loop,offset=953155584 fcsc.raw tmp/
mount: <REDACTED>/tmp: type de système de fichiers « crypto_LUKS » inconnu.
```

En effet, pour rappel l'énoncé nous avait prévenu que le système de fichier est chiffré. On sait maintenant que c'est du LUKS, et on a le mot de passe, c'est donc une formalité :

```console
$ sudo losetup /dev/loop0 fcsc.raw
$ sudo kpartx -a /dev/loop0
$ sudo cryptsetup luksOpen /dev/mapper/loop0p3 img
Saisissez la phrase secrète pour /dev/mapper/loop0p3 : 
```

On nous demande la date de création du système de fichier, on va donc regarder les méta-données de LVM :

```console
$ sudo lvdisplay 
  --- Logical volume ---
  LV Path                /dev/ubuntu-vg/ubuntu-lv
  LV Name                ubuntu-lv
  VG Name                ubuntu-vg
  LV UUID                W4Y1My-22pb-DbM1-o1IU-dBKO-pJ6O-FcE7sG
  LV Write Access        read/write
  LV Creation host, time ubuntu-server, 2022-03-27 05:44:49 +0200
  LV Status              available
  # open                 0
  LV Size                9,09 GiB
  Current LE             2328
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           253:14
```

Le premier flag est donc `FCSC{2022-03-27T03:44:49Z}`.

# Échec OP 2/3

Retrouvez le mot de passe de l'utilisateur principal de ce serveur. La force ne résout pas tout... Le mot de passe correspond au flag, entouré de FCSC{}, par exemple : FCSC{password}. Aussi, l'administrateur de ce serveur a chiffré son disque et le mot de passe est fcsc2022.

SHA256(fcsc.7z) = fe23478be033fb87db95313650619d95a3756d90d272e82887d70936c7700f5c (5.4GB).

SHA256(fcsc.raw) = 18b33658c9fc8e81666f04999bd38cb6709c6a7399d8a43a72028caa278067bf (10GB).

> Note : le fichier fcsc.7z est le même pour tous les challenges Echec OP.

**Solution**

On reprend là où on avait laissé le disque, on va maintenant monter le volume logique trouvé à l'étape précédente :

```console
$ mkdir fs
$ sudo mount /dev/ubuntu-vg/ubuntu-lv fs/
```

On cherche l'utilisateur principal. On regarde le fichier des condensats de mots de passe :

```console
$ sudo grep "\$6"  fs/etc/shadow 
obob:$6$cvD51kQkFtMohr9Q$vE2L5CUX3jDZgVUZGOFNUFsSHGomH/EP5yYQA3dcKMm9U00mvA9pLzo7Z.Ki6exchu29jEENxtBdGUXCISNxL0:19078:0:99999:7:::
```

`obob` est le seul utilisateur ayant un mot passe. C'est probablement notre homme, mais puisque la force brute n'est pas nécessaire, il faut trouver un autre moyen. On peut chercher le mot de passe dans des fichiers de configuration, par exemple, en privilégiant le répertoire de l'utilisateur.

Dans /etc, on trouve un deuxième condensat :

```console
$ sudo grep -r obob etc/
etc/cloud/cloud.cfg.d/99-installer.cfg:      \ obob\n  passwd: $6$LsjILusk598MwelR$s7gJi8gOoGhMpT312QNDZWime6sCed9MGN92XA5cS48jgOjSrfit.wmuktR0qAG8nAaaGMa4fzaHqePiFX5dE.\n\
```

Cela ne nous aide toujours pas. En revanche, dans le répertoire de root:

```console
$ sudo cat fs/root/.bash_history
exit
passwd obob 
CZSITvQm2MBT+n1nxgghCJ
exit
```

Les mots de passe qui fuitent dans les historiques de shells, un grand classique !

Le deuxième flag est `FCSC{CZSITvQm2MBT+n1nxgghCJ}`.

# Échec OP 3/3

L'administrateur semble avoir essayé de dissimuler l'une de ses adresses IP avec laquelle il a administré ce serveur. Aidez nous à retrouver cette adresse. Une fois l'IP trouvée, encadrez-la dans FCSC{} pour avoir le flag (par exemple : FCSC{1.2.3.4}).

Attention : vous n'avez que 5 essais.

SHA256(fcsc.7z) = fe23478be033fb87db95313650619d95a3756d90d272e82887d70936c7700f5c (5.4GB).

SHA256(fcsc.raw) = 18b33658c9fc8e81666f04999bd38cb6709c6a7399d8a43a72028caa278067bf (10GB).

> Note :
>     Le fichier fcsc.7z est le même pour tous les challenges Echec OP.
>     Le disque est chiffré, le mot de passe est fcsc2022.

**Solution**

On va chasser des IPs dans les logs :
```console
$ sudo egrep -r "[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+"  fs/var/log
```

Cela sort beaucoup de choses, et après un peu de ménage, on trouve :
```
fs/var/log/nginx/access.log:172.16.123.130 - - [27/Mar/2022:21:44:19 +0000] "GET / HTTP/1.1" 200 396 "-" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:87.0) Gecko/20100101 Firefox/87.0"
fs/var/log/nginx/access.log:172.16.123.130 - - [27/Mar/2022:21:44:19 +0000] "GET /favicon.ico HTTP/1.1" 404 134 "http://172.16.123.129/" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:87.0) Gecko/20100101 Firefox/87.0"
fs/var/log/nginx/access.log:172.16.123.130 - - [27/Mar/2022:21:44:23 +0000] "GET /coucou HTTP/1.1" 404 134 "-" "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:87.0) Gecko/20100101 Firefox/87.0"
```

Mais si on essaie `FCSC{172.16.123.130}`, c'est raté. Mon autre idée est alors de regarder dans les logs plus difficiles à nettoyer, notamment ceux au format binaire, comme `/var/log/btmp` ou `/var/log/wtmp`. Pour cela, je fais un `chroot` sur le système de fichiers et je lance les commandes usuelles :

```console
$ sudo chroot fs/ /bin/bash
root@XXXXXX:/# last
obob     pts/0        172.16.123.1     Sun Mar 27 21:50 - 21:51  (00:00)
obob     pts/0        172.16.123.1     Sun Mar 27 21:49 - 21:50  (00:01)
obob     pts/0        172.16.123.1     Sun Mar 27 21:40 - 21:49  (00:09)
obob     pts/0        172.16.123.1     Sun Mar 27 21:37 - 21:39  (00:02)
obob     pts/0        172.16.123.1     Sun Mar 27 21:36 - 21:36  (00:00)
obob     pts/0        172.16.123.1     Sun Mar 27 21:36 - 21:36  (00:00)
obob     pts/0        172.16.123.1     Sun Mar 27 21:35 - 21:36  (00:00)
obob     pts/0        172.16.123.1     Sun Mar 27 21:33 - 21:35  (00:02)
obob     pts/0        172.16.123.1     Sun Mar 27 21:31 - 21:32  (00:01)
obob     pts/0        172.16.123.1     Sun Mar 27 21:30 - 21:31  (00:00)
obob     pts/0        172.16.123.1     Sun Mar 27 21:29 - 21:30  (00:01)
reboot   system boot  5.4.0-105-generi Sun Mar 27 17:28 - 21:51  (04:23)

wtmp begins Sun Mar 27 17:28:29 2022
```

On essaie `FCSC{172.16.123.1}`, mais c'est encore raté ! Étant donné le nombre d'essais limité, je commence à transpirer.

Je décide alors de changer de technique. Si des traces ont été effacées, on doit pouvoir restaurer les fichiers en cause. Un premier essai avec [photorec](https://www.cgsecurity.org/wiki/PhotoRec_FR) m'a décu (il a crashé méchamment), alors je me suis retourné vers les méthodes décrites ici : https://www.forensicfocus.com/articles/a-linux-forensics-starter-case-study/ :

Je me suis appuyé sur la partie intitulée *file carving*. Étant donné que j'avais introduit des modifications dans le système (notamment à coup de `chroot`), je suis reparti de zéro en redécompressant l'archive, et en m'arrêtant avant le montage du volume logique. Cette fois-ci, on va regarder le journal du system `ext4`.

On commence par dumper le journal, puis on essaie de retrouver les éléments de fichiers disparus après une certaine date :
```console
$ sudo debugfs -R 'dump <8> ./journal' /dev/mapper/ubuntu--vg-ubuntu--lv
$ AFTER=$(date -d"2022-03-27 00:00:00" +%s)
$ echo $AFTER
1648335600
$ sudo ext4magic /dev/mapper/ubuntu--vg-ubuntu--lv -a $AFTER -f var -j journal -m -d output/
```

Les résultats sont astronomiques, donc je recherche tout ce qui ressemble à une adresse IP (`[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+`) :
```
output/MAGIC-2/text/plain/I_0000614204.txt:Mar 27 21:28:33 obob sshd[982]: Server listening on 0.0.0.0 port 22.
output/MAGIC-2/text/plain/I_0000614204.txt:Mar 27 21:29:40 obob sshd[1212]: Accepted password for obob from 172.16.123.1 port 55180 ssh2
output/MAGIC-2/text/plain/I_0000614204.txt:Mar 27 21:30:43 obob sshd[1356]: Received disconnect from 172.16.123.1 port 55180:11: disconnected by user
output/MAGIC-2/text/plain/I_0000614204.txt:Mar 27 21:30:43 obob sshd[1356]: Disconnected from user obob 172.16.123.1 port 55180
output/MAGIC-2/text/plain/I_0000614204.txt:Mar 27 21:30:47 obob sshd[1427]: Accepted password for obob from 172.16.123.1 port 55182 ssh2
output/MAGIC-2/text/plain/I_0000614204.txt:Mar 27 21:31:08 obob sshd[1515]: Received disconnect from 172.16.123.1 port 55182:11: disconnected by user
output/MAGIC-2/text/plain/I_0000614204.txt:Mar 27 21:31:08 obob sshd[1515]: Disconnected from user obob 172.16.123.1 port 55182
output/MAGIC-2/text/plain/I_0000614204.txt:Mar 27 21:31:14 obob sshd[1547]: Accepted password for obob from 172.16.123.1 port 55184 ssh2
```

On l'a déjà essayé celle-là. Mais un peu plus loin :

```
output/MAGIC-3/text/plain/0000034812.txt:Mar 27 04:12:24 obob sshd[1571]: pam_unix(sshd:auth): authentication failure; logname= uid=0 euid=0 tty=ssh ruser= rhost=192.168.37.1  user=obob
output/MAGIC-3/text/plain/0000034812.txt:Mar 27 04:12:26 obob sshd[1571]: Failed password for obob from 192.168.37.1 port 41864 ssh2
output/MAGIC-3/text/plain/0000034812.txt:Mar 27 04:12:29 obob sshd[1571]: Accepted password for obob from 192.168.37.1 port 41864 ssh2
output/MAGIC-3/text/plain/0000034812.txt:Mar 27 04:12:29 obob sshd[1571]: pam_unix(sshd:session): session opened for user obob by (uid=0)
```

On essaie de valider, et cette fois, c'est la bonne ! Le troisième flag est donc `FCSC{192.168.37.1}`.