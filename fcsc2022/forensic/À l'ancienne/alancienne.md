# À l'ancienne

Vous devez récupérer et analyser les données échangées dans cette capture. On préfère prévenir, avant de paniquer, il va falloir se décontracter et décompresser pour faire ça tranquillement.

Fichiers: [cap](cap)

SHA256(`cap`) = `27117fc9487e8ca1a54f7d6a55f39b3223153451a8df41bb02488c2a99dbf059`.

**Solution**

Une ouverture de la capture avec `wireshark` permet de constater qu'il contient essentiellement des échanges DNS :

![dns](dns1.png)

De toute évidence, les noms à résoudre sont louches :

![dns](dns2.png)

Il s'agit de requêtes DNS avec des noms à résoudre un peu bizarre, du genre :
`H4sICFctM2IAA3Bhc3N3ZACVVl9v4zYMf**n0OMG1H-.BsJ2mjt9sCbMNde0FT3Oug2GosxJY8SU7SffqRlPOv-.1nY3SHVFiqQo8icy1hjPj3wCw*IyDd*N0ulGuPquEr-.I1GiQyGAOR9s6mDkQuq1SbxmyVvkPukecwaD8u5N4d-.cGFzc3dk`

Ça ressemble à du base64 séparé par des `-.`. Le dernier élément se répète sur plusieurs lignes, et semble être un nom de fichier.

Cela est suffisant pour poser une hypothèse : on est en présence d'une exfiltration de fichiers par le canal DNS. Chaque fichier est encodé en base64, puis découpé en petit morceaux, qui vont être utilisés pour former des noms à résoudre. Le dernier morceau de chacun des noms est le nom du fichier. On remarque aussi qu'on a des caractères '*' dans le base64 et pas de '+'. On suppose que c'est une feinte parce que + n'est (ou n'était ?) pas autorisé dans un nom DNS ?

Bref, on bricole [un script](solve.py) pour décoder tout ça à l'aide du module [`dpkt`](https://pypi.org/project/dpkt/). À noter que préalablement, il faut exporter le fichier au formap `pcap` via `wireshark`, car le format d'origine n'est pas géré par `dpkt`.

Voici le script :
```python
#!/usr/bin/env python3

import dpkt
import base64

prec_name = ''
filecontent = ''
f = open('cap.pcap','rb')
for ts, pkt in dpkt.pcap.Reader(f):
    # On ne garde que les query DNS
    eth=dpkt.ethernet.Ethernet(pkt) 
    if eth.type!=dpkt.ethernet.ETH_TYPE_IP:
       continue

    ip=eth.data
    if ip.p!=dpkt.ip.IP_PROTO_UDP: 
        continue

    dns = dpkt.dns.DNS(ip.data.data)
    if dns.qr != dpkt.dns.DNS_Q:
        continue
    content = dns.qd[0].name
```
On commence par nettoyer la trace en ne gardant que les paquets UDP qui font des requêtes DNS (pour éviter les doublons si on inclut les réponses).


```python
    # On décode le truc
    frags = content.split('-.')
    name = base64.b64decode(frags[-1]).decode('utf-8')
    if name != prec_name and prec_name != '': 
        print(f'Writing {prec_name}')
        # Attention, les + ont été remplacé par des *
        filecontent = filecontent.replace('*', '+')
        filecontent = base64.b64decode(filecontent)
        with open(f'{prec_name}.gz', 'wb') as out:
            out.write(filecontent)
        filecontent = ''
    prec_name = name
    for f in frags[:-1]:
        filecontent += f
```

Pour chaque requête, on concatène tous les morceaux en base64 sauf le dernier, que l'on décode pour obtenir le nom du fichier. Si ce nom a changé depuis l'itération précédente, il est nécessaire de décoder le contenu qu'on a obtenu jusqu'ici et on le sauvegarde avec le nom que l'on avait. Remarquez qu'on les sauvegarde avec l'extension `.gz` car il s'avère qu'ils sont compressés.

Au final, on obtient 4 fichiers:

```shell
$ file *
passwd:          ASCII text
file1:           JPEG image data, JFIF standard 1.01, aspect ratio, density 1x1, segment length 16, baseline, precision 8, 1667x1667, components 3
file3:           PNG image data, 80 x 80, 8-bit/color RGBA, non-interlaced
file4:           Microsoft Word 2007+
```

Ainsi:
- [passwd](passwd) est un fichier /etc/passwd sans intérêt ici.
- [file1](file1.jpg) et [file3](file3.png) sont des images (jolies, mais bon...)
- [file4](file4.doc) est un document Word qui contient le flag.

Le flag est donc `FCSC{18e955473d2e12feea922df7e1f578d27ffe977e7fa5b6f066f7f145e2543a92}`.