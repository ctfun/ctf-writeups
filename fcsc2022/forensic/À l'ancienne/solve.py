#!/usr/bin/env python3

import dpkt
import base64

prec_name = ''
filecontent = ''
f = open('cap.pcap','rb')
for ts, pkt in dpkt.pcap.Reader(f):
    # On ne garde que les query DNS
    eth=dpkt.ethernet.Ethernet(pkt) 
    if eth.type!=dpkt.ethernet.ETH_TYPE_IP:
       continue

    ip=eth.data
    if ip.p!=dpkt.ip.IP_PROTO_UDP: 
        continue

    dns = dpkt.dns.DNS(ip.data.data)
    if dns.qr != dpkt.dns.DNS_Q:
        continue
    content = dns.qd[0].name

    # On décode le truc
    frags = content.split('-.')
    name = base64.b64decode(frags[-1]).decode('utf-8')
    if name != prec_name and prec_name != '': 
        print(f'Writing {prec_name}')
        # Attention, les + ont été remplacé par des *
        filecontent = filecontent.replace('*', '+')
        filecontent = base64.b64decode(filecontent)
        with open(f'{prec_name}.gz', 'wb') as out:
            out.write(filecontent)
        filecontent = ''
    prec_name = name
    for f in frags[:-1]:
        filecontent += f


