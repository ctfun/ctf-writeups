# FCSC 2022

![](rank.png)

- [Le teaser](teaser/teaser.md)
- [Intro](intro/intro.md)

## Crypto

- [Shuffled](crypto/Shuffled/Shuffled.md)

## Misc

- [Guess Me](misc/Guess%20Me/guessme.md)
- [Color Plant](misc/Color%20Plant/colorplant.md)

## Forensic

- [À l'ancienne](forensic/À%20l'ancienne/alancienne.md)
- [Échec OP](forensic/Échec%20OP/echecop.md)

## Hardware

- [Daddy Morse](hardware/Daddy%20Morse/daddymorse.md)
- [I2CyouToo](hardware/I2CyouToo/ic2youtoo.md)
- [Mommy Morse](hardware/Mommy%20Morse/mommymorse.md)

## Web

- [Gare au Gorille](web/Gare%20Au%20Gorille/gareaugorille.md)
- [Login Portal](web/Login%20Portal/loginportal.md)
- [MC Players](web/MC%20Players/mcplayers.md)
- [Avatar Generator](web/Avatar%20Generator/avatargenerator.md)