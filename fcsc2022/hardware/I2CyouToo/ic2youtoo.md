# I2CyouToo

Un ami vous affirme qu'une EEPROM de 1024 bits branchée à son Arduino est une solution extrêmement sécurisée pour stocker ses secrets, vu que le protocole utilisé est "obfusqué" et "bas niveau" selon ses dires, "pas comme l'USB qui est hotplug et qu'on peut brancher directement sur n'importe quel OS !".

Voulant le confronter à ses inepties, vous sortez votre analyseur logique pour sniffer la communication entre l'Arduino et l'EEPROM lorsqu'il tape son secret. Pourrez-vous remonter à ce dernier avec vos logiciels Sigrok et gtkwave ?

Fichier: [capture.vcd](capture.vcd)

SHA256(`capture.vcd`) = `790b2960100407d34c8f92b2d24fa33fd3867ce4aebe4d6c799c3b4ef4a0bbf8`.

**Solution**

On ouvre le VCD dans pulseview. On sait qu'il s'agit d'[i2c](https://fr.wikipedia.org/wiki/I2C) avec donc deux fils, la ligne de données et l'horloge. En zoomant sur une partie où on voit le signal, on détermine que `D2` est l'horloge et `D3` les données :

![pulseview](pulseview.png)

On lance alors l'analyse par `sigrok` (`scl` est l'horloge, et `sda` les données):
```shell
sigrok-cli -i capture.vcd  -P i2c:scl=D2:sda=D3 | tee values
```

Quand ça ne scrolle plus, on fait un coup de Ctrl+c car sigrok ne s'arrête pas tout seul et on regarde les valeur `Data write`. 

```
[...]
i2c-1: Start
i2c-1: 0
i2c-1: 0
i2c-1: 0
i2c-1: 0
i2c-1: 1
i2c-1: 0
i2c-1: 1
i2c-1: 1
i2c-1: Write
i2c-1: Address write: 68
i2c-1: ACK
[...]
```

Visiblement, ce sont des codes ASCII qu'on s'empresse de récupérer :
```console
$ grep "Data write" values  | cut -d" " -f4 | xargs
00 46 01 43 02 53 03 43 04 7B 05 4D 06 59 07 2D 08 50 09 52 0A 45 0B 43 0C 49 0D 4F 0E 55 0F 53 10 2D 11 50 12 4C 13 45 14 41 15 53 16 45 17 2D 18 53 19 54 1A 41 1B 59 1C 2D 1D 53 1E 45 1F 43 20 52 21 45 22 54 23 21 25 7D
```

Ça ressemble à des couples identifiants/caractères. Les caractères fournissent un flag (l'identifiant 0x24 manque mais ça ne semble pas poser de problème). On ne garde donc qu'un élément sur deux et on décode ça en python :

```python
>>> ''.join( map( lambda x: chr(int(x,16)), '00 46 01 43 02 53 03 43 04 7B 05 4D 06 59 07 2D 08 50 09 52 0A 45 0B 43 0C 49 0D 4F 0E 55 0F 53 10 2D 11 50 12 4C 13 45 14 41 15 53 16 45 17 2D 18 53 19 54 1A 41 1B 59 1C 2D 1D 53 1E 45 1F 43 20 52 21 45 22 54 23 21 25 7D'.split(' ')[1::2] ) )
'FCSC{MY-PRECIOUS-PLEASE-STAY-SECRET!}'
```