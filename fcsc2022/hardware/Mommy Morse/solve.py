#!/usr/bin/env python3

from pwn import *
import numpy as np
import base64

HOST = args.HOST or "challenges.france-cybersecurity-challenge.fr"
PORT = args.PORT or  2252
MSG = args.MSG or 'CAN I GET THE FLAG'

SAMP_RATE = 24000

FREQ_HIGH = 5e3
FREQ_LOW = 1e3

TIMING_DOT = 1/1000
TIMING_DASH = 5/1000
TIMING_SEP_LETTER = 5/1000
TIMING_SPACE = 20/1000

alphabet = { 'A':'.-', 'B':'-...',
            'C':'-.-.', 'D':'-..', 'E':'.',
            'F':'..-.', 'G':'--.', 'H':'....',
            'I':'..', 'J':'.---', 'K':'-.-',
            'L':'.-..', 'M':'--', 'N':'-.',
            'O':'---', 'P':'.--.', 'Q':'--.-',
            'R':'.-.', 'S':'...', 'T':'-',
            'U':'..-', 'V':'...-', 'W':'.--',
            'X':'-..-', 'Y':'-.--', 'Z':'--..',
            '1':'.----', '2':'..---', '3':'...--',
            '4':'....-', '5':'.....', '6':'-....',
            '7':'--...', '8':'---..', '9':'----.',
            '0':'-----', ', ':'--..--', '.':'.-.-.-',
            '?':'..--..', '/':'-..-.', '-':'-....-',
            '(':'-.--.', ')':'-.--.-'}


# Stolen from https://github.com/jgibbard/iqtool
def generateTone(fs, toneFreq, numSamples):
    #Generates a sinusoidal signal with the specified frequency
    step = (float(toneFreq) / float(fs)) * 2.0 * np.pi
    phaseArray = np.array(range(0,int(numSamples))) * step
    #Euler's Formular: e^(j*theta) = cos(theta) + j * sin(theta)
    #For a complex sinusoidal theta = 2*pi*f*t where each time step is 1/fs    
    wave = np.exp(1.0j * phaseArray)
    
    return wave

# Non, mais non...
DOT = generateTone(SAMP_RATE, FREQ_HIGH, SAMP_RATE*TIMING_DOT).astype(np.complex64).tobytes()
DASH = generateTone(SAMP_RATE, FREQ_HIGH, SAMP_RATE*TIMING_DASH).astype(np.complex64).tobytes()
SEP_DIGIT = generateTone(SAMP_RATE, FREQ_LOW, SAMP_RATE*TIMING_DOT).astype(np.complex64).tobytes()
SEP_LETTER = generateTone(SAMP_RATE, FREQ_LOW, SAMP_RATE*TIMING_SEP_LETTER).astype(np.complex64).tobytes()
SEP_SPACE = generateTone(SAMP_RATE, FREQ_LOW, SAMP_RATE*TIMING_SPACE).astype(np.complex64).tobytes()


mots = list()
for word in MSG.split(' '):
    lettres = list()
    for letter in word:
        lettre = list()
        for digit in alphabet[letter]:
            if digit == '.':
                lettre.append(DOT)
            else:
                lettre.append(DASH)
        #print(f'{letter} => {lettre}')
        lettres.append(SEP_DIGIT.join(lettre))
    mots.append(SEP_LETTER.join(lettres))
msg = SEP_SPACE.join(mots)

c = remote(HOST, PORT)
c.recvuntil(b"> ")
c.sendline(base64.b64encode(msg))
print(c.recvline())