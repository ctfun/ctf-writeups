# Mommy Morse

On vous demande d'envoyer un message en Morse avec une modulation de fréquence à deux états. Le codage choisi est que les . et - sont représentés par une porteuse pure à une fréquence de 5kHz, et les espacements sont représentés par une porteuse pure à une fréquence de 1kHz.

Vous devez envoyer CAN I GET THE FLAG.

Vous avez le code du serveur ainsi qu'un exemple de message à disposition.

Les paramètres de transmission sont les suivants :
- fréquence d'échantillonage : 24kHz
- envoi d'un . : porteuse pure de fréquence 5kHz pendant 1 milliseconde
- durée d'un - : porteuse pure de fréquence 5kHz pendant 5 millisecondes
- espacement entre deux lettres : porteuse pure de fréquence 1kHz pendant 5 millisecondes
- espace entre deux mots : porteuse pure de fréquence 1kHz pendant 20 millisecondes

`nc challenges.france-cybersecurity-challenge.fr 2252`

Fichiers : [client.py](client.py) [server.py](server.py) [signal.iq](signal.iq)

sha256(`signal.iq`) = `abaf0cabdda55a40f9707ea12813ab701b3b052142af9838a2821998354166`

**Solution**

Pas grand chose à dire ici. C'est le même principe que [Daddy Morse](../Daddy%20Morse/daddymorse.md), mais cette fois, on ne va pas couper à générer un vrai signal.

Après pas mal de galère, j'ai finalement trouvé ce que je cherchais sur dans [le projet iqtool](https://github.com/jgibbard/iqtool). J'ai repris en particulier le générateur de tonalité que j'ai simplifié pour mes besoins:

```python
# Stolen from https://github.com/jgibbard/iqtool
def generateTone(fs, toneFreq, numSamples):
    #Generates a sinusoidal signal with the specified frequency
    step = (float(toneFreq) / float(fs)) * 2.0 * np.pi
    phaseArray = np.array(range(0,int(numSamples))) * step
    #Euler's Formular: e^(j*theta) = cos(theta) + j * sin(theta)
    #For a complex sinusoidal theta = 2*pi*f*t where each time step is 1/fs    
    wave = np.exp(1.0j * phaseArray)
    return wave
```

Au final, le script complet est [ici](solve.py) et fourni le résultat suivant:

```console
$ python solve.py 
[+] Opening connection to challenges.france-cybersecurity-challenge.fr on port 2252: Done
b'Well done: FCSC{490b88345a22d35554b3e319b1200b985cc7683e975969d07841cd56dd488649}\n'
```