# Daddy Morse

Les télégraphes Morse permettaient d'échanger des messages de texte à longue distance, en encodant un message sous forme d'impulsions électriques. Le serveur se comporte comme un mini-télégraphe et décode les données que vous lui envoyez.

Vous devez envoyer CAN I GET THE FLAG.

Vous avez le code du serveur ainsi qu'un exemple de message à disposition.

Les paramètres de transmission sont les suivants :
- fréquence d'échantillonage : 24kHz
- durée d'un . : 1 milliseconde
- durée d'un - : 5 millisecondes
- espacement entre deux lettres : 5 millisecondes
- espace entre deux mots : 20 millisecondes

`nc challenges.france-cybersecurity-challenge.fr 2251`

Fichiers: [server.py](server.pty) [client.py](client.py) [signal.py](signal.py)

SHA256(`signal.iq`) = `1199792626c6894321613ec3668cb21e6521221c7665cd9b4b84db36f4e8c58e`.

**Solution**

Voyons ce que ça donne si on met le client que l'on nous a donné en face du serveur :
```console
$ python3 client.py 
[+] Opening connection to challenges.france-cybersecurity-challenge.fr on port 2251: Done
b'You said HELLO\n'
[*] Closed connection to challenges.france-cybersecurity-challenge.fr port 2251
```

On sait donc que `signal.iq` contient `HELLO`. À l'aide d'un éditeur hexadécimal :
![hexa](hexa.png)

Sachant que `HELLO` se code en morse en `.... . .-.. .-.. ---`, on en déduit que :
- `.` se code en 48 occurrences de `\x00\x00\x80\x3f`.
- `-` aussi, mais en 240 occurrences.
- enfin, la séparation entre les lettres et les mots sont des répétitions de respectivement 48 et 240 `\x00\x00\x00\x00`.

> Vous trouvez ça bourrin comme méthode ? C'est un CTF, il faut être efficace, pas élégant. ;-) Et c'est probablement pour cette raison que les organisateurs proposaient [une autre épreuve du même genre](../Mommy%20Morse/mommymorse.md).

[Ce script](solve.py) permet de résoudre l'épreuve et sa trace d'exécution est :
```console
$ python3 solve.py 
[+] Opening connection to challenges.france-cybersecurity-challenge.fr on port 2251: Done
[+] Receiving all data: Done (82B)
[*] Closed connection to challenges.france-cybersecurity-challenge.fr port 2251
Well done: FCSC{e8b4cad7d00ca921eb12824935eea3b919e5748264fe1c057eef4de6825ad06c}
```