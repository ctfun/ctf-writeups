#!/usr/bin/env python3

from pwn import *
import base64

HOST = args.HOST or "challenges.france-cybersecurity-challenge.fr"
PORT = args.PORT or  2251
MSG = args.MSG or 'CAN I GET THE FLAG'

dot = b'\x00\x00\x80\x3f' * 48 
dash = b'\x00\x00\x80\x3f' * 240
sep_digit = b'\x00\x00\x00\x00' * 48
sep_letter = b'\x00\x00\x00\x00' * 240
sep_word = sep_letter * 4

alphabet = { 'A':'.-', 'B':'-...',
            'C':'-.-.', 'D':'-..', 'E':'.',
            'F':'..-.', 'G':'--.', 'H':'....',
            'I':'..', 'J':'.---', 'K':'-.-',
            'L':'.-..', 'M':'--', 'N':'-.',
            'O':'---', 'P':'.--.', 'Q':'--.-',
            'R':'.-.', 'S':'...', 'T':'-',
            'U':'..-', 'V':'...-', 'W':'.--',
            'X':'-..-', 'Y':'-.--', 'Z':'--..',
            '1':'.----', '2':'..---', '3':'...--',
            '4':'....-', '5':'.....', '6':'-....',
            '7':'--...', '8':'---..', '9':'----.',
            '0':'-----', ', ':'--..--', '.':'.-.-.-',
            '?':'..--..', '/':'-..-.', '-':'-....-',
            '(':'-.--.', ')':'-.--.-'}

words = list()
for w in MSG.split(' '):
    letters = list()
    for c in w:
        letter = alphabet[c]
        letter = [ dot if d=='.' else dash for d in letter ]
        letter = sep_digit.join(letter)
        letters.append(letter)
    word = sep_letter.join(letters)
    words.append(word)
msg = sep_word.join(words)

c = remote(HOST, PORT)
c.recvuntil(b"> ")
c.sendline(base64.b64encode(msg))
result = c.recvall()
print(result.decode('utf-8'))