# Shuffled

Oops, nous avons mélangé les caractères du flag. Pourrez-vous les remettre dans l'ordre ?

Fichiers : [shuffle.py](shuffle.py) [output.txt](output.txt)

**Solution**

On analyse le code source qui nous est fourni : 
```python
flag = list(open("flag.txt", "rb").read().strip())
random.seed(random.randint(0, 256))
random.shuffle(flag)
print(bytes(flag).decode())
```

Le flag est lu, puis est mélangé aléatoirement, avant d'être écrit dans `output.txt`. La chose intéressante, c'est que le générateur de nombres pseudo-aléatoires est ici initialisé à l'aide d'une graine, elle-même étant un nombre tiré au hasard entre 0 et 255.

Cela signifie qu'il est assez simple pour nous de tester les 256 graines possibles pour reproduire le comportement du code. Mais comment savoir quelle graine est la bonne ? Une possibilité est de simplement se souvenir que les flags ont la forme `FCSC{xxxx}`, ainsi, si on applique la même transformation à un faux flag de la même forme, si les caractères connus (`F`, `C`, `S`, `{` et `}`) se retrouvent dans la même position dans le `output.txt` et dans notre cas, c'est que l'on a trouvé la bonne graine, et il suffit alors d'inverser le mélange.

Pour se faire, j'ai utilisé le code suivant ([solve.py](solve.py)):
```python
cipher = 'f668cf029d2dc4234394e3f7a8S9f15f626Cc257Ce64}2dcd93323933d2{F1a1cd29db'

# On cherche la bonne graine:
for seed in range(257):
    random.seed(seed)
    test = list('FCSC{                                                                }')
    random.shuffle(test)
    found = True
    for c in 'FCS{}':
        if cipher.index(c) != test.index(c):
            found = False   
            break
    if found:
        print(f'Found seed {seed}')
        break
```

Dans cette première partie, on génère toutes les graines possibles et on applique le mélange correspondant à un flag de la bonne taille, mais vide. Si les emplacements des caractères correspondent, on a notre clef.

Puis:
```python
# Maintenant, on cherche les correspondances
start = 'FCSC{0123456789abcdefghijklmnopqrstuvwxyzABDEGHIJKLMNOPQRTUVWXYZ?./,;}'
enc = list(start)
plain = ''
random.seed(seed)
random.shuffle(enc)
enc = ''.join(enc)
for c in start:
    plain += cipher[enc.index(c)]
print(plain)

# Vérification
verif = list(plain)
random.seed(seed)
random.shuffle(verif)
result = ''.join(verif)
if result != cipher:
    print('Wrong :')
    print(cipher)
    print(result)
```

Cette fois, on part d'un flag fictif dans lequel chaque caractère est unique. On applique le mélange associé à la clef, et on regarde quelle est la position finale de chacun des caractères de départ. On en déduit la position initiale du caractère dont la position est la même dans `output.txt`.

Le résultat de ce code est la suivant:
```
Found seed 125
FCSC{d93d32485aec7dc7622f13cd93b922363911c36d2ffd4f829f4e3264d0ac6952}
```