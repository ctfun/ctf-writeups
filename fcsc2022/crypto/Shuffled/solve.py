#!/usr/bin/env python3
 
import random

cipher = 'f668cf029d2dc4234394e3f7a8S9f15f626Cc257Ce64}2dcd93323933d2{F1a1cd29db'

# On cherche la bonne graine:
for seed in range(257):
    random.seed(seed)
    test = list('FCSC{                                                                }')
    random.shuffle(test)
    found = True
    for c in 'FCS{}':
        if cipher.index(c) != test.index(c):
            found = False   
            break
    if found:
        print(f'Found seed {seed}')
        break

# Maintenant, on cherche les correspondances
start = 'FCSC{0123456789abcdefghijklmnopqrstuvwxyzABDEGHIJKLMNOPQRTUVWXYZ?./,;}'
enc = list(start)
plain = ''
random.seed(seed)
random.shuffle(enc)
enc = ''.join(enc)
for c in start:
    plain += cipher[enc.index(c)]
print(plain)

# Vérification
verif = list(plain)
random.seed(seed)
random.shuffle(verif)
result = ''.join(verif)
if result != cipher:
    print('Wrong :')
    print(cipher)
    print(result)
