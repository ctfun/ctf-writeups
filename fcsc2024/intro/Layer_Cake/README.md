## Layer Cake 1/3

**Description**

Un développeur de GoodCorp souhaite publier une nouvelle image Docker. Il utilise une variable d’environnement stockant un flag au moment du build, et vous assure que ce secret n’est pas visible du public. L’image est `anssi/fcsc2024-forensics-layer-cake-1`.

Récupérez ce flag et prouvez-lui le contraire.

**Solution**

Pour obtenir les éléments ayant permis la construction de l'image `Docker`, on peut utiliser
la commande `docker history` :

```shell
$ docker pull anssi/fcsc2024-forensics-layer-cake-1:latest
[...]
$ docker history --no-trunc anssi/fcsc2024-forensics-layer-cake-1:latest 
IMAGE                                                                     CREATED        CREATED BY                                                                                          SIZE      COMMENT
sha256:0faa62781dd1db0ebb6cd83836bb4ba24f8b58b0cd761ac0cbae426bccc7666f   3 months ago   CMD ["/bin/sh"]                                                                                     0B        buildkit.dockerfile.v0
<missing>                                                                 3 months ago   USER guest                                                                                          0B        buildkit.dockerfile.v0
<missing>                                                                 3 months ago   ARG FIRST_FLAG=FCSC{a1240d90ebeed7c6c422969ee529cc3e1046a3cf337efe51432e49b1a27c6ad2}               0B        buildkit.dockerfile.v0
<missing>                                                                 3 months ago   /bin/sh -c #(nop)  CMD ["/bin/sh"]                                                                  0B        
<missing>                                                                 3 months ago   /bin/sh -c #(nop) ADD file:37a76ec18f9887751cd8473744917d08b7431fc4085097bb6a09d81b41775473 in /    7.38MB    
```

## Layer Cake 2/3

**Description**

Un développeur de GoodCorp souhaite publier une nouvelle image Docker. Il copie au moment du build un fichier contenant un flag, puis le supprime. Il vous assure que ce secret n’est pas visible du public. L’image est `anssi/fcsc2024-forensics-layer-cake-2`.

Récupérez ce flag et prouvez-lui le contraire.

**Solution**

Cette fois-ci, on va utiliser [dive](https://github.com/wagoodman/dive) pour inspecter l'image :

![](layer_cake2_dive.png)

On voit la copie d'un fichier `/tmp/secret` ajouté lors du build, mais il est supprimé à l'étape suivante. La bonne nouvelle, c'est que chaque
«instruction» Docker génère un layer spécifique, et celui-ci peut être retrouvé. On pourrait faire une analyse poussée, mais le plus simple,
c'est d'essayer de trouver le fichier secret dans le répertoire où Docker stocke ses layers (traditionnellement `/var/lib/docker`) :

```shell
$ sudo find /var/lib/docker -name secret | xargs sudo ls -l
-r-------- 1  405 root   71 mars  25 10:05 /var/lib/docker/overlay2/1d94d4e7a25d09086383eccdd0a0cbe154224fa555d721addb5a1e57cafa3f9d/diff/tmp/secret
c--------- 1 root root 0, 0 mai    1 14:43 /var/lib/docker/overlay2/e63ee0cdac634dbced880b0e03cec051c379af160c40cb1837bbc6b3808be0fa/diff/tmp/secret
```

Seul le premier est un fichier :
```shell
$ sudo cat /var/lib/docker/overlay2/1d94d4e7a25d09086383eccdd0a0cbe154224fa555d721addb5a1e57cafa3f9d/diff/tmp/secret
FCSC{b38095916b2b578109cbf35b8be713b04a64b2b2df6d7325934be63b7566be3b}
```

(Sinon, on `grep FCSC` dans `/var/lib/docker` comme un porc et ça marche aussi ;-) )

## Layer Cake 3/3

**Description**

Un développeur de GoodCorp souhaite publier une nouvelle image Docker. Suite à ses mésaventures avec les Dockerfile, il décide d’utiliser [Nix](https://nixos.org/) pour construire son image. En utilisant Nix, il donne un flag en argument à un service. Il vous assure que ce secret n’est pas visible du public. L’image est anssi/`fcsc2024-forensics-layer-cake-3`.

Récupérez ce flag et prouvez-lui le contraire.

**Solution**

On lance là aussi la commande `dive` sur l'image :

![](layer_cake3_dive.png)

Peu d'information, mais on note l'identifiant du layer, et on le recherche dans notre arborescence Docker, en particulier, on extrait l'identifiant
de cache correspondant :

```shell
$ sudo cat /var/lib/docker/image/overlay2/layerdb/sha256/8ea6eb4812d48d7aee7de57a65ba99e4d3c3958fee6eb973419cf7aace4c7fec/cache-id
a4364f8f36a327d29d323f1fe3771e06bfafcb55479d8da71f5ad86c70f08476
```

Ensuite on explore le répertoire correspondant à ce cache, `overlay2/a4364f8f36a327d29d323f1fe3771e06bfafcb55479d8da71f5ad86c70f08476` et on y trouve :
```shell
$ sudo cat /var/lib/docker/overlay2/a4364f8f36a327d29d323f1fe3771e06bfafcb55479d8da71f5ad86c70f08476/diff/nix/store/m8ww0n3iqndg8zaiwbsnij6rvmpmjbry-hello/bin/hello
#!/nix/store/5lr5n3qa4day8l1ivbwlcby2nknczqkq-bash-5.2p26/bin/bash
exec /nix/store/rnxji3jf6fb0nx2v0svdqpj9ml53gyqh-hello-2.12.1/bin/hello -g "FCSC{c12d9a48f1635354fe9c32b216f144ac66f7b8466a5ac82a35aa385964ccbb61}" -t
```

(Sinon, comme pour la précédente,  on `grep FCSC` dans `/var/lib/docker` comme un porc ;-) )
