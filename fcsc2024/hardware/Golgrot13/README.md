# Golgrot13

**Description**

Votre ami bidouilleur était frustré de la communication série trop simple entre ses deux boards IoT, susceptible d’être interceptée par des personnes malveillantes. Il a donc élaboré sa méthode d’obfuscation qu’il a fièrement nommée “Golgrot13”: saurez-vous lui prouver que son idée n’est pas des plus brillantes à partir d’une capture à l’analyseur logique?

Fichier: [golgrot13.vcd](https://hackropole.fr/challenges/fcsc2024-hardware-golgrot13/public/golgrot13.vcd)

**Solution**

Probablement l'épreuve la plus facile de cette édition (en tout cas à mon avis...) !
Comme d'habitude en présence d'un fichier `vcd`, on l'ouvre avec [pulseview](https://github.com/sigrokproject/pulseview). On
trouve deux signaux dans la capture. Le plus simple est de considérer qu'il s'agit d'[`UART`](https://fr.wikipedia.org/wiki/UART), les signaux étant `RX` et `TX`.
On active donc le décodeur associé avec une sortie `ASCII`, et :

![](pulseview.png)

Même si ça semble n'avoir aucun sens, il n'y a pas d'erreur de décodage,
donc on est sur la bonne piste. Le relevé des échanges donne ceci :

```
Cyrnfr. tvir zr gur synt!
SPFP{649rnqn20921959qrr486s0rpr7q7s89}
```

Évidemment, difficile de rater ce qui ressemble à un flag, et la mention de [`ROT13`](https://fr.wikipedia.org/wiki/ROT13) dans le nom de l'épreuve. Un décodage rapide nous donne la réponse :

```shell
$ echo "Cyrnfr. tvir zr gur synt!" | tr "a-zA-Z" "n-za-mN-ZA-M"
Please. give me the flag!
$ echo "SPFP{649rnqn20921959qrr486s0rpr7q7s89}" | tr "a-zA-Z" "n-za-mN-ZA-M"
FCSC{649eada20921959dee486f0ece7d7f89}
```