# Castafiore

**Description**

Depuis peu, votre téléphone réagit à des commandes vocales inconnues, mais vous n’entendez rien. À partir d’un enregistrement audio, comprenez pourquoi.

Fichier: [silent.wav](https://hackropole.fr/challenges/fcsc2024-hardware-castafiore/public/silent.wav)

**Solution**

On écoute le fichier wav et... on entend quelque chose, mais c'est n'est pas intelligible. Mais, et je ne suis pas le seul à
l'avoir remarqué, ça ressemble tout à fait au son historique du «cryptage» de Canal+. Donc j'ai commencé des
recherches à ce sujet :

> The system turned the audio into an unbearable whine by splitting up the signal into two bands and inverting the high and low ends. ([hackaday](https://hackaday.com/2020/06/25/old-tech-french-tv-encryption-1980s-style/))

> Non codé, le signal audio est électroniquement transposé en fréquences et sa phase inversée (12,8 kHz). Pour activer la restitution du son, le décodeur détecte le niveau de blanc et de noir des lignes 310 et 622, en procédant au traitement du signal vidéo. ([Wikipedia](https://fr.wikipedia.org/wiki/Discret_11#Principes_techniques))

Au détour [de forums sur le sujet](https://forum.audacityteam.org/t/son-crypte-canal/9103/5), on obtient un plugin Audacity.

```
;nyquist plug-in
;version 1
;type process
;name "Modulation Canal+"
;action "Modulation échantillon"
;info "by yrraHM\nLe filtre passe-haut est Butterworth 1er ordre.\n"

;control Freq "Fréquence porteuse (Hz)" real "" 12800 1 22000
;control Cutoff "Cutoff du filtre passe-haut (Hz)" real "" 1 1 22000
;control Gain "Gain applicable (dB)" real " " 0 -10 10

(loud Gain (mult (hp s Cutoff) (hzosc Freq)))
```

On l'essaie toute de suite; le résultat est pas top mais audible. 
Malgré tout, il y a un ou deux digits du flag que je ne parviens pas à 
interpréter.

En retournant dans Audacity et en changeant la visualisation pour le 
spectre d'onde, on se rend compte que la porteuse est plus vers 13.5 kHz
que les 12.8 kHz de Canal+. 

![](audacity.png)

Je relance le plug-in avec cette valeur, et cette
fois-ci, tout est parfaitement audible (enfin, j'ai quand même baissé la vitesse...) : [résultat](Castafiore.wav).

Le flag énoncé dans le wav est `FCSC{2035351597102220198194}`.