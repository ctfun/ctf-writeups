# SOC Simulator 

**Description**

Durant l’été 2022, un opérateur d’importance vitale (OIV) alerte l’ANSSI car il pense être victime d’une cyberattaque d’ampleur. Le *security operation center* (SOC) de l’OIV envoie à l’ANSSI un export de sa collecte système des derniers jours. Vous êtes chargé de comprendre les actions réalisées par l’attaquant.

File: [soc_events.zip](https://hackropole.fr/filer/fcsc2024-forensics-soc-simulator/public_filer/soc_events.zip)

**Introduction aux solutions**

Avant de démarrer les différentes épreuves, il est important de
montrer quel est l'environnement que j'ai mis en place. Mes 
résolutions se basent sur l'ingestion des évènements fournis 
dans un ELK. Voici les instructions nécessaires pour l'obtenir,
sur une machine Unix.

Premièrement, on installe le duo Elasticsearch & Kibana. J'ai simplement
suivi la documentation officielle pour [l'un](https://www.elastic.co/guide/en/elasticsearch/reference/current/deb.html) et [l'autre](https://www.elastic.co/guide/en/kibana/current/deb.html).

Ensuite, j'avais l'habitude d'utiliser [python-evtx](https://github.com/williballenthin/python-evtx) pour extraire le 
contenu des enregistrements, mais force est de constater que cela ne
fonctionne pas ici. Je me suis alors rabattu sur une alternative écrite
en Rust : [evtx](https://github.com/omerbenamram/evtx). J'ai aussi
écrit [un script python](bulk_elk.py) permettant de transformer le résultat dans
des requêtes `bulk` prêtes à être envoyée à Elasticsearch.

Au final, l'ingestion comporte les étapes suivantes :
```shell
# dumpe les evtx en json
for evtx in soc_events/*.evtx; do evtx_dump -o json "$evtx" > "evtx_json/$evtx.json"; done
# transforme les jsons en requête bulk pour Elastic
for evtx in evtx_json/soc_events/*; do python bulk_elk.py $evtx evtx_json/elk/$(basename $evtx .evtx.json).json; done
# Envoie à ELK
for evtx in evtx_json/elk/*; do curl http://192.168.1.8:9200/soc/_bulk -H "Content-Type: application/x-ndjson" --data-binary @$evtx; done
```

Ensuite, dans Kibana, on ouvre le `Discover` et on ouvre l'indice `soc`.
De mémoire, au premier accès il demande quel champ utilisé pour la
réprésentation dans le temps, et j'ai évidemment utilisé le champ `timestamp`.

J'ai aussi rapidement décidé d'utiliser [chainsaw](https://github.com/WithSecureLabs/chainsaw) :
> Chainsaw provides a powerful ‘first-response’ capability to quickly identify threats within Windows forensic artefacts such as Event Logs and the MFT file.

On consomme encore un peu de CPU pour cela :

```shell
$ chainsaw/chainsaw hunt soc_events/ -s chainsaw/sigma/ --mapping chainsaw/mappings/sigma-event-logs-all.yml  --json > chainsaw.json

 ██████╗██╗  ██╗ █████╗ ██╗███╗   ██╗███████╗ █████╗ ██╗    ██╗
██╔════╝██║  ██║██╔══██╗██║████╗  ██║██╔════╝██╔══██╗██║    ██║
██║     ███████║███████║██║██╔██╗ ██║███████╗███████║██║ █╗ ██║
██║     ██╔══██║██╔══██║██║██║╚██╗██║╚════██║██╔══██║██║███╗██║
╚██████╗██║  ██║██║  ██║██║██║ ╚████║███████║██║  ██║╚███╔███╔╝
 ╚═════╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝╚═╝  ╚═══╝╚══════╝╚═╝  ╚═╝ ╚══╝╚══╝
    By WithSecure Countercept (@FranticTyping, @AlexKornitzer)

[+] Loading detection rules from: chainsaw/sigma/
[!] Loaded 3178 detection rules (465 not loaded)
[+] Loading forensic artefacts from: soc_events/ (extensions: .evt, .evtx)
[+] Loaded 450 forensic artefacts (7.6 GB)
[+] Hunting: [========================================] 450/450                 
[+] 51465 Detections found on 47974 documents
```

J'aurai peut-être pu faire ingérer le résultat de `chainsaw` 
dant `Elasticsearch`, mais je ne l'ai pas fait. J'ai plutôt choisi
[de splitter le résultat](split_chainsaw_json.py) (c'est un tableau d'alertes en JSON) en
alertes élémentaires, que je peux attaquer à coup de `grep`.

Dernier point : je n'ai pas résolu les épreuves dans l'ordre prévu
par les organisateurs. Ce write-up reflète l'ordre que j'ai choisi,
et vous verrez que cela a son importance.

Ah, et évidemment, je montre l'accès direct à la solution. Dans la plupart des cas, ça a pris beaucoup
plus de temps à échouer qu'à trouver la bonne réponse. ;-)

## SOC Simulator 2/5 - Vol de secret 1

**Description**

Après l’action vue dans la partie 1, l’attaquant vole les identifiants système en mémoire. Retrouver le GUID du processus effectuant ce vol et le nom du fichier où il écrit les secrets volés.

**Format du flag (insensible à la casse) :** `FCSC{6ccf8905-a033-4edc-8ed7-0a4b0a411e15|C:\Windows\Users\toto\Desktop\fichier.pdf}`

**Solution**

Oui, on commence directement par la deuxième épreuve. Pour une raison 
simple : le résultat de chainsaw nous donne tout de suite des choses
à investiguer. En effet, qui dit recherche d'identifiant en mémoire, 
dit [Mimikatz](https://github.com/gentilkiwi/mimikatz). Cherchons donc
ce que `chainsaw` a à nous dire à ce sujet.

```
$ for a in $(grep -rli mimikatz chainsaw-json/); do echo -n "$a "; jq .document.data.Event.EventData.UtcTime  "$a"; done
chainsaw-json/12409.json "2022-07-04 15:50:55.855"
chainsaw-json/12504.json "2022-07-04 15:54:55.664"
chainsaw-json/35774.json "2022-07-05 14:12:58.199"
chainsaw-json/49708.json "2022-07-06 16:05:01.901"
chainsaw-json/49710.json "2022-07-06 16:05:01.901"
chainsaw-json/49772.json null
chainsaw-json/49773.json "2022-07-06 16:06:00.051"
```

Lorsqu'on ouvre le premier json qui matche, on note que c'est une
réaction au `recordId 8359` sur la machine `Exchange.tinfa.loc`. Passons sur Kibana pour voir cet évènement
de plus près : on filtre sur `Event.System.EventRecordID : 8359 and  Event.System.Computer : Exchange.tinfa.loc` :

![](2-RecordID8359.png)

Du coup, on va essayer de regarder ce qui tourne à cette période (entre 15:50 et 15:55) sur la machine. Attention au décalage ! Dans le filtre 
Kibana, il faut sélectionner 17:50 et 17:55. Et on scrollant un peu, on tombe sur :

![](2-powershell.png)

Du powershell avec du code... obfusqué à tout le moins. Ça mérite examen. En faisant des recherches autour de ces process, on 
détermine qui lance qui, et ça donne cela :

```
|- 17800 - powershell
    |- 11788 - cmd.exe
        |- 4688 - powershell
            | 17400 - "C:\Windows\system32\rundll32.exe" C:\Windows\System32\comsvcs.dll MiniDump 652 attr.exe full
```

Le mot clef `MiniDump` m'attire l'œil. Cela correspond exactement à [une
technique connue de dump de `lsass` dans MITRE ATT&CK](https://attack.mitre.org/techniques/T1003/001/). ̀`MiniDump` est l'action, `652` et le pid de `lsass` et `attr.exe` est... le fichier qui reçoit le dump !

Si on ouvre l'évènement correspondant à la création du process `̀17400` ([`EventID=1`](https://www.ultimatewindowssecurity.com/securitylog/encyclopedia/event.aspx?eventid=90001)), on a toutes les infos nécessaires à la validation :

![](2-flag.png)

Le flag est donc `FCSC{b99a131f-0d4b-62c3-ce03-00000000db01|C:\Windows\System32\inetsrv\attr.exe}`.


## SOC Simulator 5/5 - Vol de secret 2

**Description**

Sur la machine identifiée en partie 4, l’attaquant vole de nouveau les secrets du système. Retrouver le GUID du processus effectuant ce vol et le nom du fichier où il écrit les secrets volés.

**Format du flag (insensible à la casse) :** `FCSC{6ccf8905-a033-4edc-8ed7-0a4b0a411e15|C:\Windows\Users\toto\Desktop\fichier.pdf}`.

**Solution**

J'avais prévenu qu'on ne faisait pas dans l'ordre. :) Puisque les règles
`chainsaw` nous ont bien aidé pour le premier vol de secret, continuons
dans la foulée. On reprend notre `grep mimikatz`, en cherchant cette fois-ci les évènements concernant la machine `Workstation2` puisqu'elle
est citée explicitement dans l'énoncé de l'épreuve 4.

```shell
$ for a in $(egrep -ril "mimikatz" chainsaw-json/ ); do echo -n "$a "; jq '.document.data.Event.System.Computer + " " + .document.data.Event.EventData.UtcTime' "$a"; done
chainsaw-json/12409.json "exchange.tinfa.loc 2022-07-04 15:50:55.855"
chainsaw-json/12504.json "exchange.tinfa.loc 2022-07-04 15:54:55.664"
chainsaw-json/35774.json "exchange.tinfa.loc 2022-07-05 14:12:58.199"
chainsaw-json/49708.json "Workstation2.tinfa.loc 2022-07-06 16:05:01.901"
chainsaw-json/49710.json "Workstation2.tinfa.loc 2022-07-06 16:05:01.901"
chainsaw-json/49772.json "Workstation2.tinfa.loc "
chainsaw-json/49773.json "Workstation2.tinfa.loc 2022-07-06 16:06:00.051"
```

Ça a donc l'air de se jouer le 6 juillet vers 16:05. Le premier JSON correspondant, `49708.json` nous donne encore 
plus d'infos :

```json
{
  "group": "Sigma",
  "kind": "individual",
  "document": {
    "kind": "evtx",
    "path": "../soc_events/4/20220706T180641.evtx",
    "data": {
      "Event": {
        "EventData": {
          "CreationUtcTime": "2022-07-06 16:05:01.901",
          "Image": "C:\\Windows\\system32\\taskmgr.exe",
          "ProcessGuid": "{b7e8a6b7-b273-62c5-bc11-00000000d301}",
          "ProcessId": "6744",
          "RuleName": "-",
          "TargetFilename": "C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\3\\lsass.DMP",
          "User": "WORKSTATION2\\Administrator",
          "UtcTime": "2022-07-06 16:05:01.901"
        },
|...]
      },
    }
  }
}
```

À noter que dumper via le gestionnaire des tâche [est effectivement supporté par Mimikatz](https://www.oreilly.com/library/view/advanced-infrastructure-penetration/9781788624480/c54099a7-b74b-4130-9c8b-9aba41d42fb5.xhtml). D'ailleurs, l'alerte nous donne
directement tous les éléments pour construire le flag : `FCSC{b7e8a6b7-b273-62c5-bc11-00000000d301|C:\Users\ADMINI~1\AppData\Local\Temp\3\lsass.DMP}`.

Notons également que l'utilisateur ayant réalisé le vol de secret est
`WORKSTATION2\Administrator`. Cela pourra servir par la suite !

## SOC Simulator 1/5 - Vecteur initial

**Description**

Retrouver le nom de la vulnérabilité et l’heure UTC de la première tentative d’exploitation de cette vulnérabilité.

**Format du flag (insensible à la casse)**: `FCSC{EternalBlue|2021-11-27T17:38}`

**Solution**

Revenons au début, cette fois-ci avec l'avantage de pouvoir limiter nos recherches, car on sait que le vol
de secret, à l'étape 2, a eu lieu sur la machine `Exchange`, le 4 juillet à 15h50. Le vecteur initial a 
donc lieu avant, et peut-être peut-on commencer par cette machine en particulier. On se souvient aussi
du `powershell` bizarre qu'on avait observé. Filtrons là-dessus :

![](1-louche.png)

Le tout premier est observé à 15h36. Le processus père est `c:\windows\system32\inetsrv\w3wp.exe` qui est... [un worker IIS](https://stackoverflow.com/a/7822917). Il est très étonnant de 
constater qu'un processus Exchange crée un process malveillant.
C'est probablement l'exploitation d'une vulnérabilité.

Je commence donc à chercher une vulnérabilité Echange, de 2022,
impliquant `w3wp.exe`. Il en existe, mais après le mois de juillet.
On remonte donc en 2021. 

Les premières trouvées sont décrites par [Mandiant](https://www.mandiant.fr/resources/blog/detection-response-to-exploitation-of-microsoft-exchange-zero-day-vulnerabilities) sous le nom `ProxyLogon`, mais ça ne valide pas.

On continue à remonter dans le temps, pour obtenir, [chez les mêmes](https://www.mandiant.com/resources/blog/pst-want-shell-proxyshell-exploiting-microsoft-exchange-servers), la vulnerabilité ProxyShell. Et cette fois, le flag `FCSC{ProxyShell|2022-07-04T15:36}` fonctionne !


## SOC Simulator 4/5 - Latéralisation

**Description**

Sur une courte période de temps, l’attaquant a essayé de se connecter à de nombreuses machines, comme s’il essayait de réutiliser les secrets volés dans la partie 2. Cela lui a permis de se connecter à la machine Workstation2. Retrouver l’IP source, le compte utilisé et l’heure UTC de cette connexion.

**Format du flag (insensible à la casse) :** `FCSC{192.168.42.27|MYCORP\Technician|2021-11-27T17:38:54}`.


**Solution**

Ici aussi, le fait de faire les étapes dans le désordre va être à
notre avantage. On se rappelle que le deuxième vol de secret a lieu
sur `Workstation2` le 6 juillet à 16h05. Il a été réalisé par 
`Administrator`.

Cherchons dans Elasticsearch toutes les évènements de connexion (Connection EventId: [4624](https://www.ultimatewindowssecurity.com/securitylog/encyclopedia/event.aspx?eventID=4624)) sur cette machine 
dans ce créneau :

![](4-flag.png)

On voit qu'Administrator s'est connecté à 15:26:57, heure système, 
pour la première fois. On n'oublie pas de convertir l'horaire en 
UTC (-2 heures) : `FCSC{172.16.20.20|WORKSTATION2\Administrator|2022-07-06T13:26:57}`


## SOC Simulator 3/5 - Exfiltration

**Description**

Dans la continuité de ce qui été vu précédemment, l’attaquant a collecté une quantité importante de données métier. Retrouver la commande qui a permis collecter de tous ces éléments.

**Format du flag :** `FCSC{sha256(<commande en UTF8 sans saut de ligne>)}`

Par exemple si la commande malveillante était `7z a "Fichiers volés.zip" C:\Windows\System32`, le flag serait `FCSC{bc5640e69c335a8dbe369db382666070e05198a6c18ce88498563d2c4ac187b1}`

**Solution**


Reste donc le dernier à trouver, et le plus compliqué si l'on en croit
le nombre de validations.

Pour être honnête, l'énoncé est spécial. S'agit-il d'une exfiltration
ou de la collecte de données sur l'infrastructure attaquée ? Mais 
la collecte peut aussi être la récupération des données recueillies
vers une machine que l'on maîtrise, donc de l'exfiltration ?

Du coup, j'ai longtemps cherché. J'ai désobfusqué tous les powershells bizarres.
J'ai inspecté les scripts qui interagissaient avec AWS, en supposant
d'abord qu'ils étaient faux, puisqu'ils cachaient peut-être des 
commandes cachées. Sans résultat.

J'ai aussi listé tous les évènements dont l'`EventID` pouvait être
intéressant. Par exemple, l'`EventID` 3 ([*Network connection detected*](https://www.ultimatewindowssecurity.com/securitylog/encyclopedia/event.aspx?eventid=90003)), ce qui m'a fait justement me pencher 
vers AWS, car il y a beaucoup de trafic à destination de ce cloud.

Puis j'ai filtré sur la création de fichier ([`EventID 1`](https://www.ultimatewindowssecurity.com/securitylog/encyclopedia/event.aspx?eventid=90011)), en faisant l'hypothèse qu'il s'agissait de 
collecte locale, donc à un moment il y aurait création d'une archive.
Je me suis concentré plus particulièrement sur `Exchange`, de là
où tout a démarré, et qui me semblait la cible la plus évidente.

Même en se concentrant sur les horaires des étapes 2 et 4 pour borner
mes recherches, c'est encore plus de 15000 évènements à traiter. En
filtrant petit à petit les `TargetFileName` les plus courants, les 
résultats commencent à devenir plus lisibles. Et après plusieurs
heures, j'ai trouvé une création de fichier bizarre :

![](3-ftph.png)

Un fichier exécutable, créé par un powershell ? Il s'agirait de 
regarder ce process d'un peu plus près. Il s'avère qu'il s'agit, cette
fois, d'un powershell interactif. Ainsi, on va filtrer les évènements
sur le `ProcessID` et demander l'affichage des `scriptBlockText` :

![](3-powershell.png)

On peut alors extraire les différentes commandes lancées dans le
shell interactif :

```
prompt
whoami
prompt
whomai
{ Set-StrictMode -Version 1; $_.PSMessageDetails }
{ Set-StrictMode -Version 1; $this.Exception.InnerException.PSMessageDetails }
{ Set-StrictMode -Version 1; $_.ErrorCategory_Message }
{ Set-StrictMode -Version 1; $_.OriginInfo }
prompt
prompt
whoami
prompt
Add-PSSnapin Microsoft.Exchange.Management.Powershell.SnapIn;
prompt
pwd
prompt
cd ..
prompt
ls
prompt
echo $null >> ftph.exe
prompt
pwd
prompt
prompt
mkdir xwin
prompt
foreach ($Mailbox in (Get-Mailbox -ResultSize Unlimited)) {New-MailboxExportRequest -Mailbox $Mailbox.DisplayName -FilePath "C:\windows\system32\xwin\($Mailbox.Alias).pst"}
{ Set-StrictMode -Version 1; $_.PSMessageDetails }
{ Set-StrictMode -Version 1; $_.ErrorCategory_Message }
{ Set-StrictMode -Version 1; $_.OriginInfo }
prompt
{ Set-StrictMode -Version 1; $_.PSMessageDetails }
{ Set-StrictMode -Version 1; $_.ErrorCategory_Message }
{ Set-StrictMode -Version 1; $_.OriginInfo }
prompt
prompt
foreach ($Mailbox in (Get-Mailbox -ResultSize Unlimited)) {New-MailboxExportRequest -Mailbox $Mailbox.DisplayName -FilePath "\\exchange\C$\windows\system32\xwin\($Mailbox.Alias).pst"}
prompt
cd xwin
prompt
ls
prompt
Get-MailboxExportRequest
prompt
Get-MailboxExportRequest -Status Completed | Remove-MailboxExportRequest
prompt
download -h
{ Set-StrictMode -Version 1; $_.PSMessageDetails }
{ Set-StrictMode -Version 1; $_.ErrorCategory_Message }
{ Set-StrictMode -Version 1; $_.OriginInfo }
prompt
download -r *
{ Set-StrictMode -Version 1; $_.PSMessageDetails }
{ Set-StrictMode -Version 1; $_.ErrorCategory_Message }
{ Set-StrictMode -Version 1; $_.OriginInfo }
prompt
ls
prompt
download (workstationadm local.Alias).pst
{ Set-StrictMode -Version 1; $_.PSMessageDetails }
{ Set-StrictMode -Version 1; $_.ErrorCategory_Message }
{ Set-StrictMode -Version 1; $_.OriginInfo }
prompt
download "(workstationadm local.Alias).pst"
{ Set-StrictMode -Version 1; $_.PSMessageDetails }
{ Set-StrictMode -Version 1; $_.ErrorCategory_Message }
{ Set-StrictMode -Version 1; $_.OriginInfo }
prompt
exit
```

Les deux boucles `foreach` semblent coller avec l'idée de collecte dans l'énoncé. On construit un flag pour la première :

```shell
$ echo "FCSC{$(echo -n 'foreach ($Mailbox in (Get-Mailbox -ResultSize Unlimited)) {New-MailboxExportRequest -Mailbox $Mailbox.DisplayName -FilePath "C:\windows\system32\xwin\($Mailbox.Alias).pst"}' | sha256sum | cut -d" " -f1)}"
FCSC{bd4d7ea91a29c48cdf5b6b4813600dd202ea034e6aa59c1268f8210f2991955c}
```

Et ça valide ! :)