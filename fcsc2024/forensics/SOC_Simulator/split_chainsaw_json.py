#!/usr/bin/env python3

import json

id = 0

def load(filename):
    global id
    print(f'Load {filename}')
    array = json.load(open(filename, 'r'))

    for a in array:
        a['id'] = id
        open(f'chainsaw-json/{id}.json', 'w').write(json.dumps(a))
        id += 1

load('chainsaw.json')