#!/usr/bin/env python3

import sys
import os.path
import json

if len(sys.argv) != 3:
    print(f'Syntax: {sys.argv[0]} <input> <output>')
    exit(-1)
_, evtx, bulk = sys.argv

print(f'Opening {evtx}')
started = False
with open(evtx, 'r') as input:
    with open(bulk, 'w') as output:
        buffer = ''
        recordid = None
        for line in input.read().split('\n'):
            if line.startswith('Record'):
                if started:
                    payload = json.loads(buffer)
                    output.write('{"index":{}}\n')
                    output.write(json.dumps(payload) + '\n')
                started = True
                buffer = ''
            else:
                buffer += line + '\n'
