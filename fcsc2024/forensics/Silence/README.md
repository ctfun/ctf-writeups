# Silence

**Description**

Vous êtes Bob. Une vieille amie vous avait envoyé un flag pour un FCSC futur, il y a quelques années, avant que vous ne perdiez contact. Elle se rappelle à vous en indiquant qu'elle fait maintenant partie de l'organisation du FCSC. Elle a pu mettre en place ce flag et vous propose de le jouer.

Problème, le flag est resté dans une sauvegarde de SMS de votre application SMS de l'époque, [Silence](https://silence.im/). Vous avez complètement oublié le code que vous utilisiez, vous savez seulement qu'il s'agissait d'un code à 5 chiffres décimaux.

Saurez-vous retrouver ce flag et montrer à Alice que vous ne l'aviez pas oubliée ?

Toute ressemblance avec des faits et des personnages existants ou ayant existé serait purement fortuite et ne pourrait être que le fruit d’une pure coïncidence.

Aucune IA n'a été maltraitée durant la réalisation de ce challenge.

Fichier: [SilenceExport.zip](https://hackropole.fr/challenges/fcsc2024-forensics-silence/public/SilenceExport.zip)

**Solution**

On commence par regarder le contenu de l'archive:

```
$ unzip SilenceExport.zip
$ tree SilenceExport
SilenceExport
├── cache
├── code_cache
├── databases
│   ├── canonical_address.db
│   ├── canonical_address.db-journal
│   ├── _jobqueue-SilenceJobs
│   ├── _jobqueue-SilenceJobs-journal
│   ├── messages.db
│   └── messages.db-journal
├── files
│   └── sessions-v2
│       └── 3
└── shared_prefs
    ├── org.smssecure.smssecure_preferences.xml
    ├── SecureSMS-Preferences.xml
    └── SecureSMS.xml

7 directories, 10 files
```

Il s'agit bien d'un export d'une messagerie sécurisée tombée en désuétude, qui permettait le chiffrement de ses SMS et MMS.
Avant toute chose, on essaie de se renseigner sur les vulnérabilités possibles autour de cette implémentation, et on
trouve rapidement [breakthesilence](https://github.com/hydrargyrum/breakthesilence), un projet Java permettant de
déchiffrer un export (ça tombe bien !) lorsque l'on connaît le code (aïe !).

Sauf que l'on sait que le code utilisé est à 5 chiffres décimaux. Une attaque par force brute est donc envisageable si
le chiffrement et son implémentation dans breakthesilence sont suffisamment performant.

On patche donc le code source (à la truelle): 

```patch
index c5bf347..cbb0d66 100644
--- a/src/re/indigo/breakthesilence/MasterSecretUtil.java
+++ b/src/re/indigo/breakthesilence/MasterSecretUtil.java
@@ -301,24 +301,34 @@ public class MasterSecretUtil {
     silProps.encryption_salt = Base64.getDecoder().decode(props.getProperty("encryption_salt"));
     silProps.user_passphrase = userPassphrase;
 
-    MasterSecret sec;
-    try {
-       sec = getMasterSecret(silProps, silProps.user_passphrase);
-    } catch (InvalidPassphraseException exc) {
-      System.err.println("Invalid passphrase!");
-      System.exit(65);
-      return;
-    } catch (GeneralSecurityException exc) {
-      exc.printStackTrace(System.err);
-      System.exit(1);
-      return;
-    } catch (IOException exc) {
-      exc.printStackTrace(System.err);
-      System.exit(74);
-      return;
+    String[] alpha = { "0","1","2","3","4","5","6","7","8","9" };
+    for(String s1:alpha) {
+      for(String s2:alpha) {
+        for(String s3:alpha) {
+          for(String s4:alpha) {
+            for(String s5:alpha) {
+
+              silProps.user_passphrase = s1+s2+s3+s4+s5;
+
+              System.out.print("\rTesting "+silProps.user_passphrase);
+              MasterSecret sec;
+              try {
+                sec = getMasterSecret(silProps, silProps.user_passphrase);
+                System.out.println("Good: "+silProps.user_passphrase);
+                System.out.println("encryption_key = " + Base64.getEncoder().encodeToString(sec.encryptionKey));
+                System.out.println("mac_key = " + Base64.getEncoder().encodeToString(sec.macKey));
+                System.exit(0);
+              } catch (InvalidPassphraseException exc) {
+                // System.out.println("Invalid passphrase!");
+              } catch (GeneralSecurityException exc) {
+                exc.printStackTrace(System.err);
+              } catch (IOException exc) {
+                exc.printStackTrace(System.err);
+              }
+            }
+          }
+        }
+      }
     }
-
-    System.out.println("encryption_key = " + Base64.getEncoder().encodeToString(sec.encryptionKey));
-    System.out.println("mac_key = " + Base64.getEncoder().encodeToString(sec.macKey));
   }
 }
```

Puis on recompile :

```shell
$ ./build-jar.sh 
manifeste ajouté
ajout : re/(entrée = 0) (sortie = 0)(stockage : 0 %)
ajout : re/indigo/(entrée = 0) (sortie = 0)(stockage : 0 %)
ajout : re/indigo/breakthesilence/(entrée = 0) (sortie = 0)(stockage : 0 %)
ajout : re/indigo/breakthesilence/MasterSecretUtil$Context.class(entrée = 398) (sortie = 254)(compression : 36 %)
ajout : re/indigo/breakthesilence/MasterSecretUtil$InputData.class(entrée = 509) (sortie = 332)(compression : 34 %)
ajout : re/indigo/breakthesilence/MasterSecretUtil$InvalidPassphraseException.class(entrée = 475) (sortie = 295)(compression : 37 %)
ajout : re/indigo/breakthesilence/MasterSecretUtil$InvalidPropertiesException.class(entrée = 412) (sortie = 269)(compression : 34 %)
ajout : re/indigo/breakthesilence/MasterSecretUtil$MasterSecret.class(entrée = 591) (sortie = 361)(compression : 38 %)
ajout : re/indigo/breakthesilence/MasterSecretUtil.class(entrée = 9932) (sortie = 4987)(compression : 49 %)
Successful build in build/breakthesilence.jar
```

On lance et on va prendre un café. Évidemment, on met n'importe quoi dans la demande de mot de passe.

```shell
$ time ./run-jar.sh ../SilenceExport
Password (leave empty if empty): 
Testing 56849Good: 56849
encryption_key = fYnrqMm95DkntXQHuEyxRg==
mac_key = M6c/T2mk87lbBQ43GPf/+icPryU=

real	6m6,375s
user	6m5,912s
sys	0m0,371s
```

Plus qu'à décoder...
```shell
$ echo "encryption_key = fYnrqMm95DkntXQHuEyxRg==
mac_key = M6c/T2mk87lbBQ43GPf/+icPryU=" | ./breakthesilence_to_json.py ../SilenceExport ../silence-backup.json 
Enter output of run-jar.sh:
OK, successfully read properties.
Proceeding to conversion, this may take a while, please wait!
Done!
```

... et lire le résultat dans `silence-backup.json` :

```json
      {
        "_id": 108,
        "thread_id": 1,
        "address": "+33742241337",
        "address_device_id": 1,
        "person": null,
        "date": 1580234767134,
        "date_sent": 1580234766366,
        "protocol": null,
        "read": 1,
        "status": -1,
        "type": 2,
        "reply_path_present": null,
        "date_delivery_received": 0,
        "subject": null,
        "body": "Le code est FCSC{07d6313bfb88a72ca39e223a78477c45}. Ne l'oublie pas, il ne marchera peut-être pas tout de suite.",
        "mismatched_identities": null,
        "service_center": null,
        "subscription_id": 1,
        "notified": 0,
        "flags": 2155872276
      },
```