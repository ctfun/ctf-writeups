#!/usr/bin/env python3

from Crypto.Cipher import AES      # python -m pip install pycryptodome
from base64 import b64decode
from Crypto.Util.Padding import unpad
from urllib.parse import unquote
import zlib

cmd = [
    'DjrB3j2wy3YJHqXccjkWidUBniQPmhTkHeiA59kIzfA%3D',
    'K/a6JKeclFNFwnqrFW/6ENBiq0BnskUVoqBf4zn3vyQ%3D',
    '/ppF2z0iUCf0EHGFPBpFW6pWT4v/neJ6wP6dERUuBM/6CAV2hl/l4o7KqS7TvTZAWDVxqTd6EansrCTOAnAwdQ%3D%3D',
    'Lmrbj2rb7SmCkLLIeBfUxTA2pkFQex/RjqoV2WSBr0EyxihrKLvkqPKO3I7KV1bhm8Y61VzkIj3tyLKLgfCdlA%3D%3D',
    'yPfHKFiBi6MxfKlndP99J4eco1zxfKUhriwlanMWKE3NhhHtYkSOrj4QZhvf6u17fJ%2B74TvmsMdtYH6pnvcNZOq3JRu2hdv2Za51x82UYXG1WpYtAgCa42dOx/deHzAlZNwM7VvCZckPLfDeBGZyLHX/XP4spz4lpfau9mZZ%2B/o%3D',
    'E1Wi18Bo5mPNTp/CaB5o018KdRfH2yOnexhwSEuxKWBx7%2Byv4YdHT3ASGAL67ozaoZeUzaId88ImfFvaPeSr6XtPvRqgrLJPl7oH2GHafzEPPplWHDPQQUfxsYQjkbhT',
    '7JPshdVsmVSiQWcRNKLjY1FkPBh91d2K3SUK7HrBcEJu/XbfMG9gY/pTNtVhfVS7RXpWHjLOtW01JKfmiX/hOJQ8QbfXl2htqcppn%2BXeiWHpCWr%2ByyabDservMnHxrocU4uIzWNXHef5VNVClGgV4JCjjI1lofHyrGtBD%2B0nZc8%3D',
    'WzAd4Ok8kSOF8e1eS6f8rdGE4sH5Ql8injexw36evBw/mHk617VRAtzEhjXwOZyR/tlQ20sgz%2BJxmwQdxnJwNg%3D%3D',
    'G9QtDIGXyoCA6tZC6DtLz89k5FDdQNe2TfjZ18hdPbM%3D',
    'QV2ImqgrjrL7%2BtofpO12S9bqgDCRHYXGJwaOIihb%2BNI%3D',
]

dskey = '50c53be3eece1dd551bebffe0dd5535c'

def decrypt(cmd):
    aes = AES.new(dskey.encode(), AES.MODE_ECB)
    cmd = unquote(cmd)
    cmd = b64decode(cmd)
    cmd = aes.decrypt(cmd)
    cmd = unpad(cmd, 32)
    cmd = zlib.decompress(cmd)
    cmd = cmd.decode('utf-8')
    return cmd

for i, c in enumerate(cmd):
    print(f'{i}: {decrypt(c)}')