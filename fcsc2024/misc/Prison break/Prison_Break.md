# Prison Break

Carlos, bien évidement innocent, est enfermé dans une prison de haute sécurité. La gestion technique de batîment (GTB) de cette prison est contrôlée par un automate programmable industriel. L'accès à cet automate GTB a été rendu possible via un implant dissimulé lors d'une opération antérieure, celui-ci est accessible depuis l'extérieur de la prison via (protocole UDP) :

`nc -u prison-break.france-cybersecurity-challenge.fr 47808`

> **Important** : Chaque nouvelle connexion UDP à l'automate sur le port 47808 implique la réinitialisation de l'automate BACnet/IP, ainsi, un nouveau token est généré à chaque nouvelle connexion. Pour compléter les deux étapes de ce challenge dans le temps imparti, vous ne devrez donc pas couper votre connexion UDP même si cela parait surprenant étant donné qu'UDP ne maintient pas d'état de connexion. Par ailleurs, notez que UDP n'offre aucun mécanisme permettant de garantir la réception d'un paquet. Il peut s'avérer utile de lire la mémoire de l'automate après l'envoi d'une commande pour vérifier sa bonne prise en compte.

De, même, l'accès à l'interface de supervision scada est possible en lecture seule depuis :

https://prison-break.france-cybersecurity-challenge.fr/

![](prison-break.png)

Afin de faire évader Carlos, suivez **scrupuleusement** les instructions relatives aux étapes suivantes (checkpoints):

1. **Checkpoint 1 (t0+15s)** : Vous avez 15 secondes pour ouvrir l'ensemble des cellules de l'aile OUEST ainsi que la porte principale de cette aile. Cette diversion devrait attirer l'attention de certains gardes.

2. **Checkpoint 2 (t1+15s)** : Vous aurez ensuite une fenêtre de 5 secondes pour enfermer les gardes dans l'aile OUEST et faire croire un incendie dans l'aile NORD en activant l'alarme incendie. Les dix prochaines secondes devraient laisser le temps aux gardes de l'avant-poste de se diriger vers l'aile NORD.

3. **Checkpoint 3 (t2+5s)** : Vous aurez ensuite 5 secondes pour éteindre la ventilation de l'aile NORD en passant sa vitesse à 0 et enfermer les gardes de cette aile.

4. **Checkpoint 4 (t3+20s)** : Dans les 10 prochaines secondes vous devrez :
    - éteindre les lumières de l'aile EST et du centre ;
    - ouvrir la cellule de Carlos ;
    - ouvrir la porte principale de l'aile EST.

    Une fois cela fait, Carlos devrait avoir le temps de se déplacer en direction de l'avant-poste.

5. **Checkpoint 5 (t4+5s)** : Enfermez Carlos dans le poste de contrôle n°2 pour éviter qu'il ne se fasse repérer.

6. **Checkpoint 6 (t5+15s)** : Dans les 5 premières secondes, déclenchez la sécurité incendie dans le SAS de contrôle et éteingnez la lumière de l'avant-poste. Attendez quelques secondes que le dernier garde de l'avant-poste se retrouve dans le SAS de contrôle puis l'enfermer à l'intérieur.

7. **Checkpoint 7 (t6+10s)** : Réouvrez la porte du poste de contrôle n°2 pour libérer Carlos et ouvrez les deux portes finales pour permettre son évasion.

> **Attention** : Le moindre écart de votre part (ouverture massive de porte ou non respect des instructions) pourrait mettre en péril l'évasion de Carlos.
> De plus, **il n'est pas nécessaire d'acheter la norme ISO** pour compléter ce challenge !
> **Important** : La dernière version `23.07.03` de la librairie `BAC0` ne fonctionne pas pour ce challenge. Il est fortement recommandé d'utiliser la version `22.9.21`.