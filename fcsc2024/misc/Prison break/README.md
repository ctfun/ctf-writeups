# Prison Break

## Prison break 1/2 

**Description**

Dans cette première partie du challenge `Prison break`, vous manipulerez un automate de gestion technique de bâtiment dans une prison de haute sécurité. Cet automate dialogue avec une interface de supervision SCADA via le protocole `BACnet/IP`. La première étape de ce challenge est de récupérer un token présent dans la mémoire de l'automate afin de pouvoir accèder à l'interface web SCADA.

Pour cela, vous devrez lire dans les objets de l'automate à l'aide du protocole `BACnet/IP` et tenter de récupérer un `token` caché. Vous pourrez alors continuer la deuxième partie de l'épreuve à l'aide de ce token.

Les informations générales sur cette épreuve sont disponibles ici : [infos](Prison_Break.md).

https://prison-break.france-cybersecurity-challenge.fr/ 22.9.21

Fichier: [prison-break-template.py](prison-break-template.py)

**Solution**

La première étape est donc d'aller lire la doc de la librairie [`BAC0`](https://bac0.readthedocs.io/en/latest/index.html), et là,
c'est le drame. Je ne comprends pas grand chose à ce qui est dit. Au départ, je me suis dit que ce n'était pas très grave, que j'allais
utilisé le template fourni. Mais même avec le template.

Bon, la doc est effectivement pas terrible, et en plus le device n'aide pas : il semble ne pas apprécier les [`readMultiple()`](https://bac0.readthedocs.io/en/latest/BAC0.core.io.html?highlight=readMultiple#BAC0.core.io.Read.ReadProperty.readMultiple)
mais l'API haut niveau de `BAC0` utilise ces fonctions même lorsque je lui demande de ne pas le faire.

Bref, merci Google qui me fournit quelques informations pratiques. Ce qui va nous intéresser :
- On peut `read()` et `write()` des *points* (en gros, les actionneurs liés au device), avec un paramètre dont la construction est à coucher dehors.
- Un `read()` sur le mot clef objectList d'un device permet de lister ses points.

Donc on commence en exécutant le template et en listant les points du device :

> Note: `remote[0][0]` est l'adresse IP du device et `remote[0][1]` est son port d'écoute.

```python
>>> bacnet.read(f'{remote[0][0]} device {remote[0][1]} objectList')
[('device', 1556938), ('binaryValue', 1), ('binaryValue', 2), ('binaryValue', 3), ('binaryValue', 4), ('binaryValue', 5), ('binaryValue', 6), ('binaryValue', 7), ('binaryValue', 8), ('binaryValue', 9), ('binaryValue', 10), ('binaryValue', 11), ('binaryValue', 12), ('binaryValue', 13), ('binaryValue', 14), ('binaryValue', 15), ('binaryValue', 16), ('binaryValue', 17), ('binaryValue', 18), ('binaryValue', 19), ('binaryValue', 20), ('binaryValue', 21), ('binaryValue', 22), ('binaryValue', 23), ('binaryValue', 24), ('binaryValue', 25), ('binaryValue', 26), ('binaryValue', 27), ('binaryValue', 28), ('binaryValue', 29), ('binaryValue', 30), ('binaryValue', 31), ('binaryValue', 32), ('binaryValue', 33), ('binaryValue', 34), ('binaryValue', 35), ('binaryValue', 36), ('binaryValue', 37), ('binaryValue', 38), ('binaryValue', 39), ('binaryValue', 40), ('binaryValue', 41), ('binaryValue', 42), ('binaryValue', 43), ('binaryValue', 44), ('binaryValue', 45), ('binaryValue', 46), ('binaryValue', 47), ('binaryValue', 48), ('binaryValue', 49), ('binaryValue', 50), ('binaryValue', 51), ('analogValue', 1), ('analogValue', 2), ('analogValue', 3), ('analogValue', 4), ('characterstringValue', 1)]
```

Nous voilà donc avec pleins de points. Ils sont listés par type et numéro. Ce n'est qu'une partie de ce qui est défini par le standard, mais
voilà ce que notre device possède :
- `binaryValue` : booléen qui peut prendre les valeurs `active` ou `inactive`.
- `analogValue` : valeur numérique .
- `characterstringValue` : chaîne de caractères.

Ce n'est pas fini, chaque type possède différentes propriétés qu'il faudra spécifier dans les `read()` et `write()`. La propriété la
plus importante et courante est `presentValue`, la valeur du point.

Pour cette étape, on doit trouver un token quelque part dans le device. Évidemment, le suspect dans nos points est le `characterstringValue`.
Mais il n'implémente pas `presentValue`. Je teste alors différentes propriété et c'est `description` qui gagne !

```python
>>> bacnet.read(f'{remote[0][0]} characterstringValue 1 description')
'2f185aa6a2e5f793f76294f83af35805'
```

Tout en maintenant l'interpréteur ouvert, on fourni le token à la GUI, qui nous laisse entrer et fournit le flag.

![](flag1.png)

Ou `FCSC{2f3a395e619a213146d58e6e7e0758f4c66b7a65abca10bc96882a49df7fa2af}`.

Toutes les étapes peuvent être retrouvées grâce à [ce script](prison-break-1_get-token.py).

## Prison break 2/2 

**Description**

Dans cette deuxième partie du challenge `Prison break`, votre objectif est de faire sortir un prisonnier de la prison dans un temps contraint. Pour cela, vous allez devoir envoyer des commandes à l'automate GTB qui contrôle :

- l'ouverture et la fermeture des portes ;
- la ventilation ;
- la lumière ;
- la sécurité incendie.

Les informations générales sur cette épreuve sont disponibles ici : [Infos](Prison_Break.md).

**Solution**

Maintenant qu'on a la GUI de la prison, et qu'on sait à peu près comment
marchent les points, on va essayer de déterminer la relation entre les
points présentés par le device et la prison «réelle». Pour cela, je 
complète le script de récupération du token avec une boucle demandant
la saisie d'un nombre qui va basculer l'état du point associé :

```python
    # Discovery
    while True:
        i = input("index ?")
        status = bacnet.read(f'{remote[0][0]} binaryValue {i} presentValue')
        if status == 'active':
            status = 'inactive'
        else:
            status = 'active'
        bacnet.write(f'{remote[0][0]} binaryValue {i} presentValue {status}')
```

Le bout de code et [le script complet](prison-break-2_test.py) se font
sur les points de type valeur analogique, et le même principe est
réalisé pour interpréter les points booléens. Au final, on obtient
le mapping suivant :

- Portes:
  - Aile nord: 1-8 + central(9)
  - Aile est: 10-21 + carlos(19) + central(22)
  - Aile ouest: 23-34 + central(35)
  - Aile sud: pc1(37) pc2(38) sas(36) sud(39) central(42)
  - Portes extérieures: 40-41 
- Capteur incendie: nord(43), est(44), ouest(45), sud(46)
- Lumières: nord(47), est(48), ouest(49), sud(50), centre(51)
- Ventilation: nord(1), est(2), ouest(3), sud(4)

Arrivé ici, il ne reste plus qu'à suivre le protocole indiqué avec
les bonnes temporisations. Quelques ajustements sont nécessaires, mais
on obtient [le script final](prison-break-3_escape.py).

Je ne résiste pas au plaisir de l'affichage de l'interface pendant la résolution (mais en accéléré parce qu'on n'est pas des bêtes).

![](Solve.gif)

Le flag est donné dans la table des évènements plus bas :

![](flag2.png)

Ou `FCSC{bd539d9aae28c88f9b38d9bbd123d9c2c86e09cbe6a47d2dc7f08e3276fa731e}`.