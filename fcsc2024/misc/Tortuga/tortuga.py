#!/usr/bin/env python3

from PIL import Image, ImageDraw
import sys

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
SIZE = 10

if '-test' in sys.argv:
    trace = [
        (2, 0), (-1, 2), (-1, -2),
        (0, 0), (3, 0),
        (-1, 2), (2, 0), (-1, -2),
        (0, 0), (1, 0), 
    ]
else:
    trace = [(0,2),(0,-2),(1,0),(-1,0),(0,1),(1,0),(0,0),(1,1),(0,-2),(1,0),(-1,0),(0,2),(1,0),(0,0),(2,-2),(-1,0),(0,1),(1,0),(0,1),(-1,0),(0,0),(2,0),(0,-2),(1,0),(-1,0),(0,2),(1,0),(0,0),(3,-2),(-1,0),(0,1),(-1,0),(1,0),(0,1),(1,0),(0,0),(4,-2),(-2,0),(0,0),(0,2),(2,0),(0,-2),(0,1),(-2,0),(0,0),(3,-1),(0,2),(0,0),(3,-2),(-1,0),(-1,1),(0,1),(2,0),(0,-1),(-2,0),(0,0),(3,0),(1,0),(0,-1),(-1,0),(0,2),(1,0),(0,-1),(0,0),(1,1),(1,0),(0,-2),(-1,0),(0,0),(0,1),(1,0),(0,0),(2,1),(0,-2),(-1,1),(2,0),(0,0),(1,-1),(1,0),(-1,2),(0,0),(0,-1),(1,0),(0,0),(1,-1),(1,0),(0,1),(-1,0),(0,1),(1,0),(0,0),(1,0),(1,0),(0,-1),(-1,0),(0,-1),(1,0),(0,0),(1,2),(0,-2),(1,0),(-1,0),(0,2),(1,0),(0,-1),(-1,0),(0,0),(2,1),(1,0),(-1,0),(0,-2),(1,0),(-1,2),(1,0),(0,-2),(0,0),(1,0),(0,1),(1,0),(0,-1),(0,2),(0,0),(2,-2),(1,0),(0,1),(1,0),(-1,0),(0,1),(-1,0)]


img = Image.new('RGB', (500,50), WHITE)
draw = ImageDraw.Draw(img)
current = (10, 10)
on = True
iterator = iter(trace)
try:
    d = next(iterator)
    while d is not None:
        if d == (0, 0):
            d = next(iterator)
            current = tuple([ x+y*SIZE for x, y in zip(current, d)])
            print(f'Move to {current}')
            d = next(iterator)
        else:
            next_current = tuple([ x+y*SIZE for x, y in zip(current, d)])
            print(f'Draw {current} to {next_current}')
            draw.line([current, next_current], fill=BLACK)
            current = next_current
            d = next(iterator)
except StopIteration:
    pass
img.save('result.png')