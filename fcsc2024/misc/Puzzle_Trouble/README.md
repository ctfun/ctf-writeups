# Puzzle Trouble

J'ai simplement recyclé [la page web](https://github.com/lordOric/steg-tools/blob/main/puzzle.html) utilisée
pour le [Préchall de l'an dernier](../../../fcsc2023/Welcome/Prechall/README.md).

## Puzzle Trouble 1/2

**Description**

On vous demande de retrouver le flag dans ce bazar de tuiles ! Il paraît qu’elles n’ont pas été retournées…

![](puzzle-trouble-easy.jpg)

**Solution**

![](solution_easy.png)

Le flag: `FCSC{1283AEBF19387475}`

## Puzzle Trouble 2/2

**Description**

On vous demande de retrouver le flag dans ce bazar de tuiles ! Il paraît qu’elles n’ont pas été retournées…

![](puzzle-trouble-hard.jpg)

**Solution**

![](hard_almost.png)

(Ok, c'est approximatif ! :) )

Le flag : `FCSC{93784AE384999121FE3123441984}`