# FCSC 2024

![](rank.png)

Et satisfaction personnelle :

![](forensics.png)

**Intro:**
- [Layer Cake 1/3](intro/Layer_Cake/README.md)
- [Layer Cake 2/3](intro/Layer_Cake/README.md#layer-cake-23)
- [Layer Cake 3/3](intro/Layer_Cake/README.md#layer-cake-33)


**Forensics:**
- [Silence](forensics/Silence/README.md)
- [Horreur, malheur 1/5 - Archive chiffrée.](forensics/Horreur_malheur/README.md)
- [Horreur, malheur 2/5 - Accès initial.](forensics/Horreur_malheur/README.md#horreur-malheur-25---accès-initial)
- [Horreur, malheur 3/5 - Simple persistance.](forensics/Horreur_malheur/README.md#horreur-malheur-35---simple-persistance)
- [Horreur, malheur 4/5 - Pas si simple persistance.](forensics/Horreur_malheur/README.md#horreur-malheur-45---pas-si-simple-persistance)
- [Horreur, malheur 5/5 - Un peu de CTI.](forensics/Horreur_malheur/README.md#horreur-malheur-55---un-peu-de-cti)
- [SOC Simulator - Introduction aux solutions.](forensics/SOC_Simulator/README.md)
- [SOC Simulator 1/5 - Vecteur initial.](forensics/SOC_Simulator/README.md#soc-simulator-15---vecteur-initial)
- [SOC Simulator 2/5 - Vol de secret 1.](forensics/SOC_Simulator/README.md#soc-simulator-25---vol-de-secret-1)
- [SOC Simulator 3/5 - Exfiltration.](forensics/SOC_Simulator/README.md#soc-simulator-35---exfiltration)
- [SOC Simulator 4/5 - Latéralisation.](forensics/SOC_Simulator/README.md#soc-simulator-45---latéralisation)
- [SOC Simulator 5/5 - Vol de secret 2.](forensics/SOC_Simulator/README.md#soc-simulator-55---vol-de-secret-2)


**Hardware**
- [Golgrot13](hardware/Golgrot13/README.md)
- [Castafiore](hardware/Castafiore/README.md)


**Misc:**
- [Puzzle Trouble 1/2](misc/Puzzle_Trouble/README.md)
- [Puzzle Trouble 2/2](misc/Puzzle_Trouble/README.md#puzzle-trouble-22)
- [Prison Break 1/2](misc/Prison%20break/README.md)
- [Prison Break 2/2](misc/Prison%20break/README.md#prison-break-22)
- [Illuminated](misc/Illuminated/README.md)
- [Tortuga](misc/Tortuga/README.md)