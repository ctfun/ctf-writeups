# PNG : Un logo obèse [1/4]

L'heure est grave. Hallebarde ne se cache plus et échange des messages en toute liberté. Un tel manque de précautions de leur part est alarmant, et même presque suspect. Un logo a été utilisé dans l'un de leurs courriels et d'après de premières analyses, il pourrait avoir été employé pour ouvrir un canal de stéganographie. Pourriez-vous tirer cela au clair ?

Auteur : `Smyler#7078`

Fichier: [steg.png](steg.png)

**Solution**

On commence par passer un coup de [`hachoir`](https://hachoir.readthedocs.io/en/latest/) pour analyser le contenu de l'image fournie :
```
0) file:steg.png: PNG picture: 1103x319x32 (alpha layer) (514.0 KB)
   0) id= "\x89PNG\r\n\x1a\n": PNG identifier ('\x89PNG\r\n\x1A\n') (8 bytes)
 + 8) header: Header: 1103x319 pixels and 32 bits/pixel (25 bytes)
 + 33) data[0]: Image data (8204 bytes)
 + 8237) data[1]: Image data (8204 bytes)
 + 16441) data[2]: Image data (8204 bytes)
 + 24645) data[3]: Image data (5817 bytes)
 + 30462) end: End (12 bytes)
   30474) raw[]= "PK\3\4\x14\0\0\0\b\0\x12\xbd\xabT(...)" (484.2 KB)
```

On voit des données présentes à la fin du fichier, qui commencent par `PK`, donc probablement une archive ZIP !

```console
$ unzip steg.png
Archive:  steg.png
warning [steg.png]:  30474 extra bytes at beginning or within zipfile
  (attempting to process anyway)
  inflating: out/stage2.png          
```

Cela nous donne l'image suivante :

![state2](stage2.png)

Qui contient le flag : `404CTF{0b3z3_f1l3_h4z_zup3r_spy_s3cr37}`.


# PNG : Drôles de chimères [2/4]

Il semblerait que votre dernière analyse ait été fructueuse, mais nos analystes trouvent encore à redire. Il faudrait aller plus loin dans vos recherches et comprendre pourquoi la structure du fichier que vous nous avez remis leur paraît étrange, même si elle est parfaitement valide.

Auteur: `Smyler#7078`

Fichier : [stage2.png](stage2.png)

**Solution**

Effectivement, le format ne semble pas correct. L'outil [`pngcheck`](http://www.libpng.org/pub/png/apps/pngcheck.html) nous indique :

```console
$ pngcheck -vvv stage2.png 
File: stage2.png (497701 bytes)
  chunk IHDR at offset 0x0000c, length 13
    513 x 340 image, 32-bit RGB+alpha, non-interlaced
  chunk sTeG at offset 0x00025, length 388350:  illegal reserved-bit-set chunk
ERRORS DETECTED in stage2.png
```

Avec `hachoir`, on extrait le chunk `sTeG` bizarre :
```
1) file:stage2.png: PNG picture: 513x340x32 (alpha layer) (486.0 KB)
   1) id= "\x89PNG\r\n\x1a\n": PNG identifier ('\x89PNG\r\n\x1A\n') (8 bytes)
 + 8) header: Header: 513x340 pixels and 32 bits/pixel (25 bytes)
 - 33) chunk[0] (379.3 KB)
      0) size= 388350: Size (4 bytes)
      4) tag= "sTeG": Tag (4 bytes)
      8) content= "\0\0\0\rIHDR\0\0\1\xf4\0\0(...)": Data (379.2 KB)
      388358) crc32= 0xc482b335: CRC32 (4 bytes)
 + 388395) data[0]: Image data (8204 bytes)
+ (...)
```

Il semble contenir d'autres chunks. Serait-il un PNG lui-même ? Si c'est le cas, il manque l'entête PNG:
```
 00000000:  00 00 00 0d 49 48 44 52  00 00 01 f4 00 00 01 d9  ....IHDR........
 00000010:  08 06 00 00 00 70 e4 c9  62 00 00 20 00 49 44 41  .....p..b.. .IDA
 00000020:  54 78 9c ec bd 07 94 24  c7 79 e7 f9 cf f2 d5 5d  Tx.....$.y.....]
 00000030:  ed dd 74 f7 78 07 37 33  18 58 02 20 08 90 30 34  ..t.x.73.X. ..04
 00000040:  00 45 52 94 28 4a a4 28  77 92 9e 74 ba bd 3b e9  .ER.(J.(w..t..;.
 00000050:  76 9f f6 74 27 69 f7 9e  f6 9d cc ed 9d 28 47 51  v..t'i.......(GQ
 00000060:  94 44 8a 14 44 03 92 a2  11 45 90 20 09 47 03 80  .D..D....E. .G..
```

On l'ajoute (`89 50 4e 47 0d 0a 1a 0a`), et on a une nouvelle image :

![stage3.png](stage3.png)

Le flag est `404CTF{7h47_v1c10us_m1zzing_z19natur3}`.

#  PNG : Toujours obèse [3/4]

Nous avançons, mais la taille du dernier fichier n'est toujours pas cohérente statistiquement avec l'image elle-même. Il semblerait qu'il y ait beaucoup trop de données dans cette image. À vous de jouer.

Auteur: `Smyler#7078`

Fichier : [stage3.png](stage3.png)

**Solution**

Rien à signaler sur la structure cette fois-ci :
```
$ pngcheck -vvv stage3.png 
File: stage3.png (388358 bytes)
  chunk IHDR at offset 0x0000c, length 13
    500 x 473 image, 32-bit RGB+alpha, non-interlaced
  chunk IDAT at offset 0x00025, length 8192
    zlib: deflated, 32K window, default compression
  chunk IDAT at offset 0x02031, length 8192
  chunk IDAT at offset 0x0403d, length 8192
  chunk IDAT at offset 0x06049, length 8192
  chunk IDAT at offset 0x08055, length 8192
  chunk IDAT at offset 0x0a061, length 8192
  chunk IDAT at offset 0x0c06d, length 8192
  chunk IDAT at offset 0x0e079, length 8192
  chunk IDAT at offset 0x10085, length 8192
  chunk IDAT at offset 0x12091, length 2213
  chunk IDAT at offset 0x12942, length 312240
  chunk IEND at offset 0x5ecfe, length 0
No errors detected in stage3.png (13 chunks, 58.9% compression).
```

Je passe une batterie d'outils personnels d'analyse (valeurs d'opacité, [LSB](https://en.wikipedia.org/wiki/Steganography#Example_from_modern_practice), ...) et
 par hasard, l'un d'entre eux crache :
`Caught exception: Magick: IDAT: Too much image data (stage3.png) reported by coders/png.c:1105 (PNGWarningHandler)`. C'est vrai que le dernier IDAT (cf ci-dessus), paraît très très gros...

Je passe sur les péripéties pour essayer de comprendre ce qui se passe, jusqu'à je me rende compte, à coup d'éditeur hexédécimal, qu'il y'a 35 chunks `IDAT` dans le fichier !
Les décompresseurs croient avoir fini, mais en fait, non !

De nouvelles péripéties pendant quelques heures pour essayer de donner du sens à tout ça (que des échecs, bien sûr...) jusqu'à ce que je change de stratégie : je décide d'écrire [un script python](solve_part3.py) qui cherche tous les IDAT (par recherche de chaînes), décompresse leur contenu par zlib, et sauvegarde tout ça dans un fichier.


À l'arrivée, on a un début de fichier rempli de caractères nuls (c'est le contenu de l'image de départ), et au bout d'un moment, on tombe... sur une en-tête PNG.

```
 000e7100:  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
 000e7110:  00 00 00 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
 000e7120:  00 00 00 00 00 00 00 00  00 89 50 4e 47 0d 0a 1a  ..........PNG...
 000e7130:  0a 00 00 00 0d 49 48 44  52 00 00 03 20 00 00 02  .....IHDR... ...
 000e7140:  58 08 06 00 00 00 9a 76  82 70 00 00 00 06 62 4b  X......v.p....bK
 000e7150:  47 44 00 ff 00 ff 00 ff  a0 bd a7 93 00 00 00 09  GD..............
 000e7160:  70 48 59 73 00 00 0b 13  00 00 0b 13 01 00 9a 9c  pHYs............
 000e7170:  18 00 00 20 00 49 44 41  54 78 9c ec bd 07 7c 94  ... .IDATx....|.
 000e7180:  55 f6 ff 7f 9f 36 bd 65  52 29 16 5c 65 15 cb 8a  U....6.eR).\e...
 000e7190:  80 14 81 d0 7b 0b 21 d4  d0 23 bd 23 5d 0c 58 16  ....{.!..#.#].X.
 000e71a0:  5d 5d 45 d7 b2 ac ba 76  44 c0 8e 5d d7 45 29 02  ]]E....vD..].E).
 000e71b0:  21 09 a4 87 40 12 4a 20  21 99 64 fa cc 53 7f e7  !...@.J !.d..S..
 000e71c0:  3c 33 41 77 7f bf ff eb  ff ad e2 ae f7 8d c7 fb  <3Aw............
 000e71d0:  d4 99 e7 99 cc dc b9 9f  39 f7 9c c3 10 0a 85 42  ........9......B
```

On supprime tout ce qui précède, on essaie d'ouvrir le fichier avec un éditeur d'image, et on retient son souffle... On a bien un PNG tout propre !

![stage4.png](stage4.png)

Le flag de cette partie est `404CTF{z71ll_0b3z3_&_st1ll_h4d_s3cr3tz_4_U}`.

En tout cas un long moment d'analyse et de *try & error*, au point que l'étape suivante sera presque frustrante de facilité. ;-)

# PNG : Une histoire de filtres [4/4]

Les statisticiens se plaignent toujours de la taille du fichier. Selon eux la compression devrait être plus efficace.

Bonne chance.

Image d'origine par `Ed g2s` (Wikimédia), license [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/).

Auteur : `Smyler#7078`

**Solution**

On ressort `pngcheck`, car on sait qu'il peut afficher les filtres :

```console
$ pngcheck -vv stage4.png 
File: stage4.png (309200 bytes)
  chunk IHDR at offset 0x0000c, length 13
    800 x 600 image, 32-bit RGB+alpha, non-interlaced
  chunk bKGD at offset 0x00025, length 6
    red = 0x00ff, green = 0x00ff, blue = 0x00ff
  chunk pHYs at offset 0x00037, length 9: 2835x2835 pixels/meter (72 dpi)
  chunk IDAT at offset 0x0004c, length 8192
    zlib: deflated, 32K window, default compression
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      1 1 0 3 1 2 1 1 1 2 0 3 1 3 0 2 1 2 1 1 1 3 1 0 0
      2 0 0 1 2 0 3 1 2 3 3 1 2 1 0 1 2 (42 out of 600)
  chunk IDAT at offset 0x02058, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      1 1 0 3 2 2 0 2 0 0 0 (53 out of 600)
  chunk IDAT at offset 0x04064, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      3 1 0 0 3 0 0 0 (61 out of 600)
  chunk IDAT at offset 0x06070, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      3 1 0 1 0 0 3 1 1 1 (71 out of 600)
  chunk IDAT at offset 0x0807c, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      0 1 0 1 2 1 3 2 3 (80 out of 600)
  chunk IDAT at offset 0x0a088, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      0 3 1 3 1 2 2 0 0 (89 out of 600)
  chunk IDAT at offset 0x0c094, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      3 0 3 1 3 0 2 0 (97 out of 600)
  chunk IDAT at offset 0x0e0a0, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      3 0 3 1 1 3 3 0 3 (106 out of 600)
  chunk IDAT at offset 0x100ac, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      0 1 0 3 1 1 1 1 3 (115 out of 600)
  chunk IDAT at offset 0x120b8, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      3 1 2 3 2 0 3 0 (123 out of 600)
  chunk IDAT at offset 0x140c4, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      0 0 3 1 3 1 2 2 0 (132 out of 600)
  chunk IDAT at offset 0x160d0, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      0 3 0 1 1 2 (138 out of 600)
  chunk IDAT at offset 0x180dc, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      3 2 0 3 2 1 1 1 (146 out of 600)
  chunk IDAT at offset 0x1a0e8, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      3 3 1 2 0 2 0 3 1 (155 out of 600)
  chunk IDAT at offset 0x1c0f4, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      0 1 2 1 0 1 1 3 (163 out of 600)
  chunk IDAT at offset 0x1e100, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      3 0 3 1 0 1 2 0 (171 out of 600)
  chunk IDAT at offset 0x2010c, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      2 0 3 0 0 1 3 (178 out of 600)
  chunk IDAT at offset 0x22118, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      1 1 0 3 1 3 1 1 (186 out of 600)
  chunk IDAT at offset 0x24124, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      3 3 1 3 0 3 1 2 (194 out of 600)
  chunk IDAT at offset 0x26130, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      0 3 0 3 1 0 (200 out of 600)
  chunk IDAT at offset 0x2813c, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      1 2 3 2 1 2 3 2 0 (209 out of 600)
  chunk IDAT at offset 0x2a148, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      3 0 3 1 3 0 (215 out of 600)
  chunk IDAT at offset 0x2c154, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      2 1 3 2 2 1 3 3 1 (224 out of 600)
  chunk IDAT at offset 0x2e160, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      0 0 2 2 4 4 4 4 4 (233 out of 600)
  chunk IDAT at offset 0x3016c, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      4 4 4 4 4 4 4 4 4 4 4 (244 out of 600)
  chunk IDAT at offset 0x32178, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      4 4 4 4 4 4 4 4 4 4 4 4 4 4 (258 out of 600)
  chunk IDAT at offset 0x34184, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      4 4 4 4 2 4 4 4 4 4 4 4 4 4 4 2 2 2 2 2 4 3 4 4 4
      4 4 4 2 (287 out of 600)
  chunk IDAT at offset 0x36190, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      2 2 2 2 2 2 2 2 2 2 2 2 4 4 4 4 4 2 2 2 4 4 4 4 4
      4 4 4 2 2 4 (318 out of 600)
  chunk IDAT at offset 0x3819c, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 (341 out of 600)
  chunk IDAT at offset 0x3a1a8, length 8192
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      4 4 2 4 4 4 4 4 4 4 4 4 2 4 4 4 4 4 4 4 4 (362 out of 600)
  chunk IDAT at offset 0x3c1b4, length 874
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      4 4 (364 out of 600)
  chunk IDAT at offset 0x3c52a, length 62098
    row filters (0 none, 1 sub, 2 up, 3 avg, 4 paeth):
      4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4
      4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4
      4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4
      4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4
      4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4
      4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 4 1 1
      1 1 1 4 1 4 4 1 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0
      0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 4 4 4 4
      4 2 4 2 4 4 4 2 2 4 2 4 4 2 4 2 4 4 2 1 4 4 4 4 0
      0 0 0 0 0 0 0 0 0 0 0 (600 out of 600)
  chunk IEND at offset 0x4b7c8, length 0
No errors detected in stage4.png (36 chunks, 83.9% compression).
```

Puisqu'on nous demande de regarder les filtres, posons nous la question de comment y cacher un message. On voit qu'au début, ils sont compris entre 0 et 3, la valeur 4 n'apparaissant que sur la fin. C'est un peu curieux, on pourrait supposer que toutes les valeurs de filtres seraient utilisées si on souhaite compresser au maximum sans perte.

Est-ce que les valeurs de filtres ne sont pas simplement les valeurs à décoder ? En effet, 0 à 3 convertis en binaire nous donne à chaque fois 2 bits. Essayons :

Valeur des filtres :
` 1 1 0 3 1 2 1 1 1 2 0 3 1 3 0 2 1 2 1 1 1 3 1 0 0 2 0 0 1 2 0 3 1 2 3 3 1 2 1 0 1 2 1 1 0 3 2 2 0 2 0 0 0 3 1 0 0 3 0 0 0 3 1 0 1 0 0 3 1 1 1 0 1 0 1 2 1 3 2 3 0 3 1 3 1 2 2 0 0 3 0 3 1 3 0 2 0 3 0 3 1 1 3 3 0 3 0 1 0 3 1 1 1 1 3 3 1 2 3 2 0 3 0 0 0 3 1 3 1 2 2 0 0 3 0 1 1 2 3 2 0 3 2 1 1 1 3 3 1 2 0 2 0 3 1 0 1 2 1 0 1 1 3 3 0 3 1 0 1 2 0 2 0 3 0 0 1 3 1 1 0 3 1 3 1 1 3 3 1 3 0 3 1 2 0 3 0 3 1 0 1 2 3 2 1 2 3 2 0 3 0 3 1 3 0 2 1 3 2 2 1 3 3 1`

Convertis en binaire :
`0101001101100101011000110111001001100101011101000010000001100011011011110110010001100101001110100010000000110100001100000011010001000011010101000100011001111011001101110110100000110011011100100011001101011111001100010011010101011111011011100011000000110111011010000011000101101110001110010101111101100010001101000110010001011111001101000110001000110000011101010011011101011111011100110110001100110100011011100110111000110011011100100111101001111101`

C'est évidemment de l'ASCII, qui contient notre dernier flag : 
```
Secret code: 404CTF{7h3r3_15_n07h1n9_b4d_4b0u7_sc4nn3rz}
```