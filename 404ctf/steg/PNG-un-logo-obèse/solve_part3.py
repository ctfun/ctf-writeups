#!/usr/bin/env python3

import struct
import zlib

output = open('test.png', 'wb')
content = open('stage3.png', 'rb').read()
dec = zlib.decompressobj()

idat = content.index(b'IDAT')
while True:
    l_idat = struct.unpack('!I', content[idat-4:idat])[0]
    print(f'Found IDAT at offset {idat:x}, length {l_idat}')
    part = content[idat+4:idat+4+l_idat]
    part = dec.decompress(part)
    output.write(part)
    try:
        idat = content.index(b'IDAT', idat+1)
    except ValueError:
        break