# Découpé

Lors d'une interpellation, nous avons réussi à récupérer un disque dur. Hélas, un mécanisme de protection fait main a détruit un document qui se trouvait dessus ! Seriez-vous capable de le reconstituer ?

Auteur : `mh4ckt3mh4ckt1c4s#0507`

Fichier: [decoupe.zip](decoupe.zip)

**solution**

On ouvre l'archive et on constate une liste de petites images numérotées. On remarque que deux images d'index consécutifs peuvent être jointes (les éléments affichés se complètent). On peut donc poser l'hypothèse qu'une image a été découpée en petit morceaux, qui ont heureusement numérotés dans l'ordre. Il reste la géométrie à déterminer, mais à tâtons on découvre que l'image d'origine est un carré découpé en 24x24 fragments.

[Un petit script](solve.py) et on obtient l'image d'origine :

![result](result.png)

Une fois décodé, on obtient le flag `404CTF{M4n1PuL4T10N_d'1M4g3S_F4c1L3_n0N?}`.