#!/usr/bin/env python3

# pip install Pillow
from PIL import Image

output = Image.new('RGB', (24*33, 24*33))
index = 1
for i in range(24):
    for j in range(24):
        img = Image.open(f'output/{index}.png')
        output.paste(img, (j*33, i*33))
        index += 1
output.save('result.png')
