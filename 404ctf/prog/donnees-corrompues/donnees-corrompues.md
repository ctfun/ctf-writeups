# Données corrompues

Bonjour, agent. Comme vous le savez, vous êtes dans une unité spécialisée dans la cybersécurité, mais vous n'êtes pas sans savoir que la programmation est un outil qui nous est très précieux et qui apparaît dans la plupart de nos tâches, ce qui explique l'utilité d'une section dédiée.
Pour votre première mission avec nous, vous aurez à décoder des données interceptées entre deux machines de Hallebarde. Il s'agit de base64 mais elle semble corrompue... nos experts en ont déjà décodé une partie, à vous de finir le travail. Retrouvez les informations qui ont été échangées.

Format du flag : `404CTF{flag}` avec flag en minuscules

Auteur : `mh4ckt3mh4ckt1c4s#0705`

`nc challenge.404ctf.fr 30117`

**Solution**

Anecdote amusante : ce défi était le point d'entrée de la catégorie programmation à 100 points, au départ. Sauf qu'il est un peu casse-pied et qu'il a été totalement délaissé jusqu'à ce que les équipes le passe à 1000 points. ;-)

Voilà ce que nous donne un test de connexion sur le service distant :
```console
$ nc challenge.404ctf.fr 30117
Oulah, ces données semblent bien étranges ! Pouvez-vous les décoder pour nous ?
Il faut nous renvoyer les octets sous forme binaire (avec les zéros inutiles), tout collé, sans aucun autre caractère !
Un exemple qu'a réussi à reconstituer notre groupe d'experts :
L'entrée : Rmх%hZуА*6KQ
Doit donner en sortie : 01000110011011000110000101100111001000000011101000101001

[1 / 250] Voilà les données : SUQzBAAAAAAAI1RTU0"UAAAAPAAA}DTGF2ZjU4Ljc2LjEwMAAAAAAAAAAAAAAA//NkxAAYsEoIVGYYJBaZlVgmBiGZQLQ4sOTpRiIjhwEiwEENI2cLz6R&UCIlBuAyAhPureD6nBEefOShcH/kInhjTPsDAfWHylRzUcD6z9RwQHIg3`OwQwcdW/1Ag4o5WfRxAP5QInwxhiUcUDCgQwfUj9KtGraZiqmIvB7XAeJmHjLxpcSZ+HGflJjYeQhgCJ//NkxCAcy5pINNmE1CbjDwMkrMvmLGIWYfsUhGbjEJg8HTMQe22PzIdOibAgRjQg8f/1er6EI09AAQc5BDk//o^RW/fsd/6ZG//nf+T/////6/J0I3/8mpznAAAAAJA5Jqk3JbNZGxQo3JogiYCsZsS5yjJbgzJFGVaSDSgD4xx4Icp6eLxPkZcic7heE/aja//NkxC8na4p4ftPE/0kdZLgmDfij5B/Q1edLPbVKPh1CwosJQG8aQZwCOEyxs5rq1cqRU2///^//+9XufpIg1CkM1cB\qC3F9kXiljf///5fx16HFrHj2+YLx9Gf45qXbnVX+UrCxlUhSL/P//y/5pl+W2dv/60cyAhiZKFRWaXjsgiacjGjLP\gbhgsMiQqYcu//NkxBQh8mJsXtsK3AGXlYECICKw4A
>> 
Mmh, ça m'a pas l'air d'être ça... Réessaie !!
```

On apprend donc qu'on a 250 framgents à traiter en temps limité. Qu'il semble s'agir de `base64`, mais qu'il y a des erreurs dedans, mais lesquelles ? À force d'essai et d'erreur, on finit par trouver que certains caractères sur un octet ont été transformé en deux octets, et que des caractères parasites en dehors de l'alphabet `base64` sont à supprimer. Évidemment, il faut scripter tout ça 250 fois, en temps limité sinon on se fait déconnecter.

[On scripte donc](solve2.py), et au final on obtient le message suivant :

```console
=> b'Wouaouh, tu as r\xc3\xa9ussi ! Tu peux maintenant utiliser ces donn\xc3\xa9es pour obtenir le flag ! Il suffit de les assembler... ;-)\n'
```

Alors ça, on ne s'y attendait pas du tout ! On finit donc avec [un mystérieux fichier](output.bin) qui s'avère être un fichier wav. En l'écoutant attentivement, on entend le flag épelé : `404CTF{l4_b4s3_64_3ff1c4c3_m41s_c4pr1c13us3}`.