#!/usr/bin/env python3

# pip install pwntools
from pwn import *
from base64 import b64decode

conn = remote("challenge.404ctf.fr", 30117)
count = 0

fix = {
    b'\xd0\x90': b'\x41',
    b'\xd0\x92': b'\x42',
    b'\xd0\x9a': b'\x4b',
    b'\xd0\x9d': b'\x48',
    b'\xd0\xa2': b'\x54',
    b'\xd0\xb0': b'\x61',
    b'\xd0\xb5': b'\x65',
    b'\xd0\xbe': b'\x6F',
    b'\xd1\x80': b'\x70',
    b'\xd1\x81': b'\x63',
    b'\xd1\x83': b'\x79',
    b'\xd1\x85': b'\x78',
}

output = open('output.bin', 'wb')

q = 'start'
while q != b'\n':
    q = conn.recvline()

while count < 250:
    q = conn.recvline()      # [x / 250] Voilà les données...
    conn.recvuntil(b'>> ')   # >> 

    idx = q.index(b'[')+1
    idx2 = q.index(b' ', idx)
    count = int(q[idx:idx2])
    b64 = q.split(b' ')[7].rstrip(b'\n')
    print(f"\x0d[{count}] ", end='')


    # On fixe les caractères bizarres
    for f, r in fix.items():
        b64 = b64.replace(f, r)
    print(b64.decode('utf-8'))

    # On vire le reste
    cleaned = bytearray()
    for c in b64:
        if c >= 0x30 and c <= 0x39:
            cleaned.append(c)
        elif  c >= 0x41 and c <= 0x5A:
            cleaned.append(c)
        elif  c >= 0x61 and c <= 0x7A:
            cleaned.append(c)
        elif c==0x2b or c==0x2f or c==0x3D:
            cleaned.append(c)

    # On rectifie le padding
    lpad = len(cleaned) % 4
    if lpad != 0:
        cleaned += b'=' * (4 - lpad)

    # On décode
    pt = b64decode(cleaned)
    output.write(pt)

    # On recode en binaire
    rep = ''.join([ f'{c:08b}' for c in pt ])
    conn.sendline(rep.encode('utf-8'))
    q = conn.recvline().decode('utf-8').rstrip('\n') # Ça semble bon

flag = conn.recvall()
print(flag)
output.close()