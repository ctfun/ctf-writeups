# Mot de passe ?

Nous avons retrouvé un petit ordinateur avec un drôle de programme pour chiffrer le mot de passe administrateur. Ta mission est simple : déchiffrer ce mot de passe !

Fichier: [Mdp.class](Mdp.class)

Auteur : `Astate#3107`

**Solution**

On est en présence d'un fichier Java compilé. On utilise [procyon](https://github.com/mstrobel/procyon/wiki/Java-Decompiler) pour le décompiler :

```console
$ java -jar procyon-decompiler-0.6.0.jar Mdp.class 
// 
// Decompiled by Procyon v0.6.0
// 

package chall;

import java.util.Scanner;

public class Mdp
{
    public static void main(final String[] array) {
        final String s = "4/2@PAu<+ViNgg%^5NS`#J\u001fNK<XNW(_";
        final Scanner scanner = new Scanner(System.in);
        System.out.print("Mot de passe Admin:");
        if (s.equals(hide(scanner.nextLine()))) {
            System.out.println("Bienvenue Admin");
        }
        else {
            System.out.println("Au revoir non admin");
        }
    }
    
    static String hide(final String s) {
        String s2 = "";
        for (char index = '\0'; index < s.length(); ++index) {
            s2 += (char)((char)(s.charAt(index) - index) % '\u0080');
        }
        return s2;
    }
}
```

Le mot de passe saisi est donc un peu bricolé avant d'être comparé à une constante. On reprend celle-ci et on fait la modification inverse à coup de python:

```console
>>> ''.join([ chr(ord(x)+i) for i, x in enumerate("4/2@PAu<+ViNgg%^5NS`#J\u001fNK<XNW(_") ])
'404CTF{C3_sYst3mE_es7_5ecUrisE}'
```