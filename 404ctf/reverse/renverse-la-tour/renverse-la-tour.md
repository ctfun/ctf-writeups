# Renverse la tour ! [1/2]

Nos experts ont réussi à mettre la main sur un algorithme d'authentification ultra secret utilisé par notre ennemi !

Un unique mot de passe est accepté mais nous n'avons pas pu le recupérer. Pouvez-vous nous aider ?

Auteur : `seaweedbrain#1321`

Fichier : [reverse1.py](reverse1.py)

**Solution**

On voit qu'on applique successivement trois fonctions sur le mot de passe saisi, puis le résultat est comparé à une valeur inintelligible. On va donc creuser les fonctions :
- une simple exécution de `tour1()` avec un mot de passe montre que la fonction renverse la chaîne et la retourne sous forme hexadécimale.
- `tour2()`, quant à elle, retourne une liste `(x1, x1+x2, x2, x2+x3 ... xn, xn-1+xn)` quand on lui passe une liste `(x1, x2, x3 ... xn)`.
- enfin, `tour3()` fait de légers décalages sur les codes ascii.

Toutes ces fonctions peuvent être retournées sans peine, c'est ce que j'ai fais dans [ce script](solve_reverse1.py), dont la trace d'exécution est la suivante :

```python
$ python solve_reverse1.py 
[52, 119, 67, 118, 51, 168, 117, 198, 81, 132, 51, 167, 116, 231, 115, 200, 85, 183, 98, 146, 48, 130, 82, 131, 49, 132, 83, 198, 115, 167, 52, 132, 80, 160]
[52, 67, 51, 117, 81, 51, 116, 115, 85, 98, 48, 82, 49, 83, 115, 52, 80]
P4sS1R0bUst3Qu3C4
```

Le mot de passe est donc `404CTF{P4sS1R0bUst3Qu3C4}`.


#  Renverse la tour ! [2/2]

Nos experts ont eu vent d'un tout nouvel algorithme d'authentification qui donne du fil à retordre à nos agents ! Certains disent que réussir à le craquer pourrait inverser le cours des choses.

Attention, nous ne sommes pas sûr que ce fichier soit exactement ce qu'il semble être.

Auteur : `seaweedbrain#1321`

Fichier : [reverse2.asm](reverse2.asm)

**Solution**

Cette fois, on a du bytecode python. Ce n'est pas très difficile à remettre sous une forme python, par exemple en suivant [la documentation officielle](https://docs.python.org/3/library/dis.html). Au final, on obtient un code équivalent :

```python
def mystery():
    inp = input()

    s = True
    n = ''
    p = ''
    f = (88, 1, 140, 1, 203, 208, 89, 207, 132, 191, 178, 110, 138, 132, 210, 1, 140, 156, 138, 140, 191, 187, 89, 89, 187, 1, 208, 231, 161, 235, 178, 188, 187, 132, 187)

    if inp=='':
        print('Nope')
        return s

    for k in range(len(inp)):
        n += inp[int(len(inp) - k - 1)]

    d = (159, 44, 176, 145, 103, 133, 49, 97, 113, 136, 184, 60, 85, 69, 64, 186, 182, 37, 56, 170, 19, 108, 152, 183, 41, 197, 252, 77, 35, 127, 198, 43, 148, 48, 46, 62, 15, 139, 95, 9, 38, 73, 160, 175, 226, 254, 129, 211, 132, 7, 90, 208, 187, 164, 158, 201, 116, 93, 54, 87, 126, 128, 16, 50, 244, 12, 4, 188, 166, 59, 235, 28, 199, 92, 216, 192, 231, 51, 61, 39, 220, 180, 204, 210, 178, 75, 17, 91, 143, 94, 34, 70, 222, 125, 131, 195, 33, 223, 242, 156, 232, 140, 67, 24, 111, 141, 162, 66, 45, 207, 138, 202, 89, 122, 191, 1, 110, 203, 241, 196, 82, 72, 76, 161, 117, 88, 105, 147, 119, 6, 157, 249, 168, 81, 32, 224, 237, 5, 146, 27, 80, 57, 42, 102, 172, 219, 114, 8, 31, 26, 238, 30, 212, 106, 221, 240, 118, 149, 165, 65, 83, 154, 151, 96, 36, 253, 250, 100, 74, 21, 189, 169, 239, 142, 173, 217, 181, 86, 29, 68, 155, 115, 225, 135, 0, 130, 101, 112, 206, 185, 227, 245, 18, 58, 243, 137, 20, 99, 3, 2, 233, 22, 55, 11, 13, 214, 84, 200, 47, 190, 205, 209, 53, 194, 229, 171, 248, 230, 109, 234, 236, 98, 213, 247, 150, 104, 79, 134, 71, 144, 25, 218, 107, 179, 124, 167, 251, 14, 78, 193, 40, 163, 123, 10, 246, 120, 23, 174, 63, 153, 228, 52, 121, 177, 215)

    for i in range(len(inp)):
        p += chr(d[int(ord(n[i]))])

    for j in range(len(f)):
        if f[j] != ord(p[j]):
            print('Nope !')
            return s

    return s

mystery()
```

D'ailleurs, pour s'assurer qu'on ne s'est pas trompé sur son interprétation, on peut utiliser le module Python `dis` pour refaire l'opération inverse, et comparer le bytecode de notre code avec celui fourni. 

À l'analyse, cette fonction `mystery()` :
- attend la saisie d'un mot de passe.
- inverse le sens de ce mot de passe.
- substitue chacun des caractères du mot de passe avec les valeurs contenues dans le tableau `d`.

On recode l'inverse en python, on voit que ça se simplifie très facilement, au point qu'on peut le résoudre avec un `oneliner`, si les tableaux `f` et `d` sont définis dans le contexte :

```python
>>> ''.join([ chr(d.index(c)) for c in f ][::-1])
'404CTF{L3s4pp4rencesS0ntTr0mp3uses}'
```