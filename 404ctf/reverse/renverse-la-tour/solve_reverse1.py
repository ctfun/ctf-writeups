# reverse la chaine et la transforme en code ascii décimaux.
def tour1(password):
    string = str("".join( "".join(password[::-1])[::-1])[::-1])
    return [ord(c) for c in string]

# (x1, x2, x3 ... xn) => (x1, x1+x2, x2, x2+x3 ... xn, xn-1+xn)
def tour2(password):
    new = []
    i = 0
    while password != []:
        new.append(password[password.index(password[i])])
        new.append(password[password.index(password[i])] + password[password.index(password[ i + 1 %len(password)])])
        password.pop(password.index(password[i]))
    return new

# On sait que le mot de passe de départ fait 17 caractères.
def tour3(password):
    mdp =['l', 'x', 'i', 'b', 'i', 'i', 'q', 'u', 'd', 'v', 'a', 'v', 'b', 'n', 'l', 'v', 'v', 'l', 'g', 'z', 'q', 'g', 'i', 'u', 'd', 'u', 'd', 'j', 'o', 'r', 'y', 'r', 'u', 'a']
    print(len(mdp))
    mdp = [ x for x  in 'abcdeghijklnmopqrstuvwxyz012345678' ]
    print(len(mdp))
    for i in range(len(password)):
        mdp[i], mdp[len(password) - i -1 ] = chr(password[len(password) - i -1 ] + i % 4),  chr(password[i] + i % 4)
    return "".join(mdp)


def rev_tour3(password):
    password = [ x for x in password ]
    mdp =['l', 'x', 'i', 'b', 'i', 'i', 'q', 'u', 'd', 'v', 'a', 'v', 'b', 'n', 'l', 'v', 'v', 'l', 'g', 'z', 'q', 'g', 'i', 'u', 'd', 'u', 'd', 'j', 'o', 'r', 'y', 'r', 'u', 'a']
    for i in range(len(password)):
        mdp[i], mdp[len(password) - i -1 ] = ord(password[len(password) - i -1 ]) - i % 4,  ord(password[i]) - i % 4
    return mdp

def rev_tour2(password):
    return password[::2]

def rev_tour1(password):
    return ''.join([ chr(x) for x in password[::-1]])

# mdp = input("Mot de passe : ")
#
# if tour3(tour2(tour1(mdp))) == "¡P6¨sÉU1T0d¸VÊvçu©6RÈx¨4xFw5":
#     print("Bravo ! Le flag est 404CTF{" + mdp + "}")
# else :
#     print("Désolé, le mot-de-passe n'est pas correct")

plop = "¡P6¨sÉU1T0d¸VÊvçu©6RÈx¨4xFw5"
print(rev_tour3(plop))
print(rev_tour2(rev_tour3(plop)))
print(rev_tour1(rev_tour2(rev_tour3(plop))))
