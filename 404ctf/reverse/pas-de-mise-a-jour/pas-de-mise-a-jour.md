# Pas de mise à jour...

Le fan de Python de **Hallebarde** est de retour! Mais il ne veut plus que tout le monde puisse lire ses codes sources...

Auteur: `Redhpm #8108`

Fichier : [chall.pyc](chall.pyc)

**Solution**

On a donc le script de chiffrement, mais seulement sous forme compilé. Dans ce cas, mon outil habituel est [uncompyle6](https://github.com/rocky/python-uncompyle6), malheureusement, il ne supporte pas encore Python 3.10. On se rabat sur [pycdc](https://github.com/zrax/pycdc), qui n'est pas exempt de défaut non plus : il ne parvient pas à restaurer le code python d'origine :

```python
# Source Generated with Decompyle++
# File: chall.pyc (Python 3.10)

userInput = (lambda .0: [ ord(e) for e in .0 ])(input('Password:'))
key = 'd1j#H(&Ja1_2 61fG&'

def code(l):
Unsupported opcode: MATCH_SEQUENCE
    pass
# WARNING: Decompyle incomplete

if code(userInput) == [ 292, 194, 347, 382, 453, 276, 577, 434, 183, 295, 318, 196, 482, 325, 412, 502, 396, 402, 328, 194, 473, 490, 299, 503, 386, 215, 263, 211, 318, 206, 533]:
    print('Bravo!')
    return None
None('Dommage...')
```

Mais on peut l'utiliser aussi pour récupérer le bytecode de la fonction `code()`qui nous manque :

```
                0       LOAD_FAST               0: l
                2       DUP_TOP                 
                4       MATCH_SEQUENCE          
                6       POP_JUMP_IF_FALSE       33 (to 66)
                8       GET_LEN                 
                10      LOAD_CONST              1: 1
                12      COMPARE_OP              5 (>=)
                14      POP_JUMP_IF_FALSE       33 (to 66)
                16      UNPACK_EX               1
                18      STORE_FAST              1: el
                20      STORE_FAST              2: rest
                22      POP_TOP                 
                24      LOAD_CONST              2: 5
                26      LOAD_FAST               1: el
                28      BINARY_MULTIPLY         
                30      LOAD_GLOBAL             0: ord
                32      LOAD_GLOBAL             1: key
                34      LOAD_GLOBAL             2: len
                36      LOAD_FAST               2: rest
                38      CALL_FUNCTION           1
                40      LOAD_GLOBAL             2: len
                42      LOAD_GLOBAL             1: key
                44      CALL_FUNCTION           1
                46      BINARY_MODULO           
                48      BINARY_SUBSCR           
                50      CALL_FUNCTION           1
                52      BINARY_XOR              
                54      BUILD_LIST              1
                56      LOAD_GLOBAL             3: code
                58      LOAD_FAST               2: rest
                60      CALL_FUNCTION           1
                62      BINARY_ADD              
                64      RETURN_VALUE            
                66      POP_TOP                 
                68      MATCH_SEQUENCE          
                70      POP_JUMP_IF_FALSE       43 (to 86)
                72      GET_LEN                 
                74      LOAD_CONST              3: 0
                76      COMPARE_OP              2 (==)
                78      POP_JUMP_IF_FALSE       43 (to 86)
                80      POP_TOP                 
                82      BUILD_LIST              0
                84      RETURN_VALUE            
                86      POP_TOP                 
                88      LOAD_CONST              0: None
                90      RETURN_VALUE            
```

Le code n'est pas très long, donc il suffit ensuite de se référer à [la doc officielle](https://docs.python.org/3/library/dis.html) pour essayer de reconstruire le code d'origine. Ça doit ressembler à peu près à ça :

```python
# Pas sûr, mais en gros
def code(l):
    long = len(l)
    if long != 33:
        print(f'Mauvaise taille {long}')
        return
    ret = list()
    for i in range(long):
        c = ord(key[len(l)-i]) % len(key)
        ret.append(c ^ l[0] * 5)
    return ret
```

On peut maintenant essayer de renverser la fonction pour obtenir le mot de passe de départ :

```python
>>> key = 'd1j#H(&Ja1_2 61fG&'
>>> def decode(cipher):
...     text = list()
...     long = len(cipher)
...     for i in range(long):
...         plop = ord(key[(long-i-1) % len(key)])
...         car = (plop ^ cipher[i] ) // 5 
...         text.append(car)
...     return ''.join( chr(x) for x in text )
... 
>>> print(decode([ 292, 194, 347, 382, 453, 276, 577, 434, 183, 295, 318, 196, 482, 325, 412, 502, 396, 402, 328, 194, 473, 490, 299, 503, 386, 215, 263, 211, 318, 206, 533 ]))
404CTF{R34D1NG_PYTH0N_BYT3C0D3}
```