# Collaborateur suspect

Nous avons eu vent d'une activité étrange qui aurait eu lieu sur la page qui mène à cette interface de mission. Nous soupçonnons une tentative de la part de Hallebarde de nous discréditer, ils nous ont peut-être infiltrés. Des informations confidentielles auraient été brièvement diffusées avant d'être retirées. Pourriez-vous nous dire lesquelles ?

> Attention, https://www.404ctf.fr ne fait PAS partie de la zone de jeu sur laquelle vous pouvez utiliser des méthodes offensives. Il s'agit d'un challenge d'OSINT ! Aucune action ou attaque particulière sur le site web en question n'est nécessaire pour réussir ce challenge.

Auteur : `mh4ckt3mh4ckt1c4s#0705`

**Solution**

On farfouille sur archive.org pour la page https://www.404ctf.fr/.
Rien de fifou dans les modifs archivées, mais en cliquant sur «Site Map» on voit que la page de crédit a aussi eu des modifs:
https://web.archive.org/web/20220419012412/https://www.404ctf.fr/credit.html

On revient à une version antérieure :
https://web.archive.org/web/20220411084948/https://www.404ctf.fr/credit.html

On y trouve le flag `404CTF{R3G4rd3r_3n_arr13r3_p3uT_3tR3_1Nt3r3ss4Nt}`.