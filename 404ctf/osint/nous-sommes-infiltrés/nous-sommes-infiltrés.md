# Nous sommes infiltrés !

Nouvelle recrue, je peux vous parler en privé ? C'est très urgent...

Nous avons eu vent de nouvelles pratiques de **Hallebarde** particulièrement insidieuses. Dans le but d'inflitrer la DGSE, Hallebarde recrute de jeunes étudiants en école d'ingénieur. Quel est le lien me direz-vous ? Ils envoient ces étudiants s'inscrire dans les clubs de cybersécurité de ces écoles, et au terme de leur scolarité, ces étudiants sont potentiellement embauchés par nos équipes, sans que l'on se doute de quoi que ce soit !

Nous soupçonnons notamment le club de cybersécurité HackademINT de Télécom SudParis de s'être fait infiltrer en cette fin d'année scolaire. Nous avons récupéré une photo de leur local assez récente. Utilisez la pour identifier le ou les nouveaux membres du club et dénicher une preuve de leur appartenance à **Hallebarde**.

Auteur : `Artamis`

Fichier: [Local_HackademINT.jpg](Local_HackademINT.jpg)

**Solution**

On va chercher les 5 comptes root-me qui nous sont indiqués :
- https://www.root-me.org/mh4ckt3mh4ckt1c4s
- https://www.root-me.org/Artamis
- https://www.root-me.org/Redhpm
- https://www.root-me.org/Xx_Noel_Janvier_xX
- https://www.root-me.org/Alternatif

Le website de Xx_Noel_Janvier_xX renvoie vers https://e10pthes.github.io/about/. Le repository correspondant est https://github.com/e10Pthes/about.
Le [dernier commit](https://github.com/e10Pthes/about/commit/0e92db3b1cfc38c06225e4fcd8036bca6f86924e) supprime une référence vers http://hallebarde.duckdns.org/. On va y faire un tour :

![hallebarde](hallebarde.png)

Le flag est `404CTF{Att3nt10n_AU8_V13ux_C0mMiT5}`.
