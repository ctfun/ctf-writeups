# À l'aube d'un échange

Nouvelle recrue ! Nous avons besoin de toi par ici. Un de nos agents vient d'intercepter une courte conversation téléphonique entre deux agents de Hallebarde. Un important échange de documents confidentiels doit avoir lieu et pour indiquer l'endroit du rendez-vous, l'un des agents ennemis a envoyé la photo ci-dessous à son collègue tout en précisant ceci :

> Quel beau lever de soleil n'est-ce pas ? J'attendrai dans la rue qui sépare le bâtiment au premier plan de ceux au second plan. Rendez-vous ce soir, 22h00.

Nous avons moins d'une journée pour découvrir le nom de ladite rue et empêcher l'échange !

Format du flag : `404CTF{md5 du nom complet de la rue}`
Le nom de la rue doit être en **minuscule**, inclure **le type de rue** ( ex : avenue, rue, boulevard... ), **sans accents, sans abréviation, et tous les espaces doivent être remplacés par des tirets.** Par exemple : si la rue est l'Avenue de Saint-Mandé à Paris, le flag correct est `404CTF{129af9edde5659143536427f9a5f659a}`.

Auteur : `Artamis`

Fichier : [Lieu.jpg](Lieu.jpg)

**Solution**

En cherchant quelques panomaras de villes française, on obtient Lyon.
La tour «crayon» est la tour de la Part-Dieu. Le toit penché est la tour Incity. Entre les deux on voit la tour Oxygène.

A tâtons sur Google Earth, on essaie de se mettre à peu près dans [les mêmes conditions de directions et d'angle](https://www.google.fr/maps/@45.7642817,4.8242055,17a,35y,84.09h,78.8t/data=!3m1!1e3). On repère une façade qui ressemble au premier plan recherché.

![](lieu1.jpg)


On essaie de se positionner dans la rue et on confirme qu'il s'agit [du bon immeuble](https://www.google.fr/maps/@45.7641295,4.8263469,3a,75y,83.99h,127.35t/data=!3m6!1e1!3m4!1svJ0Vq_x5dsqtYtlRjGL-PQ!2e0!7i16384!8i8192) :

![](lieu2.jpg)

Il s'agit de la montée Saint-Barthélemy. Le format attendu est `montee-saint-barthelemy` et le flag `404CTF{eb66c65861da9fe667f26667b3427d2c}`.
