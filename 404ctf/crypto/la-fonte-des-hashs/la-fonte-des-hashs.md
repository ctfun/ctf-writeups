# La fonte des hashs

Nos experts ont réussi à intercepter un message de **Hallebarde** : `18f2048f7d4de5caabd2d0a3d23f4015af8033d46736a2e2d747b777a4d4d205`

Malheureusement il est haché ! L'équipe de rétro-ingénierie vous a laissé cette note :

> Voici l'algorithme de hachage. Impossible de remonter le haché mais vous, vous trouverez peut être autre chose. Voici comment lancer le hachage : `python3 hash.py [clair]`
> 
> PS : Les conversations interceptées parlaient d'algorithme "très frileux" ...

Auteur : `seaweedbrain#1321`

Fichier: [hash.py](hash.py)

**Solution**

On exécute partiellement le code fourni et on regarde le contenu de trust. C'est du base64, et décodé, ça nous donne un script plus compréhensible : [trust.py](trust.py).

Dans le déroulé général, le script effectue :
- une conversion du texte en binaire.
- un padding permettant de compléter les blocs si nécessaire
- si la taille est supérieure à celle d'un bloc, une opération qui réduit la taille (`phase1`)
- trois opérations identiques consistant en une suite de XOR (`phase2`).
- un mélange suivant une SBOX (`sbox_ope`)
- enfin, une conversion en une sortie hexadécimale.

On peut noter que les opérations sur la SBOX et les XOR sont parfaitement réversibles. En revanche, la fonction `phase1` ne l'est pas, ou plus exactement elle génère une perte d'information. Compte tenu du fait qu'on nous demande de retrouver le message d'origine, on va donc supposer que la fonction n'a pas été appelée, donc que le message de départ tient sur un bloc. ;-)

Il suffit donc de simplement renverser les fonctions `sbox_ope` et `phase2`, ce que fait le script [solve.py](solve.py) dont l'exécution donne le résultat suivant :

```console
$ python solve.py 18f2048f7d4de5caabd2d0a3d23f4015af8033d46736a2e2d747b777a4d4d205
404CTF{yJ7dhDm35pLoJcbQkUygIJ}
```