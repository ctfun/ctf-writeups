#!/usr/bin/env python3

#
# From https://crypto.stackexchange.com/a/2331
#

from pwn import *

HOST = "challenge.404ctf.fr"
PORT = 32128

# Connect and get info
conn = remote(HOST, PORT)
conn.recvline()

input = conn.recvline().decode('utf-8').rstrip()
c = int(input)
print(f'{c=}')

conn.recvline()
input = conn.recvline().decode('utf-8').rstrip()
n = int(input[4:])
print(f'{n=}')

input = conn.recvline().decode('utf-8').rstrip()
e = int(input[4:])
print(f'{e=}')

# We encrypt 2 :
ca = pow(2, e, n)
print(f'{ca=}')
cb = ca * c
print(f'{cb=}')

# We ask for decryption of C(2) * C
conn.recvuntil(b'> ')
conn.sendline(str(cb).encode('utf-8'))
conn.recvline()
input = conn.recvline().decode('utf-8').rstrip()

# Nown, the plaintext of C is the last result // 2
text = int(input) // 2
text = text.to_bytes(40, 'big').decode('utf-8')
print(text)