#!/usr/bin/env python3

#
# From https://crypto.stackexchange.com/a/2331
#

from pwn import *
from gmpy2 import gcd, mpz, c_div, to_binary  # pip install gmpy2

HOST = "challenge.404ctf.fr"
PORT = 30594
e = 65537

def sign(conn, plain):
    conn.recvuntil(b'> ')
    send = str(plain).encode('utf-8')
    #print(f'> {send}')
    conn.sendline(send)
    conn.recvline()
    input = conn.recvline().decode('utf-8').rstrip()
    #print(f'< {input}')
    return mpz(input)

# Connect and get info
conn = remote(HOST, PORT)
conn.recvline()
conn.recvline()

input = conn.recvline().decode('utf-8').rstrip()
c = int(input)
print(f'{c=}')

conn.recvline()
input = conn.recvline().decode('utf-8').rstrip()
e = int(input[4:])
print(f'{e=}')

# Stolen from https://github.com/silentsignal/rsa_sign2n/blob/release/sig2n.py
m1 = 0x0001FFFFFFFFFFFFFFFFFF0043AD847DC0ED5CCFB74F00F2BC517D73C3527481BBDB8AA9157F2DAE1442CFA90000000000000000000000000000000000000000
s1 = sign(conn, m1)
print(f'{m1=} => {s1=}')
m2 = 0x0001FFFFFFFFFFFFFFFFFF00DEADBEEFC0ED5CCFB74F00F2BC517D73C3527481BBDB8AA9157F2DAE1442CFA90000000000000000000000000000000000000000
s2 = sign(conn, m2)
print(f'{m2=} => {s2=}')

process = log.progress('Computing...')
gcd_res = gcd(pow(s1,e)-m1,pow(s2,e)-m2)
print(f'{gcd_res=}')
for my_gcd in range(1,1000):
    print(f'{my_gcd=}')
    n = c_div(gcd_res, my_gcd)
    if pow(s1,e,n)==m1:
        print(f'{n=} ({my_gcd=})')
        break
process.success(' complete !')
print(f'{n=}')

# We encrypt 2 :
ca = pow(2, e, n)
print(f'{ca=}')
cb = ca * c
print(f'{cb=}')

# We ask for decryption of C(2) * C
text = sign(conn, cb)

# Now, the plaintext of C is the last result // 2
text = text // 2
text = to_binary(text).decode('utf-8')[::-1]
print(text)