# Weak Signature

Un nouveau système a été mis en place pour exécuter du code de façon sécurisée sur l'infrastructure. Il suffit d'envoyer une archive signée et encodée en base 64 pour exécuter le code Python qu'elle contient !

Vous trouverez la documentation de ce système et un exemple en pièces jointes. Tentez de voir si vous ne pourriez pas l'exploiter afin de lire le précieux fichier `flag.txt`

Auteur : `Cyxo#0458`

`nc challenge.404ctf.fr 32441 `

Fichiers: [script.py.zsig](script.py.zsig) [sign.py](sign.py) [ARCHIVE.md](ARCHIVE.md)

**Solution**

On essaie de péter la clef à coup de [RsaCtfTools](https://github.com/Ganapati/RsaCtfTool), mais sans conviction. Évidemment, ça ne marche pas.

On regarde le calcul de signature : 
```python
def checksum(data: bytes) -> int:
    # Sum the integer value of each byte and multiply the result by the length
    chksum = sum(data) * len(data)

    return chksum


def compute_signature(data: bytes, private_key: int, mod: int) -> int:
    # Compute the checksum
    chksum = checksum(data)
    # Sign it
    signature = pow(chksum, private_key, mod)

    return signature
```

La signature dépend entièrement du checksum, et il est facile d'obtenir des collisions puisqu'il est calculé à partir de la taille de la payload et de la somme des caractères qu'elle contient. Et comme on a un exemple fonctionnel, on peut générer une payload avec les mêmes caractéristiques et réutiliser cette signature. Pour se faire, c'est facile, on génère notre payload et on la complète avec une ligne de commentaire et un bourrage des caractères nécessaires pour obtenir la bonne somme.

Dans l'exemple, la payload est :
```
# This is a demo script. It doesn't do much, but I like it. You can use it too if you want :D

print("Hello, CTF player!")
```

C'est largement suffisant pour nos besoins. Il faut une payload de 122 caractères (c'est vérifié) dont la somme des caractères est identique à celle de l'exemple. J'écris [un petit script](solve.py) qui prend sur stdin la payload que je souhaite, crée le bourrage nécessaire et retourne le base64 attendu par le service :

```console
$ printf "print(open('flag.txt','r').readlines())" | python3 solve.py 
Old payload:
b'# This is a demo script. It doesn\'t do much, but I like it. You can use it too if you want :D\n\nprint("Hello, CTF player!")'
size=122, somme=10129
With padding:
print(open('flag.txt','r').readlines())
#TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT

Payload:
b'AVpTaWcCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA28PjfBE7saFl5OLzbCpAbI+sXlK3fpUSFH5K6oXBMFo5yN8OeTeIhdGZyELGoga6BKprc7B4L6FxQkKRsQ53tROJeqEZtAC7UZNhZAjqSSntfyOAvDyCnfOoMs1HF2QoWoIsnHXZ+E9bbBjJEIe1JgLD2bYORSjUaO342dMBoJhOmipCDQUnfwxtxO5jhu0eUJvyH1bVT9ZU0K1NxHWCWR1s/P6L80f0MuCjII6kVVcU+M47ySSKocNcQP+obVonXLV7L52a9eUk9NtViTgst5KIkAE3TCLUK1Yt1/DnM8F7s32PyrOWgMDJ2f4q/UweoiyqPoPk63BQkhEWTnc+AwAAAHoEcHJpbnQob3BlbignZmxhZy50eHQnLCdyJykucmVhZGxpbmVzKCkpCiNUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVAs='
```

On soumet notre payload au service :
```console
$ nc challenge.404ctf.fr 32441
Send me a signed archive encoded in base 64:
AVpTaWcCAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA28PjfBE7saFl5OLzbCpAbI+sXlK3fpUSFH5K6oXBMFo5yN8OeTeIhdGZyELGoga6BKprc7B4L6FxQkKRsQ53tROJeqEZtAC7UZNhZAjqSSntfyOAvDyCnfOoMs1HF2QoWoIsnHXZ+E9bbBjJEIe1JgLD2bYORSjUaO342dMBoJhOmipCDQUnfwxtxO5jhu0eUJvyH1bVT9ZU0K1NxHWCWR1s/P6L80f0MuCjII6kVVcU+M47ySSKocNcQP+obVonXLV7L52a9eUk9NtViTgst5KIkAE3TCLUK1Yt1/DnM8F7s32PyrOWgMDJ2f4q/UweoiyqPoPk63BQkhEWTnc+AwAAAHoEcHJpbnQob3BlbignZmxhZy50eHQnLCdyJykucmVhZGxpbmVzKCkpCiNUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVFRUVAs=
['404CTF{Th1s_Ch3cksum_W4s_Tr4sh}']
```