#!/usr/bin/env python3

import sys
from base64 import b64encode

def checksum(data: bytes) -> int:
    # Sum the integer value of each byte and multiply the result by the length
    chksum = sum(data) * len(data)

    return chksum

# On reprend le parsing et on charge l'exemple.
def load():
    with open('script.py.zsig', "rb") as f:
        f.read(5)
        
        f.read(1)
        signature = f.read(300)

        f.read(1)
        size = int.from_bytes(f.read(4), "big")

        f.read(1)
        data = f.read()

        return size, signature, data


# On génère l'output en base64
def generate(signature, data):
    size_bytes = len(data).to_bytes(4, "big")
    out_bytes = b"\x01ZSig\x02" + signature + b"\x03" + size_bytes + b"\x04" + data
    return b64encode(out_bytes)


size, signature, old_payload = load()
somme = sum(old_payload)
print(f'Old payload:\n{old_payload}')
print(f'{size=}, {somme=}')

payload = "\n".join(sys.stdin.readlines())
payload = bytearray(payload, encoding='ascii')
payload = payload + "\n".encode() + b'#'
car_to_jam = size - len(payload)
sum_to_jam = somme - sum(payload)
jamming_car = ( sum_to_jam // car_to_jam ) +1
for _ in range(car_to_jam - 1):
    payload.append(jamming_car)
payload.append(somme - sum(payload))

print(f'With padding:\n{payload.decode()}')
print(f'Payload:\n{generate(signature, bytes(payload))}')