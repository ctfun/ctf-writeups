# 404CTF (DGSE)

- Analyse forensique
  - [Ping Pong](forensic/ping-pong/ping-pong.md)
  - [Un agent compromis (1/3)](forensic/un-agent-compromis/un-agent-compromis.md)
  - [Un agent compromis (2/3)](forensic/un-agent-compromis/un-agent-compromis.md#un-agent-compromis-23)
  - [Un agent compromis (3/3)](forensic/un-agent-compromis/un-agent-compromis.md#un-agent-compromis-33)
  - [SOS RAID (1/2)](forensic/SOS-RAID/SOS-RAID.md)
  - [SOS RAID (2/2)](forensic/SOS-RAID/SOS-RAID.md#sos-raid-22)
  - [Hackllebarde ransomware (1/4)](forensic/hackllebarde-ransomware/hackllebarde-ransomware.md)
  - [Hackllebarde ransomware (2/4)](forensic/hackllebarde-ransomware/hackllebarde-ransomware.md#hackllebarde-ransomware-24)

- Rétro-ingénierie
  - [Mot de passe ?](reverse/mot-de-passe/mot-de-passe.md)
  - [Renverse la tour (1/2)](reverse/renverse-la-tour/renverse-la-tour.md)
  - [Renverse la tour (2/2)](reverse/renverse-la-tour/renverse-la-tour.md#renverse-la-tour--22)
  - [Mise à jour requise](reverse/mise-a-jour-requise/mise-a-jour-requise.md)
  - [Pas de mise à jour](reverse/pas-de-mise-a-jour/pas-de-mise-a-jour.md)

- Stéganographie
  - [PNG : Un logo obèse (1/4)](steg/PNG-un-logo-obèse/PNG-un-logo-obèse.md)
  - [PNG : Drôle de chimères (2/4)](steg/PNG-un-logo-obèse/PNG-un-logo-obèse.md#png-drôles-de-chimères-24)
  - [PNG : Toujours obèse (3/4)](steg/PNG-un-logo-obèse/PNG-un-logo-obèse.md#png-toujours-obèse-34)
  - [PNG : Une histoire de filtres (4/4)](steg/PNG-un-logo-obèse/PNG-un-logo-obèse.md#png-une-histoire-de-filtres-44)

- Cryptanalyse
  - [Un RSA incassable](crypto/un-rsa-incassable.md)
  - [Un simple Oracle (1/2)](crypto/un-simple-oracle/un-simple-oracle.md)
  - [Un simple Oracle (2/2)](crypto/un-simple-oracle/un-simple-oracle.md#un-simple-oracle-22)
  - [Weak Signature](crypto/weak-signature/weak-signature.md)
  - [La Fonte des Hashs](crypto/la-fonte-des-hashs/la-fonte-des-hashs.md)

- Renseignement en sources ouvertes
  - [À l'aube d'un échange](osint/a-l-aube-d-un-echange/a-l-aube-d-un-echange.md)
  - [Collaborateur suspect](osint/collaborateur-suspect.md)
  - [Nous sommes infiltrés !](osint/nous-sommes-infiltrés/nous-sommes-infiltrés.md)

- Programmation
  - [Données corrompues](prog/donnees-corrompues/donnees-corrompues.md)
  - [Découpé](prog/decoupe/decoupe.md)
  - [128code128](prog/128code128/128code128.md)
  
- Exploitation de binaires
  - [Trop Facile](exploit/trop-facile/trop-facile.md)
  - [Sans protection](exploit/sans-protection/sans-protection.md)
  - [Cache-cache](exploit/cache-cache/cache-cache.md)
  - [Patchwork](exploit/patchwork/patchwork.md)

- Divers  
  - [Pierre-papier-Hallebarde](divers/pierre-papier-hallebarde/pierre-papier-hallebarde.md)
  - [Par câble](divers/par-cable/par-cable.md)
  - [Un utilisateur suspicieux (1/2)](divers/un-utilisateur-suspicieux.md)
  - [Un utilisateur suspicieux (2/2)](divers/un-utilisateur-suspicieux.md#un-utilisateur-suspicieux-22)