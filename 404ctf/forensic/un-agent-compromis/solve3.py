#!/usr/bin/env python3

import dpkt

next_is_name = False
next_is_content = False
f = open('capture-reseau.pcap','rb')
for ts, pkt in dpkt.pcap.Reader(f):
    # On ne garde que les query DNS
    eth=dpkt.ethernet.Ethernet(pkt) 
    if eth.type!=dpkt.ethernet.ETH_TYPE_IP:
       continue

    ip=eth.data
    if ip.p!=dpkt.ip.IP_PROTO_UDP: 
        continue

    try:
        dns = dpkt.dns.DNS(ip.data.data)
        if dns.qr != dpkt.dns.DNS_Q:
            continue
        name = dns.qd[0].name
    except:
        # On va supposer que ça ne gênera pas. :/
        continue

    if "hallebarde.404ctf.fr" in name:
        name = name.split('.')[0]
        if next_is_name:
            filename = bytes.fromhex(name).decode('utf-8')
            print(f'Writing {filename}')
            output = open(filename, 'wb')
            next_is_name = False
        elif name == '656E64':  # end
            next_is_content = False
            output.close()
        elif next_is_content:
            output.write(bytes.fromhex(name))
        elif name == '626567696E':  # begin
            next_is_content = True
        elif name == 'never-gonna-give-you-up':
            next_is_name = True