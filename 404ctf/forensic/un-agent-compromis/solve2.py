#!/usr/bin/env python3

import dpkt

files = list()
next_is_name = False
f = open('capture-reseau.pcap','rb')
for ts, pkt in dpkt.pcap.Reader(f):
    # On ne garde que les query DNS
    eth=dpkt.ethernet.Ethernet(pkt) 
    if eth.type!=dpkt.ethernet.ETH_TYPE_IP:
       continue

    ip=eth.data
    if ip.p!=dpkt.ip.IP_PROTO_UDP: 
        continue

    try:
        dns = dpkt.dns.DNS(ip.data.data)
        if dns.qr != dpkt.dns.DNS_Q:
            continue
        name = dns.qd[0].name
    except:
        # On va supposer que ça ne gênera pas. :/
        continue

    if "hallebarde.404ctf.fr" in name:
        #print(next_is_name, ts, name)
        if next_is_name:
            name = name.split('.')[0]
            filename = bytes.fromhex(name).decode('utf-8')
            print(f'{name} => {filename}')
            files.append(filename)
            next_is_name = False
        if name == 'never-gonna-give-you-up.hallebarde.404ctf.fr':
            next_is_name = True

files.sort()
print('404CTF{' + ','.join(files) + '}')