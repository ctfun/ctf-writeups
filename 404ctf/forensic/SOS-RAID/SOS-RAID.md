# SOS RAID [1/2]

Nous avons réussi à infiltrer Hallebarde et à exfiltrer des données. Cependant nos agents se sont fait repérer durant la copie et ils n'ont pas pu copier l'intégralité des données. Pouvez-vous analyser ce qu'ils ont réussi à récupérer ?

Fichiers: [manuel.txt](manuel.txt) [disk1.img](disk1.img) [disk2.img](disk2.img)

Auteur : `Typhlos#9037`

**Solution**

Le fichier [manuel.txt](manuel.txt) indique qu'on est présence de disques en RAID5 de blocs de 1 octet. On a plus de détails sur [wikipedia](https://fr.wikipedia.org/wiki/RAID_(informatique)#RAID_5_:_volume_agr%C3%A9g%C3%A9_par_bandes_%C3%A0_parit%C3%A9_r%C3%A9partie)

Le RAID5 est composé en théorie de 3 disques. On nous en a fourni que 2, il va donc falloir reconstruire. Les blocs sont organisés comme suit :

0 | 1 | 2
:-:|:-:|:-:
a1|a2|ap
b1|bp|b2
cp|c1|c2
...

Avec Xp = X1 xor X2

Il est alors assez trivial d'écrire [un petit script python](solve.py) pour la reconstruction. Finalement, on obtient une archive ZIP qui contient, entre autre, un flag :
```console
$ python3 solve.py 
$ unzip disk.zip 
Archive:  disk.zip
  inflating: flag.txt                
  inflating: flag_c0rr_pt3d.png      
$ cat flag.txt 
404CTF{RAID_5_3st_p4s_tr3s_c0mpl1qu3_1abe46685ecf}
```

# SOS RAID [2/2]

Bravo, vous avez réussi à récupérer les données. Cependant, il s'avère que l'un des fichiers a été corrompu pendant la copie. Pouvez-vous le réparer pour en extraire des informations ?

Fichier: [flag_c0rr_pt3d.png](flag_c0rr_pt3d.png)

Auteur : `Typhlos#9037`

**solution**

Le PNG aussi présent dans l'archive est effectivement corrompu :

```console
$ pngcheck -vvv flag_c0rr_pt3d.png 
File: flag_c0rr_pt3d.png (61863 bytes)
  File is CORRUPTED.  It seems to have suffered EOL conversion.
ERRORS DETECTED in flag_c0rr_pt3d.png
```

À ce sujet, on peut consulter [un autre write-up](https://blog.raw.pm/en/IceCTF-2016-IceCTF-50-Corrupt-Tansmission-Forencics/) sur une épreuve similaire, mais en gros, la démarche globale va être d'alterner des vérifications avec [`pngcheck`](http://www.libpng.org/pub/png/apps/pngcheck.html) et des modifications à l'aide d'un éditeur hexadécimal pour réparer «à la main».

On commence en vérifiant la position du chunk `IHDR` qui semble ok, donc ce sont juste des octets qui ont changé. À commencer par le header `PNG`, que l'on corrige :
```
Avant: 89 50 4e 47 0d 00 00 0a
Après: 09 50 4e 47 0d 0a 1a 0a
```

On regarde ce que ça donne :

```console
$ pngcheck -vvv WORK-flag_c0rr_pt3d.png
File: WORK-flag_c0rr_pt3d.png (61863 bytes)
  chunk IHDR at offset 0x0000c, length 65293:  invalid length
ERRORS DETECTED in WORK-flag_c0rr_pt3d.png
```

En effet, on voit que le `IHDR` devrait faire 13 octets. On corrige sa taille :
```
00 00 ff 0d 49 48 44 52 ....IHDR
00 00 00 0d 49 48 44 52 ....IHDR
```

Vérification :

```console
$ pngcheck -vvv WORK-flag_c0rr_pt3d.png
File: WORK-flag_c0rr_pt3d.png (61863 bytes)
  chunk IHDR at offset 0x0000c, length 13
    1152 x 62088 image, 32-bit RGB+alpha, non-interlaced
  CRC error in chunk IHDR (computed 6c55bbd4, expected 082b810d)
ERRORS DETECTED in WORK-flag_c0rr_pt3d.png
```

Cette fois, c'est le CRC du même chunk qui est mauvais. On change le CRC avec la valeur fournie par pngcheck. :)

```console
$ pngcheck -vvv WORK-flag_c0rr_pt3d.png
File: WORK-flag_c0rr_pt3d.png (61863 bytes)
  chunk IHDR at offset 0x0000c, length 13
    1152 x 62088 image, 32-bit RGB+alpha, non-interlaced
  chunk sRGB at offset 0x00025, length 1
    rendering intent = perceptual
  chunk gAMA at offset 0x00032, length 4: 0.45455
  chunk pHYs at offset 0x00042, length 9: 3780x3780 pixels/meter (96 dpi)
  chunk IDAT at offset 0x00057, length 61756
    zlib: deflated, 32K window, fast compression
  chunk IEND at offset 0x0f19f, length 173:  EOF while reading data
ERRORS DETECTED in WORK-flag_c0rr_pt3d.png
```

La taille du `IEND` devrait être nulle, donc on corrige encore. Puis :

```console
$ pngcheck -vvv WORK-flag_c0rr_pt3d.png
File: WORK-flag_c0rr_pt3d.png (61863 bytes)
  chunk IHDR at offset 0x0000c, length 13
    1152 x 62088 image, 32-bit RGB+alpha, non-interlaced
  chunk sRGB at offset 0x00025, length 1
    rendering intent = perceptual
  chunk gAMA at offset 0x00032, length 4: 0.45455
  chunk pHYs at offset 0x00042, length 9: 3780x3780 pixels/meter (96 dpi)
  chunk IDAT at offset 0x00057, length 61756
    zlib: deflated, 32K window, fast compression
  chunk IEND at offset 0x0f19f, length 0
No errors detected in WORK-flag_c0rr_pt3d.png (6 chunks, 100.0% compression).
```

On note que la géométrie annoncée dans `IHDR` est totalement incohérente. Mais pas de panique, on arrive quand même à l'ouvrir et obtenir le flag `404CTF{L4_C0rr_pt10N_s3_r_p4r_}`:

![Fixed-flag_c0rr_pt3d.png](Fixed-flag_c0rr_pt3d.png)
