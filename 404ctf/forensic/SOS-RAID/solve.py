#!/usr/bin/env python3

d0 = bytearray(open('disk0.img', 'rb').read(999999))
d1 = bytearray(open('disk1.img', 'rb').read(999999))

content = bytearray()
while len(d0) > 0:
    # Round 1 : A1 A2 AP
    a1 = d0.pop(0) 
    a2 = d1.pop(0)
    content.extend((a1, a2))
    # Round 2 : B1 BP B2
    b1 = d0.pop(0)
    b2 = b1 ^ d1.pop(0)
    content.extend((b1, b2))
    # Round 3 : CP C1 C2
    c1 = d1.pop(0)
    c2 = c1 ^ d0.pop(0)
    content.extend((c1, c2))

open('disk.zip', 'wb').write(content)