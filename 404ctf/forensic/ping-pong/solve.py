#!/usr/bin/env python3

import dpkt

f = open('ping.pcap','rb')
count = 0
content = list()
for ts, pkt in dpkt.pcap.Reader(f):
    count += 1
    if count % 2 == 1:
        continue

    eth=dpkt.ethernet.Ethernet(pkt) 
    content.append(len(eth.data))

content = [ chr(x-28) for x in content ]
print(''.join(content))