# Ping Pong

Nous avons repéré une communication bizarre provenant des serveurs de Hallebarde. On soupçonne qu'ils en aient profité pour s'échanger des informations vitales. Pouvez-vous investiguer ?

Auteur : `Typhlos#9037`

Fichier : [ping.pcapng](ping.pcapng)

Indice: Les données en tant que telles ne sont pas forcément pertinentes, pensez à regarder autour des données.

**Solution**

On voit une suite de paquets `ICMP`. Le contenu ne donne pas grand chose, et c'est ce que semble confirmer l'indice. En revanche, parmi les choses qui bougent un peu, on voit la taille des paquets `IP`. Et effectivement, en jouant avec, on se rend compte que si on leur retranche 26, on a quelque chose d'intelligible. 

On [scripte](solve.py) et on obtient `404CTF{Un_p1ng_p0ng_p4s_si_1nn0c3nt}`.