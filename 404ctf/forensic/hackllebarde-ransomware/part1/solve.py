#!/usr/bin/env python3

import dpkt

f = open('ransomware1.pcap','rb')
pdf = bytearray()
for ts, pkt in dpkt.pcap.Reader(f):
    eth=dpkt.ethernet.Ethernet(pkt) 
    if not isinstance(eth.data, dpkt.ip.IP):
        continue
    ip = eth.data
    if ip.src != b'\xac\x11\x00\x01':   # 172.17.0.1
        continue

    tcp = ip.data

    f = tcp.flags & 0xff
    # if f >= 32 and f <= 127:
    #    print(f'{f:c}', end='')
    # else:
    #    print('.', end='')
    pdf.append(f)

open('result.pdf', 'wb').write(pdf)