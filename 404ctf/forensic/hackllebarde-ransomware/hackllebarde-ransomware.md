# Hackllebarde ransomware [1/4]

Alerte, nous sommes attaqués ! Un ransomware vient de frapper l'infrastructure de notre centre de commandement ! Hors de question de payer la rançon.
Comme vous êtes notre meilleur élément, nous vous chargeons de la gestion de cette crise. Nous pourrions faire une simple réinstallation avec nos sauvegardes, mais nous souhaitons récupérer avant tout le plus d'informations. Avant de nous intéresser à l'origine de ce malware, nous devons trouver si des informations confidentielles ont été exfiltrées afin de prendre les mesures appropriées. Nous avons pu enregistrer un trafic réseau suspect, mais impossible de savoir ce qu'il contient. Jetez-y un œil !

Auteur : `mh4ckt3mh4ckt1c4s#0705`

Fichier: [ransomware1.pcapng](ransomware1.pcapng)

**Solution**

Effectivement, on ne voit que des paquets bizarres. Y'aurait-il une exfiltration via un champ particulier ? On peut remarquer que les flags `TCP`n'ont aucun sens. Donc on commence par regarder ces flags (et ce qui change dedans surtout). On dumpe les 8 bits de poids faibles des flags et...

```
%P.DF-1..7..%......1. .0. .ob.j...<<../Ty.p.e ./C.a.ta.log../P.a.ges. .2. .0. .R...>>..endob.j...3. .0. .ob.j...<<../C.r.ea.ti.onDa.te .(.D:.2.0.2.2.0.2.0.42.1.1.1.44Z.0.0.'0.0.').../C.r.ea.tor. .(.P.DF2.4).../P.r.oduc.er. .(.i.LoveP.DF).../ModDa.te .(.D:.2.0.2.2.0.2.0.42.1.1.3.1.2.Z.)...>>..endob.j...2. .0. .ob.j...
```

Ok, on a un PDF là-dedans, mais il y a du ménage à faire... ;-) En creusant, il ne faut garder que les paquets émis par 172.17.0.1. Du coup, on code [un petit extracteur](part1/solve.py), et on obtient effectivement [un PDF](part1/result.pdf), qui contient le flag `404CTF{L3s_fL4gS_TCP_Pr1S_3n_fL4G}`.


# Hackllebarde ransomware [2/4]

Pour la suite de cette investigation, on vous donne accès à un dump mémoire d'une de nos stations qui a été compromise. Vous devez trouver la source de l'infection ! Aussi, il semblerait que le hackeur ait consulté des ressources depuis cette machine. Savoir quelles sont les techniques sur lesquelles il s'est renseigné nous aiderait beaucoup, alors retrouvez cette information !

Vous devez retrouver :
- une adresse IP distante qui a été contactée pour transmettre des données
- un numéro de port de la machine compromise qui a servi à échanger des données
- le nom d'un binaire malveillant
- un lien web correspondant à la ressource consultée par l'attaquant

Le flag est sous ce format : `404CTF{ip:port:binaire:lien}` Par exemple, `404CTF{127.0.0.1:80:bash:https://google.fr}` est un format de flag valide.

Somme de contrôle md5 du fichier extrait `dumpmem.raw` : `dc2c324bf5fc80b6bf17a9def1b386ee`

Auteur : `mh4ckt3mh4ckt1c4s#0705`

Fichier : [ransomware2.7z](https://mega.nz/file/swhxUALS#adZT9Hb6vbZ_nCOWADhTzLoZb8LvxpULw--xc9EgV_E)

**Solution**

C'est une image mémoire, on sort donc le vénérable [Volatility2](https://github.com/volatilityfoundation/volatility) (`vol2` est un wrapper qui l'appelle avec le bon profil et le bon dump). On commence par chercher le noyau Linux dont il est question.

```console
$ ./vol2 linux_banner
Volatility Foundation Volatility Framework 2.6.1
Linux version 5.4.0-107-generic (buildd@lcy02-amd64-070) (gcc version 7.5.0 (Ubuntu 7.5.0-3ubuntu1~18.04)) #121~18.04.1-Ubuntu SMP Thu Mar 24 17:21:33 UTC 2022 (Ubuntu 5.4.0-107.121~18.04.1-generic 5.4.174)
```

On se fait [un profil adapté](part2/linux-5.4.0-107-404CTF.zip) en suivant [la documentation officielle](https://github.com/volatilityfoundation/volatility/wiki/Linux#creating-a-new-profile), et on démarre l'analyse. Immédiatement, dans la liste des processus qui fonctionnent (`linux_psaux`):

```
2576   1000   1000   /usr/lib/gnome-terminal/gnome-terminal-server                   
2586   1000   1000   bash                                                            
2607   1000   1000   bash                                                            
2627   1000   1000   bash                                                            
2645   1000   1000   /usr/bin/python3 ./JeNeSuisPasDuToutUnFichierMalveillant        
2646   1000   1000   sh -c nc -lvnp 13598 > /tmp/secret                              
2647   1000   1000   nc -lvnp 13598                                                  
2662   1000   1000   bash      
```

Le nom du process est donc assez évident, c'est `JeNeSuisPasDuToutUnFichierMalveillant`. On voit qu'en parallèle, il y a un `netcat` qui écoute sur le port 13598. On regarde qui est connecté dessus par `linux_netstat` :
```
TCP      192.168.61.2    :13598 192.168.61.137  :38088 ESTABLISHED                    nc/2647 
```

On a donc le port et l'adresse IP distante et le port local. Reste l'URL à trouver. Après avoir tenter de restaurer l'historique de Firefox, j'ai fini en récupérant toutes les URLS présentes dans le dump à coup de [strings]. Je m'attendais un peu à une idiotie, donc les URLS youtube étaient de bons candidats. C'est finalement [Full Ethical Hacking Course - Network Penetration Testing for Beginners (2019)](https://www.youtube.com/watch?v=3Kq1MIfTWCE) la bonne URL.

Le flag complet est donc `404CTF{192.168.61.137:13598:JeNeSuisPasDuToutUnFichierMalveillant:https://www.youtube.com/watch?v=3Kq1MIfTWCE}`


# Hackllebarde ransomware [3/4]

Alors que nous enquêtions sur ce ransomware, les agents de sécurité du batiment ont arrêté un individu suspect qui tentait de s'enfuir de notre complexe. Nous avons trouvé une clé USB sur lui, mais elle ne semble rien contenir d'intéressant, vous pouvez vérifier ?

Somme de contrôle md5 de l'image disque : 567646e7a1e35d8d6b19447f4868f028

Auteur : mh4ckt3mh4ckt1c4s#0507

**Solution**

[...]