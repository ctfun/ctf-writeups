# Pierre-papier-Hallebarde

Hallebarde a mis en place sa variante du Pierre-papier-ciseaux. À ce jour, personne de nos services n'est parvenu à vaincre l'ordinateur. Montrez-leur de quoi vous êtes capable en récupérant leur précieux flag.txt !

Auteur : `Woni`

`nc challenge.404ctf.fr 30806`

Fichier: [pierre-papier-hallebarde.py](pierre-papier-hallebarde.py)

**Solution**

On est en présence d'un pierre papier ciseau, sauf que l'ordinateur triche et fait son choix en fonction de notre proposition. La blague est ailleurs. On remarque d'abord que le script est écrit en python 2.7. Or, dans cette version, le mot clef `input` est :

```
 input([prompt])¶
    Equivalent to eval(raw_input(prompt)).
```

On peut donc exécuter du code à travers notre saisie. Mais il faut que le résultat de l'évaluation de notre saisie soit 1, 2 ou 3 pour qu'il soit accepté par l'ordinateur. La possibilité que j'ai choisi, c'est de lire chacun des caractères du flag, de comparer sa valeur avec une que l'on devine, et retourner 1 ou 2 en conséquence. Pour faire plus propre, j'ai choisi de faire une petite dichotomie (ça va un peu plus vite).

Le script est [solve.py](solve.py) et un exemple d'exécution :

```console
$ python solve.py 
[+] Opening connection to challenge.404ctf.fr on port 30806: Done
flag='4'
flag='40'
flag='404'
flag='404C'
flag='404CT'
flag='404CTF'
flag='404CTF{'
flag='404CTF{c'
flag='404CTF{cH'
flag='404CTF{cH0'
flag='404CTF{cH0i'
flag='404CTF{cH0iX'
flag='404CTF{cH0iX_'
flag='404CTF{cH0iX_n'
flag='404CTF{cH0iX_nU'
flag='404CTF{cH0iX_nUm'
flag='404CTF{cH0iX_nUm3'
flag='404CTF{cH0iX_nUm3r'
flag='404CTF{cH0iX_nUm3r0'
flag='404CTF{cH0iX_nUm3r0_'
flag='404CTF{cH0iX_nUm3r0_4'
flag='404CTF{cH0iX_nUm3r0_4_'
flag='404CTF{cH0iX_nUm3r0_4_v'
flag='404CTF{cH0iX_nUm3r0_4_v1'
flag='404CTF{cH0iX_nUm3r0_4_v1c'
flag='404CTF{cH0iX_nUm3r0_4_v1c7'
flag='404CTF{cH0iX_nUm3r0_4_v1c70'
flag='404CTF{cH0iX_nUm3r0_4_v1c701'
flag='404CTF{cH0iX_nUm3r0_4_v1c701r'
flag='404CTF{cH0iX_nUm3r0_4_v1c701r3'
flag='404CTF{cH0iX_nUm3r0_4_v1c701r3}'
[*] Closed connection to challenge.404ctf.fr port 30806
```