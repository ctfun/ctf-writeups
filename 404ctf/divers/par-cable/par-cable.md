# Par câble

Un de nos agents a réussi à acquérir les tensions qui passaient à travers un câble de communication de Hallebarde. Pouvez-vous interpréter le signal ?

Fichier: [Cable.txt](Cable.txt)

Auteur : `Astate#3107`

**Solution**

On est en présence d'une liste de valeurs 1 et -1. Il y a de multiple façon d'encoder un signal numérique via des transitions, l'une d'entre elles est simplement de conserver une même valeur pour encoder un `0`, et faire une transition vers une autre valeur pour encoder un `1`. On peut se rendre compte que c'est ce qui est fait ici : si deux valeurs qui se suivent dans la liste sont égales, c'est un `0`, sinon c'est un `1`.

On peut donc résoudre le défi à l'aide d'un simple bout de python :

```python
>>> serie = "1 1 1 -1 1 1 -1 -1 -1 -1 -1 1 -1 -1 -1 -1 -1 -1 -1 1 -1 -1 1 1 1 1 -1 -1 -1 -1 -1 1 -1 -1 1 1 -1 -1 1 1 1 1 -1 -1 -1 -1 1 -1 -1 -1 1 -1 1 -1 -1 1 -1 -1 1 1 1 -1 1 -1 -1 -1 -1 1 -1 -1 -1 -1 -1 -1 1 -1 -1 1 -1 1 1 1 1 -1 1 1 1 -1 1 1 -1 -1 1 -1 1 -1 1 1 -1 -1 1 1 1 -1 -1 -1 -1 1 -1 -1 -1 1 -1 -1 1 -1 1 1 -1 -1 -1 -1 1 1 -1 -1 1 1 -1 -1 1 -1 1 1 1 -1 -1 -1 1 -1 -1 1 -1 1 1 1 -1 -1 1 -1 1 -1 1 1 -1 -1 1 -1 -1 1 1 1 1 -1 1 1 1 -1 1 1 -1 1 -1 -1 -1 1 1 1 1 -1 1 1 1 1 1 1 -1 -1 1 -1 1 -1 1 1 -1 -1 -1 1 1 1 -1 -1 1 -1 -1 1 -1 1 1 1 -1 -1 1 1 -1 1 1 1 1 -1 1 1 1 -1 1 1 -1 1 -1 -1 -1 1 1 1 -1 1 -1 -1 1 1 1 1 -1 -1 -1 -1 1 1 -1 -1 1 -1 -1 -1 1 1 1 1 -1 -1 1 -1 1 -1 1 1 -1 1 1 1 -1 1 1 1 1 -1 1 1 1 1 1 1 -1 1 -1 -1 -1 1 1 1 1 -1 1 1 1 -1 1 1 -1 1 -1 -1 1 -1 -1 -1 -1 1 -1 -1 -1 1 -1 -1 1 -1 1 1 1 -1 -1 -1 1 -1 1 -1 1 1 -1".split(' ')
>>> result = ''
>>> for a, b in zip(serie, serie[1:]):
...   if a == b:
...     result += '0'
...   else:
...     result += '1'
>>> result
'0011010000110000001101000100001101010100010001100111101101001110001100000110111000110011010111110101001000110011011101000101010101110010011011100101111101011010001100110111001000110000010111110100100101101110010101100011001101110010011101000100010101100100010111110110011000110000011100100011001101110110001100110111001001111101'
```

Une fois décodé, le binaire nous donne le flag `404CTF{N0n3_R3tUrn_Z3r0_InV3rtEd_f0r3v3r}`.