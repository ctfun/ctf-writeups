# Secure FTP over UDP (3 challenges)

**Description**

Votre société a acheté un nouveau logiciel de serveur FTP. Celui-ci est un peu spécial et il n'existe pas de client pour Linux !

- Étape 1

    À l'aide de la documentation fournie ci-dessous, implémentez un client allant jusqu'à l'établissement d'une session.

- Étape 2

    À l'aide de la documentation fournie ci-dessous, implémentez un client allant jusqu'à la connexion utilisateur.

- Étape 3

    À l'aide de la documentation fournie ci-dessous, implémentez un client allant jusqu'à la récupération du dernier flag stocké dans un fichier.

**Accès à l'épreuve**

URL: [udp://secure-ftp.dghack.fr:4445/](udp://secure-ftp.dghack.fr:4445/)

**Fichiers joints**

[dghack2021-secure-ftp-over-udp-protocol.md](dghack2021-secure-ftp-over-udp-protocol.md) 6.02kB  
sha256: 45dbfa59cb127d8148ba9c07c7378f0ccc0cb18d348f4977b140b8b1365dafcd

**Solution**

Mon implémentation est disponible dans le script [client.py](client.py). Une trace d'exécution :
```python
[+] FTP connection
> ConnectMessage(CONNECT)
< ConnectReply(d5d64cb1-efe4-40f5-96b8-612dc0c35f08, DGA{746999b743b91605261e})
[+] Flag #1: DGA{746999b743b91605261e}
> RsaKeyMessage(d5d64cb1-efe4-40f5-96b8-612dc0c35f08)
< RsaKeyReply(ZOpoUXl+SGZe1SfVkm5zZHVVbGbjcmpDWONvbWfLdVVouXgBsj81LBaMJ+cBQ3Q7YJKfQXMRvVCxEkcuMtUKbMj1TmXOBcavs7wysI72yxlPnNIyEiuEMPvexwr3TcLZnA246qNx3AN5nF3iez8SNxTzwrLY2k2tDWc2SO9FobMg1ETJq/eyBY64BYGm+ucxzT2HC07UxT8GH3mWLciLaXrCR65HMwDzysEIY1Ib1JkvKCnxCCNKSMUCXQIZGOhBxiXDZEKxC5+dFc+yxdRtSGDliq8Fqr5wjjV5NQ/5F4LSWk2odVX4pFmXzgVd7xuOoy4Q/ozXfFNQ8w9mYbJWMTFf3PED/3zH24NOIOUbrd7SHgMDm6EK3wuAiqurYeR2DG1QZGNz)
self.servPubKey=bytearray(b'0\x82\x01"0\r\x06\t*\x86H\x86\xf7\r\x01\x01\x01\x05\x00\x03\x82\x01\x0f\x000\x82\x01\n\x02\x82\x01\x01\x00\xd0\x0bH\xc1qZXE\xe3t\x82b1\x11O0\xfe\xfa \x00t\xfe8\xd0| K{\xa1^\x04\xa1\x86\x07\x16\x80j\xb2\xfc\xdc\xefW\xd3\xfc\x93\xbfI#\xf9\xb3Awh\xecQ\x95\xb9\xa2C\x83\x19\xaa\xb0\xefD\xcb\xa4\xcc\x05\x8fl*\xf9>\x90\x1eKB[q\x92\xb1\xd7\x9b\xb2,\xc3j\x02\x7f<\xbb-\xc8\xc0i\xa7\n\xa6\xdf\xa4\xddV\xeb\xdbw\xe4\xd2\xaa\x8bT\xacN\xe2H&\xb5\xabXcV\r\xc2E\xa1\xf8 \t\x8c(\xda\x14\\S\x96\xa9\xb3m\x17\x02w\xb1\xf8\\Mj\x99iM--\x8cv\tjpk\xa12\x88J\xb77-\xe2n\xfc\xefp\xbb\xe2\xa9\xb1\x0c;\x05\xa6\xe2\xcek\xcd\xdb9\xfaa\x11\\|\xb0d\xcc\xbd.\x1e\xc7&0\x9b\xd6<\xe3\x9ei8\x8eh\xeb\xe0Fq\x90\xeb\xb25\'\x04\x9bf\x15(\xc1\x18^E\x0c\xb3\xa2f\x9c\x0e\xa2\xaf\xd3"E\x84h\xc8\x9d\xba\x7fmd\xfe\xe8~\x8bc\xe9\xf9\xe2\xd8/\x8b\x02_\x02\x03\x01\x00\x01')
> SessionKeyMessage(d5d64cb1-efe4-40f5-96b8-612dc0c35f08, j7VibjsJul/Axl5HfRE9wBgxK5TUQzPU61XitEwVYKy8ASXO9lSF3Ztdn1So0u/PcwLPIx0aPVHE59F75BWTEL8UYC9IdZp6MRPkdZWNDm+3uT/prFTQ9VZBpisEyNWe9UXJAzQiuwmswOVWglDWFDViKLoj6lF28oSfH9O1y3cvmvtitqiNuFRCRD9ri32PQUo0vr7AP4FS5wUMLgx/2k3kKPD0WY0NIhDA6S54UUdnl1e7DEfHjlLx/E5jVICItW4vNtJmxd6PT+YE0ZvZMoqppRjfg9P6rxSLmvBn0R1zabcUhgVW3Qy27ZkUrTtGp3+fwg/kMDgtHX6W3Hl0yg==)
> SessionKeyMessage(d5d64cb1-efe4-40f5-96b8-612dc0c35f08, j7VibjsJul/Axl5HfRE9wBgxK5TUQzPU61XitEwVYKy8ASXO9lSF3Ztdn1So0u/PcwLPIx0aPVHE59F75BWTEL8UYC9IdZp6MRPkdZWNDm+3uT/prFTQ9VZBpisEyNWe9UXJAzQiuwmswOVWglDWFDViKLoj6lF28oSfH9O1y3cvmvtitqiNuFRCRD9ri32PQUo0vr7AP4FS5wUMLgx/2k3kKPD0WY0NIhDA6S54UUdnl1e7DEfHjlLx/E5jVICItW4vNtJmxd6PT+YE0ZvZMoqppRjfg9P6rxSLmvBn0R1zabcUhgVW3Qy27ZkUrTtGp3+fwg/kMDgtHX6W3Hl0yg==)
< SessionKeyReply(bWlZxISpJU2po4U3d/8S2ahm/NWx+jQCh3YOa13oMSA=)
> AuthMessage(d5d64cb1-efe4-40f5-96b8-612dc0c35f08, bWlZxISpJU2po4U3d/8S2ahm/NWx+jQCh3YOa13oMSA=, AAAAAAAAAAAAAAAAAAAAAAypamOaivjI3+DEjIFqO4E=, AAAAAAAAAAAAAAAAAAAAAKgIg0i8YbWyfFrC13UtuIA=)
< AuthReply(AUTH_OK, DGA{bc3fc7a1a08d5749aa01})
[+] Flag #2: DGA{bc3fc7a1a08d5749aa01}
> GetFilesMessage(d5d64cb1-efe4-40f5-96b8-612dc0c35f08, AAAAAAAAAAAAAAAAAAAAAI/TVOP2nNvdqQ9nkleRCgU=)
< GetFilesReply(eDQb/i7ghsVOnGnoJm4CDSargEDZ7UDBEmFhV02DC20=)
[+] Content of folder /opt: ['dga2021', '']
> GetFilesMessage(d5d64cb1-efe4-40f5-96b8-612dc0c35f08, AAAAAAAAAAAAAAAAAAAAAJTCIzMXDrwvKrA/nSujurA=)
< GetFilesReply(Z+qpeVGYBZKe/dmwb0djXv7YZXe8/o4u7lkQtnMg9NA=)
[+] Content of folder /opt/dga2021: ['flag', '']
> GetFileMessage(d5d64cb1-efe4-40f5-96b8-612dc0c35f08, AAAAAAAAAAAAAAAAAAAAABJtH5q4gOUgWFyrNIPucontNgztXg5Em2TAsOlPWaaW)
< GetFileReply(XJvcmGrXcxa1D2vgMoWojzNerG0I2vrWOGgxB0zNCmNbyTzGFL5aK9qM41KuV+Az)
[+] Flag #3: DGA{222df851d8a68bda4a85}
```
