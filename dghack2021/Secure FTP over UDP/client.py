#!/usr/bin/env python3

from array import array
import socket, select
from functools import reduce
from zlib import crc32
from base64 import b64decode, b64encode
from Crypto.Cipher import AES, PKCS1_v1_5
from Crypto.Util import Padding
from Crypto.PublicKey import RSA

aesKey = ('TheVerySecretKey' * 2).encode('utf-8')
iv = b'\x00' * 16

def xor(data, key = 'ThisIsNotSoSecretPleaseChangeIt'):
    while  len(key) < len(data):
        key += key
    result = map(lambda a,b: a^b, key.encode('utf-8'), data)
    return bytearray(result)


class Message:
    def __init__(self, id, attributes):
        self.id = id
        if type(attributes) is not list:
            raise TypeError('Attributes must be a list of strings')
        self.attributes = attributes

    def encode(self):
        packet = bytearray()
        l = reduce(lambda a,b:a+b, map(len, self.attributes))
        l += 2*len(self.attributes)
        #print(f'{l=}')
        if l < 256:
            size = 1
        elif l < 65536:
            size = 2
        else:
            size = 3
        # ID sur 14 bits
        # size(taille) sur deux bits
        packet.extend((self.id << 2 | size).to_bytes(2, byteorder='big'))
        # Taille sur 1, 2 ou 3 octets
        packet.extend(l.to_bytes(size, byteorder='big'))
        # Contenu, pour chaque attribut:
        #   Taille sur 2 octets
        #   Contenu
        for a in self.attributes:
            packet.extend(len(a).to_bytes(2, byteorder='big'))
            packet.extend(a.encode('utf-8'))
        # CRC32 sur 4 octets
        crc = crc32(packet) & 0xffffffff
        packet.extend(crc.to_bytes(4, byteorder='big'))
        return packet

    @staticmethod
    def decode(packet):
        prefix = int.from_bytes(packet[0:2], byteorder='big')
        #print(f'{prefix=}')
        id = prefix >> 2
        #print(f'{id=}')
        size = prefix & 0x03
        #print(f'{size=}')
        total = int.from_bytes(packet[2:2+size], byteorder='big')
        #print(f'{total=}')
        start = 2+size
        attributes = []
        while start <= total + 2 + size - 4:
            s = int.from_bytes(packet[start:start+2], byteorder='big')
            #print(f'{s=}')
            attr = packet[start+2:start+2+s].decode('utf-8', errors='ignore')
            #print(f'{attr=}')
            attributes.append(attr)
            start = start+2+s
        crc_expected = int.from_bytes(packet[-4:], byteorder='big')
        #print(f'{crc_expected=:x}')
        crc_computed = crc32(packet[:-4]) & 0xffffffff
        #print(f'{crc_computed=:x}')
        if crc_computed != crc_expected:
            raise RuntimeError(f'Bad CRC !')
        return Message(id, attributes)

    def __str__(self):
        if self.id == 1:
            msg = 'ErrorMessage'
        elif self.id == 10:
            msg = 'PingMessage'
        elif self.id == 11:
            msg = 'PingReply'
        elif self.id == 45:
            msg = 'GetFilesMessage'
        elif self.id == 46:
            msg = 'GetFilesReply'
        elif self.id == 78:
            msg = 'RsaKeyMessage'
        elif self.id == 98:
            msg = 'RsaKeyReply'            
        elif self.id == 666:
            msg = 'GetFileMessage'
        elif self.id == 1337:
            msg = 'SessionKeyMessage'            
        elif self.id == 1338:
            msg = 'SessionKeyReply'            
        elif self.id == 1921:
            msg = 'ConnectMessage'
        elif self.id == 4444:
            msg = 'AuthMessage'
        elif self.id == 4875:
            msg = 'ConnectReply'
        elif self.id == 6789:
            msg = 'AuthReply'
        elif self.id == 7331:
            msg = 'GetFileReply'
        else:
            msg = f'Unknown[{self.id}]'
        attrs = ', '.join(self.attributes)
        return f'{msg}({attrs})'


class FtpOverUdp:

    def __init__(self, server, port):
        self.server = server
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.bind(('0.0.0.0', self.port))

    def _encrypt(self, data):
        aes = AES.new(aesKey, AES.MODE_CBC, iv)
        padded = Padding.pad(data, AES.block_size, style='pkcs7')
        encrypted = aes.encrypt(padded)
        return b64encode(iv + encrypted).decode('utf-8')

    def _decrypt(self, data):
        #print(f'{data=}')
        encrypted = b64decode(data)
        #print(f'{encrypted.hex()}')
        iv = encrypted[:AES.block_size]
        #print(f'{iv=}')
        aes = AES.new(aesKey, AES.MODE_CBC, iv)
        decrypted = Padding.unpad(aes.decrypt(encrypted[AES.block_size:]), AES.block_size, style='pkcs7')
        #print(f'{decrypted=}')
        return decrypted

    def _send(self, message):
        print(f'> {message}')
        packet = message.encode()
        #print(f'{packet=} => {Message.decode(packet)}')
        self.sock.sendto(packet, (self.server, self.port))

    def _receive(self):
        self.sock.setblocking(0)
        ready = select.select([self.sock], [], [], 5)
        if ready[0]:
            data, _ = self.sock.recvfrom(2048)
            message = Message.decode(data)
            print(f'< {message}')
            #print("received message: %s" % data)
            return message
        return None

    def _send_and_received(self, message):
        answer = None
        while answer is None:
            self._send(message)
            answer = self._receive()
        return answer

    def ping(self):
        message = Message(10, ['plop'])
        answer = self._send_and_received(message)
        return answer

    def connect(self):
        message = Message(1921, ['CONNECT'])
        answer = self._send_and_received(message)
        self.session_id = answer.attributes[0]
        return answer

    def auth(self):
        # RsaKey
        message = Message(78, [self.session_id])
        answer = self._send_and_received(message)
        self.servPubKey = xor(b64decode(answer.attributes[0]))
        print(f'{self.servPubKey=}')

        # SessionKey
        key = RSA.import_key(self.servPubKey)
        rsa = PKCS1_v1_5.new(key)
        sessionKey = rsa.encrypt(aesKey)
        b64_sessionKey = b64encode(sessionKey).decode('utf-8)')
        message = Message(1337, [self.session_id, b64_sessionKey])
        answer = self._send_and_received(message)
        enc_salt = answer.attributes[0]

        # AuthMessage
        enc_user = self._encrypt(b'GUEST_USER')
        enc_pass = self._encrypt(b'GUEST_PASSWORD')
        message = Message(4444, [self.session_id, enc_salt, enc_user, enc_pass])
        answer = self._send_and_received(message)

        return answer

    def dir(self, path):
        enc_path = self._encrypt(path.encode('utf-8'))
        message = Message(45, [self.session_id, enc_path])
        answer = self._send_and_received(message)
        result = self._decrypt(answer.attributes[0]).decode('utf-8')
        result = result.split('\x00')
        return result

    def cat(self, path):
        enc_path = self._encrypt(path.encode('utf-8'))
        message = Message(666, [self.session_id, enc_path])
        answer = self._send_and_received(message)
        result = self._decrypt(answer.attributes[0]).decode('utf-8')
        return result


ftp = FtpOverUdp('secure-ftp.dghack.fr', 4445)
#ftp.ping()
print(f'[+] FTP connection')
r = ftp.connect()
print(f'[+] Flag #1: {r.attributes[1]}')
r = ftp.auth()
print(f'[+] Flag #2: {r.attributes[1]}')
r = ftp.dir('/opt')
print(f'[+] Content of folder /opt: {r}')
r = ftp.dir('/opt/dga2021')
print(f'[+] Content of folder /opt/dga2021: {r}')
r = ftp.cat('/opt/dga2021/flag')
print(f'[+] Flag #3: {r}')


