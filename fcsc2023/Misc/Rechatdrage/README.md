# Rechatdrage

## Énoncé

Un de vos amis vous a envoyé une photo sur Discord de son chat. Il vous dit qu'un flag était sur la photo mais qu'il a recadré l'image.

Arriverez-vous à retrouver le précieux sésame ?

![](rechatdrage.jpg)

## Solution

On commence comme d'habitude en faisant quelques tests basiques en présence d'une stégano.
- On cherche [des données cachées en LSB](https://github.com/lordOric/steg-tools/blob/main/lsb_detect.py).
- On cherche la présence de données EXIF.
- On tente une recherche inversée sur [Google](https://www.google.com/imghp?tbm=isch&tbs=imgo:1) et [TinEye](https://tineye.com/).
- On ouvre l'image avec [hachoir](https://hachoir.readthedocs.io/en/latest/).

C'est ce dernier point qui est intéressant, car il montre des données après la fin du jpg.

![](hachoir.png)

Me vient une intuition : puisque les autres méthodes ne semblent rien
donner, et que de toute façon l'énoncé suggère qu'il faut récupérer 
l'image originale, peut-être que cette partie est, justement, l'originale ?

On peut le tester rapidement tout en shell, en découpant le fichier à coup
de `tail`/`head` et en le reconstituant avec `cat`:

```console
$ head -c +623 rechatdrage.jpg > debut.jpg
$ tail -c +41731 rechatdrage.jpg > milieu.jpg
$ cat debut.jpg milieu.jpg > test.jpg
```

Si on ouvre le résultat avec `hachoir`, on se rend compte qu'on
n'a même pas besoin d'ajouter le chunk `end_image`, il est déjà là.
Le résultat :

![](test.jpg)

Cela valide notre hypothèse. Par contre, évidemment, l'affichage ne ressemble
à rien car la géométrie indiquée dans les entêtes du fichier ne 
correspond plus à la payload. Il va falloir les patcher. `hachoir` 
nous indique que la hauteur et la largeur sont codées sur deux octets,
aux offsets 163 et 165. Là, j'ai fait à tâtons, en doublant la largeur,
jusqu'à repérer un alignement possible. Au final, une largeur de 1193 et 
une hauteur de 768 donne :

![](win.jpg)

C'est pas nickel, mais suffisant pour avoir le flag !