#!/usr/bin/env python3

from PIL import Image
import sys

# Stolen https://github.com/TheToddLuci0/PyPunch/blob/master/PyPunch/mappings.py
MAPPINGS = {
    # Digits
    '&': (0,),
    '-': (1,),
    '0': (2,),
    '1': (3,),
    '2': (4,),
    '3': (5,),
    '4': (6,),
    '5': (7,),
    '6': (8,),
    '7': (9,),
    '8': (10,),
    '9': (11,),

    # Alpha
    'A': (0, 3),
    'B': (0, 4),
    'C': (0, 5),
    'D': (0, 6),
    'E': (0, 7),
    'F': (0, 8),
    'G': (0, 9),
    'H': (0, 10),
    'I': (0, 11),

    'J': (1, 3),
    'K': (1, 4),
    'L': (1, 5),
    'M': (1, 6),
    'N': (1, 7),
    'O': (1, 8),
    'P': (1, 9),
    'Q': (1, 10),
    'R': (1, 11),

    '/': (2, 3),
    'S': (2, 4),
    'T': (2, 5),
    'U': (2, 6),
    'V': (2, 7),
    'W': (2, 8),
    'X': (2, 9),
    'Y': (2, 10),
    'Z': (2, 11),

    # Special
    '¢': (0, 4, 10),
    '.': (0, 5, 10),
    '<': (0, 6, 10),
    '(': (0, 7, 10),
    '+': (0, 8, 10),
    '|': (0, 9, 10),

    '!': (1, 4, 10),
    '$': (1, 5, 10),
    '*': (1, 6, 10),
    ')': (1, 7, 10),
    ';': (1, 8, 10),
    '¬': (1, 9, 10),

    ',': (2, 5, 10),
    '%': (2, 6, 10),
    '_': (2, 7, 10),
    '>': (2, 8, 10),
    '?': (2, 9, 10),

    ':': (4, 10),
    '#': (5, 10),
    '@': (6, 10),
    '\'': (7, 10),
    '=': (8, 10),
    '"': (9, 10),
}

def indices_to_key(l):
    return '-'.join([ str(x) for x in l ])

rev_mapping = { indices_to_key(v) : k for k, v in MAPPINGS.items()}

def decode_car(l):
    key = indices_to_key(l)
    if key in rev_mapping:
        return rev_mapping[key]
    else:
        return ' '

posh = [53, 64, 75, 86, 96, 107, 118, 129, 140, 151, 162, 172, 183, 194, 205, 216, 227, 238, 248, 259, 270, 281, 292, 303, 314, 325, 335, 346, 357, 368, 379, 390, 401, 411, 422, 433, 444, 455, 466, 477, 488, 498, 509, 520, 531, 542, 553, 564, 574, 585, 596, 607, 618, 629, 640, 650, 661, 672, 683, 694, 705, 716, 727, 737, 748, 759, 770, 781, 792, 803, 813, 824, 835, 846, 857, 868, 879, 889, 900, 911]
posv = (49, 80, 111, 142, 174, 205, 236, 267, 303, 332, 368, 395)

def decode_image(img):
    px = img.load()
    card = list()
    for i in posh:
        byte = []
        for jv, jp in enumerate(posv):
            color = px[i, jp][0]
            if color > 127:
                byte.append(jv)
        card.append(byte)
    return card

for i in range(1, 80):
    img = Image.open(f'{i:010d}.jpg')
    card = decode_image(img)
    txt = ''.join([ decode_car(x) for x in card ])
    print(i, txt)