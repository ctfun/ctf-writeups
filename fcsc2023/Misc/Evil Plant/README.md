# Evil Plant

## Énoncé

D'après nos analyses couplées à nos relevés satellites, nous avons confirmation que la cible, sous couverture d'être une usine de production de vaccins, est en réalité une usine de production de liquide toxique utilisée à des fins militaires.

Les reconnaissances du réseau effectuées nous indiquent que la cible est contrôlée par un automate programmable industriel, lui-même communiquant via une interface SCADA par le protocole OPC-UA en mode binaire. Nous avons exposé la cible sur internet via un implant UMTS, celle-ci est désormais accessible sur le réseau :

`evil-plant.france-cybersecurity-challenge.fr:4841`

Un screenshot de l'interface SCADA à un temps indéterminé a également pu être récupéré :

![](evil-plant.png)

L'analyse des documents d'ingénierie récupérés a montré que la formule du liquide toxique est composée de 16 éléments. Nous ignorons les taux utilisés dans la formule ainsi que l'ordonnancement des différents éléments : nous faisons appel à vous pour les récupérer.

Il semblerait que les éléments soient ajoutés dans la cuve `MIX` (en bas du *screenshot*) deux par deux, mais pour pouvoir créer un remède efficace, nous avons besoin de savoir exactement dans quel ordre et avec quels taux les couples d'éléments sont mélangés.

Faites vite, le temps presse...

**Note** : Le numéro des éléments dans un couple d'éléments est à indiquer dans l'ordre croissant (`030c` et pas `0c03` dans l'étape 2 de l'exemple ci-dessous), et les taux correspondants dans le même ordre.

**Exemple** : On donne un exemple du format du flag à soumettre. Supposons que le processus de fabrication comporte les trois étapes suivantes :

    Étape 1 : ajout de 27 unités (`0x1b`) de l'élément 1 (`0x01`) et de 47 unités (`0x2f`) de l'élément 8 (`0x08`) dans la cuve `MIX`.
    Étape 2 : ajout de 95 unités (`0x5f`) de l'élément 12 (`0x0c`) et de 141 unités (`0x8d`) de l'élément 3 (`0x03`) dans la cuve `MIX`.
    Étape 3 : ajout de 230 unités (`0xe6`) de l'élément 5 (`0x05`) et de 177 unités (`0xb1`) de l'élément 16 (`0x10`) dans la cuve `MIX`.

Le flag à soumettre serait `FCSC{01081b2f030c8d5f0510e6b1}`, où toutes les valeurs sont exprimées en notation hexadécimale.


## Solution

Un tout petit peu de recherche sur le protocole `OPC-UA` fait un peu peur. Il y a l'air d'avoir des 
centaines de pages de spécifications, probablement très intéressantes à étudier, mais peu 
compatibles avec un CTF. En revanche, il semble exister une bibliothèque python, [python OPC-UA](https://python-opcua.readthedocs.io/en/latest/), 
et le repo fournit des exemples. Entre autre, un [client minimaliste](https://github.com/FreeOpcUa/python-opcua/blob/master/examples/client-minimal.py)
qui permet de voir comment se connecter, et comment l'architecture est composée d'une hiérarchie de nœuds.

Du coup, première idée, on liste les nœuds disponibles sur notre serveur. À tâtons, à coup d'échecs et de réussites, 
j'obtiens le script [list_nodes.py](list_nodes.py) qui me permet de lister [l'intégralité des nœuds](list_nodes.log), 
mais les nœuds intéressants sont les suivants :
```
    name='E1' ns=1;i=6011 classe=<NodeClass.Variable: 2>
    name='E2' ns=1;i=6012 classe=<NodeClass.Variable: 2>
    name='E3' ns=1;i=6013 classe=<NodeClass.Variable: 2>
    name='E4' ns=1;i=6014 classe=<NodeClass.Variable: 2>
    name='E5' ns=1;i=6015 classe=<NodeClass.Variable: 2>
    name='E6' ns=1;i=6016 classe=<NodeClass.Variable: 2>
    name='E7' ns=1;i=6017 classe=<NodeClass.Variable: 2>
    name='E8' ns=1;i=6018 classe=<NodeClass.Variable: 2>
    name='E9' ns=1;i=6019 classe=<NodeClass.Variable: 2>
    name='E10' ns=1;i=6020 classe=<NodeClass.Variable: 2>
    name='E11' ns=1;i=6021 classe=<NodeClass.Variable: 2>
    name='E12' ns=1;i=6022 classe=<NodeClass.Variable: 2>
    name='E13' ns=1;i=6023 classe=<NodeClass.Variable: 2>
    name='E14' ns=1;i=6024 classe=<NodeClass.Variable: 2>
    name='E15' ns=1;i=6025 classe=<NodeClass.Variable: 2>
    name='E16' ns=1;i=6026 classe=<NodeClass.Variable: 2>
    name='V1' ns=1;i=6031 classe=<NodeClass.Variable: 2>
    name='V2' ns=1;i=6032 classe=<NodeClass.Variable: 2>
    name='V3' ns=1;i=6033 classe=<NodeClass.Variable: 2>
    name='V4' ns=1;i=6034 classe=<NodeClass.Variable: 2>
    name='V5' ns=1;i=6035 classe=<NodeClass.Variable: 2>
    name='V6' ns=1;i=6036 classe=<NodeClass.Variable: 2>
    name='V7' ns=1;i=6037 classe=<NodeClass.Variable: 2>
    name='V8' ns=1;i=6038 classe=<NodeClass.Variable: 2>
    name='V9' ns=1;i=6039 classe=<NodeClass.Variable: 2>
    name='V10' ns=1;i=6040 classe=<NodeClass.Variable: 2>
    name='V11' ns=1;i=6041 classe=<NodeClass.Variable: 2>
    name='V12' ns=1;i=6042 classe=<NodeClass.Variable: 2>
    name='V13' ns=1;i=6043 classe=<NodeClass.Variable: 2>
    name='V14' ns=1;i=6044 classe=<NodeClass.Variable: 2>
    name='V15' ns=1;i=6045 classe=<NodeClass.Variable: 2>
    name='V16' ns=1;i=6046 classe=<NodeClass.Variable: 2>
    name='mix_element' ns=1;i=6050 classe=<NodeClass.Variable: 2>
    name='VMIX' ns=1;i=6051 classe=<NodeClass.Variable: 2>
    name='production_count' ns=1;i=6052 classe=<NodeClass.Variable: 2>
```
Les libellés collent parfaitement à la description faite dans l'énoncé. Excellente nouvelle, on commence alors
à lire les valeurs à plusieurs reprises ([read_values.py](read_values.py)) et ça nous permet de comprendre :
- les valeurs `E*X*` sont la quantité des éléments dans chacune des citernes. Elles baissent quand la vanne correspondante est ouverte.
- les valeurs `V*X*` indiquent la position des vannes sous les citernes (`True` pour ouverte). 
- `mix_element` semble être la quantité d'éléments dans le mélangeur.
- `production_count` semble être le nombre de fois où le mélange a été réalisé.

Sachant cela, il suffit de lire les valeurs à intervalle régulier pour déterminer ce qui se passe. J'ai commencé à écrire [un solver](solve.py),
mais comme vous pouvez le constater, il n'est pas fini. Il est écrit à la
truelle et ne fonctionne pas bien, mais il m'a permis de repérer l'ordre
d'ouverture des vannes, et la quantité d'éléments dans les citernes 
avant et après un mélange. Suffisant pour valider, donc on fera
plus propre plus tard (*ce qui signifie probablement : jamais*).

[Un log de fonctionnement du solver](solve.log) me permet de déterminer
l'ordre d'ouverture des vannes au cours du temps :
- 0, 2
- 6, 10
- 1, 12
- 5, 14
- 7, 11
- 3, 15
- 8, 9
- 4, 13

Le même me donne la quantité d'éléments dans les citernes :
- avant : [99998570, 99712570, 99724010, 99792650, 99671100, 99798370, 99919920, 99714000, 99738310, 99748320, 99958530, 99669670, 99659660, 99795510, 99715430, 99801230]
- après : [99998569, 99712369, 99723817, 99792505, 99670870, 99798229, 99919864, 99713800, 99738127, 99748144, 99958501, 99669439, 99659422, 99795367, 99715231, 99801091]

J'ai quand même écris un [bidule](format.py) pour formater le flag à 
partir de ces valeurs sans me prendre la tête. Et le flag obtenu est : 
`FCSC{010301c1070b381d020dc9ee060f8dc7080cc8e70410918b090ab7b0050ee68f}`