#!/usr/bin/env python3

import asyncio
from asyncua.sync import Client

async def readNode(client, name, id):
    node = client.get_node(f'ns=1;i={id}')
    value = node.get_value()
    print(f'{name}: {value=}')


async def main():
    with Client(url='opc.tcp://evil-plant.france-cybersecurity-challenge.fr:4841') as client:
        for id in range(1, 17):
            await readNode(client, f'E{id}', id+6010)
        for id in range(1, 16):
            await readNode(client, f'V{id}', id+6031)
        await readNode(client, f'VMIX', 6051)
        await readNode(client, f'mix_element', 6050)
        await readNode(client, f'production_count', 6052)
        

if __name__ == "__main__":
    asyncio.run(main())