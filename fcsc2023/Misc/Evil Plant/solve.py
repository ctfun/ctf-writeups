#!/usr/bin/env python3

import asyncio
from asyncua.sync import Client

def readNode(client, id):
    node = client.get_node(f'ns=1;i={id}')
    value = node.get_value()
    # print(f'DEBUG: {id=}, {value=}')
    return value

def readVMIX(client):
    return readNode(client, 6051)

def readVanne(client, i):
    return readNode(client, 6031+i)

def readCiterne(client, i):
    return readNode(client, 6011+i)

def readCiternes(client):
    return [ readCiterne(client, i) for i in range(16) ]

def readVannes(client):
    return [ readVanne(client, i) for i in range(15) ]    # FIXME On n'a pas la V17 ? :/

def which_one(l):
    result = list()
    for i, b in enumerate(l):
        if b:
            result.append(i)
    return result

async def main():
    with Client(url='opc.tcp://evil-plant.france-cybersecurity-challenge.fr:4841') as client:
        # On attend la fin d'un cycle, VMIX se ferme
        vmix_is_open = readVMIX(client)
        print(f'Waiting for VMIX to close')
        while vmix_is_open:
            vmix_is_open = readVMIX(client)
        
        # On lit alors les citernes
        citernes = readCiternes(client)
        print(f'VMIX closed ! tanks={citernes}')

        # while not vmix_is_open:
        while True:
            # On attend qu'une vanne s'ouvre
            print(f'Waiting for a valve to open')
            vannes = readVannes(client)
            while not any(vannes):
                vannes = readVannes(client)

            opened_vannes = which_one(vannes)
            print(f'Valves {opened_vannes} opened !')
            citernes = readCiternes(client)
            print(f'Tanks: {citernes}')

            # vmix_is_open = readVMIX(client)

        citernes2 = readCiternes(client)
        print(f'VMIX closed again ! tanks={citernes2}')
        diff = [ a-b for a, b in zip(citernes, citernes2)]
        print(f'Level difference in tanks : {diff}')

if __name__ == "__main__":
    asyncio.run(main())