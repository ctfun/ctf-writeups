#!/usr/bin/env python3

import asyncio
from asyncua.sync import Client

async def discoverNode(client, node, indent):
    children = node.get_children_descriptions()
    for c in children:
        id = c.NodeId.Identifier
        namespace = c.BrowseName.NamespaceIndex
        name = c.BrowseName.Name
        classe = c.NodeClass
        print(f'{"  "*indent}{name=} ns={namespace};i={id} {classe=}')
        n = client.get_node(f'i={id}')
        await discoverNode(client, n, indent+2)


async def main():
    with Client(url='opc.tcp://evil-plant.france-cybersecurity-challenge.fr:4841') as client:
        # Do something with client
        node = client.get_root_node()
        await discoverNode(client, node, 0)

if __name__ == "__main__":
    asyncio.run(main())