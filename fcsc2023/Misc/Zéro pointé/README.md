# Zéro pointé

## Énoncé

Vous devez trouver une manière d'afficher le flag, mais attention, j'ai pris toutes les précautions pour que vous n'y arriviez jamais !

`nc challenges.france-cybersecurity-challenge.fr 2050`

- [zero-pointe](zero-pointe)
- [zero-pointe.c](zero-pointe.c)

## Solution

La fonction `flag()` est appelée sur la survenue du signal `SIGFPE`, c'est à dire dans le cas d'une *Floating-point exception*.

```C
    if (signal(SIGFPE, flag) == SIG_ERR) {
        perror("signal");
        exit(EXIT_FAILURE);
    }
```

À priori, ça touche la division par zéro, qui n'est pas possible ici car le cas est testé. Mais pas seulement : dans `man signal`: 

> *(Also dividing the most negative integer by -1 may generate SIGFPE.)*

Comme je ne connais pas ces valeurs par cœur, je cherche dans `/usr/include/newlib/limits.h` :
```C
#   define LONG_MIN (-LONG_MAX-1)
[...]
#     define __LONG_MAX__ 9223372036854775807L
```

On en déduit que `LONG_MIN` vaut `-9223372036854775808`. Demandons la
division pour voir ce que ça donne :

```console
$ nc challenges.france-cybersecurity-challenge.fr 2050
-9223372036854775808
-1
FCSC{0366ff5c59934da7301c0fc6cf7d617c99ad6f758831b1dc70378e59d1e060bf}
```