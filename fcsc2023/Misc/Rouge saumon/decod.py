#!/usr/bin/env python3

from reedsolo import RSCodec # pip install reedsolo

input = open('sat_data.bin', 'rb')

chunks = dict()

rsc= RSCodec(32)
packet = b'plop'
while packet != b'':
    packet = input.read(255)
    payload, _, _ = rsc.decode(packet)
    header = payload[0:4]
    if header == b'':
        break
    idx = int.from_bytes(payload[4:8], byteorder='little')
    length = int.from_bytes(payload[8:12], byteorder='little')-12
    data = payload[12:]
    print(f'{header=}, {idx=}, {length=}')
    chunks[idx] = data

with open('output.bin', 'wb') as output:
    for k in sorted(chunks.keys()):
        output.write(chunks[k])