# Rouge saumon

## Énoncé

Vous avez intercepté une communication envoyée par un satellite depuis le lointain espace, et avez décodé son flux binaire. Curieux, vous décidez d'investiguer le contenu de cette communication. Vous savez que là haut, les rayons cosmiques peuvent perturber le signal : vous vous attendez donc à ce que le protocole de communication soit un minimum robuste et résilient au bruit et à la désynchronisation des trames !

- [sat_data.bin](sat_data.bin)

## Solution

Tellement simple quand on sait ce qu'il faut faire... Mais n'anticipons pas.

On repère une structure dans les données : la chaîne `HDR\x00` se répète à intervalle régulier. 
Elle est suivie d'un champ de 4 octets, de valeur faible. Puis d'un autre, qui vaut tout le temps
`0x000000FF`. On peut rapidement faire l'hypothèse que le premier pourrait être un compteur de 
paquets, et l'autre une taille. Pour ce dernier, ça se confirme en regardant au bout de combien 
de temps le `HDR\x00` apparaît.

![](struct.png)

On peut aussi remarquer, au moins pour certains paquets, que le début est
ASCII, mais la fin en binaire. En rapport avec l'énoncé, on peut imaginer
que la communication inclut un code de détection et de correction d'erreur 
(ECC), et probablement costaud vu les conditions de transmission.

C'est le moment où j'ai passé pas mal de temps à chercher de quel ECC il
pouvait s'agir. En lisant simplement wikipédia, j'arrive à la conclusion
qu'il s'agit d'un ECC de la famille des [Linear Code](https://en.wikipedia.org/wiki/Linear_code), 
car le signal est découpé en blocs avant d'être transmis. Mais en dehors de ça, ça reste obscur. Jusqu'à ce que j'arrive sur la page des  [Block code](https://en.wikipedia.org/wiki/Block_code), qui indique que l'un d'
entre eux s'appelle [Reed–Solomon](https://en.wikipedia.org/wiki/Reed%E2%80%93Solomon_error_correction). *Reed-Solomon* ? *Saumon Rouge* ? C'est tiré par les cheveux, mais ce serait une sacré coïncidence ! :D

Persuadé d'avoir identifié l'ECC, je pars à la recherche d'implémentation
existante en python (on se refait pas) et je découvre [reddsolo](https://pypi.org/project/reedsolo/), qui correspond exactement à ce que je cherche.

Reste à trouver les bons paramètres. Au début, j'ai essaye d'estimer la
taille du code word à 211 caractères, suivi de 32 caractères d'ECC, mais
ça ne fonctionnait pas. Jusqu'à ce que je lise : [*The typical RSCoder is RSCoder(255, 223)*](https://github.com/brownan/Reed-Solomon#documentation). Et 
évidemment, il faut les headers soient inclus dans l'ECC, car ils 
sont eux aussi sujet aux erreurs ! Et on est pile poil dans les bonnes
hyopothèses, avec des paquets de 255 octets, contenant 223 octets utiles
suivis de 32 d'ECC.

J'écris [un script python](decod.py) qui va lire les différents paquets, 
appliquer le code correcteur, puis les réordonner en fonction du champ
que j'avais identifié comme le compteur. 

```python
#!/usr/bin/env python3

from reedsolo import RSCodec # pip install reedsolo

input = open('sat_data.bin', 'rb')

chunks = dict()

rsc= RSCodec(32)
packet = b'plop'
while packet != b'':
    packet = input.read(255)
    payload, _, _ = rsc.decode(packet)
    header = payload[0:4]
    if header == b'':
        break
    idx = int.from_bytes(payload[4:8], byteorder='little')
    length = int.from_bytes(payload[8:12], byteorder='little')-12
    data = payload[12:]
    print(f'{header=}, {idx=}, {length=}')
    chunks[idx] = data

with open('output.bin', 'wb') as output:
    for k in sorted(chunks.keys()):
        output.write(chunks[k])
```

Bonne nouvelle, plus d'erreur dans le processus de correction d'erreur.
J'obtiens un fichier qui démarre par une chaîne évoquant un fichier BMP, 
un *Lorem Ipsum*, puis du binaire :

![](output.png)

Allez, un coup de [binwalk](https://github.com/ReFirmLabs/binwalk) là-dedans pour extraire le BMP en question :

![](flag.jpg)

`FCSC{8a65bcd7ad3f1dcec4f22106b636f86ec98a4e14fdb9b5b8de87fafdab11386f}`
