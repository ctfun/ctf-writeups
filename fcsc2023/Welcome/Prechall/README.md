# Préchall FCSC 2023

## Énoncé

Dans le code source de la page d'accueil :

```html
<!-- En attendant l'ouverture, un flag est à trouver sur ce site. Voir sur /teasing 🔥 -->
```

Comme l'an dernier, donc... On se rend sur la page en question :

> Pour vous faire patienter jusqu'au 21 avril 2023 à 14h pour le lancement du FCSC 2023, nous vous proposons cette année encore une épreuve de teasing ! Le point de départ est l'image donnée ci-dessous et vous permettra d'aboutir à un flag au format FCSC{xxx}. La résolution de ce challenge apportera 1 point symbolique pour le FCSC 2023, et sera matérialisée sur le site par un 🔥 à côté des noms d'utilisateur.

![](teaser.png)


## Solution

### Étape 1

Il faut commencer par résoudre ce puzzle. Je me suis inspiré d'un [projet codepen](https://codepen.io/ycw/pen/pKdoww) pour me faire
[une simple page HTML](https://github.com/lordOric/steg-tools/blob/main/puzzle.html) qui permet de faire cela, et d'exporter le résultat. En quelques minutes, on obtient :

![](solved-1.png)

Vu que ça évoque du LSB, [on vérifie](https://github.com/lordOric/steg-tools/blob/main/lsb_detect.py) et effectivement, il y a quelque chose sur le plan 0 :

![](solved-1_bit0.png)

[Un coup d'extraction](https://github.com/lordOric/steg-tools/blob/main/lsb_extract.py) (en retirant préalablement le canal alpha ajouté par mon outil de résolution de puzzle) et on passe à l'étape suivante.

```
$ remove_alpha.py solved-1.png - | lsb_extract.py - 0 step2.png
```

### Étape 2

On a cette nouvelle image :

![](step2.png)

Ok, donc rebelote, on résout le puzzle, ce qui nous donne :

![](solved-2.png)

Et on refait un coup de LSB :

```
$ remove_alpha.py solved-2.png - | lsb_extract.py - 0 step3.bin
```

### Étape 3

Ce coup-ci, on a [un binaire](step3.bin) !

```
$ file step3.bin 
step3.bin: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=8633214f4900c48d504eb171c5013837f19a7d85, for GNU/Linux 3.2.0, stripped
```

On ouvre dans `IDA`. Il n'y a pas 36 fonctions, essentiellement un `main` qui fait beaucoup de choses. On remarque notamment cette partie :

![](ida.png)

Deux variables sont incrémentées ou décrémentées en fonction des valeurs `L`, `R`, `U`, `D`... Ça sent le labyrinthe ! Et en effet, en analysant plus précisément, le binaire attend en entrée une chaîne de 188 caractères, qui représente le parcours entre le coin en haut à gauche et celui en bas à droite. Si ça fonctionne, le flag est généré comme le `sha256` du parcours réalisé.

Il nous manque juste le labyrinthe : il ne semble pas présent de façon évidente dans le binaire, donc on va le sortir en dynamique avec `gdb`. Au moment où l'analyse du parcours démarre, on trouve en mémoire cette structure :

```
  ############################################################# 
#   # # #   #   #   # #           #   #     # #   #           # 
# ### # # ##### ### # ### # ### # # # # # ### ### # # ### ##### 
#   # #   #         # # # # #   #   # # # #   #   # # # # #   # 
# ### ### # ####### # # # # ### # ### ### ### # # ### # ##### # 
# #   #     # # # #   #   #   # #   #       #   # #     #     # 
# # ####### # # # ### ##### ### ##### ######### ### ##### ### # 
#               # #   #   #   # #     #     #   # #   #   # # # 
# ####### ####### # # ### ### ########### ##### # # ##### # ### 
#   #       #     # #   #   #   # #           #     # #   #   # 
### ######### ##### ##### ### ### # ####### ### ### # # ### ### 
#   #         #     #   #             #   # #   # # #     #   # 
### # # ######### ##### ### # # # ### ### ### # # # ##### # # # 
#   # # #   #     #     # # # # # # #     #   # #   # #   # # # 
### # ### ### ### # # # # # ####### # # ########### # ### # ### 
#         #     # # # # # #   #     # #   #       # # #   # # # 
# # # ####### ##### ##### ### ##### # ########### # # ### # # # 
# # #     # # # #           # #   #               #           # 
# ####### # ### ### ### # ### # ########### # ##### ####### # # 
# # #   #           #   #         #       # #   # #   # # # # # 
# # # ### # ##### ### # # ### ######### # ####### ##### # ### # 
# # #     # #     #   # # #   # #       # #                   # 
# # # # ### ### ### ### ##### # # ####### ########### ##### # # 
#   # #   # #     # # # #     #   # #     #   # #     # # # # # 
# ####### ####### ### # ####### # # # ####### # # # ### # # # # 
#       #     #   #   # #   #   # #   #   #       # #     # # # 
# # # ####### # # ### ### # ##### # ### ### ### ####### ####### 
# # #     # # # # # #     # #     # #     #   # #   #   #     # 
### # ##### # ### # ####### # # ######### # ##### ### # ### ### 
# # #       # #     #   # #   #             #   #     #   #   # 
# ##### ####### ##### ### ### ######### ### # ### ### ### # ### 
# #         #   # #           #         # # #   # #   #       # 
# # ### ### ### # ### # # # ### ######### # # # # # # ####### # 
#     # #     #       # # # #   #           # #   # #   #     # 
# ### ########### ####### ### # ##### ##### # ### ########### # 
#   # # #   # # #   # # # #   # # #     #     # # # #     #   # 
### ### # ### # ##### # ######### ######### # # ### ##### # # # 
#   # #   #       #     #     #         # # #   #   # # #   # # 
##### # # ### # # ### ### # # # # # ##### # # # ### # # ### ### 
# # #   # #   # #   #   # # #   # # #       # # # # #     #   # 
# # # ### ### # # ### ##### ##### ######### ##### # ##### # # # 
#       # #   # # #         #   #     #         #           # # 
### # ### # ####### # ##### # ############# ##### ######### # # 
#   #   #   #   #   # # #     #       # #   #   #       #   # # 
### ### # ### ### ### # ##### ####### # ####### # ######### ### 
#     # #           #   # #   #     #     # #         #       # 
# # ##### # ### ### ### # # ### ##### # ### ### ### ######### # 
# # #   # # #   #   # #   # #     #   # #     # #     #       # 
# ##### ### ####### # # ### ##### ##### ##### ##### ##### ##### 
#     #   # #   #     #   #     #   #       #     # #       # # 
# ####### ##### ### ####### # ##### # # ##### ### ####### ### # 
#       #   #   #     #     # #   #   # #       # # #     # # # 
# ######### ### # ####### ##### ### ##### ### ### # # ##### # # 
# #     #   #   # #       # #   #     # # #     #           # # 
### ### # # # # # # ####### # # # # # # # ##### ##### ### ### # 
#   #   # #   #   #   #   #   #   # #   #     #   #     #     # 
# ####### ### # ### # # # ### ##### ### # # # # ############# # 
#   #     # # # # # #   #     # # # #     # # #         # #   # 
# # ### ### ### # ### ##### # # # # ### # ### ### ### # # # ### 
# #       # #   #   #   #   # # # # #   #   #   # #   #   # # # 
### # ##### # ##### # ### ##### # # # ########### ##### ### # # 
#   #       # #       #         #   # #             #     #     
##############################################################  
```

On se bricole [un script python](solve.py) qui résout le labyrinthe, et on soumet le résultat au binaire :

```console
$ python3 solve.py 
RDDDDDDDDDRRDDDDDDRRDDRRRRDDRRRRRRRRDDLLDDRRDDDDDDLLDDDDRRRRRRUURRRRRRRRUUUURRDDRRRRRRRRRRRRDDDDDDRRUUUURRDDRRUUUURRRRUURRDDRRDDRRRRDDDDLLDDDDDDDDDDRRDDLLLLDDDDLLLLDDRRRRDDRRRRDDLLDDDDRRRD
$ ./step3.bin 
RDDDDDDDDDRRDDDDDDRRDDRRRRDDRRRRRRRRDDLLDDRRDDDDDDLLDDDDRRRRRRUURRRRRRRRUUUURRDDRRRRRRRRRRRRDDDDDDRRUUUURRDDRRUUUURRRRUURRDDRRDDRRRRDDDDLLDDDDDDDDDDRRDDLLLLDDDDLLLLDDRRRRDDRRRRDDLLDDDDRRRD
Congrats!! You can use the flag given by this command to validate the challenge:
echo -n RDDDDDDDDDRRDDDDDDRRDDRRRRDDRRRRRRRRDDLLDDRRDDDDDDLLDDDDRRRRRRUURRRRRRRRUUUURRDDRRRRRRRRRRRRDDDDDDRRUUUURRDDRRUUUURRRRUURRDDRRDDRRRRDDDDLLDDDDDDDDDDRRDDLLLLDDDDLLLLDDRRRRDDRRRRDDLLDDDDRRRD | sha256sum | awk '{ print "FCSC{" $1 "}" }'
$ echo -n RDDDDDDDDDRRDDDDDDRRDDRRRRDDRRRRRRRRDDLLDDRRDDDDDDLLDDDDRRRRRRUURRRRRRRRUUUURRDDRRRRRRRRRRRRDDDDDDRRUUUURRDDRRUUUURRRRUURRDDRRDDRRRRDDDDLLDDDDDDDDDDRRDDLLLLDDDDLLLLDDRRRRDDRRRRDDLLDDDDRRRD | sha256sum | awk '{ print "FCSC{" $1 "}" }'
FCSC{5cf9940286533f76743984b95c8edede9dbfde6226de012b8fe84e15f2d35e83}
```

Fun !
