# Lapin blanc

## Énoncé

Saurez-vous retrouver la phrase de passe pour accéder au pays des merveilles d'Alice ?

La porte n'est pas très patiente, vous n'avez que 10 minutes pour l'ouvrir. Bonne chance !

`nc challenges.france-cybersecurity-challenge.fr 2350`

## Solution

On se connecte sur le service distant pour voir de quoi il retourne...

```
$ nc challenges.france-cybersecurity-challenge.fr 2350
[0000014080] Initializing Wonderland...
[0001326237] Searching for a tiny golden key...
[0001678436] Looking for a door...
[0001990638] Trying to unlock the door...

    __________________
    ||              ||
    ||   THE DOOR   ||
    ||              ||  .--------------------------.
    |)              ||  | What's the magic phrase? |
    ||              ||  /--------------------------'
    ||         ^_^  ||
    ||              ||
    |)              ||
    ||              ||
    ||              ||
____||____.----.____||_______________________________________

Answer: please
[0007397675] The door is thinking...
[0007400796] Your magic phrase is invalid, the door refuses to open.
Answer:
```

Ok, tentons un truc au hasard...

```
Answer: Please open
[0014355134] The door is thinking...
[0014358271] Your magic phrase is invalid, the door refuses to open.
```

Le compteur à gauche ressemble à un timestamp. Vu la catégorie, ça pourrait bien
être une attaque sur le temps de traitement, qui serait différent si la réponse, 
ou probablement ses caractères, sont corrects.

Quelques tests rapides montrent qu'effectivement, si on saisi la réponse `I`, le
traitement dure plus longtemps, c'est donc probablement la première lettre de la réponse.

On écrit [un script](solve.py) pour automatiser le tout, et il trouve est `I'm late, I'm late! For a very important date!` avant une coupure de connexion.

Il reste à valider ça :
```
$ nc challenges.france-cybersecurity-challenge.fr 2350
[0000014071] Initializing Wonderland...
[0001326222] Searching for a tiny golden key...
[0001678430] Looking for a door...
[0001990621] Trying to unlock the door...

    __________________
    ||              ||
    ||   THE DOOR   ||
    ||              ||  .--------------------------.
    |)              ||  | What's the magic phrase? |
    ||              ||  /--------------------------'
    ||         ^_^  ||
    ||              ||
    |)              ||
    ||              ||
    ||              ||
____||____.----.____||_______________________________________

Answer: I'm late, I'm late! For a very important date!
[0014763502] The door is thinking...
[0014951721] FCSC{t1m1Ng_1s_K3y_8u7_74K1nG_u00r_t1mE_is_NEce554rY}
```
