# canflag

## Énoncé

Lors d'une séance de tuning de votre voiture de compétition, la malette de votre ami garagiste a enregistré des trames dans `canflag.pcap`. On dirait que votre bolide veut vous parler...

- [canflag.pcap](canflag.pcap)

## Solution

La trace nous montre des paquets [CAN](https://fr.wikipedia.org/wiki/Protocole_CAN), d'où le jeu de mot dans le titre.

![](wireshark.png)

Pour être honnête, je n'y comprends pas grand chose. Ce que je remarque, c'est que les paquets
sont plutôt petits, et peu de champs varient d'un paquet à l'autre :

![](first.png)

Ce qui varie, c'est l'`ID`, le flag `Extended`, et la payload. À la lecture de quelques pages sur le sujet, 
je note que l'identifiant indique une priorité. Cela semble se confirmer car le paquet ci-dessus, avec
l'identifiant le plus bas (donc la priorité la plus haute) semble être le début d'un flag.

Un autre paquet, avec le même identifiant (2) mais sans le flag `Extended`, semble être la suite :

![](second.png)

Je décide alors de relever toutes les payloads dans les paquets, et de les ordonner en fonction de 
l'identifiant d'une part, puis en favorisant les paquets ayant le flag `Extended` :

|ID |  Extended        | Normal          
|---|------------------|------------------
| 2  |  FCSC            | {aa9e           
| 4  |  d               | *               
| 6  |  *               | f0a2ea5e233     
| 8  |  *               | *               
| 10 |  *               | bdc3fc49535a09  
| 12 |  cc055a          | 456df7db4       
| 14 |  ab              | 8e              
| 16 |  *               | *               
| 18 |  *               | *               
| 20 |  5               | *               
| 22 |  f6b47b9f        | 603d6a11        
| 24 |  71d20b15458b7   | b4              
| 26 |  *               | 4ac             
| 28 |  1               | ffbeb9ca92ac    
| 30 |  1d8a            | ee3cd919184a    
| 32 |  *               | *               
| 34 |  *               | 27d             
| 36 |  *               | 6afd            
| 38 |  2               | dd4f            
| 40 |  *               | 01c50b0         
| 42 |  *               | e57be6a1560     
| 44 |  a78e68255       | 6               
| 46 |  *               | *               
| 48 |  c               | 529f74d8b04f8   
| 50 |  e660ce71f6e210  | 13d1f248        
| 52 |  2f60c           | e1732e5983      
| 54 |  *               | d               
| 56 |  *               | bc14f37020b5a4  
| 58 |  *               | 39e60c6         
| 60 |  *               | 797295c3cc5875  
| 62 |  *               | 7bdeaa          
| 64 |  *               | *               
| 66 |  85              | *               
| 68 |  a96eef37}       | N/A             

Le résultat est le flag `FCSC{aa9edf0a2ea5e233bdc3fc49535a09cc055a456df7db4ab8e5f6b47b9f603d6a1171d20b15458b7b44ac1ffbeb9ca92ac1d8aee3cd919184a27d6afd2dd4f01c50b0e57be6a1560a78e682556c529f74d8b04f8e660ce71f6e21013d1f2482f60ce1732e5983dbc14f37020b5a439e60c6797295c3cc58757bdeaa85a96eef37}`. Mais je n'ai pas compris grand chose.
