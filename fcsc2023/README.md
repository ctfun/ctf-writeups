# FCSC 2023

![](rank.png)

**Welcome:**
- [Prechall FCSC 2023](Welcome/Prechall/README.md)
 
**Intro:**
- [La gazette de Windows](Intro/La%20gazette%20de%20Windows/README.md)
- [ROT13](Intro/ROT13/README.md)
- [T'es lent](Intro/T'es%20lent/README.md)

**Web:**
- [ENISA Flag Store 1/2](Web/ENISA%20Flag%20Store/README.md)
- [ENISA Flag Store 2/2](Web/ENISA%20Flag%20Store/README.md#enisa-flag-store-22)
- [Hello from the inside](Web/Hello%20from%20the%20inside/README.md)
- [Peculiar Caterpillar](Web/Peculiar%20Caterpillar/README.md)
- [Salty Authentication](Web/Salty%20Authentication/README.md)
- [Whiskers in the Dark](Web/Whiskers%20in%20the%20Dark/README.md)

**Misc:**
- [Zéro pointé](Misc/Z%C3%A9ro%20point%C3%A9/README.md)
- [Des p'tits trous](Misc/Des%20p'tits%20trous/README.md)
- [Rechatdrage](Misc/Rechatdrage/README.md)
- [Evil Plant](Misc/Evil%20Plant/README.md)
- [Rouge Saumon](Misc/Rouge%20saumon/README.md)

**Forensics:**
- [Weird Shell](Forensics/Weird%20Shell/README.md)
- [Ransomémoire 0/3](Forensics/Ransom%C3%A9moire/README.md)
- [Ransomémoire 1/3](Forensics/Ransom%C3%A9moire/README.md#ransomémoire-13-mon-précieux)
- [Ransomémoire 2/3](Forensics/Ransom%C3%A9moire/README.md#ransomémoire-23-début-dinvestigation)
- [C3-PO](Forensics/C-3PO/README.md)

**Pwn:**
- [robot](Pwn/robot/README.md)

**Hardware:**
- [canflag](Hardware/canflag/README.md)

**Side Channel and Fault Attacks:**
- [Lapin Blanc](Fault/Lapin%20blanc/README.md)