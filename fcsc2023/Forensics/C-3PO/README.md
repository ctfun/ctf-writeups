# C-3PO

**Description**

Pour votre première analyse, on vous confie le téléphone du PDG de GoodCorp. Ce dernier est certain que les précieuses photos stockées sur son téléphone sont récupérées par un acteur malveillant. Vous décidez de mettre en place une capture réseau sur le téléphone, afin de voir ce qu’il en est…

Cette épreuve est composée de trois parties :
- C-3PO.
- R2-D2.
- R5-D4.

Fichier: [capture.cap](https://hackropole.fr/filer/fcsc2022-forensics-android-c3po/public_filer/capture.cap)

**Solution**

> Résolue en 2024 grâce à hackropole.fr ;-)

Comme d'habitude sur une capture réseau, je jette un œil aux statistiques pour déterminer quel protocole 
est concerné :

![](network-stats.png)

Bizarre, beaucoup de TCP, mais pas beaucoup de HTTP ou de TLS, c'est curieux pour un Android. On regarde
alors les conversations :

![](network-conversation.png)

La majorité du traffic hors HTTP et HTTPS semble liée à l'IP `172.18.0.1` sur les ports non standards `1337` et `1338`.
On filtre pour ne garder que l'IP en question et on suit le premier échange TCP :

![](androidpayload.png)

Un meterpreter Android ? On est très clairement en face d'un truc pas net. Le reste du trafic sur le port `1337` n'est pas
lisible directement, donc probablement compressé ou chiffré. Voyons ce qui se passe le port `1338` :

![](b64.png)

Ça, c'est par contre lisible, et ça ressemble à du base64. On vérifie :

![](b64_decod.png)

C'est même un fichier PNG, non complet sur ce paquet. On est probablement sur un canal d'exfiltration des données contenues
dans le smartphone. C'est le moment de sortir [une moulinette](decod.py) pour extraire toutes ces payloads base64 pour déterminer ce
qui a fuité. Le résultat :

```
$ file result.png 
result.png: PNG image data, 1771 x 2657, 8-bit/color RGB, non-interlaced
```

Une version réduite :

![](result.jpg)

Le flag est `FCSC{2d47d546d4f919e2d50621829a8bd696d3cv1938}`.



