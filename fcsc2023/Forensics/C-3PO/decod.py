#!/usr/bin/env python3

import dpkt
from socket import inet_pton, AF_INET
from base64 import b64decode

target_ip = inet_pton(AF_INET, '172.18.0.1')

f = open('capture.cap','rb')
content = b''
for ts, pkt in dpkt.pcap.Reader(f):
    
    eth = dpkt.ethernet.Ethernet(pkt) 
    if eth.type != dpkt.ethernet.ETH_TYPE_IP:
       continue

    ip = eth.data
    if ip.p != dpkt.ip.IP_PROTO_TCP or ip.dst != target_ip:
        continue

    tcp = ip.data
    if tcp.dport != 1338:
        continue

    payload = tcp.data
    content += payload

open('result.png', 'wb').write(b64decode(content))
