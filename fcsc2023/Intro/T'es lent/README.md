# T'es lent

## Énoncé

Vous avez très envie de rejoindre l'organisation du FCSC 2024 et vos skills d'OSINT vous ont permis de trouver ce site en construction. Prouvez à l'équipe organisatrice que vous êtes un crack en trouvant un flag !

https://tes-lent.france-cybersecurity-challenge.fr/

## Solution

On se rend sur le site :

![](teslent.png)

On jette un œil au code source :
```html
      <!--
      <div class="col-md-6">
        <div class="h-100 p-5 text-white bg-dark rounded-3">
          <h2>Générateur de noms de challenges</h2>
          <p>Vous serez en charge de trouver le noms de toutes les épreuves du FCSC 2024.</p>
          <a class="btn btn-outline-light" href="/stage-generateur-de-nom-de-challenges.html">Plus d'infos</a>
        </div>
      </div>
      -->
```

On y va !

![](stage.png)

Rebelote dans le code source :

```html
    <div class="container py-4">
      <!--
      Ne pas oublier d'ajouter cette annonce sur l'interface d'administration secrète : /admin-zithothiu8Yeng8iumeil5oFaeReezae.html
      -->
    </div>
```

On suit toujours :

![](flag.png)

`FCSC{237dc3b2389f224f5f94ee2d6e44abbeff0cb88852562b97291d8e171c69b9e5} `