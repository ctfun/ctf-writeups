# QR Code

**Description**

QR Codes everywhere!

Fichier: [docker-compose.yml](https://hackropole.fr/challenges/fcsc2019-misc-qrcode/docker-compose.public.yml)

**Solution**

On se connecte au port 4000 du container lancé :
```
Programming challenge
---------------------
I will send you a PNG image compressed by zlib encoded in base64 that contains 64 encoded numbers.
The expected answer is the sum of all the numbers (in decimal).
You have 2 seconds.
Are you ready? [Y/N]
>> 
```

Facile, en python pour changer (😉) :
```python
#!/usr/bin/env python3

from telnetlib import Telnet
from base64 import b64decode
from PIL import Image
import io
import zlib
from pyzbar import pyzbar

host = 'localhost'
port = 4000

with Telnet(host, port) as tn:
    # Telnet read challenge
    tn.read_until(b'>> ')
    tn.write(b'Y\n')
    chall = tn.read_until(b'What is you answer?')
    chall = chall.split(b'\n')[0]

    # Decode input as an image
    zlib_data = b64decode(chall)
    image_data = zlib.decompress(zlib_data)
    image = Image.open(io.BytesIO(image_data))

    # Decode the embedded QR codes
    codes = pyzbar.decode(image)
    values = [ int(x.data) for x in codes ]
    answer = sum(values)

    # Send the answer & get the flag
    tn.write(f'{answer}\n'.encode('utf-8'))
    flag = tn.read_all()
    print(flag)
```

Et ainsi:
```
❯ python solve.py 
b'\n>> Congrats! Here is your flag: ECSC{e076963c132ec49bce13d47ea864324326d4cefa}\n'
```