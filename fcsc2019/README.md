# FCSC 2019

Pas de classement, car je ne connaissais pas le FCSC à l'époque. Je valide les challenges en 2024 grâce à [Hackropole](https://hackropole.fr/).

## Intro

- [Crayon Cochon](intro/README.md#crayon-cochon)
- [Not so FAT](intro/README.md#not-so-fat)
- [Petites Notes](intro/README.md#petites-notes)
- [Scully 1](intro/README.md#scully-1)
- [StegCryptoDIY - PNG](intro/README.md#stegcryptodiy-png)
- [Vault](intro/README.md#vault)
- [ybab](intro/README.md#ybab)


## Forensic

- [103_spx](forensic/103_spx/README.md)
- [3615 Incident (3 épreuves)](forensic/3615%20Incident/README.md)
- [Exfiltration](forensic/Exfiltration/README.md)
- [Take It Down](forensic/TakeItDown/README.md)


## Crypto

- [2tp](crypto/2tp/README.md)
- [Weak RSA](crypto/weak_rsa/README.md)

## Pwn

- [Aarchibald](pwn/aarchibald/README.md)

## Misc
- [QR Code](misc/QR%20Code/README.md)
