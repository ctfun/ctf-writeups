# 103_spx

**Description**

Super ! J’ai retrouvé ma toute première clé USB. Je l’avais gardée pour savoir si un jour je pourrais récupérer mes souvenirs effacés. Pouvez-vous m’aider ?

Fichier: [103_spx.zip](103_spx.zip)

**Solution**

On jette un œil à l'archive donnée:
```
❯ unzip 103_spx.zip 
Archive:  103_spx.zip
  inflating: USB_a_analyser  

❯ file USB_a_analyser 
USB_a_analyser: DOS/MBR boot sector, code offset 0x52+2, OEM-ID "NTFS    ", sectors/cluster 8, Media descriptor 0xf8, sectors/track 62, heads 8, dos < 4.0 BootSector (0x80), FAT (1Y bit by descriptor); NTFS, sectors/track 62, sectors 507903, $MFT start cluster 4, $MFTMirror start cluster 31743, bytes/RecordSegment 2^(-1*246), clusters/index block 1, serial number 06d84ef355f47cf91
```

Petit système de fichier NTFS, on sort immédiatement [The Sleuth Kit](https://sleuthkit.org) pour l'inspecter :

```
❯ fls  USB_a_analyser 
r/r 4-128-1:	$AttrDef
r/r 8-128-2:	$BadClus
r/r 8-128-1:	$BadClus:$Bad
r/r 6-128-1:	$Bitmap
r/r 7-128-1:	$Boot
d/d 11-144-2:	$Extend
r/r 2-128-1:	$LogFile
r/r 0-128-1:	$MFT
r/r 1-128-1:	$MFTMirr
r/r 9-128-2:	$Secure:$SDS
r/r 9-144-3:	$Secure:$SDH
r/r 9-144-4:	$Secure:$SII
r/r 10-128-1:	$UpCase
r/r 10-128-2:	$UpCase:$Info
r/r 3-128-3:	$Volume
d/d 132-144-2:	.Trash-1000
r/r 64-128-2:	message.txt
r/r 102-128-2:	Peugeot 103 SPX : tous les modèles de 1987 à 2003 | Actualités de la mobylette par Mobylette Mag.html
d/d 67-144-2:	Peugeot 103 SPX : tous les modèles de 1987 à 2003 | Actualités de la mobylette par Mobylette Mag_files
r/r 65-128-2:	Peugeot103SPXFILI.jpg
r/- * 0:	secret.xz
r/r * 66-128-2(realloc):	secret.xz
V/V 154:	$OrphanFiles
```

Commençons pas lire le fichier `message.txt` (entrée 64) :

```
❯ icat USB_a_analyser 64
Si un jour je relis ce message, le mot de passe utilisé pour chiffrer mon plus grand secret était "vgrohhfyek0wkfi5fv13anexapy3sso6" et j'avais utilisé openssl.
En revanche, j'ai effacé par erreur le fichier contenant mon plus grand secret (voir s'il existe des techniques de la mort pour le retrouver mon fichier secret.xz sha256(0fb08681c2f8db4d3c127c4c721018416cc9f9b369d5f5f9cf420b89ee5dfe4e) de 136 octets) et de toute façon, impossible de me rappeler de l'algo utilisé -_- (donc si je le retrouve... il faudra aussi retrouver l'algo pour utiliser ce mot de passe).
```

Le fichier secret.xz semble avoir été effacé. Tentons de le récupérer :
```
❯ icat USB_a_analyser 66 > secret.xz
❯ sha256sum secret.xz 
0fb08681c2f8db4d3c127c4c721018416cc9f9b369d5f5f9cf420b89ee5dfe4e  secret.xz
```

Nickel, maintenant décompressons-le pour examiner son contenu :

```
❯ xz -d secret.xz 
❯ cat secret 
Salted__��k��.�@���>�#��z���
�S�n���!��O�ND�5����k�b=��N-�㖿�,��s�٪
```

C'est conforme à ce qu'on attendait : de l'openssl. On a le mot de passe utilisé, il ne manque que l'algo. On essaie de les énumérer un par un, et c'est `aes-192-ecb` qui parvient à déchiffrer :
```
❯ openssl aes-192-ecb -d -pass pass:vgrohhfyek0wkfi5fv13anexapy3sso6 < secret 
*** WARNING : deprecated key derivation used.
Using -iter or -pbkdf2 would be better.
flag : lh_6c31ba64e522b5f9326b7bee0abef6547f60d214
```

