# Take It Down

**Énoncé**

Un botnet vient d’être démantelé ! Les communications avec le C2 sont détournées avec un Sinkhole depuis lequel des captures réseau sont faites. Malheureusement, l’analyste en charge de cette investigation n’a pas réussi à décoder ces communications… Il a tout de même mis en place un honeypot et a réussi à tromper ce botnet qui s’attaque principalement à des Raspberry PI exposés sur Internet avec le service SSH activé et mot de passe par défaut.

Votre mission, retrouver et décoder les données contenues dans la capture réseau pour identifier la victime à l’aide du script client `client.py`.

Fichiers:

- [client.py](https://hackropole.fr/challenges/fcsc2019-forensics-take-it-down/public/client.py)
- [sinkhole_capture.pcap](https://hackropole.fr/challenges/fcsc2019-forensics-take-it-down/public/sinkhole_capture.pcap)


**Solution**

On commence par regarder `client.py`. Il est obfusqué, mais on remarque l'utilisation de [scapy](https://scapy.net/) pour envoyer des choses.
Plutôt que de le désobfusquer statiquement, je décide de [le modifier](client_trace.py) pour ajouter des traces et l'examiner dynamiquement :
- j'ajoute des `print` pour chaque entrée et sortie de fonction.
- j'élimine les appels à scapy :
  - je génère un couple `(seq, ack)` dans `send_it()` là où ils sont déduits des paquets `SYN` et `ACK` échangés avec la cible.
  - je trace la payload importante dans la fausse requête HTTP envoyée dans `req()` : il s'agit du contenu du header `Content-MD5`.
- je modifie `noraj()` pour qu'il liste les fichiers du répertoire courant plutôt que ceux de `/home/`.

Sur un exemple, cela donne [cette trace](client_trace.log), dans laquelle on comprend:
- `noraj()` liste récursivement les fichiers présents dans `/home`.
- `obnco()` envoie la liste des fichiers (séparés par un retour chariot).
- `wnytyx()` envoie le contenu de chacun des fichiers.

La méthode d'envoi (commune à la liste des fichiers comme à leur contenu) est la suivante :
- `ej6h13cyvp()` découpe ce qui est à envoyer par paquets de 32 octets.
- `send_it()` initialise une session TCP.
- `req()` envoie chacun des blocs de 32 octets.
- ce dernier semble préalablement chiffrer le bloc en appelant `dfqguvb8qz4cas9()` avec en paramètre le numéro d'acquittement de la session TCP ouverte et le contenu du bloc.

On a une bonne idée de ce qui se passe, mais avant de pouvoir décoder le trafic fourni par la capture, il va falloir comprendre le chiffrement.
En examinant la fonction `dfqguvb8qz4cas9()`, il semble que la première partie crée une table de transposition à partir de la clef de chiffrement (premier paramètre, donc le numéro d'acquittement du paquet TCP) :

```python
def dfqguvb8qz4cas9(k, d):
    eb3o0v9ie8 = 0o400
    dpw8ko0y3gi7h = []
    uajxoyu = list(range(eb3o0v9ie8))
    ozi4iqwms99xv = bdpw8ko0y3 = r589862eilxu = 0o0
    for pls in list(range(eb3o0v9ie8)):
        ozi4iqwms99xv = (
            ozi4iqwms99xv + uajxoyu[pls] + ord(k[pls % len(k)])) % eb3o0v9ie8
        uajxoyu[pls], uajxoyu[ozi4iqwms99xv] = uajxoyu[ozi4iqwms99xv], uajxoyu[pls]
```

Puis, cette table est utilisée de façon un peu obscure pour XORer chaque octet du bloc à chiffrer :

```python
    ozi4iqwms99xv = lq4vbnj2t = 0o0
    for xl4xr9hh in d:
        ozi4iqwms99xv = (ozi4iqwms99xv + 0o1) % eb3o0v9ie8
        lq4vbnj2t = (lq4vbnj2t + uajxoyu[ozi4iqwms99xv]) % eb3o0v9ie8
        uajxoyu[ozi4iqwms99xv], uajxoyu[lq4vbnj2t] = uajxoyu[lq4vbnj2t], uajxoyu[ozi4iqwms99xv]
        dpw8ko0y3gi7h.append(chr(ord(xl4xr9hh) ^ uajxoyu[(
            uajxoyu[ozi4iqwms99xv] + uajxoyu[lq4vbnj2t]) % eb3o0v9ie8]))
    return ''.join(dpw8ko0y3gi7h).encode().hex()
```

À ce stade, il semble qu'on puisse
rappeler cette fonction tel quelle, avec en paramètre le numéro d'acquittement et la payload chiffrée, pour obtenir le clair.
Attention, le chiffré est plus long que le clair, parce qu'au moment de l'encodage, on a des octets supérieurs à 127 qui sont encodés en utf-8.
Mais ça fonctionne. Exemple :

```python
>>> encrypted = dfqguvb8qz4cas9('12345', '2f205443502873706f727')
>>> encrypted
'62530a52192a385cc2ae13c3a6c288c3af55c3a526c38f000e0bc28a'
>>> bytes.fromhex(dfqguvb8qz4cas9('12345', bytes.fromhex(encrypted).decode()))
b'2f205443502873706f727'
```

On notera aussi que chaque contenu de fichier (ou de la liste des fichiers) possède un `id` différent dans la query de la requête HTTP. Suivre la valeur
de cet `id` permet de détecter le passage d'un fichier à l'autre.

Plus qu'à scripter le parsing et le déchiffrement de 
la capture fournie ! C'est fait dans le script [decode.py](decode.py). Lors de son lancement, il extrait tous les fichiers dans le répertoire courant (sans reconstruire 
l'arborescence d'origine). Voici sa sortie:

```
❯ python3 decode.py
Extracting .bash_history
Extracting enisa.png
Extracting pop.ret
Extracting info_perso.zip
Extracting lol.txt
Extracting 9BBCB225C4A7C14BE24906115F84DFF2FD81ACA1.rev
Extracting id_rsa.pub
Missing data in capture, stopping
```

On passe en revue les différents fichiers, et c'est `info_perso.zip` qui contient ce qui nous intéresse :
```
❯ unzip info_perso.zip 
Archive:  info_perso.zip
  inflating: info_perso.txt
❯ cat info_perso.txt 
Vous venez de retrouver des informations sur la victime !
Flag :
lh_3cd2d5296cc2bea8dbaf47ca23f82e1d5ca9b01668426ad07c653877e31d718c
```






