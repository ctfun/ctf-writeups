#!/usr/bin/env python3

import dpkt
from urllib.parse import urlparse, parse_qs
from socket import inet_pton, AF_INET
from base64 import b64decode

target_ip = inet_pton(AF_INET, '192.168.1.30')

# From client.py
def dfqguvb8qz4cas9(k, d):
    eb3o0v9ie8 = 0o400
    dpw8ko0y3gi7h = []
    uajxoyu = list(range(eb3o0v9ie8))
    ozi4iqwms99xv = bdpw8ko0y3 = r589862eilxu = 0o0
    for pls in list(range(eb3o0v9ie8)):
        ozi4iqwms99xv = (
            ozi4iqwms99xv + uajxoyu[pls] + ord(k[pls % len(k)])) % eb3o0v9ie8
        uajxoyu[pls], uajxoyu[ozi4iqwms99xv] = uajxoyu[ozi4iqwms99xv], uajxoyu[pls]
    ozi4iqwms99xv = lq4vbnj2t = 0o0
    for xl4xr9hh in d:
        ozi4iqwms99xv = (ozi4iqwms99xv + 0o1) % eb3o0v9ie8
        lq4vbnj2t = (lq4vbnj2t + uajxoyu[ozi4iqwms99xv]) % eb3o0v9ie8
        uajxoyu[ozi4iqwms99xv], uajxoyu[lq4vbnj2t] = uajxoyu[lq4vbnj2t], uajxoyu[ozi4iqwms99xv]
        dpw8ko0y3gi7h.append(chr(ord(xl4xr9hh) ^ uajxoyu[(uajxoyu[ozi4iqwms99xv] + uajxoyu[lq4vbnj2t]) % eb3o0v9ie8]))
    result = ''.join(dpw8ko0y3gi7h).encode().hex()
    return result


# Try to decode a TCP packet, returns the id in the query and the decrypted Content-MD5 header
# or None if the packet is not an HTTP GET /panel.php
def decode_tcp(pkt):
    # Only Ethernet
    eth = dpkt.ethernet.Ethernet(pkt) 
    if eth.type != dpkt.ethernet.ETH_TYPE_IP:
        return

    # Only TCP towards target
    ip = eth.data
    if ip.p != dpkt.ip.IP_PROTO_TCP or ip.dst != target_ip:
        return

    # Only HTTP that GET /panel.php
    tcp = ip.data
    payload = tcp.data
    if b'panel.php' not in payload:
        return

    # Retrieve the id query parameter, the Content-MD5 header and the ACK number
    request = dpkt.http.Request(tcp.data)
    id = parse_qs(urlparse(request.uri).query)['id'][0]
    encrypted = request.headers['content-md5']
    ack = tcp.ack

    # Decrypt the payload & return
    encrypted = b64decode(encrypted).decode()
    plain = bytes.fromhex(dfqguvb8qz4cas9(str(ack), bytes.fromhex(encrypted).decode()))
    plain = bytes.fromhex(plain.decode())
    return (id, plain)


# Iters on a list of packets, searching for a complete transmission, and return its content
def transmission_generator(reader):
    current_id = None
    content = b''
    
    for _, pkt in reader:
        decoded = decode_tcp(pkt)
        if not decoded:
            continue
        id, frag = decoded

        # This is the first transmission
        if current_id is None:
            current_id = id

        # This is a new transmission, we keep the current fragment for the next call
        # and return the previous transmission complete content
        if current_id != id:
            complete_content = content
            content = b''
            current_id = id
            yield complete_content

        content += frag


# Main loop
f = open('sinkhole_capture.pcap','rb')
reader = dpkt.pcap.Reader(f)
t_gen = transmission_generator(reader)
filename_list = [ x.decode() for x in next(t_gen).split(b'\n') ]
try:
    for filename in filename_list:
        filename = filename.split('/')[-1]
        file_content = next(t_gen)
        print(f'Extracting {filename}')
        open(filename, 'wb').write(file_content)
except:
    print('Missing data in capture, stopping...')


    