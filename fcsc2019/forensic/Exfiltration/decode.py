#!/usr/bin/env python3

import dpkt

from urllib import parse

start = None
f = open('exfiltration.pcap','rb')
result = ''
for ts, pkt in dpkt.pcap.Reader(f):
    # Only TCP
    eth=dpkt.ethernet.Ethernet(pkt) 
    if eth.type != dpkt.ethernet.ETH_TYPE_IP:
        continue
    ip = eth.data
    if ip.p != dpkt.ip.IP_PROTO_TCP:
        continue

    # Filter source and destination
    if ip.src != bytes([192, 168, 1, 26]):  # TODO str_to_inet ?
        continue
    if ip.dst != bytes([198, 18, 0, 10]):   # TODO str_to_inet ?
        continue

    # Only HTTP requests
    tcp = ip.data
    try:
        request = dpkt.http.Request(tcp.data)
    except (dpkt.dpkt.NeedData, dpkt.dpkt.UnpackError):
        continue

    # Only POST
    if request.method != 'POST':
        continue

    # Parse form
    form = parse.parse_qs(request.body.decode('utf-8'))
    data = form['data'][0]
    result += data

# Decrypt the whole content
key = "ecsc"
output = open('result.docx', 'wb')
result = bytes.fromhex(result)
decoded = bytearray( [ x^ord(key[i%len(key)]) for i, x in enumerate(result) ] )
output.write(decoded)