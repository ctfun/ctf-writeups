# Intro

## Crayon Cochon

**Description**

Cette épreuve avait été proposée lors de l’entrainement de la Team France en septembre 2019.

![](crayon-cochon.png)

**Solution**

L'alphabet utilisé ressemble à celui de [Pig Pen](https://www.apprendre-en-ligne.net/crypto/subst/pigpen.html) (d'où le titre de l'épreuve, vous l'avez ?).

La traduction est la suivante :
```
BELOW IS THE FLAG

KFNOKBHDBLEBSVJMRXKYOXDQZAEAH
```

## Not so FAT

**Description**

J’ai effacé mon flag par erreur, pourriez-vous le retrouver pour moi ?

Fichier: [not-so-fat.dd](not-so-fat.dd)

**Solution**

On regarde ce qu'est ce fichier fourni :
```
❯ file not-so-fat.dd 
not-so-fat.dd: DOS/MBR boot sector, code offset 0x3c+2, OEM-ID "mkfs.fat", sectors/cluster 4, reserved sectors 4, root entries 512, sectors 32768 (volumes <=32 MB), Media descriptor 0xf8, sectors/FAT 32, sectors/track 32, heads 64, serial number 0x3be84c04, unlabeled, FAT (16 bit)
```

On peut tenter de monter l'image, mais elle est vide. Logique, puisque l'énoncé
indique un fichier effacé par mégarde. C'est le moment de sortir [SleuthKit](https://sleuthkit.org/). On commence par lister les entrées du système de fichiers :

```
❯ fls not-so-fat.dd 
r/r * 4:	ziEuYrJW
r/r * 6:	flag.zip
v/v 523203:	$MBR
v/v 523204:	$FAT1
v/v 523205:	$FAT2
V/V 523206:	$OrphanFiles
```

Le fichier flag.zip est probablememnt notre cible, regardons donc cette entrée 6 dans le répertoire :

```
❯ istat not-so-fat.dd  6
Directory Entry: 6
Not Allocated
File Attributes: File, Archive
Size: 241
Name: _LAG.ZIP

Directory Entry Times:
Written:	2019-05-10 07:09:50 (CEST)
Accessed:	2019-05-10 00:00:00 (CEST)
Created:	2019-05-10 07:09:50 (CEST)

Sectors:
104 0 0 0 
```

Le nom dont le premier caractère est absent est typique d'un fichier effacé sur FAT16. On va le récupérer :

```
❯ icat -r not-so-fat.dd 6 > flag.zip
❯ unzip flag.zip 
Archive:  flag.zip
[flag.zip] flag.txt password: 
^C
```

Ce serait trop facile, il y a un mot de passe à l'extraction. On passe cette 
fois sur [John The Ripper](https://www.openwall.com/john/) pour casser le mot de passe, d'abord en extrayant un hash du mot de passe avant de lancer le bruteforce :

```
❯ /opt/john/run/zip2john flag.zip > hash.txt
ver 1.0 efh 5455 efh 7875 flag.zip/flag.txt PKZIP Encr: 2b chk, TS_chk, cmplen=59, decmplen=47, crc=007E07DB ts=4EB7 cs=4eb7 type=0
❯ /opt/john/run/john hash.txt 
Using default input encoding: UTF-8
Loaded 1 password hash (PKZIP [32/64])
Will run 12 OpenMP threads
Proceeding with single, rules:Single
Press 'q' or Ctrl-C to abort, 'h' for help, almost any other key for status
Almost done: Processing the remaining buffered candidate passwords, if any.
0g 0:00:00:00 DONE 1/3 (2024-01-05 15:28) 0g/s 875066p/s 875066c/s 875066C/s Ztxt1900..Tzip1900
Proceeding with wordlist:/opt/john/run/password.lst
Enabling duplicate candidate password suppressor
password         (flag.zip/flag.txt)     
1g 0:00:00:00 DONE 2/3 (2024-01-05 15:28) 7.142g/s 363057p/s 363057c/s 363057C/s 123456..123qweas
Use the "--show" option to display all of the cracked passwords reliably
Session completed. 
```

Le mot de passe est donc... `password`. :D

```
❯ unzip flag.zip 
Archive:  flag.zip
[flag.zip] flag.txt password: 
 extracting: flag.txt                
❯ cat flag.txt 
ECSC{eefea8cda693390c7ce0f6da6e388089dd615379}
```

## Petites Notes

**Description**

Connaissez-vous bien le format PCAPng ?

Fichier: [petites-notes.pcapng](petites-notes.pcapng)

**Solution**

À coup de strings, on voit qu'il y a probablement un flag qui se balade :
```
❯ strings petites-notes.pcapng  | grep ECSC
ECSC{cShl
```

Mais impossible de le trouver dans les paquets capturés ! Il doit donc s'agir de meta-données, 
mais où les trouver ? Finalement, dans Wireshark, en ouvrant le menu Analyser -> Information Expert,
la fenêtre suivante s'affiche :

![](wireshark.png)

`ECSC{cShle5dOKYBfjLNzT}`

Il est aussi possible d'obtenir ces commentaires en ouvrant la boîte des propriétés de la capture, ou en filtrant les paquets par `frame.comment`.

## Scully 1

**Description**

Notre développeur web n’est pas très doué en matière de sécurité… Saurez-vous afficher le flag afin que nous puissions lui montrer qu’il faut faire des efforts ?

Note : Aucun bruteforce n’est nécessaire.

Fichier: [docker-compose.scully1.yml](docker-compose.scully1.yml)

**Solution**

TODO

## StegCryptoDIY - PNG

**Description**

Apparemment des paramètres cryptographiques sont cachés dans ce fichier, à vous de les trouver.

Fichiers:
- [leHACK19_chall.png](leHACK19_chall.png)
- [leHACK19_ref.png](leHACK19_ref.png)

**Solution**

On regarde l'image avec le nom chall rapidement. En particulier, j'aime vérifier le format du fichier à l'aide de [hachoir](https://github.com/vstinner/hachoir). 

![](StegCryptoDIY-1.png)

Et justement, on constate que le fichier contient un chunk non standard `duMB`. On sauvegarde son contenu, qui ressemble fortement à du base64 :

```
❯ base64 -d <  plop.bin 
N = 11755068944142969498743245984761181325605304625286958721391766091283132398437645038349775898923465331260308064611167926187399384723360941883732174901666803 ,g1 = 3808819505564999545252623631192503212186598088555343738111165382623421720119837544658173822461834076347795159366593177669058705087266898444818926668584519, g2 = 8721787400327783748014849037626039397807612372804016643653047575976009827003447778861262036752326827613078346225192581708240820325867658742078694445677877 I use this functons for enciphering our skey : encipher(int.from_bytes(skey,'big'),g1,g2,N) with def encipher(m,g1,g2,N): s1=random.randrange(2**127,2**128) s2=random.randrange(2**127,2**128) return (m*pow(g1,s1,N))%N, (m*pow(g2,s2,N))%N and here is a flag: lehack2019{aef9556a575cc9de8fc9609bd034d63fe0a01470eb401378253f723bbc5cc16c}
```


## Vault

**Description**

Saurez-vous retrouver la clé pour rentrer dans le coffre ?

Fichier: [vault](vault)

**Solution**

On ouvre le binaire dans [ghidra](https://ghidra-sre.org/), et on regarde le `main`:

```
  puts("=-=-=-= Very secure vault =-=-=-=");
  printf("Please enter you very secure password: ");
  fflush(stdout);
  while( true ) {
    uVar4 = getchar();
    cVar1 = (char)uVar4;
    if (cVar1 == '\n') break;
    uVar3 = check_char(local_a4,cVar1);
    local_a0 = local_a0 & uVar3;
    if (local_a0 == 1) {
      local_9c = local_9c + (uVar4 & 0xff);
      flag[local_a4 % 100] = cVar1;
    }
    local_a4 = local_a4 + 1;
  }
  putchar(10);
  iVar2 = tcsetattr(0,0,&local_58);
  if (iVar2 == -1) {
    fwrite("tcsetattr",1,9,stderr);
                    /* WARNING: Subroutine does not return */
    exit(-1);
  }
  if ((local_a0 == 1) && (local_9c == 0xb0b)) {
    puts("\\o/ Access granted! \\o/");
    printf("Here is your flag: ECSC{%s}\n",flag);
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  puts("Wrong password: authorities have been alerted!");
                    /* WARNING: Subroutine does not return */
  exit(-1);
```

On voit que chaque caractères du mot de passe saisi est contrôlé dans `check_char`. On regarde :

```
bool check_char(int param_1,byte param_2)
{
  return (int)"b87de397e1346bc605be4ed8361a68a3d9748fc9"[(param_1 + 10U) % 0x28] == (uint)param_2;
}
```

C'est tout ? Donc il vérifie que notre mot de passe correspond au hash littéral, décalé de 10 caractères, soit `346bc605be4ed8361a68a3d9748fc9b87de397e1`:

```
❯ ./vault 
=-=-=-= Very secure vault =-=-=-=
Please enter you very secure password: 
\o/ Access granted! \o/
Here is your flag: ECSC{346bc605be4ed8361a68a3d9748fc9b87de397e1}
```

## ybab

**Description**

.semèlborp ed resop suov sap tiarved en egnellahc eC

Fichier: [ybab](ybab)

**Solution**

L'énoncé dans l'ordre :
> Ce challenge ne devrait pas vous poser de problèmes.

On ouvre le binaire dans [Ghidra](https://ghidra-sre.org/). On regarde les fonctions
qui y sont définies, toutes semblent vides, jusqu'à 0x001006ca :

```
void FUN_1006CA(void)
{
  printf("The flag is %s.\n","ECSC{cdcd13c4c81a23a21506fa8efa5edff781e9fe80}");
  return;
}
```

Et c'est tout.