# FCSC 2021

![](rank.png)

## intro

- [Snake](intro/snake.md)
- [Dérèglement](intro/dereglement.md)
- [Clair connu](intro/clair_connu.md)
- [bofbof](intro/bofbof.md)
- [Random Search](intro/random_search.md)
- [Push It to the Limit](intro/push_it_to_the_limit.md)
- [Guessy](intro/guessy.md)
- [Bonus Points](intro/bonuspoints.md)
- [La PIN](intro/lapin.md)
- [Rituel du Boutisme](intro/rituel_du_boutisme.md)
- [Rituel en Chaîne](intro/rituel_en%20chaîne.md)


## crypto

- [Macaque](crypto/macaque.md)

## forensic

- [Malware 1/3](forensic/malware1.md)
- [Ordiphone 0](forensic/ordiphone0.md)
- [Ordiphone 1](forensic/ordiphone1.md)
- [Ordiphone 2](forensic/ordiphone2.md)
- [Disque nuagique 1](forensic/Disque%20nuagique%201.md)
- [Disque nuagique 2](forensic/Disque%20nuagique%202.md)

## misc

- [My Whois](misc/my_whois.md)
- [Privesc Me 1](misc/privesc_me1.md)
- [Privesc Me 2](misc/privesc_me2.md)
- [Ventriglisse](misc/ventreglisse.md)


## pwn


## reverse

- [Jakarta](reverse/Jakarta/README.md)


## web

- [BaguetteVPN 1/2](web/baguettevpn_1.md)
- [BaguetteVPN 2/2](web/baguettevpn_2.md)