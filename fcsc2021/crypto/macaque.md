# Macaque - crypto

**Challenge**

Retrouvez le flag avec l'accès à ce service distant.

nc challenges1.france-cybersecurity-challenge.fr 6000

[macaque.py](macaque.py)

**Solution**

Que fait le code applicatif ? Il y a deux fonctions offertes :
- tag: on fournit un buffer binaire sous forme hexadécimal, l'outil le chiffre par AES CBC avec deux clefs 
différentes, puis retourne le dernier bloc chiffré par chacune des clefs. On a le droit de n'appeler cette
fonction que trois fois au maximum, et les données en entrées sont sauvegardées.
- verify: on fournit un buffer binaire et le résultat qu'on recevrait de la fonction tag avec ce buffer. 
Bien évidemment, on ne peut pas passer un buffer déjà taggé (d'où la sauvegarde au point précédent). Si
on parvient à «deviner» le résultat de verify, on gagne le flag.

Les points à remarquer :
- seul le dernier bloc chiffré est retourné par la fonction tag.
- c'est le mode CBC qui est utilisé pour chaîner les blocs :
!(https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/Schema_CBC.svg/600px-Schema_CBC.svg.png)[Mode CBC sur wikipédia]
(Source: https://fr.wikipedia.org/wiki/Mode_d%27op%C3%A9ration_(cryptographie)#Encha%C3%AEnement_des_blocs_:_%C2%AB_Cipher_Block_Chaining_%C2%BB_(CBC))

D'une manière générale, un bloc chiffré C d'un bloc M est composé comme suite:
`C = enc(M ^ enc(C-1))`

[TODO: la suite]

[Exploit](exploit_macaque.py)

=> FCSC{f7c50c0e5ad148a3321d9dd0e72c91420e243b42c9c803814f6d8554163b6260}