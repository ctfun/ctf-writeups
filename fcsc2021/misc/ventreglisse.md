# Ventriglisse - misc algorithmique

**Challenge**

Session de ventriglisse !

nc challenges1.france-cybersecurity-challenge.fr 7002

**Solution**

Il s'agit de la (tradionnelle) épreuve de labyrinthe. Une connexion sur le service donne ceci :

```
Hey there!
Welcome to our new attraction: the famous Ventriglisse!
A square area is filed with soap and some water and is very slippery!
Here is an example (base64-encoded image):
------------------------ BEGIN MAZE ------------------------
[...
]------------------------- END MAZE -------------------------
You are intially located outside the area at the bottom red mark.
At the beginning, you jump right in the area (head first!) and you slide up and stop at the first wall encountered.
Once you hit a wall, you can jump again in another direction to continue your amazing soapy journey.
Your goal is get out of the zone at the red mark at the top.
Send me how you would like to move within the maze as follows:
 - N to go North
 - S to go South
 - E to go East
 - W to go West
On the example above, a valid path would be NESENONOSENENON.
Note that a valid path would always start and end by 'N'.
In this example, paths starting by NO, NN or NEN would be invalid (as your head would hit a wall).
Press a key when you are ready...
```

Un exemple de labyrinthe :
![laby](ventreglisse.png)

Comme l'an dernier, j'ai deux scripts python, l'un pour l'interaction avec le serveur (pas très intéressant, et donc non fourni ici) et [le solver](ventreglisse_solve.py).

Et également comme l'an dernier, la liste des 27 (!) labyrinthe qui ont été soumis à mon script par le serveur :
|||||||
|---|---|---|---|---|---|
| [1](ventreglisse/maze01.png)  | [2](ventreglisse/maze02.png)  | [3](ventreglisse/maze03.png)  | [4](ventreglisse/maze04.png)  | [5](ventreglisse/maze05.png)  | [6](ventreglisse/maze06.png)  |
| [7](ventreglisse/maze07.png)  | [8](ventreglisse/maze08.png)  | [9](ventreglisse/maze09.png)  | [10](ventreglisse/maze10.png) | [11](ventreglisse/maze11.png) | [12](ventreglisse/maze12.png) | 
| [13](ventreglisse/maze13.png) | [14](ventreglisse/maze14.png) | [15](ventreglisse/maze15.png) | [16](ventreglisse/maze16.png) | [17](ventreglisse/maze17.png) | [18](ventreglisse/maze18.png) |
| [19](ventreglisse/maze19.png) | [20](ventreglisse/maze20.png) | [21](ventreglisse/maze21.png) | [22](ventreglisse/maze22.png) | [23](ventreglisse/maze23.png) | [24](ventreglisse/maze24.png) |
| [25](ventreglisse/maze25.png) | [26](ventreglisse/maze26.png) | [27](ventreglisse/maze27.png) ||||
