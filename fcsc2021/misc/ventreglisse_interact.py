#!/usr/bin/env python3

import sys
import re
from telnetlib import Telnet
from base64 import b64decode
from subprocess import check_output

if len(sys.argv) != 3:
    print(f'Syntax: {sys.argv[0]} host port')
    exit(-1)
host = sys.argv[1]
port = sys.argv[2]

id = 1
with Telnet(host, port) as tn:
    prompt = tn.read_until(b'Press a key when you are ready...')
    tn.write(b'\n')
    
    while True:
        filename = f'level-{id:02d}.png'
        prompt = tn.read_until(b'Enter your solution within 5 seconds:').decode().replace('\n', '')
        m = re.search(r'------------------------ BEGIN MAZE ------------------------(.*)------------------------- END MAZE -------------------------', prompt)
        if m is None:
            print(prompt)
            exit()
        payload = m.group(1)
        open(filename, 'wb').write(b64decode(payload))
        a = check_output(['python3', 'ventreglisse_solve.py', filename])
        format_a = a.decode().strip("\n")
        print(f'{filename} => {format_a}')
        tn.write(a)
        id += 1
        
