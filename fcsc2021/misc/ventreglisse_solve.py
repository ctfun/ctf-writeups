#!/usr/bin/env python

import sys
from PIL import Image

BLUE = (0, 0, 255)
RED = (229, 20, 0)

def is_wall(pixels, x, y):
    for i in range(-2, 3):
        for j in range(-2, 3):
            if pixels[x+i, y+j] == BLUE:
                return True
    return False

# Côté de 64
def load_maze(filename):
    im = Image.open(filename)
    #print(im.size)
    width, height = im.size
    pixels = im.load()

    # Sortie
    for col in range(96, width-32, 64):
        if pixels[col, 112] == RED:
            ending = (col-96)//63

    # Entrée
    for col in range(96, width-32, 64):
        if pixels[col, height-42] == RED:
            entry = (col-96)//63

    # Les cellules
    cells = list()
    for col in range(96, width-32, 64):
        h_cell = list()
        for lig in range(160, height-32, 64):
            possible_move = ''
            if not is_wall(pixels, col-32, lig):
                possible_move += 'O'
            if not is_wall(pixels, col+32, lig):
                possible_move += 'E'
            if not is_wall(pixels, col, lig-32):
                possible_move += 'N'
            if not is_wall(pixels, col, lig+32):
                possible_move += 'S'
            h_cell.append(possible_move)
        cells.append(h_cell)
    return entry, ending, cells

# Load
entry, ending, cells = load_maze(sys.argv[1])
#print(entry, ending, cells)

def move(dir, c, l):
    if dir == 'N':
        incc, incl = 0, -1
    elif dir == 'S':
        incc, incl = 0, 1
    elif dir == 'O':
        incc, incl = -1, 0
    else:
        incc, incl = 1, 0

    while dir in cells[c][l]:
        #print(c, l, cells[c][l])
        l, c = l+incl, c+incc

    return c, l

def iter(path, visited, c, l):
    #print(f'{path=}, {visited=}, {c=}, {l=}')
    # Sortie trouvée !
    if c == ending and l == 0:
        if path[-1] != 'N':
            path += 'N'
        print(path)
        exit(0)

    # Récursion
    for mv in cells[c][l]:
        if ( mv in 'OE' and path[-1] in 'OE' ) or ( mv in 'NS' and path[-1] in 'NS' ):
            # Éviter les A/R inutiles (le serveur n'aime pas...)
            continue
        nc, nl = move(mv, c, l)
        if (nc, nl) not in visited:
            nvisited = list(visited)
            nvisited.append((nc, nl))
            iter(path + mv, nvisited, nc, nl)

# Au départ, pas le choix
c, l = entry, len(cells[0])-1
visited = list()
visited.append((c, l))
c, l = move('N', c, l)
visited.append((c, l))
iter('N', visited, c, l)
print('Raté :(')