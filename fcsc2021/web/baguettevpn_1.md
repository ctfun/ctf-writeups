# BaguetteVPN 1/2 - web

**Challenge**

Un nouveau service de VPN vient de faire son apparition : http://challenges2.france-cybersecurity-challenge.fr:5002/

Essayer d'en apprendre davantage sur le fonctionnement de leur site web.

Note : cette épreuve débloque BaguetteVPN 2/2

**Solutions**

Le site est juste une page statique. On regarde son code source et il y a un commentaire intéressant

```html
<!-- 
  Changelog :
    - Site web v0
    - /api/image : CDN interne d'images
    - /api/debug
  TODO :
    - VPN
-->
```

On a deux URLs intéressantes. Commençons pas la première, qui [donne un grande quantité d'information de debug](baguettevpn_1-debug.json).
Les éléments intéressants : le nom du script ! `__file__:	"./baguettevpn_server_app_v0.py"` 

On va probablement avoir besoin de le lire, et la deuxième URL va nous aider. En effet, sur le site on 
voit un exemple d'appel par `<img src="/api/image?fn=/logo.png" height="200">`, et en sollicitant cette URL, 
on reçoit l'image, avec un content-type text/html, ce qui laisse entendre un script un peu simpliste, 
probablement vulnérable à un Path Traversal.

On tente `/api/image?fn=/baguettevpn_server_app_v0.py`, mais ça nous donne une erreur 404, fausse piste ?

Finalement, il suffit juste de mentionner le nom du script à la racine du site pour récupérer le fichier :
[http://challenges2.france-cybersecurity-challenge.fr:5002//baguettevpn_server_app_v0.py](http://challenges2.france-cybersecurity-challenge.fr:5002//baguettevpn_server_app_v0.py)

On ouvre alors [le fichier](baguettevpn_1-server_app_v0.py) et...


```python
# Congrats! Here is the flag for Baguette VPN 1/2
#   FCSC{e5e3234f8dae908461c6ee777ee329a2c5ab3b1a8b277ff2ae288743bbc6d880}
```


