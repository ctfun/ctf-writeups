# Ordiphone 1 - forensic

**Challenge :**

Le maître d’apprentissage, un peu fou, veut savoir depuis combien de nanosecondes l'ordiphone était allumé lors du lancement de la capture mémoire. Retrouvez cette information, contenue dans la variable real_start_time du processus ayant effectué la capture !

Le flag est au format FCSC{real_start_time}, où real_start_time est un nombre entier.

Fichier : [lime.dump.7z (180M)](https://hackropole.fr/filer/fcsc2021-forensics-ordiphone/public_filer/lime.dump.tar.xz)

SHA256(lime.dump) = 21575c12bcb8d67e6ca269bac6c3d360847b16922f2f44b0b360790862afe46d.


**Solution :**

*Note: je n'ai pas validé ce challenge pendant la compétition, je n'ai jamais réussi à créer le profil volatility adéquat. Je rédige après coup, en me basant sur [un autre write-up](https://github.com/SorCelien/CTF-WRITEUPS/blob/main/FCSC-2021/forensics/ordiphone-1.md) pour générer le profil, tout en gardant ma méthode pour la suite de la résolution.*

On va créer un profil Volatility pour Android, en suivant [la documentation officielle](https://github.com/volatilityfoundation/volatility/wiki/Android). Pour cela, on a besoin de connaître la version du noyau utilisé, par un simple strings sur le dump :

```console
$ strings lime.dump | grep "Linux version"
Kernel: Linux version 3.18.91+ (android-build@wphr1.hot.corp.google.com) (gcc version 4.9 20140827 (prerelease) (GCC) ) #1 SMP PREEMPT Tue Jan 9 20:30:51 UTC 2018
Linux version 4.4.124+ (forensics@fcsc2021) (gcc version 4.9.x 20150123 (prerelease) (GCC) ) #3 SMP PREEMPT Sun Mar 21 19:15:33 CET 2021
Linux version 4.4.124+ (forensics@fcsc2021) (gcc version 4.9.x 20150123 (prerelease) (GCC) ) #3 SMP PREEMPT Sun Mar 21 19:15:33 CET 2021
Kernel: Linux version 3.18.91+ (android-build@wphr1.hot.corp.google.com) (gcc version 4.9 20140827 (prerelease) (GCC) ) #1 SMP PREEMPT Tue Jan 9 20:30:51 UTC 2018
Linux version 4.4.124+ (forensics@fcsc2021) (gcc version 4.9.x 20150123 (prerelease) (GCC) ) #3 SMP PREEMPT Sun Mar 21 19:15:33 CET 2021
```

Attention, j'ai été piégé pendant le challenge en me concentrant sur le `3.18.91+` (je ne sais pas pourquoi il apparaît ici). C'est bien le 4.4.124+ dont on a besoin. On clone les repos de noyau et de compilateur :

*Notes:*
- comment connaît-on le bonne branche de gcc à utiliser ?
(gcc version 4.9 20140827 (prerelease) (GCC) )
- comment retrouver le makefile de .config ?
$ grep defconfig ~/tmp/FCSC/2021/forensic/Ordiphone/lime.txt 
"arch/x86/configs/x86_64_defconfig"


```console
git clone https://android.googlesource.com/kernel/goldfish.git android-source
git clone https://android.googlesource.com/platform/prebuilts/gcc/linux-x86/x86/x86_64-linux-android-4.9 -b android10-mainline-release gcc
export PATH=$(pwd)/gcc/bin:$PATH
export CROSS_COMPILE=x86_64-linux-android-
export ARCH=x86_64
cd android-source
make x86_64_ranchu_defconfig
make -j4
```

Cela nous donne le `System.map`. Puis, côté Volatility, on utilise le `Makefile` suivant :

```Makefile
obj-m += module.o
KDIR := <path to android kernel source>/android-source
CCPATH := <path to android gcc binary>/gcc/bin

-include version.mk

all: dwarf 

dwarf: module.c
	$(MAKE) ARCH=x86_64 CROSS_COMPILE=$(CCPATH)/x86_64-linux-android- -C $(KDIR) CONFIG_DEBUG_INFO=y M=$(PWD) modules
	dwarfdump -di module.ko > module.dwarf

clean:
	rm -f module.dwarf
```

À partir de là, le profil généré permet l'usage de Volatility (j'ai un wrapper `vol` qui ajoute automatiquement les paramètre de dump et de profil pour la suite). Rapidement, on se rend compte qu'il n'existe pas de commande permettant de répondre à la question du challenge. Il va falloir mettre les mains dans le cambouis avec l'interpréteur Volatility. On note que `linux_psaux` montre le process que l'on cherche :
```
4752   0      0      insmod /sdcard/lime.ko path=/sdcard/lime.dump format=lime
```

Lançons `linux_volshell` pour aller plus loin. La fonction `hh()` nous indique quelques points d'entrée. Par exemple, `cc()` va nous permettre de passer dans le contexte de notre process :

```
>>> cc(pid=4752)
Current context: process insmod, pid=4752 DTB=0x379f000
```

On sait que champ `real_start_time` appartient [à la structure task_struct](https://docs.huihoo.com/doxygen/linux/kernel/3.7/structtask__struct.html). Ça tombe bien, c'est exactement ce qui est retourné par la command `proc()` :

```
>>> proc()
[task_struct task_struct] @ 0xFFFF880011DA12C0
```

Du coup, est-ce qu'il suffirait simplement de... ?
```
>>> proc().real_start_time
 [unsigned long long]: 63951047224
```

Tout simplement... `FCSC{63951047224}`