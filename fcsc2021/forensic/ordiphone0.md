# Ordiphone 0 - forensic

**Challenge**

Un nouvel apprenti vient d'effectuer une capture mémoire mais a oublié de noter la date du lancement de celle-ci.

Pour valider cette première étape, vous devez retrouver la date à laquelle le processus permettant la capture a été lancé. Le flag est au format FCSC{sha256sum(date)}, avec la date au format YYYY-MM-DD HH:MM en UTC.

Fichier : [lime.dump.7z (180M)](https://hackropole.fr/filer/fcsc2021-forensics-ordiphone/public_filer/lime.dump.tar.xz)

SHA256(lime.dump) = 21575c12bcb8d67e6ca269bac6c3d360847b16922f2f44b0b360790862afe46d.

**Solution**

Dans un premier temps, il n'est pas nécessaire de sortir volatility. On va regarder les chaînes présentes dans le dump. En effet, les [exemples](https://github.com/504ensicsLabs/LiME#example) montre que le dump est déclenché par le chargement du module `lime.ko` qui va se retrouver probablement :

```
type=1400 audit(1616526815.693:11968): avc: denied { module_load } for pid=4752 comm="insmod" path="/storage/emulated/0/lime.ko" dev="sdcardfs" ino=57349 scontext=u:r:su:s0 tcontext=u:object_r:sdcardfs:s0 tclass=system permissive=1
```

Audit nous fournit le timestamp de lancement de la commande. Il nous suffit de le convertir au bon format et de calculer le condensat SHA256 pour avoir le flag.

1616526815.693 => 23/3/2021 à 19:13:35 => 2021-03-23 19:13

`FCSC{b7dc08558ee16d1acbf54db67263c1d92e9a9d9603e6a1345550c825527adc06}`
