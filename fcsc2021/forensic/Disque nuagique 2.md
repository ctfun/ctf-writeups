# Disque nuagique 2 - forensics

**Challenge**

Pour cette étape de l'investigation, vous devez retrouver un élément pouvant être lié au nom ou au pseudo de l'administrateur. L'élément à rechercher a été remplacé par un flag au format FCSC{xxx}.

La copie du disque est identique à l'épreuve Disque nuagique 1.

SHA256(data.zip) = f25ad71798caa9a7c1a1bdb57cd4b189251b6fbcee116a0def914750de2aff70.

data.zip (1.6GB) : https://files.france-cybersecurity-challenge.fr/dl/cloudisk/data.zip


**Solution**

On reprend l'analyse laissée à la fin de [la première étape](Disque%20nuagique%201.md) en essayant de trouver plus d'information sur la partition principale. La commande `fdisk` nous avait donné un type `crypto_luks`, donc une partition chiffrée. Regardons les caractéristiques de plus près :

```shell
# losetup -o $((512*1001472)) -f  ewf/ewf1
# cryptsetup luksDump /dev/loop8 
LUKS header information
Version:        2
Epoch:          3
Metadata area:  16384 [bytes]
Keyslots area:  16744448 [bytes]
UUID:           e61a1da4-b95d-4df5-ab40-bbffc505b3f2
Label:          (no label)
Subsystem:      (no subsystem)
Flags:          (no flags)

Data segments:
  0: crypt
    offset: 16777216 [bytes]
    length: (whole device)
    cipher: aes-xts-plain64
    sector: 512 [bytes]

Keyslots:
  0: luks2
    Key:        512 bits
    Priority:   normal
    Cipher:     aes-xts-plain64
    Cipher key: 512 bits
    PBKDF:      argon2i
    Time cost:  4
    Memory:     559034
    Threads:    2
    Salt:       5f 57 43 28 bd 7a 10 aa e6 a5 18 27 e7 0e 85 d8 
                8d df 02 1b 6b 54 43 1b aa ce 26 2d b6 0c 08 51 
    AF stripes: 4000
    AF hash:    sha256
    Area offset:32768 [bytes]
    Area length:258048 [bytes]
    Digest ID:  0
Tokens:
Digests:
  0: pbkdf2
    Hash:       sha256
    Iterations: 138115
    Salt:       3c 09 38 f6 1d a0 9f d7 76 f1 c7 2e cf a1 f0 43 
                0e 7d 6b 8e b5 8a 83 4f 03 e2 41 74 5e 7a 78 df 
    Digest:     3e 4e c0 1d fe bc 72 c4 14 de b6 11 79 8d 15 14 
                45 04 81 60 bd 88 be f4 37 8f 93 bd 91 54 e1 cf 
```

Les points importants sont LUKS v2, et chiffrement aes-xts-plain64 et conduisent à penser qu'on ne peut pas attaquer de front (par exemple en bruteforçant la passphrase). Je décide de regarder la partition de boot. Pas d'information précise en regardant les chaînes, je décide donc d'essayer de créer une VM [associée à ce disque](https://www.serverwatch.com/guides/using-a-physical-hard-drive-with-a-virtualbox-vm/) et de tenter de la démarrer.

![1](disque_nuagique_1.png)

On voit qu'au boot, la VM demande le mot de passe pour déchiffrer le disque. On voit également
la recherche d'une adresse IP par DHCP. Ce qui fait sens, puisque l'on est en présence d'un
disque d'une machine hébergée dans le cloud. En faisant un scan de port sur cette VM, on
peut même identifier un serveur SSH qui est activé. C'est un exemple classique qui permet
notamment de déverrouiller un disque chiffré à distance, avec [le serveur dropBear](https://www.cyberciti.biz/security/how-to-unlock-luks-using-dropbear-ssh-keys-remotely-in-linux/).

Dans l'immédiat, profitons simplement de notre accès physique pour aller un peu plus loin. 
En effet, il existe [un bug connu](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2016-4484) 
qui va nous aider : un grand nombre de saisies de mots de passe erronés va 
finalement nous donner un shell busybox sur le disque de boot. Voici :

![2](disque_nuagique_2.png)

À partir de là, on peut aller se balader dans le système de fichier pour aller à la pêche
aux informations. On note notamment qu'effectivement, le serveur dropBear est lancé pour
permettre l'accès réseau. Et en farfouillant dans tous les fichiers disponibles sur cette
partition, on trouve le flag dans `/root-LfekH8/.ssh/.authorized_keys` :

![3](disque_nuagique_3.png)

Et donc, toute la partie chiffrée... ne sert à rien. Le flag est `FCSC{0fb01eb22d4f812dcbdfcb}`.