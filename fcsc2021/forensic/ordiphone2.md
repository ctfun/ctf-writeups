 # Ordiphone 2 - forensics Android

**Challenge**

Pour avancer sur cette investigation, vous devez analyser cette capture mémoire et une copie du stockage interne d'un téléphone Android utilisé par un cybercriminel.

Votre mission est de retrouver les secrets que ce dernier stocke sur son téléphone.


Fichiers:
- [lime.dump.7z (180MB)](https://hackropole.fr/filer/fcsc2021-forensics-ordiphone/public_filer/lime.dump.tar.xz)
- [sdcard.zip (17MB)](https://hackropole.fr/filer/fcsc2021-forensics-ordiphone/public_filer/sdcard.zip)

    SHA256(lime.dump) = 21575c12bcb8d67e6ca269bac6c3d360847b16922f2f44b0b360790862afe46d.
    SHA256(sdcard.zip) = e19e449c3bc7a9d04cc7f665fb494d857b9f019d8fec2ba08ab40c117fa2f8d8.


**Solution**

Comme je ne suis pas parvenu à générer un profil Volatility correct pour cette image (j'y suis finalement parvenu en lisant les autres write-ups, cf. [Ordiphone 1](ordiphone1.md)), je vais la résoudre de nouveau par recherche de chaînes dans le dump. 

En recherchant les occurrences du chargement du module lime.ko, on trouve une liste de commandes lancées depuis le shell Android :

```console
generic_x86_64:/ $ su
generic_x86_64:/ # cd /sd
generic_x86_64:/ # cd /sdcard/
generic_x86_64:/sdcard # mknod /dev/loop0 b 7 0
generic_x86_64:/sdcard # losetup /dev/loop0 /sdcard/secrets
generic_x86_64:/sdcard # /data/data/com.termux/files/usr/bin/cryptsetup luksOpen /dev/loop0 secrets
Enter passphrase for /storage/emulated/0/secrets: 
generic_x86_64:/sdcard # mkdir /sdcard/very_secret
generic_x86_64:/sdcard # mount /dev/mapper/secrets /sdcard/very_secret
generic_x86_64:/sdcard # cd /sdcard/very_secret
generic_x86_64:/sdcard/very_secret # sh script.sh
/lime.ko "path=/sdcard/lime.dump format=lime"
*** WARNING : deprecated key derivation used.
Using -iter or -pbkdf2 would be better.
generic_x86_64:/sdcard/very_secret # insmod /sdcard/lime.ko "path=/sdcard/lime.dump format=lime"
```

On voit donc les actions de l'utilisateur :
- la création d'un périphérique de loop sur la carte SD qui va pointer sur `/sdcard/secrets`.
- le montage de ce périphérique par cryptsetup, il est donc chiffré.
- l'édition et le lancement d'un script `script.sh`

Et ça tombe bien, on retrouve les éléments dans l'archive `sdcard.zip`qui nous est fournie. On va donc répéter les mêmes opérations pour ce que cela donne :

```shell
$ unzip sdcard.zip
$ sudo cryptsetup luksOpen /dev/loop11 secrets
$ sudo mount /dev/mapper/secrets secrets/
$ ls secrets/
flag.enc  script.sh
```

On a trouvé notre flag, il est visiblement chiffré. Regardons ce que fait le script (note: j'ai ajouté les commentaires dans le code) :

```shell
# Création d'un aléa
aleatoire=$(cat /dev/urandom | head | xxd -p -l 30 | tr -d " ")
# Log l aléa dans dmesg
echo $aleatoire > /dev/kmsg
# Ajoute les pid des démons adbd, vold et logd à l aléa
aleatoirebis="$aleatoire$(pidof adbd | tr -d ' ')$(pidof vold | tr -d ' ')$(pidof logd | tr -d ' ')"
# Utilisation de l aléa comme clef de chiffrement AES du flag
echo $aleatoirebis | /data/data/com.termux/files/usr/bin/openssl aes-256-cbc -in flag -out flag.enc -pass stdin
# Réécriture du contenu initial du flag pour éviter sa récupération
/data/data/com.termux/files/usr/bin/shred flag
```

Il va donc nous falloir reconstituer l'aléa qui a servi de clef de chiffrement. Là non plus, pas besoin de Volatility, retournons aux chaînes présentes dans le dump .

D'abord l'aléa poussé dans dmesg, en utilisant une expression rationnelle (`^[a-f0-9]{60}$`). On n'a que deux candidats possibles :
```
387e8985bd75be1b922eddaadde934e70465424ab4b0c3da98763c094432
4100038121f20004150605040710000041637469766174653a64743d3135
```

*Note: si on est parvenu à construire un profil Volatility, c'est la même méthode, et on récupère tout de suite la bonne valeur :*
```console
$ ./vol linux_dmesg | egrep "[a-f0-9]{60}"
Volatility Foundation Volatility Framework 2.6.1
[63788233965.63] 387e8985bd75be1b922eddaadde934e70465424ab4b0c3da98763c094432
```

Ensuite les démons, en recherchant les occurrences à `/proc/<pid>/stat` et en examinant les lignes qui suivent et qui contiennent le nom du process :
```console
$ strings lime.dump | grep -B 2 "/proc/[0-9]*/stat"
[...]
/proc/1581/cmdline
adbd
/proc/1581/stat
[...]
/proc/1539/cmdline
vold
/proc/1539/stat
[...]
/proc/1529/cmdline
logd
/proc/1529/stat
[...]
```

*Note: si on est parvenu à construire un profil Volatility, c'est plus simple :*
```shell
$ ./vol linux_psaux | egrep "(vold|adbd|logd)"
Volatility Foundation Volatility Framework 2.6.1
1529   1036   1036   /system/bin/logd                                                
1539   0      0      /system/bin/vold --blkid_context=u:r:blkid:s0 --blkid_untrusted_context=u:r:blkid_untrusted:s0 --fsck_context=u:r:fsck:s0 --fsck_untrusted_context=u:r:fsck_untrusted:s0
1581   2000   2000   /sbin/adbd --root_seclabel=u:r:su:s0      
```

On n'a plus qu'à reconstituer la clef complète en concaténant les différents éléments. On a deux aléas possibles donc au pire deux essais, le premier est suffisant :

```shell
echo "387e8985bd75be1b922eddaadde934e70465424ab4b0c3da98763c094432158115391529" | openssl aes-256-cbc -d -in flag.enc -out ../flag.txt -pass stdin
```

On obtient alors un joli PNG :

![flag](ordiphone_flag.png)

`FCSC{ba5dc3f62c971c212133bb45b76084732c86936b76a026dc89c7b34fd3df29ae}`