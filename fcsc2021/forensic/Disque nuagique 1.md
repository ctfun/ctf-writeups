# Disque nuagique 1 - forensics

**Challenge**

Vous trouverez ci-joint la copie d'un disque (1.6GB).

Pour commencer cette investigation, vous devez retrouver l'offset du début de la partition qui possède l'UUID e61a1da4-b95d-4df5-ab40-bbffc505b3f2.

Format du flag : FCSC{offset_en_hexadécimal} (example : FCSC{123abc}).

SHA256(data.zip) = f25ad71798caa9a7c1a1bdb57cd4b189251b6fbcee116a0def914750de2aff70.

data.zip (1.6GB) : https://files.france-cybersecurity-challenge.fr/dl/cloudisk/data.zip


**Solution**

Lorsque l'on décompresse l'archive on obtient des fichiers .e[0-9] :

```shell
$ unzip data.zip 
Archive:  data.zip
  inflating: disque.e01              
  inflating: disque.e02              
```

Il s'agit d'un [Expert Witness Disk Image Format](https://www.loc.gov/preservation/digital/formats/fdd/fdd000406.shtml). On utilise les utilitaires adéquats pour monter le disque :

```shell
$ mkdir ewf
$ ewfmount disque.e01 ewf/
ewfmount 20140807
```

On analyse ensuite la table des partitions :
```shell
$ fdisk -l  ewf/ewf1 
Disque ewf/ewf1 : 10 GiB, 10737418240 octets, 20971520 secteurs
Unités : secteur de 1 × 512 = 512 octets
Taille de secteur (logique / physique) : 512 octets / 512 octets
taille d'E/S (minimale / optimale) : 512 octets / 512 octets
Type d'étiquette de disque : dos
Identifiant de disque : 0xb69e7b6d

Périphérique Amorçage   Début      Fin Secteurs Taille Id Type
ewf/ewf1p1   *           2048   999423   997376   487M 83 Linux
ewf/ewf1p2            1001470 20969471 19968002   9,5G  5 Étendue
ewf/ewf1p5            1001472 20969471 19968000   9,5G 83 Linux
```

La première partition est probablement le boot, suivi d'une partition étendue qui contient la partition principale, dont l'UID correspond à celui recherché :
```shell
$ blkid --probe --offset $((512*1001472)) ewf/ewf1
ewf/ewf1: VERSION="2" UUID="e61a1da4-b95d-4df5-ab40-bbffc505b3f2" TYPE="crypto_LUKS" USAGE="crypto"
```

Le flag est la valeur de décalage de la partition en hexadécimal :
`FCSC{1E900000}`