# Jakarta

**Énoncé**

Voici une application Android qui permet de vérifier si le flag est correct.

Fichier: [jakarta.apk](https://hackropole.fr/challenges/fcsc2021-reverse-jakarta/public/jakarta.apk)


**Solution**

On va voir si on peut décompiler le bazar sans trop de souci pour s'éviter de lire le bytecode directement.
On commence par dézipper l'archive APK, puis on utilise [dex2jar](https://github.com/pxb1988/dex2jar) pour
transformer l'archive DEX en JAR. On ouvre ce dernier avec [jd-gui](http://java-decompiler.github.io/).

On parcours rapidement les résultats, pour tomber sur la classe qui va probablement nous intéresser, 
`com.fcsc2021.jakarta.Check.class`. Son code est le suivant :

```Java
package com.fcsc2021.jakarta;

public class Check {
  int[] enc = new int[] { 
      11, 152, 177, 51, 145, 152, 153, 185, 26, 156, 
      177, 19, 177, 50, 156, 26, 156, 35, 176, 159, 
      185, 185, 185, 26, 19, 152, 177, 50, 144, 144, 
      176, 177, 26, 184, 190, 50, 11, 26, 51, 26, 
      26, 156, 19, 58, 148, 19, 176, 51, 26, 177, 
      58, 58, 144, 139, 152, 50, 185, 153, 177, 153, 
      144, 26, 176, 144, 50, 156, 145, 153, 156, 156 };
  
  String flag;
  
  public Check(String paramString) {
    this.flag = paramString;
  }
  
  public boolean valid() {
    int i = this.flag.length();
    int j = this.enc.length;
    boolean bool = false;
    if (i != j)
      return false; 
    int[] arrayOfInt = new int[i];
    for (j = 0; j < i; j++) {
      char c = this.flag.charAt((j * 37 + 1) % i);
      for (byte b = 7; b >= 0; b--)
        arrayOfInt[j] = arrayOfInt[j] ^ (c >> b & 0x1) << (b * 5 + 3) % 8; 
    } 
    int k = 0;
    for (j = 0; j < i; j++)
      k |= arrayOfInt[j] ^ this.enc[j]; 
    if (k == 0)
      bool = true; 
    return bool;
  }
}
```

La classe contient un tableau d'entier. Il semble y avoir trois grandes étapes :
- on vérifie que le flag passé est d'une taille identique au tableau interne.
- on construit un deuxième tableau qui prend les codes ascii du flag et effectue
des opérations booléennes dessus.
- on compare le résultat au tableau interne.

Ainsi, si on parvient à renverser les opérations booléennes, et les appliquer 
au tableau interne, on devrait obtenir le flag. On peut le faire en python :

```python
#!/usr/bin/env python3

enc = [
      11, 152, 177, 51, 145, 152, 153, 185, 26, 156, 
      177, 19, 177, 50, 156, 26, 156, 35, 176, 159, 
      185, 185, 185, 26, 19, 152, 177, 50, 144, 144, 
      176, 177, 26, 184, 190, 50, 11, 26, 51, 26, 
      26, 156, 19, 58, 148, 19, 176, 51, 26, 177, 
      58, 58, 144, 139, 152, 50, 185, 153, 177, 153, 
      144, 26, 176, 144, 50, 156, 145, 153, 156, 156
]

flag = bytearray(len(enc))
for j, e in enumerate(enc):
    c = 0
    for b in range(7, -1, -1):
        c ^= ((e >> ((b * 5 + 3 ) % 8)) & 0x1)<< b
    flag[(j*37+1) % len(enc)] = c
print(bytes(flag))
```

Et le résultat:
```
b'FCSC{6df723aa33b1aa8d604069a693e5990d411a7f7a7169b70e694b0bdf4d26aa9e}'
```