# Rituel en chaine

**Challenge**

Cette étape est un rituel dans l’apprentissage de l’investigation numérique. Retrouvez le `flag` dans le fichier ci-joint à l’aide des ressources suivantes :

    https://www.nist.gov/itl/ssd/software-quality-group/computer-forensics-tool-testing-program-cftt/cftt-technical-1
    https://www.sans.org/blog/strings-strings-are-wonderful-things/
    https://docs.microsoft.com/en-us/sysinternals/downloads/strings


Fichier: [rituel-en-chaine.img](https://hackropole.fr/filer/fcsc2021-forensics-rituel-en-chaine/public_filer/rituel-en-chaine.img)

**Solution**

*Résolue en 2024 grâce à hackropole.fr*

Comme l'épreuve nous y encourage, on tente de trouver la chaîne `FCSC` dans l'image fournie:

```
❯ strings rituel-en-chaine.img | grep FCSC
FCSC{6b8aa82937ec0ff5a3eaffa74b3b2de2e83c6a94eef6d96b59b50f65e8e35fa9}
```
