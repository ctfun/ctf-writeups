# bofbof - pwn buffer overflow

**Challenge**

Votre but est d'obtenir un shell sur la machine pour lire le fichier flag.txt.

nc challenges2.france-cybersecurity-challenge.fr 4002

SHA256([bofbof](bofbof)) = bd78bff2d72fcc6ebc82f248ea14761d66fc476aca13eed22db541f2960f9fce.


**Solution**

On charge le binaire dans IDA pour voir à quoi ça ressemble. Voici le `main` :

![main](bofbof_ida_main.png)

On voit qu'il existe une fonction `vuln` :

![vuln](bofbof_ida_vuln.png)

La cible est donc que `vuln` soit appelée, puisqu'elle nous donne un shell qui nous permettra de lire le flag. Pour y parvenir, on suit le code :
- une variable locale est initialisée à la valeur 0x4141414141414141.
- une question est posée à l'utilisateur, on lit sa réponse.
- on teste la valeur de la variable locale initiale, si elle n'a pas bougé, on sort.
- si la valeur a bougé, mais qu'elle est différente de 0x1122334455667788, on affiche un petit message d'encouragement et on sort.
- sinon, on appelle `vuln`.

Il s'agit donc d'un exercice de `Buffer Overflow` sans exploitation réelle, juste pour prouver qu'on peut modifier la variable locale. En effet, la réponse à la question est lue par un appel à `gets()` qui ne vérifie pas la taille des données reçues. Ainsi, une réponse trop grande va écraser les valeurs des variables locales situées au-dessus du buffer de réception dans la pile.

Reste à savoir quelle taille de réponse donner pour obtenir le bon débordement. On peut débugger à coup de `gdb`, on peut analyser le code pour calculer la taille, ou simplement bourriner. ;-) La bonne réponse est en fait de fournir une réponse de 39 octets, suivi de la valeur attendue par le binaire (en little-endian) :

```console
$ (python -c 'print("A"*39+"\x88\x77\x66\x55\x44\x33\x22\x11")'; cat -) | nc challenges2.france-cybersecurity-challenge.fr 4002
Comment est votre blanquette ?
>>> 
cat flag.txt
FCSC{ec30a448a777b571734d8d9e4036b3a6e87d1005446f80dffb26c3e4f5cd02ba}
```