# Snake - python

**Challenge**

L'apprentissage du langage Python est très utile en CTF. On vous demande ici de lire le fichier flag.txt.

nc challenges1.france-cybersecurity-challenge.fr 7000

**Solution**

On se connecte et on tombe sur un prompt python :

```python
>>> import os
>>> os.system('ls')
flag.txt
0
>>> os.system('cat flag.txt')
FCSC{d6125af647740672b2899a2ee563a011755ba0d665e852fb360614dd52418d60}
0
```