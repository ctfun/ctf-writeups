# Rituel du boutisme

**Challenge**

Votre rituel continue avec le boutisme ! Recherchez le `flag` dans le fichier ci-joint à l’aide des ressources suivantes :

- https://www.nist.gov/itl/ssd/software-quality-group/computer-forensics-tool-testing-program-cftt/cftt-technical-1
- https://www.sans.org/blog/strings-strings-are-wonderful-things/
- https://docs.microsoft.com/en-us/sysinternals/downloads/strings
- https://fr.wikipedia.org/wiki/Boutisme

Fichier: [rituel-du-boutisme.img](https://hackropole.fr/filer/fcsc2021-forensics-rituel-du-boutisme/public_filer/rituel-du-boutisme.img)

**Solution**

*Résolue en 2024 grâce à hackropole.fr*

Comme l'épreuve nous y encourage, on tente de trouver la chaîne `FCSC` dans l'image fournie:

```
❯ strings rituel-du-boutisme.img | grep FCSC
❯
```

C'est raté, car évidemment, il y a une subtilité de «boutisme». On retente la recherche avec différents paramètres d'encodage, 
et c'est le 16 bits en little endian qui gagne :

```
❯ strings -td -el rituel-du-boutisme.img | grep FCSC
8656899 FCSC{6a8024a83d9ec2d1a9c36c51d0408f15836a043ae0431626987ce2b8960a5937}
```
