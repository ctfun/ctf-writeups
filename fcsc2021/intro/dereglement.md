# Dérèglement - forensics

**Challenge**

Un problème a été rencontré lors de la rédaction du règlement et le fichier au format Microsoft Office est corrompu. Vous devez retrouver son contenu pour obtenir le flag.

SHA256([2021-fcsc-reglement_de_participation.docx](2021-fcsc-reglement_de_participation.docx)) = 7538e4b5a4b392e6810e1bd6360dcf000424cf66c6b1dcc8f296751785c1b020.


**Solution**

Les fichiers docx sont au format Open Office, il s'agit d'une archive zip :

```shell
$ unzip ~/Challenges/ctf-writeups/fcsc2021/intro/2021-fcsc-reglement_de_participation.docx 
Archive:  /home/papy/Challenges/ctf-writeups/fcsc2021/intro/2021-fcsc-reglement_de_participation.docx
  inflating: [Content_Types].xml     
   creating: docProps/
  inflating: docProps/app.xml        
  inflating: docProps/core.xml       
   creating: _rels/
  inflating: _rels/.rels             
   creating: word/
   creating: word/_rels/
  inflating: word/_rels/document.xml.rels  
  inflating: word/document.xml       
  inflating: word/styles.xml         
  inflating: word/numbering.xml      
  inflating: word/settings.xml       
  inflating: word/fontTable.xml      
   creating: word/media/
  inflating: word/media/image1.jpeg  
$ grep -r FCSC
[...]
<w:t xml:space="preserve">En raison de l’annulation de la compétition européenne en 2020 suite à la crise sanitaire, l’âge maximal de participation a été </w:t></w:r><w:r><w:rPr><w:rStyle w:val="StrongEmphasis"/></w:rPr><w:t>relevé d’un an. FCSC{9bc5a6d51022ac}</w:t>
```