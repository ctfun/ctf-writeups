# La PIN - crypto

**Challenge :**

J'ai protégé le flag en le chiffrant avec des algorithmes modernes. Pourrez-vous le retrouver ?

Fichier lapin.py :
```python
from Crypto.Cipher import AES
from Crypto.Protocol.KDF import scrypt
from Crypto.Util.number import long_to_bytes

while True:
	pin = int(input(">>> PIN code (4 digits): "))
	if 0 < pin < 9999:
		break

flag = open("flag.txt", "rb").read()
k = scrypt(long_to_bytes(pin), b"FCSC", 32, N = 2 ** 10, r = 8, p = 1)
aes = AES.new(k, AES.MODE_GCM)
c, tag = aes.encrypt_and_digest(flag)

enc = aes.nonce + c + tag
print(enc.hex())
```

Fichier output.txt:
```console
f049de59cbdc9189170787b20b24f7426ccb9515e8b0250f3fc0f0c14ed7bb1d4b42c09d02fe01e0973a7233d99af55ce696f599050142759adc26796d64e0d6035f2fc39d2edb8a0797a9e45ae4cd55074cf99158d3a64dc70a7e836e3b30382df30de49ba60a
```

**Solution**

Le chiffrement utilisé est peut-être costaud, mais la clef ne dépend que d'un code à 4 chiffres. Il est trivial de tous les énumérer afin de décoder le flag. Il faut juste retrouver les données dans output, sachant que le nonce et le tag font 16 octets. Un exemple en python :

```python
from Crypto.Cipher import AES
from Crypto.Protocol.KDF import scrypt
from Crypto.Util.number import long_to_bytes

nonce = bytearray.fromhex('f049de59cbdc9189170787b20b24f742')
ciphertext = bytearray.fromhex('6ccb9515e8b0250f3fc0f0c14ed7bb1d4b42c09d02fe01e0973a7233d99af55ce696f599050142759adc26796d64e0d6035f2fc39d2edb8a0797a9e45ae4cd55074cf99158d3a6')
tag = bytearray.fromhex('4dc70a7e836e3b30382df30de49ba60a')

for pin in range(1, 9999):
	k = scrypt(long_to_bytes(pin), b"FCSC", 32, N = 2 ** 10, r = 8, p = 1)
	aes = AES.new(k, AES.MODE_GCM, nonce=nonce)
	plaintext = aes.decrypt(ciphertext)
	try:
		aes.verify(tag)
		print(f'Found : {pin=}, {plaintext=}')
		exit(0)
	except ValueError:
	    pass
```

Et voici le résultat de l'exécution :

```console
=> Found : pin=6273, plaintext=b'FCSC{c1feab88e6c6932c57fbaf0c1ff6c32e51f07ae87197fcd08956be4408b2c802}\n'
```