# Guessy - reverse

**Challenge**

Il va probablement vous falloir beaucoup de chance pour deviner le flag. Ou quelques compétences en rétro-ingénierie.

SHA256([guessy](guessy)) = f9388d92cbf5de8c92831555608162c672a99d164aed789638fcc91a66ab13e5.


**Solutions**

On lance le binaire, qui nous demande de saisir le flag par fragment. Il teste alors les fragments un par un et nous indique sa validité ou non. Il suffit d'un peu de rétro-ingéniérie pour déterminer la valeur de ces fragments.

Le premier, dans la fonction `validate()` :

![1](guessing1.png)

Le résultat est trivial : `FCSC{`. On passe à la deuxième fonction, `difficult_part()` :

![2](guessing2.png)

Ici aussi, il s'agit purement de convertir l'hexadécimal en ascii : `e7552cf6`. La suite dans la même fonction :

![3](guessing3.png)

Petite subtilité cette fois-ci, les valeurs sont toutes doublées, ce qui donne : `4ce2e5ad`. La suite au même endroit :

![4](guessing4.png)

Maintenant les valeurs subissent un décalage de 3 bits vers la gauche (ou dit autrement, sont multipliées par 8). Le résultat est `0bb0954f` et la suite :

![5](guessing5.png)

Cette fois les valeurs ascii sont XORées avec `0x30 0x62 0x62 0x30 0x09 0x07 0x57`, sauf la dernière qui vaut exactement `0X60`. Ce fragment vaut `167a02cf` et on arrive à la dernière fonction, `most_difficult_part` :

![6](guessing6.png)

Plutôt facile, c'est l'accolade fermante. ;-) Voici donc le flag complet :

`FCSC{e7552cf64ce2e5ad0bb0954f167a02cf}`