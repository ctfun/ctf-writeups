# Push It to the Limit - web sql

**Challenge**

Exploitez une injection SQL afin de vous connecter sur l'application web.

http://challenges2.france-cybersecurity-challenge.fr:5000/


**Solution**

La résolution est assez évidente, et facilitée par l'appli elle-même. Allons la voir :

![index](pushit_index.png)

On tente une connexion avec un login et un mot de passe bidon. Elle est évidemment refusée, mais le code source de la page nous donne la requête exécutée :

```html
<!-- SELECT * FROM users WHERE username="qsd" AND password="qsd" -->
```

L'injection est alors triviale : essayons la valeur `" or "1"="1` pour le nom d'utilisateur et le mot de passe.

```
Erreur : trop de lignes sont retournées par la requête SQL.
```

Ceci explique le nom de l'épreuve ! Ajoutons donc la limite : on ne touche pas à l'utilisateur, mais pour le mot de passe, on va injecter `" or 1=1 limit 1;--`, et alors :

`FCSC{5012fb37d7886deaa5c4e209cf683286}`