# Clair connu - crypto known-plaintext

**Challenge**
Votre but est de déchiffrer le flag.

- clair-connu.py :
```python
import os
from Crypto.Util.number import long_to_bytes
from Crypto.Util.strxor import strxor

FLAG = open("flag.txt", "rb").read()

key = os.urandom(4) * 20
c = strxor(FLAG, key[:len(FLAG)])
print(c.hex())
```

- output.txt 
```
d91b7023e46b4602f93a1202a7601304a7681103fd611502fa684102ad6d1506ab6a1059fc6a1459a8691051af3b4706fb691b54ad681b53f93a4651a93a1001ad3c4006a825
```

**Solution**

Le code python nous montre que le flag est simplement chiffré par XOR avec une clef de 4 caractères. Or, on peut supposer que ce flag commence, comme d'habitude, par `FCSC`. La taille de cet élément connu est supérieur à la taille de la clef utilisée, on peut donc la retrouver.

On peut faire ça avec un interpréteur python:
```python
>>> from Crypto.Util.strxor import strxor
>>> cipher=bytes.fromhex('d91b7023e46b4602f93a1202a7601304a7681103fd611502fa684102ad6d1506ab6a1059fc6a1459a8691051af3b4706fb691b54ad681b53f93a4651a93a1001ad3c4006a825')
>>> key = strxor(cipher[:4], b'FCSC')
>>> key
b'\x9fX#`'
>>> strxor(cipher, (key*100)[:len(cipher)])
b'FCSC{3ebfb1b880d802cb96be0bb256f4239c27971310cdfd1842083fbe16b3a2dcf7}'
```