Protocole simplifié de discussions relayées par internet (CompactIRC / CIRC)
===========================================================================

### Statut de ce mémo

Ce mémo définit un protocole expérimental pour la communauté internet.
Veuillez vous référer à la version courante de "IAB Official Protocol Standards" pour connaître l'état
de standardisation et le statut de ce protocole. La distribution de ce mémo n'est pas limitée.

### Résumé

Le protocole CIRC a été développé au cours des 4 dernières années. Il a été initialement implémenté pour permettre a des
utilisateurs d'un serveur de discuter entre eux.
Il est utilisé sur un réseau mondial de serveurs et de clients, et a du mal à gérer sa croissance.
Au cours des deux dernières années, le nombre moyen d'utilisateurs connectés au réseau CIRC principal a été multiplié
par 10.

Le protocole CIRC est un protocole en mode texte, dont le client le plus simple est n'importe quel programme TCP capable
de se connecter à un serveur.

## 1. Introduction

Le protocole CIRC (Compact Internet Relay Chat) a été conçu pendant de nombreuses années pour l'usage de conférences en
mode texte. Ce document décrit le protocole CIRC actuel.

Le protocole CIRC a été développé sur des systèmes utilisant le protocole réseau TCP/IP, bien qu'il n'y ait pas de
raison que cela reste la seule sphère dans laquelle il opère.

CIRC, en lui-même, est un système de téléconférence qui (grâce à l'utilisation d'un modèle client/serveur) est adapté à
une exécution sur de nombreuses machines, de façon distribuée. Une configuration type comprend un processus unique (le
serveur) qui fournit un point d'accès pour les clients , et qui traite l'acheminement / le
multiplexage requis de messages, ainsi que d'autres fonctions.


### 1.1 Serveurs
Le serveur est la colonne vertébrale de CIRC. Il fournit un point auquel les clients peuvent se connecter pour parler
entre eux.

### 1.2 Clients

Un client est tout ce qui se connecte à un serveur et qui n'est pas un autre serveur. Chaque client est différencié des
autres clients par un pseudonyme unique ayant une longueur maximale de neuf (9) caractères. Voir dans les règles de
grammaire du protocole ce qui est autorisé et ce qui ne l'est pas dans un pseudonyme. En plus du pseudonyme, tous les
serveurs doivent connaître les informations suivantes sur tous les clients : le vrai nom de l'hôte sur lequel le client
est exécuté, le nom de l'utilisateur du client sur cet hôte, et le serveur auquel le client est connecté.

### 1.2.1 Opérateurs

Pour permettre de maintenir un niveau d'ordre raisonnable dans un réseau IRC, une catégorie de clients spéciale (les
opérateurs) est autorisée à exécuter des fonctions de maintenance générale sur le réseau. Bien que les pouvoirs donnés
aux opérateurs puissent être considérés comme 'dangereux', ils sont néanmoins indispensables. Les opérateurs doivent
être capables de faire certaines tâches de base, telles que la déconnexion et la reconnexion de serveurs, ce qui est
nécessaire pour prévenir les problèmes à long terme de mauvais routage réseau. Étant donnée cette nécessité, le
protocole décrit ici n'autorise que les opérateurs à effectuer ces fonctions.


### 1.3 Les canaux

Un canal est un groupe nommé d'un ou plusieurs clients qui recevront tous les messages adressés à ce canal. Les canaux
sont créés implicitement quand le premier client y accède, et le canal disparaît lorsque le dernier client le quitte.
Tant qu'un canal existe, tous les clients peuvent y accéder en utilisant le nom du canal.

Les noms de canaux sont des chaînes de caractères (commençant par un caractère '#') d'une longueur maximale de
200 caractères. En dehors du fait que le premier caractère doive être un '#', la seule restriction sur le nom
d'un canal est qu'il ne peut pas contenir d'espace (' '), de contrôle G (^G ou ASCII 7), ou de virgule (',' qui est
utilisée comme séparateur de liste dans le protocole).

Pour créer un nouveau canal, ou pour faire partie d'un canal existant, un utilisateur doit accéder au canal. Si le canal
n'existe pas avant l'accès, le canal est créé et l'utilisateur créateur devient opérateur de canal. Si le canal existait
déjà au moment de l'accès, l'autorisation ou non d'accès dépend du mode du canal. Par exemple, si le canal est en "
invités uniquement" (+i), vous ne pourrez joindre le canal que si vous êtes invité. Le protocole spécifie qu'un
utilisateur peut être membre de plusieurs canaux à la fois, mais une limite de dix (10) canaux est recommandée comme
étant amplement suffisante aussi bien pour les utilisateurs novices que pour les experts.

## 2. La spécification IRC

### 2.1 Aperçu

Le protocole décrit ici vaut aussi bien pour les connexions des serveurs que pour celles des clients.

### 2.2 Les jeux de caractères

Aucun jeu de caractères n'est imposé. Le protocole est basé sur un jeu de caractères de huit (8) bits, qui forment un
octet ; cependant, certains codes sont utilisés en tant que codes de contrôle, et agissent comme délimiteurs de
messages.

Indépendamment du fait qu'il s'agisse d'un protocole 8 bits, les délimiteurs et les mots-clés sont tels que le protocole
est utilisable d'un terminal US-ASCII et d'une connexion telnet.

Les caractères {}| sont considérés comme étant respectivement les équivalents en minuscules des caractères \[\]\\.
Ceci est particulièrement important pour déterminer l'équivalence de deux pseudonymes.

### 2.3 Messages

Les serveurs et les clients s'envoient chacun des messages qui peuvent ou non générer une réponse. Si le message
contient une commande valide, comme il est décrit dans les sections suivantes, le client devrait s'attendre à une
réponse comme spécifié, mais il n'est pas conseillé d'attendre éternellement une réponse. La communication de client à
serveur est essentiellement de nature asynchrone.

Chaque message CIRC peut contenir jusqu'à trois parties : le préfixe (optionnel), la commande, et les paramètres de la
commande. Le préfixe, la commande, et tous les paramètres sont séparés par un (ou plusieurs) caractère(s) ASCII espace
(0x20).

La présence d'un préfixe est indiquée par un simple caractère ASCII deux-points (':', 0x3b), qui doit être le premier
caractère du message. Il ne doit pas y avoir de trou (d'espace blanc) entre les deux-points et le préfixe. Le préfixe
est utilisé pour indiquer la véritable origine du message. S'il n'y a pas de préfixe, le message est considéré comme
ayant pour origine la connexion de laquelle il est issu. Les clients ne doivent pas utiliser de préfixe lorsqu'ils
envoient un message d'eux-mêmes. S'il utilise un préfixe, le seul valable est le pseudonyme associé au client. Si la
source identifiée par le préfixe n'est pas trouvée dans la base de données interne du serveur, ou si la source est
enregistrée avec une liaison différente de celle avec laquelle le message est arrivé, le serveur doit ignorer le message
en silence.

La commande doit être soit une commande CIRC valide, soit un nombre de trois (3) chiffres représentés en texte ASCII.

Les messages IRC sont toujours des lignes de caractères terminés par une paire CR-LF (retour chariot - saut de ligne),
et ces messages ne doivent pas dépasser 512 caractères de long, en comptant tous les caractères y compris le CR-LF
final. Donc, il y a un maximum de 510 caractères autorisés pour la commande et ses paramètres. Il n'y a pas de système
permettant une ligne de continuation de message.

### 2.3.1 Le format de message en 'pseudo' BNF

Les messages du protocole doivent être extraits du flux continu d'octets. La solution actuelle consiste à désigner deux
caractères donnés, CR et LF, comme séparateurs de messages. Les messages vides sont ignorés en silence, ce qui permet
l'usage d'une suite de CR-LF entre les messages sans problèmes supplémentaires.

Le message extrait est décomposé en un <préfixe>, <commande> et liste de paramètres correspondant soit au
composant <milieu> ou <fin>.

La représentation BNF de ceci est :  
<message> ::= \[':' <préfixe> <espace> \] <command> <params> <crlf>  
<préfixe> ::= <nom de serveur> | <pseudo> \[ '!' <utilisateur> \] \[ '@' <hôte> \]  
<command> ::= <lettre> { <lettre> } | <nombre> <nombre> <nombre>  
<espace> ::= ' ' { ' ' }  
<params> ::= <espace> \[ ':' <fin> | <milieu> <params> \]

<milieu> ::= <Toute séquence **non vide** d'octets à l'exclusion de ESPACE, NUL, CR, LF, le premier d'entre eux étant différent de ':'>  
<fin> ::= <Toute suite, éventuellement vide, d'octets, à l'exception de NUL, CR et LF>  
<crlf> ::= CR LF

NOTES:

1) <espace> est constitué uniquement de caractère(s) ESPACE (0x20). Notez particulièrement que la TABULATION et tout autre caractère de contrôle sont considérés comme ESPACE-NON-BLANC.

2) Après avoir extrait la liste de paramètres, tous les paramètres sont égaux, et correspondent soit à <milieu> soit
   à <fin>. <Fin> est une simple astuce syntaxique pour autoriser ESPACE dans le paramètre.

3) Le fait que CR et LF ne puissent pas apparaître dans la chaîne paramètre est une simple assertion. Cela pourrait
   changer dans le futur.

4) Le caractère NUL n'est pas spécial dans la construction du message, et pourrait a priori être à l'intérieur d'un
   message, mais cela complexifierait la gestion ordinaire des chaînes en C. C'est pourquoi NUL n'est pas autorisé dans
   les messages.

5) Le dernier paramètre peut être une chaîne vide.

La plupart des messages du protocole spécifient une sémantique additionnelle, et la syntaxe pour les chaînes de
paramètres extraites est dictée par leur position dans la liste. Par exemple, de nombreuses commandes de serveurs vont
considérer que le premier paramètre après la commande est la liste de destinataires, ce qui peut être décrit avec :  
<destinataire> ::= <à> \[ "," < destinataire > \]  
<à> ::= <canal> | < utilisateur > '@' <nom de serveur> | <pseudo> | <masque>  
<canal> ::= ('#' | '&') <chaîne canal>  
<nom de serveur> ::= <hôte>  
<hôte> ::= voir RFC 952 \[DNS:4\] pour les détails sur les noms d'hôte autorisés  
<pseudo> ::= <lettre> { <lettre> | <nombre> | <spécial> }  
<masque> ::= ('#' | '$') <chaîne canal>  
<chaîne canal> ::= <n'importe quel code 8bits excepté ESPACE, BELL, NUL, CR, LF et virgule (,)>

Les autres paramètres sont :  
<utilisateur> ::= <non blanc> { <non blanc> }  
<lettre> ::= 'a' ... 'z' | 'A' ... 'Z'  
<nombre> ::= '0' ... '9'  
<spécial> ::= '-' | '\[' | '\]' | '\\' | '\`' | '^' | '{' | '}'

<non blanc> ::= <n'importe quel code 8bits excepté ESPACE (0x20), NUL(0x0), CR(0xd), et LF(0xa) >  

### 2.4 Réponses numériques

La plupart des messages envoyés aux serveurs génèrent une réponse. Les réponses les plus courantes sont des réponses
numériques, aussi bien pour les messages d'erreurs que pour les réponses normales. La réponse numérique est envoyée
comme un message contenant le préfixe de l'expéditeur, le nombre de trois chiffres, et le destinataire de la réponse.
Une réponse numérique ne peut pas émaner d'un client, et tout message de ce style reçu par un serveur doit être ignoré
silencieusement. Hormis cela, une réponse numérique est un message comme un autre, si ce n'est que le mot-clé est
composé de trois chiffres, plutôt que d'une suite de lettre. Une liste des réponses possibles est fournie à la
section correspondante.

## 3. Concepts CIRC

Cette section décrit les concepts sous-jacents à l'organisation du protocole IRC et comment les implémentations
actuelles délivrent les différentes classes de messages.

### 3.2.2 À un groupe (canal)

Sur CIRC, un canal a un rôle équivalent à celui d'un groupe de multi-diffusion ; son existence est dynamique (il va et
vient au fur et à mesure que les gens accèdent et quittent le canal).

Exemple :

Pour tout canal qui contient un seul client, les messages du canal vont au serveur et nulle part ailleurs.

Exemple :

Il y a deux clients dans un canal. Tous les messages traversent le chemin comme s'ils étaient des messages privés entre
les deux clients en dehors du canal.

Exemple :

Les clients 1 et 2 sont dans un canal. Tous les messages adressés à un canal sont envoyés à tous les clients.
Si le client 1 envoie un message, il est envoyé au client 2.


### 3.3.1 Client à client

Il n'y a pas de classe de message qui, à partir d'un simple message, résulte en un message envoyé à tous les autres
clients.


### 3.3.2 Client à serveur

La plupart des commandes qui résultent en un changement d'état (tels que l'appartenance à un canal, le mode d'un canal,
le statut d'un utilisateur, etc.) doivent être, par défaut, envoyés au serveur, et le client ne peut pas
changer cette distribution.

## 4. Détails des messages

Les pages suivantes décrivent les messages reconnus par les serveurs CIRC et les clients. Toutes les commandes décrites
dans cette section doivent être implémentées par tous les serveurs utilisant ce protocole.

Si la réponse ERR\_NOSUCHSERVER est reçue, cela signifie que le paramètre <serveur> n'a pas été trouvé. Le serveur ne
doit alors plus envoyer d'autres réponses pour cette commande.

Le serveur auquel un client est connecté doit traiter le message complet, et retourner les messages d'erreur appropriés.
Si le serveur rencontre une erreur fatale en décomposant un message, une erreur doit être envoyée au client et la
décomposition interrompue. Peuvent être considérés comme une erreur fatale une commande incorrecte, une destination
inconnue du serveur (noms de serveur, pseudo, et noms de canaux entrent dans cette catégorie), un nombre de paramètres
insuffisant, ou un niveau de privilège insuffisant.

Si un jeu de paramètres complet est présent, la validité de chacun d'entre eux doit être vérifiée, et les réponses
appropriées envoyées au client. Dans le cas de messages dont la liste de paramètres utilise une virgule comme
séparateur, une réponse doit être envoyée à chaque élément.

Dans les exemples suivants, certains messages apparaissent au format complet :  
:Nom COMMANDE paramètre liste

Ces exemples représentent un message de "Nom" dans le transfert entre serveurs, où il est essentiel d'inclure le nom de
l'expéditeur originel du message, de façon à ce que les serveurs distants puissent renvoyer un message le long d'un
chemin valide.

## 4.1 Établissement de connexion

Les commandes décrites dans cette section sont utilisées pour établir une connexion avec un serveur IRC par
un client, ainsi qu'une déconnexion correcte.

L'ordre des commandes recommandé pour l'enregistrement d'un client est le suivant :  
1\. Message UTILISATEUR
2\. Message PSEUDO

### 4.1.3 Message UTILISATEUR

Commande: UTILISATEUR  
Paramètres: <nom d'utilisateur> <hôte> <nom de serveur> <nom réel>

Le message UTILISATEUR est utilisé au début d'une connexion pour spécifier le nom d'utilisateur, le nom d'hôte, le nom de
serveur, et le véritable nom d'un nouvel utilisateur.

Notez aussi que le paramètre 'vrai nom' doit être le dernier paramètre, car il peut contenir des espaces, et il doit
être préfixé par deux points (':') de façon à être reconnu comme tel.

Réponses numériques :

           ERR\_NEEDMOREPARAMS              ERR\_ALREADYREGISTRED

Exemples:

UTILISATEUR guest tolmoon tolsun :Ronnie Reagan ; Utilisateur s'enregistrant avec un nom d'utilisateur de "guest" et un vrai
nom de "Ronnie Reagan".

### 4.1.2 Message PSEUDO

Commande : PSEUDO  
Paramètres : <pseudonyme>

Le message PSEUDO est utilisé pour donner un pseudonyme à un utilisateur, ou pour changer le pseudonyme précédent.

Si un message PSEUDO arrive à un serveur qui connaît déjà un autre client de pseudo identique, une collision de
pseudonymes a lieu. Le résultat d'une collision de pseudonymes est la suppression de toutes les instances du pseudonyme
de la base du serveur, et l'envoi d'un message KILL afin de retirer le pseudonyme des bases de données de tous les
autres serveurs. Si le message PSEUDO à l'origine de la collision de pseudonymes est un changement de pseudonyme, alors le
pseudo originel (l'ancien) doit aussi être retiré.

Si le serveur reçoit un PSEUDO identique d'un client auquel il est connecté directement, il peut envoyer un
ERR\_NICKCOLLISION au client local, ignorer la commande PSEUDO, et ne pas générer de KILLs.

Réponses numériques :

           ERR\_NONICKNAMEGIVEN             ERR\_ERRONEUSNICKNAME
           ERR\_NICKNAMEINUSE               ERR\_NICKCOLLISION

Exemples:

PSEUDO Wiz ; Ajout d'un nouveau pseudon, ou change si deja existant, "Wiz".

### 4.1.6 Message QUITTER

Commande: QUITTER  
Paramètres: \[<Message de départ >\]

Une session de client se termine par un message QUITTER. Le serveur doit rompre la connexion avec le client qui envoie un
message QUITTER. Si un <Message de départ> est fourni, il sera transmis au lieu du message par défaut, le pseudonyme.

Si pour une autre raison, une connexion d'un client est fermée sans que le client ait envoyé de message QUITTER (par
exemple, le programme client se termine, et une fin de fichier est envoyée sur la socket), le serveur doit remplir le
message QUITTER avec un message reflétant la nature de l'événement à l'origine de cette déconnexion.

Réponses numériques:

           Aucune.

Exemples:

QUITTER :Parti déjeuner ; Format de message préféré.

## 4.2 Opérations sur les canaux

Ce groupe de messages s'intéresse à la manipulation de canaux, à leurs propriétés (mode des canaux), et à leur contenu (
typiquement des clients). En implémentant ces commandes, la résolution de conflits entre les clients est inévitable, par
exemple lorsque deux clients à deux extrémités du réseau envoient des commandes incompatibles. Il est aussi nécessaire
aux serveurs de garder l'historique d'un pseudonyme afin que, quand un paramètre <pseudo> est donné, le serveur puisse
vérifier dans l'historique si le pseudo n'a pas changé récemment.

### 4.2.1 Message JOIN

Commande: JOIN  
Paramètres: <canal>

La commande JOIN est utilisée par un client pour commencer à écouter un canal spécifique. L'accès à un canal est
autorisé ou non uniquement par le serveur auquel le client est connecté.

Une fois qu'un utilisateur a accès à un canal, il reçoit des notifications de toutes les commandes que son serveur
reçoit et qui affectent le canal. Cela inclut MODE, PART, QUITTER, et bien sûr PRIVMSG.

Si un JOIN a lieu avec succès, on envoie à l'utilisateur le sujet du canal (en utilisant RPL\_TOPIC) et la liste des
utilisateurs du canal (en utilisant RPL\_NAMREPLY), y compris lui-même.

Réponses numériques :

           ERR\_NEEDMOREPARAMS              ERR\_BANNEDFROMCHAN
           ERR\_INVITEONLYCHAN              ERR\_BADCHANNELKEY
           ERR\_CHANNELISFULL               ERR\_BADCHANMASK
           ERR\_NOSUCHCHANNEL               ERR\_TOOMANYCHANNELS
           RPL\_TOPIC

Exemples:

JOIN #foobar ; accède au canal #foobar.

JOIN #foo,#bar ; accède au canaux #foo and #bar.

### 4.2.2 Message PART

Commande: PART  
Paramètres: <canal>{,< canal >}

Le message PART provoque le retrait du client expéditeur de la liste des utilisateurs actifs pour tous les canaux listés
dans la chaîne de paramètres.

Réponses numériques:

           ERR\_NEEDMOREPARAMS              ERR\_NOSUCHCHANNEL
           ERR\_NOTONCHANNEL  

Exemples:

PART #twilight_zone ; quitte le canal "#twilight_zone"

PART #oz-ops,&group5 ; quitte les canaux "&group5" et "#oz-ops".


### 4.2.4 Message SUJET

Commande: SUJET  
Paramètres: <canal> \[<sujet>\]

Le message SUJET est utilisé pour modifier ou voir le sujet d'un canal. Le sujet du canal <canal> est renvoyé s'il n'y a
pas de <sujet> fourni en paramètre.

Réponses numériques :

           ERR\_NEEDMOREPARAMS              ERR\_NOTONCHANNEL
           RPL\_NOTOPIC                     RPL\_TOPIC
           ERR\_CHANOPRIVSNEEDED  

Exemples:

:Wiz TOPIC #test :New topic ; L'utilisateur Wiz définit le sujet.  
SUJET #test :autre sujet ; Change le sujet du canal #test en "autre sujet".

### 4.2.6 Message LIST

Commande: LIST  
Paramètres: \[<canal>{,<canal>} \[<serveur>\]\]

Le message LIST est utilisé pour lister les canaux et leur sujet. Si le paramètre <canal> est utilisé, seul le statut de
ces canaux est affiché. Les canaux privés sont listés (sans leur sujet) comme canal "Prv" à moins que le client qui
génère la requête soit effectivement sur le canal. De même, les canaux secrets ne sont pas listés du tout, à moins que
le client soit un membre du canal en question.

Réponses numériques :

           ERR\_NOSUCHSERVER                RPL\_LISTSTART
           RPL\_LIST                        RPL\_LISTEND

Exemples:

LIST ; Liste tous les canaux.  
LIST #twilight\_zone,#42 ; Liste les canaux #twilight\_zone et #42

## 4.4 Envoi de messages

Le but principal du protocole IRC est de fournir une base afin que des clients puissent communiquer entre eux. PRIVMSG
et NOTICE sont les seuls messages disponibles qui réalisent effectivement l'acheminement d'un message textuel d'un
client à un autre - le reste le rend juste possible et assure que cela se passe de façon fiable et structurée.

### 4.4.1 Messages privés

Commande: PRIVMSG  
Paramètres: <destinataire>{,<destinataire>} <texte à envoyer >

PRIVMSG est utilisé pour envoyer un message privé entre des utilisateurs. <destinataire> est le pseudonyme du
destinataire du message. <destinataire> peut aussi être une liste de noms ou de canaux, séparés par des virgules.

Le paramètre <destinataire> peut aussi être un masque d'hôte (masque #) ou un masque de serveur (masque $). Le masque
doit contenir au moins un (1) ".", et aucun joker après le dernier ".". Cette limitation a pour but d'empêcher les gens
d'envoyer des messages à "#\*" ou à "$\*", ce qui provoquerait la diffusion à tous les utilisateurs ; l'expérience
montre qu'on en abuse plus qu'on en use intelligemment, de façon responsable. Les jokers sont les caractères '\*' et '
?'. Ces extensions de PRIVMSG ne sont accessibles qu'aux opérateurs.

Réponses numériques:

           ERR\_NORECIPIENT                 ERR\_NOTEXTTOSEND
           ERR\_CANNOTSENDTOCHAN            ERR\_NOTOPLEVEL
           ERR\_WILDTOPLEVEL                ERR\_TOOMANYTARGETS
           ERR\_NOSUCHNICK
           RPL\_AWAY

Exemples:

:Angel PRIVMSG Wiz :Salut, est-ce que tu reçois ce message ? ; Message d'Angel à Wiz.  
PRIVMSG Angel :oui, je le reçois ! ; Message à Angel.  
PRIVMSG jto@tolsun.oulu.fi :Hello ! ; Message à un client du serveur tolsun.oulu.fi dont le nom est "jto".  
PRIVMSG $\*.fi :Server tolsun.oulu.fi rebooting. ; Message à tous sur les serveurs dont les noms correspondent à
\*.fi.  
PRIVMSG #\*.edu :NSFNet is undergoing work, expect interruptions ; Message à tous les utilisateurs qui viennent d'un
hôte dont le nom correspond à \*.edu.

## 4.5 Requêtes basées sur les utilisateurs

Les requêtes utilisateurs sont un groupe de commandes dont le but principal est la recherche d'informations sur un
utilisateur particulier, ou sur un groupe d'utilisateurs. Lorsqu'on utilise des jokers avec ces commandes, elles ne
renvoient les informations que sur les utilisateurs qui vous sont 'visibles'. La visibilité d'un utilisateur est
déterminée par la combinaison du mode de cet utilisateur, et des canaux sur lesquels tous les deux êtes.

### 4.5.1 Requête WHO

Commande: WHO  
Paramètres: \[<nom> \[<o>\]\]

Le message WHO est utilisé par un client pour générer une requête qui renvoie une liste d'informations qui correspondent
au paramètre <nom> donné par le client. Si le paramètre nom est absent, tous les utilisateurs visibles (ceux qui ne sont
pas invisibles (mode utilisateur +i) et qui n'ont pas de canal en commun avec le client émettant la requête) sont
listés. Le même résultat peut être obtenu en utilisant le <nom> "0" ou tout joker correspondant à toutes les entrées
possibles.

Le <nom> passé en paramètre est mis en correspondance avec les hôtes des utilisateurs, leurs véritables noms, et leurs
pseudonymes si le canal <nom> n'est pas trouvé.

Si le paramètre "o" est passé, seuls les opérateurs sont listés, et ce, en fonction du masque fourni.

Réponses numériques :

           ERR\_NOSUCHSERVER
           RPL\_WHOREPLY                    RPL\_ENDOFWHO

Exemples:

WHO \*.fi ; Liste tous les utilisateurs qui correspondent à "\*.fi".  
WHO jto\* o ; Liste tous les utilisateurs qui correspondent à "jto\*", s'ils sont opérateurs.

### 4.5.2 Requête WHOIS

Commande: WHOIS  
Paramètres: \[<serveur>\] <masque de pseudo>\[,<masque de pseudo>\[,...\]\]

Ce message est utilisé pour obtenir des informations sur un utilisateur donné. Le serveur répondra à ce message avec des
messages numériques indiquant les différents statuts de chacun des utilisateurs qui correspondent
au <masque de pseudo> (si vous pouvez les voir). S'il n'y a pas de joker dans le <masque de pseudo>, toutes les
informations auxquelles vous avez accès au sujet de ce pseudo seront présentées. On peut séparer la liste des
pseudonymes avec une virgule (',').

La dernière version envoie une requête à un serveur spécifique. C'est utile si vous voulez savoir depuis combien de
temps l'utilisateur concerné a été oisif, car seul le serveur local (celui auquel cet utilisateur est directement
connecté) connaît cette information, alors que tout le reste est connu par tous les serveurs.

Réponses numériques :

           ERR\_NOSUCHSERVER                ERR\_NONICKNAMEGIVEN
           RPL\_WHOISUSER                   RPL\_WHOISCHANNELS
           RPL\_WHOISCHANNELS               RPL\_WHOISSERVER
           RPL\_AWAY                        RPL\_WHOISOPERATOR
           RPL\_WHOISIDLE                   ERR\_NOSUCHNICK
           RPL\_ENDOFWHOIS  

Exemples:

WHOIS wiz ; renvoie les informations disponibles sur le pseudo WiZ  
WHOIS eff.org trillian ; demande au serveur eff.org les informations concernant trillian


## 4.6 Messages divers

Les messages de cette catégorie ne font partie d'aucune des catégories ci-dessus, mais font néanmoins partie intégrante
du protocole, et sont indispensables.

### 4.6.2 Message PING

Commande: PING  
Paramètres: <message>

Le message PING est utilisé pour tester la présence d'un client actif à l'autre bout de la connexion. Un message PING
est envoyé régulièrement si aucune activité n'est détectée sur une connexion. Si la connexion ne répond pas à la
commande PING dans un certain délai, la connexion est fermée.

Tout client qui reçoit un message PING doit répondre au serveur qui a envoyé le message aussi
rapidement que possible, avec un message PONG approprié pour indiquer qu'il est toujours là et actif. Les serveurs ne
doivent pas répondre aux commandes PING, mais se fier au PING dans l'autre sens pour indiquer que la connexion est
toujours active.

Réponses numériques :

           ERR\_NOORIGIN                    ERR\_NOSUCHSERVER

Exemples:

PING tolsun.oulu.fi ; serveur envoyant un message PING à un autre serveur pour indiquer qu'il est toujours actif.  
PING WiZ ; message PING envoyé au pseudo WiZ

### 4.6.3 Message PONG

Commande: PONG  
Paramètres: <message>

Le message PONG est la réponse à un message PING. Si le paramètre <message> est donné, le message doit être transmis au
serveur.

Réponses numériques :

           ERR\_NOORIGIN                    ERR\_NOSUCHSERVER

Exemples:

PONG csd.bu.edu tolsun.oulu.fi ; message PONG de csd.bu.edu à tolsun.oulu.fi

6. Réponses
   ===========

Ce qui suit est une liste de réponses numériques générées à la suite des commandes spécifiées ci-dessus. Chaque réponse
numérique est donnée avec son numéro, son nom, et sa chaîne de réponse (en anglais).

6.1 Réponses d'erreur
---------------------

401 ERR\_NOSUCHNICK

"<pseudonyme> :No such nick/channel"

Utilisé pour indiquer que le pseudonyme passé en paramètre à la commande n'est pas actuellement utilisé.

402 ERR\_NOSUCHSERVER

"<nom de serveur> :No such server"

Utilisé pour indiquer que le nom du serveur donné n'existe pas actuellement.

403 ERR\_NOSUCHCHANNEL

"<nom de canal> :No such channel"

Utilisé pour indiquer que le nom de canal donné est invalide.

404 ERR\_CANNOTSENDTOCHAN

"<nom de canal> :Cannot send to channel"

Envoyé à un utilisateur qui (a) soit n'est pas dans un canal en mode +n ou (b) n'est pas opérateur (ou mode +v) sur un
canal en mode +m ; et essaie d'envoyer un PRIVMSG à ce canal.

405 ERR\_TOOMANYCHANNELS

"<nom de canal> :You have joined too many channels"

Envoyé à un utilisateur quand il a atteint le nombre maximal de canaux qu'il est autorisé à accéder simultanément, s'il
essaie d'en rejoindre un autre.

406 ERR\_WASNOSUCHNICK

"<nom de canal> :There was no such nickname"

Renvoyé par WHOWAS pour indiquer qu'il n'y a pas d'information dans l'historique concernant ce pseudonyme.

407 ERR\_TOOMANYTARGETS

"<destination> :Duplicate recipients. No message delivered"

Renvoyé à un client qui essaie d'envoyer un PRIVMSG/NOTICE utilisant le format de destination utilisateur@hôte pour
lequel utilisateur@hôte a plusieurs occurrences.

409 ERR\_NOORIGIN

":No origin specified"

Message PING ou PONG sans le paramètre origine qui est obligatoire puisque ces commandes doivent marcher sans préfixe.

411 ERR\_NORECIPIENT

":No recipient given (<commande>)"

Pas de destinataire.

412 ERR\_NOTEXTTOSEND

":No text to send"

Pas de texte à envoyer.

413 ERR\_NOTOPLEVEL

"<masque> :No toplevel domain specified"

Domaine principal non spécifié.

414 ERR\_WILDTOPLEVEL

"<masque> :Wildcard in toplevel domain"

Joker dans le domaine principal

Les erreurs 412-414 sont renvoyées par PRIVMSG pour indiquer que le message n'a pas été délivré. ERR\_NOTOPLEVEL et
ERR\_WILDTOPLEVEL sont les erreurs renvoyées lors d'une utilisation invalide de "PRIVMSG $<serveur>" ou de "PRIVMSG #<
hôte>".

421 ERR\_UNKNOWNCOMMAND

"<commande> :Unknown command"

Renvoyé à un client enregistré pour indiquer que la commande envoyée est inconnue du serveur.

422 ERR\_NOMOTD

":MOTD File is missing"

Le fichier MOTD du serveur n'a pas pu être ouvert.

423 ERR\_NOADMININFO

"<serveur> :No administrative info available"

Renvoyé par un serveur en réponse à un message ADMIN quand il y a une erreur lors de la recherche des informations
appropriées.

424 ERR\_FILEERROR

":File error doing <opération> on <fichier>"

Message d'erreur générique utilisé pour rapporter un échec d'opération de fichier durant le traitement d'un message.

431 ERR\_NONICKNAMEGIVEN

":No nickname given"

Renvoyé quand un paramètre pseudonyme attendu pour une commande n'est pas fourni.

432 ERR\_ERRONEUSNICKNAME

"<pseudo> :Erroneus nickname"

Renvoyé après la réception d'un message PSEUDO qui contient des caractères qui ne font pas partie du jeu autorisé. Voir
les sections [1](#1) et [2.2](#22) pour les détails des pseudonymes valides.

433 ERR\_NICKNAMEINUSE

"<nick> :Nickname is already in use"

Renvoyé quand le traitement d'un message PSEUDO résulte en une tentative de changer de pseudonyme en un déjà existant.

436 ERR\_NICKCOLLISION

"<nick> :Nickname collision KILL"

Renvoyé par un serveur à un client lorsqu'il détecte une collision de pseudonymes (enregistrement d'un pseudonyme qui
existe déjà sur un autre serveur).

441 ERR\_USERNOTINCHANNEL

"<pseudo> <canal> :They aren't on that channel"

Renvoyé par un serveur pour indiquer que l'utilisateur donné n'est pas dans le canal spécifié.

442 ERR\_NOTONCHANNEL

"<canal> :You're not on that channel"

Renvoyé par le serveur quand un client essaie une commande affectant un canal dont il ne fait pas partie.

443 ERR\_USERONCHANNEL

"<utilisateur> <channel> :is already on channel"

Renvoyé quand un client essaie d'inviter un utilisateur sur un canal où il est déjà.

444 ERR\_NOLOGIN

"<utilisateur> :User not logged in"

Renvoyé par un SUMMON si la commande n'a pas pu être accomplie, car l'utilisateur n'est pas connecté.

445 ERR\_SUMMONDISABLED

":SUMMON has been disabled"

Renvoyé en réponse à une commande SUMMON si le SUMMON est désactivé. Tout serveur qui ne gère pas les SUMMON doit
retourner cette valeur.

446 ERR\_USERSDISABLED

":USERS has been disabled"

Retourné en réponse à une commande USERS si la commande est désactivée. Tout serveur qui ne gère pas les USERS doit
retourner cette valeur.

451 ERR\_NOTREGISTERED

":You have not registered"

Retourné par le serveur pour indiquer à un client qu'il doit être enregistré avant que ses commandes soient traitées.

461 ERR\_NEEDMOREPARAMS

"<commande> :Not enough parameters"

Renvoyé par un serveur par de nombreuses commandes, afin d'indiquer que le client n'a pas fourni assez de paramètres.

462 ERR\_ALREADYREGISTRED

":You may not reregister"

Retourné par le serveur à tout lien qui tente de changer les détails enregistrés (tels que mot de passe et détails
utilisateur du second message UTILISATEUR)

463 ERR\_NOPERMFORHOST

":Your host isn't among the privileged"

Renvoyé à un client qui essaie de s'enregistrer sur un serveur qui n'accepte pas les connexions depuis cet hôte.

464 ERR\_PASSWDMISMATCH

":Password incorrect"

Retourné pour indiquer l'échec d'une tentative d'enregistrement d'une connexion dû à un mot de passe incorrect ou
manquant.

465 ERR\_YOUREBANNEDCREEP

":You are banned from this server"

Retourné après une tentative de connexion et d'enregistrement sur un serveur configuré explicitement pour vous refuser
les connexions.

467 ERR\_KEYSET

"<canal> :Channel key already set"

Clé de canal déjà définie.

471 ERR\_CHANNELISFULL

"<canal> :Cannot join channel (+l)"

Impossible de joindre le canal (+l)

472 ERR\_UNKNOWNMODE

"<caractère> :is unknown mode char to me"

Mode inconnu.

473 ERR\_INVITEONLYCHAN

"<canal> :Cannot join channel (+i)"

Impossible de joindre le canal (+i).

474 ERR\_BANNEDFROMCHAN

"<canal> :Cannot join channel (+b)"

Impossible de joindre le canal (+b).

475 ERR\_BADCHANNELKEY

"<canal> :Cannot join channel (+k)"

Impossible de joindre le canal (+k).

481 ERR\_NOPRIVILEGES

":Permission Denied- You're not an IRC operator"

Toute commande qui requiert le privilège d'opérateur pour opérer doit retourner cette erreur pour indiquer son échec.

482 ERR\_CHANOPRIVSNEEDED

"<canal> :You're not channel operator"

Toute commande qui requiert les privilèges 'chanop' (tels les messages MODE) doit retourner ce message à un client qui
l'utilise sans être chanop sur le canal spécifié.

483 ERR\_CANTKILLSERVER

":You cant kill a server!"

Toute tentative d'utiliser la commande KILL sur un serveur doit être refusée et cette erreur renvoyée directement au
client.

491 ERR\_NOOPERHOST

":No O-lines for your host"

Si un client envoie un message OPER et que le serveur n'a pas été configuré pour autoriser les connexions d'opérateurs
de cet hôte, cette erreur doit être retournée.

501 ERR\_UMODEUNKNOWNFLAG

":Unknown MODE flag"

Renvoyé par un serveur pour indiquer que le message MODE a été envoyé avec un pseudonyme et que le mode spécifié n'a pas
été identifié.

502 ERR\_USERSDONTMATCH

":Cant change mode for other users"

Erreur envoyée à tout utilisateur qui essaie de voir ou de modifier le mode utilisateur d'un autre client.

6.2 Réponses aux commandes.
---------------------------

300 RPL\_NONE

Numéro de réponse bidon. Inutilisé.

302 RPL\_USERHOST

":\[<réponse>{<espace><réponse>}\]"

Format de réponse utilisé par USERHOST pour lister les réponses à la liste des requêtes. La chaîne de réponse est
composée ainsi :  
<réponse> ::= <pseudo>\['\*'\] '=' <'+'|'-'><hôte>  
Le '\*' indique si le client est enregistré comme opérateur. Les caractères '-' ou '+' indiquent respectivement si le
client a défini un message AWAY ou non.

303 RPL\_ISON

":\[<pseudo> {<espace><pseudo>}\]"

Format de réponse utilisé par ISON pour lister les réponses à la liste de requête.

301 RPL\_AWAY

"<pseudo> :<message d'absence>"

305 RPL\_UNAWAY

":You are no longer marked as being away"

306 RPL\_NOWAWAY

":You have been marked as being away"

Ces trois réponses sont utilisées avec la commande AWAY (si elle est autorisée). RPL\_AWAY est envoyé à tout client qui
envoie un PRIVMSG à un client absent. RPL\_AWAY n'est envoyé que par le serveur sur lequel le client est connecté. Les
réponses RPL\_UNAWAY et RPL\_NOWAWAY sont envoyées quand un client retire et définit un message AWAY.

311 RPL\_WHOISUSER

"<pseudo> <utilisateur> <hôte> \* :<vrai nom>"

312 RPL\_WHOISSERVER

"<pseudo> <serveur> :<info serveur>"

313 RPL\_WHOISOPERATOR

"<pseudo> :is an IRC operator"

317 RPL\_WHOISIDLE

"<pseudo> <integer> :seconds idle"

318 RPL\_ENDOFWHOIS

"<pseudo> :End of /WHOIS list"

319 RPL\_WHOISCHANNELS

"<pseudo> :{\[@|+\]<canal><espace>}"

Les réponses 311 à 313 et 317 à 319 sont toutes générées en réponse à un message WHOIS. S'il y a assez de paramètres, le
serveur répondant doit soit formuler une réponse parmi les numéros ci-dessus (si le pseudo recherché a été trouvé) ou
renvoyer un message d'erreur. Le '\*' dans RPL\_WHOISUSER est là en tant que littéral et non en tant que joker. Pour
chaque jeu de réponses, seul RPL\_WHOISCHANNELS peut apparaître plusieurs fois (pour les longues listes de noms de
canaux). Les caractères '@' et '+' à côté du nom de canal indiquent si un client est opérateur de canal, ou si on l'a
autorisé à parler dans un canal modéré. La réponse RPL\_ENDOFWHOIS est utilisée pour marquer la fin de la réponse WHOIS.

314 RPL\_WHOWASUSER

"<pseudo> <utilisateur> <hôte> \* :<vrai nom>"

369 RPL\_ENDOFWHOWAS

"<pseudo> :End of WHOWAS"

Lorsqu'il répond à un message WHOWAS, un serveur doit utiliser RPL\_WHOWASUSER, RPL\_WHOISSERVER ou ERR\_WASNOSUCHNICK
pour chacun des pseudonymes de la liste fournie. A la fin de toutes les réponses, il doit y avoir un RPL\_ENDOFWHOWAS (
même s'il n'y a eu qu'une réponse, et que c'était une erreur).

321 RPL\_LISTSTART

"Channel :Users Name"

322 RPL\_LIST

"<canal> <# visible> :<sujet>"

323 RPL\_LISTEND

":End of /LIST"

Les réponses RPL\_LISTSTART, RPL\_LIST, RPL\_LISTEND marquent le début, les réponses proprement dites, et la fin du
traitement d'une commande LIST. S'il n'y a aucun canal disponible, seules les réponses de début et de fin sont envoyées.

324 RPL\_CHANNELMODEIS

"<canal> <mode> <paramètres de mode>"

331 RPL\_NOTOPIC

"<canal> :No topic is set"

332 RPL\_TOPIC

"<canal> :<sujet>"

Lors de l'envoi d'un message SUJET pour déterminer le sujet d'un canal, une de ces deux réponses est envoyée. Si le
sujet est défini, RPL\_TOPIC est renvoyée, sinon c'est RPL\_NOTOPIC.

341 RPL\_INVITING

"<canal> <pseudo>"

Renvoyé par un serveur pour indiquer que le message INVITE a été enregistré, et est en cours de transmission au client
final.

342 RPL\_SUMMONING

"<utilisateur> :Summoning user to IRC"

Renvoyé par un serveur en réponse à un message SUMMON pour indiquer qu'il appelle cet utilisateur.

351 RPL\_VERSION

"<version>.<debuglevel> <serveur> :<commentaires>"

Réponse du serveur indiquant les détails de sa version. <version> est la version actuelle du programme utilisé (
comprenant le numéro de mise à jour) et <debuglevel> est utilisé pour indiquer si le serveur fonctionne en mode
débugage.  
Le champ <commentaire> peut contenir n'importe quel commentaire au sujet de la version, ou des détails supplémentaires
sur la version.

352 RPL\_WHOREPLY

"<canal> <utilisateur> <hôte> <serveur> <pseudo> <H|G>\[\*\]\[@|+\] :<compteur de distance> <vrai nom>"

315 RPL\_ENDOFWHO

"<nom> :End of /WHO list"

La paire RPL\_WHOREPLY et RPL\_ENDOFWHO est utilisée en réponse à un message WHO. Le RPL\_WHOREPLY n'est envoyé que s'il
y a une correspondance à la requête WHO. S'il y a une liste de paramètres fournie avec le message WHO, un RPL\_ENDOFWHO
doit être envoyé après le traitement de chaque élément de la liste, <nom> étant l'élément.

353 RPL\_NAMREPLY

"<canal> :\[\[@|+\]<pseudo> \[\[@|+\]<pseudo> \[...\]\]\]"

366 RPL\_ENDOFNAMES

"<canal> :End of /NAMES list"

En réponse à un message NAMES, une paire consistant de RPL\_NAMREPLY et RPL\_ENDOFNAMES est renvoyée par le serveur au
client. S'il n'y a pas de canal résultant de la requête, seul RPL\_ENDOFNAMES est retourné. L'exception à cela est
lorsqu'un message NAMES est envoyé sans paramètre et que tous les canaux et contenus visibles sont renvoyés en une suite
de message RPL\_NAMEREPLY avec un RPL\_ENDOFNAMES indiquant la fin.

364 RPL\_LINKS

"<masque> <serveur> :<compteur de distance> <info serveur>"

365 RPL\_ENDOFLINKS

"<mask> :End of /LINKS list"

En réponse à un message LINKS, un serveur doit répondre en utilisant le nombre RPL\_LINKS, et indiquer la fin de la
liste avec une réponse RPL\_ENDOFLINKS.

367 RPL\_BANLIST

"<canal> <identification de bannissement>"

368 RPL\_ENDOFBANLIST

"<canal> :End of channel ban list"

Quand il liste les bannissements actifs pour un canal donné, un serveur doit renvoyer la liste en utilisant les messages
RPL\_BANLIST et RPL\_ENDOFBANLIST. Un RPL\_BANLIST différent doit être utilisé pour chaque identification de
bannissement. Après avoir listé les identifications de bannissement (s'il y en a), un RPL\_ENDOFBANLIST doit être
renvoyé.

371 RPL\_INFO

":<chaîne>"

374 RPL\_ENDOFINFO

":End of /INFO list"

Un serveur répondant à un message INFO doit envoyer toute sa série d'info en une suite de réponses RPL\_INFO, avec un
RPL\_ENDOFINFO pour indiquer la fin des réponses.

375 RPL\_MOTDSTART

":- <serveur> Message of the day - "

372 RPL\_MOTD

":- <texte>"

376 RPL\_ENDOFMOTD

":End of /MOTD command"

Lorsqu'il répond à un message MOTD et que le fichier MOTD est trouvé, le fichier est affiché ligne par ligne, chaque
ligne ne devant pas dépasser 80 caractères, en utilisant des réponses au format RPL\_MOTD. Celles-ci doivent être
encadrées par un RPL\_MOTDSTART (avant les RPL\_MOTDs) et un RPL\_ENDOFMOTD (après).

381 RPL\_YOUREOPER

":You are now an IRC operator"

RPL\_YOUREOPER est renvoyé à un client qui vient d'émettre un message OPER et a obtenu le statut d'opérateur.

382 RPL\_REHASHING

"<fichier de configuration> :Rehashing"

Si l'option REHASH est activée et qu'un opérateur envoie un message REHASH, un RPL\_REHASHING est renvoyé à l'opérateur.

391 RPL\_TIME

"<serveur> :<chaîne indiquant l'heure locale du serveur>"

Lorsqu'il répond à un message TIME, un serveur doit répondre en utilisant le format RPL\_TIME ci-dessus. La chaîne
montrant l'heure ne doit contenir que le jour et l'heure corrects. Il n'y a pas d'obligation supplémentaire.

392 RPL\_USERSSTART

":UserID Terminal Hôte"

393 RPL\_USERS

":%-8s %-9s %-8s"

394 RPL\_ENDOFUSERS

":End of users"

395 RPL\_NOUSERS

":Nobody logged in"

Si le message USERS est géré par un serveur, les réponses RPL\_USERSTART, RPL\_USERS, RPL\_ENDOFUSERS et RPL\_NOUSERS
sont utilisées. RPL\_USERSSTART doit être envoyé en premier, suivi par soit une séquence de RPL\_USERS soit un unique
RPL\_NOUSER. Enfin, vient un RPL\_ENDOFUSERS.

200 RPL\_TRACELINK

"Link <version & niveau de débugage> <destination> <serveur suivant>"

201 RPL\_TRACECONNECTING

"Try. <classe> <serveur>"

202 RPL\_TRACEHANDSHAKE

"H.S. <classe> <serveur>"

203 RPL\_TRACEUNKNOWN

"???? <classe> \[<adresse IP du client au format utilisant des points>\]"

204 RPL\_TRACEOPERATOR

"Oper <classe> <pseudo>"

205 RPL\_TRACEUSER

"User <classe> <pseudo>"

206 RPL\_TRACESERVER

"Serv <classe> <int>S <int>C <serveur> <pseudo!utilisateur|\*!\*>@<hôte|serveur>"

208 RPL\_TRACENEWTYPE

"<nouveau type> 0 <nom du client>"

261 RPL\_TRACELOG

"File <fichier log> <niveau de débugage>"

Les RPL\_TRACE\* sont tous renvoyés par le serveur en réponse à un message TRACE. Le nombre de messages retournés dépend
de la nature du message TRACE, et s'il a été envoyé par un opérateur ou non. Il n'y a pas d'ordre définissant lequel
doit être le premier. Les réponses RPL\_TRACEUNKNOWN, RPL\_TRACECONNECTING et RPL\_TRACEHANDSHAKE sont toutes utilisées
pour des connexions qui n'ont pas été complètement établies, et sont soit inconnues, soit toujours en cours de
connexion, soit dans la phase terminale de la 'poignée de main du serveur'. RPL\_TRACELINK est envoyé par tout serveur
qui traite un message TRACE et doit le transmettre à un autre serveur. La liste de RPL\_TRACELINK envoyée en réponse à
une commande TRACE traversant le réseau IRC devrait refléter la connectivité actuelle des serveurs le long du chemin.
RPL\_TRACENEWTYPE est utilisé pour toute connexion qui n'entre pas dans les autres catégories, mais qui est néanmoins
affichée.

211 RPL\_STATSLINKINFO

"<nom du lien> <sendq> <messages envoyés> <octets envoyés> <message reçus> <octets reçus> <temps de connexion>"

212 RPL\_STATSCOMMANDS

"<commande> <compteur>"

213 RPL\_STATSCLINE

"C <hôte> \* <nom> <port> <classe>"

214 RPL\_STATSNLINE

"N <hôte> \* <nom> <port> <classe>"

215 RPL\_STATSILINE

"I <hôte> \* <hôte> <port> <classe>"

216 RPL\_STATSKLINE

"K <hôte> \* <nom d'utilisateur> <port> <classe>"

218 RPL\_STATSYLINE

"Y <classe> <fréquence des PINGS> <fréquence de connexion> <sendq max>"

219 RPL\_ENDOFSTATS

"<lettre de stats> :End of /STATS report"

241 RPL\_STATSLLINE

"L <masque d'hôte> \* <nom de serveur> <profondeur maxi>"

242 RPL\_STATSUPTIME

":Server Up %d days %d:%02d:%02d"

243 RPL\_STATSOLINE

"O <masque d'hôte> \* <nom>"

244 RPL\_STATSHLINE

"H <masque d'hôte> \* <nom de serveur>"

221 RPL\_UMODEIS

"<chaîne de mode utilisateur>"

Pour répondre à une requête au sujet du mode du client, RPL\_UMODEIS est renvoyé.

251 RPL\_LUSERCLIENT

":There are <entier> users and <entier> invisible on <entier> servers"

252 RPL\_LUSEROP

"<entier> :operator(s) online"

253 RPL\_LUSERUNKNOWN

"<entier> :unknown connection(s)"

254 RPL\_LUSERCHANNELS

"<entier> :channels formed"

255 RPL\_LUSERME

":I have <entier> clients and <integer> servers"

Lors du traitement d'un message LUSERS, le serveur envoie un lot de réponses parmi RPL\_LUSERCLIENT, RPL\_LUSEROP,
RPL\_USERUNKNOWN, RPL\_LUSERCHANNELS et RPL\_LUSERME. Lorsqu'il répond, un serveur doit envoyer RPL\_LUSERCLIENT et
RPL\_LUSERME. Les autres réponses ne sont renvoyées que si le nombre trouvé n'est pas nul.

256 RPL\_ADMINME

"<serveur> :Administrative info"

257 RPL\_ADMINLOC1

":<info admin>"

258 RPL\_ADMINLOC2

":<info admin>"

259 RPL\_ADMINEMAIL

":<info admin>"

Lorsqu'il répond à un message ADMIN, un serveur doit renvoyer les réponses RLP\_ADMINME à RPL\_ADMINEMAIL et fournir un
texte de message avec chacune. Pour RPL\_ADMINLOC1, on attend une description de la ville et de l'état où se trouve le
serveur, suivie des détails de l'université et du département (RPL\_ADMINLOC2), et finalement le contact administratif
pour ce serveur (avec obligatoirement une adresse email) dans RPL\_ADMINEMAIL.

6.3 Nombres réservés.
---------------------

Ces nombres ne sont pas décrits ci-dessus parce qu'ils tombent dans l'une des catégories suivantes :

1. Plus utilisés ;
2. Réservés à une future utilisation ;
3. Utilisés à l'heure actuelle, mais faisant partie des caractéristiques non génériques des serveurs IRC courants.

       209     RPL\_TRACECLASS          217     RPL\_STATSQLINE
       231     RPL\_SERVICEINFO         232     RPL\_ENDOFSERVICES
       233     RPL\_SERVICE             234     RPL\_SERVLIST
       235     RPL\_SERVLISTEND
       316     RPL\_WHOISCHANOP         361     RPL\_KILLDONE
       362     RPL\_CLOSING             363     RPL\_CLOSEEND
       373     RPL\_INFOSTART           384     RPL\_MYPORTIS
       466     ERR\_YOUWILLBEBANNED     476     ERR\_BADCHANMASK
       492     ERR\_NOSERVICEHOST



