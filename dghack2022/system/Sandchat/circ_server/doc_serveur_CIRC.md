# Documentation serveur CompactIRC / CIRC

Ceci est la documentation de l'implémentation serveur de CIRC.

Ce projet implémente un serveur CIRC de base qui répond à un sous-ensemble du protocole Compact Internet Relay Chat.

Pour se connecter au serveur :

  `/connect <adresse_serveur> <port>`

Le serveur implémente les commandes IRC suivantes :

* PSEUDO : Change le pseudonyme d'un utilisateur.

  `/PSEUDO <pseudonyme>`
* SUJET : Change la description d'un canal. 

  `/SUJET #<canal> :<sujet>`
* UTILISATEUR : Spécifie le pseudonyme, le nom d'hôte, le nom de serveur, et le véritable nom d'un nouvel 
  utilisateur. 

  `/UTILISATEUR <pseudonyme> 0 * :<nom_reel>`
* LISTE : Liste les canaux du serveur

  `/LISTE`
* REJOINDRE : rejoint un canal

  `/REJOINDRE #<canal>`
* PARTIR : Quitte un canal

  `/PARTIR #<canal>`
* QUI : Liste les informations d'un utilisateur

  `/QUI <pseudonyme>`
* QUIEST : Lister plus d'informations sur un utilisateur.

  `/QUIEST <pseudonyme>`
* MSGPRIV : Envoie des messages à un canal ou à un utilisateur.

   `/MSGPRIV <pseudonyme | canal> <message>`
* QUITTER : Déconnecte l'utilisateur du serveur.

   `/QUITTER`

De plus, le serveur implémente partiellement certaines commandes pour l'interaction avec le client, qui assurent
la connexion est maintenue :

* PING : Répond avec un PONG ;
* NOTICE : envoie des notifications à un client.
