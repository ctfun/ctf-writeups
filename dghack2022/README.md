# DG'hAck 2022

Note: d'autres writeups (notamment pour les épreuves que je n'ai pas validées) ont été partagés sur [le canal #writeups du Discord](https://discord.com/channels/1035458318442967090/1045386509697691681).

Crypto :
- Cryptobvious
- Maillon Faible
- RSSA

Détection :
- [Piratecraft](detection/piratecraft/Piratecraft.md)
- [Shadow4DGA (niveau 1)](detection/Shadow4DGA/Shadow4DGA.md)
- [Shadow4DGA (niveau 2)](detection/Shadow4DGA/Shadow4DGA.md#shadow4dga-niveau-2)

Dev :
- [Coffre-fort Secret](dev/Coffre-fort%20Secret/Coffre-fort%20Secret.md)
- [ASM ère](dev/ASM%20ère/ASM%20ère.md)
- [Pas si chronophage](dev/Pas%20si%20chronophage/Pas%20si%20chronophage.md)

Exploit :
- Buffoid

Forensic :
- Ministry of catapult
- Shiftdows
- [Pas Un Bon Nom](forensics/Pas%20Un%20Bon%20Nom/Pas%20Un%20Bon%20Nom.md)
- [Vivre pas cher](forensics/Vivre%20pas%20cher/Vivre%20pas%20cher.md)

Reverse :
- Wanna more features ?
- Hack Trick

Stéganographie:
- [Is it art ?](stegano/Is%20it%20art/Is%20it%20art.md)
- [Ne Jamais Renoncer](stegano/Ne%20Jamais%20Renoncer/Ne%20Jamais%20Renoncer.md)

Système :
- [Sandchat](system/Sandchat/Sandchat.md)

Web :
- [Curlify](web/Curlify/Curlify.md)
- [Un chasseur sachant chasser - partie 1](web/Un%20chasseur%20sachant%20chasser/Un%20chasseur%20sachant%20chasser.md)
- Un chasseur sachant chasser - partie 2