# Coffre-fort Secret

## Description

Le coffre-fort secret ne semble pas déchiffrer correctement. Corrigez-le pour obtenir des points. Il y a peut-être d'autres problèmes, qui sait !

Fichiers joints
- [SecretVault.go](SecretVault_origin.go)

## Solution

On regarde rapidement le code. Il semble implémenter un outil de chiffrement/déchiffrement AES, dont la clef et l'IV sont hardcodés dans le source. On remarque aussi qu'il «autodétecte» si on passe un chiffré (il suffit que l'entrée ressemble à du base64).

On lance une compilation de l'outil :
```console
$ go build SecretVault.go 
# command-line-arguments
./SecretVault.go:85:45: cannot use '\n' (untyped rune constant 10) as string value in argument to strings.Replace
```

Effectivement, sur cette ligne, on remplace les simple quotes par des doubles :
`InputString = strings.Replace(InputString, "\n", "", -1)`

Maintenant, ça compile, on peut lancement un chiffrement.

```console
$ ./SecretVault "Coin plop on test si ça marche bien. Mais peut-être faut-il une longue phrase pour détecter autre chose ?"
The message was successfully encrypted: FgPUEDTESzfIl5Vc431C5vM4mNlUUCn6xVScNO15SSpqArOSuJvEnzilcJ0Td3oYmlvWo1BIY6FX2jc4nmhxiaQL1LZJgBSAbylnUWw0WEWbrnf3ZhJbcBKBtM7mpjjlcOL+uDF/ZVmCO6vo
```

Ça a l'air de fonctionner. Par contre, quand on tente un déchiffrement :
```console
$ ./SecretVault FgPUEDTESzfIl5Vc431C5vM4mNlUUCn6xVScNO15SSpqArOSuJvEnzilcJ0Td3oYmlvWo1BIY6FX2jc4nmhxiaQL1LZJgBSAbylnUWw0WEWbrnf3ZhJbcBKBtM7mpjjlcOL+uDF/ZVmCO6vo
The message was successfully encrypted: EwvtK1Hwcx3rzZx7rzxx9qGPHtUbFKC3k4sP7qoP+80cJj9TTngKDvbvzSoDFC0gyQ5BwVfl5PpfeAFAHNpBLTe4O1Tpjp4PQYOW2Ni1vKnTpbtYQnaeEWAfDTLYNGLOSvioYLKP1YCE68qaqpj3IdUKarWqX2IZENrSZhex8FCR4GM9VQxAQy7hUk8rGeIg
```

Il est censé détecté du base64 en entrée, mais ça échoue. Le `main` est curieux :
```go
	// If the input text is suspected to be an encrypted message, we decrypt it and display its content
	if IsBase64(InputString) {
		go handleBase64String(InputString)
	}
	handleNormalString(InputString)
```

Ce qui se passe est que le mot-clef `go` sur le déchiffrement ne nous permet pas de voir ce qui se passe. De plus, l'absence de `else` fait que le chiffrement est effectué quand même ! On modifie :

```go
	// If the input text is suspected to be an encrypted message, we decrypt it and display its content
	if IsBase64(InputString) {
		handleBase64String(InputString)
	} else {
		handleNormalString(InputString)
	}
```

Maintenant, on a une erreur au déchiffrement :

```console
$ go build SecretVault.go && ./SecretVault FgPUEDTESzfIl5Vc431C5vM4mNlUUCn6xVScNO15SSpqArOSuJvEnzilcJ0Td3oYmlvWo1BIY6FX2jc4nmhxiaQL1LZJgBSAbylnUWw0WEWbrnf3ZhJbcBKBtM7mpjjlcOL+uDF/ZVmCO6vo
panic: nil

goroutine 1 [running]:
main.Decode(...)
	/home/papy/tmp/DGHack2022/dev/Coffre-fort Secret/SecretVault.go:23
main.Decrypt({0x7ffc8ed20024, 0x90}, {0x49f763?, 0xc00006af08?})
	/home/papy/tmp/DGHack2022/dev/Coffre-fort Secret/SecretVault.go:49 +0x16c
main.handleBase64String({0x7ffc8ed20024?, 0x90?})
	/home/papy/tmp/DGHack2022/dev/Coffre-fort Secret/SecretVault.go:64 +0x2e
main.main()
	/home/papy/tmp/DGHack2022/dev/Coffre-fort Secret/SecretVault.go:90 +0x93
```

Dans `Decode()`, il `panic` s'il n'y a pas d'erreur du décodage du Base64 ! On inverse donc le test :

```go
func Decode(s string) []byte {
	data, err := base64.StdEncoding.DecodeString(s)
	if err != nil {
		panic(err)
	}
	return data
}
```

Et on recommence:

```console
$ go build SecretVault.go && ./SecretVault FgPUEDTESzfIl5Vc431C5vM4mNlUUCn6xVScNO15SSpqArOSuJvEnzilcJ0Td3oYmlvWo1BIY6FX2jc4nmhxiaQL1LZJgBSAbylnUWw0WEWbrnf3ZhJbcBKBtM7mpjjlcOL+uDF/ZVmCO6vo
We believe this may be an encrypted message, here is what it would say: 
```

Et c'est tout ? On se plonge un peu plus dans les fonctions de chifrement et de déchiffrement et on remarque un truc bizarre :

- chiffrement :
```go
	cfb.XORKeyStream(cipherText, plainText)
```
- déchiffrement :
```go
	cfb.XORKeyStream(cipherText, plainText)
```

Il n'est pas logique que la fonction soit appelée avec les paramètres dans le même ordre ! On inverse donc dans la fonction de déchiffrement :

```go
    cfb.XORKeyStream(plainText, cipherText)
```

On lance une nouvelle fois :

```console
$ go build SecretVault.go && ./SecretVault "Ceci est un test"
The message was successfully encrypted: FgneFzTRVCyYwpQSt2xU4Q==
$ go build SecretVault.go && ./SecretVault FgneFzTRVCyYwpQSt2xU4Q==
We believe this may be an encrypted message, here is what it would say: Ceci est un test
```

[Noter nouveau code](SecretVault.go) a l'air maintenant fonctionnel. Sur le portail, on fait les modifications nécessaires en suivant le diff :

```diff
22c22
< 	if err == nil {
---
> 	if err != nil {
52c52
< 	cfb.XORKeyStream(cipherText, plainText)
---
> 	cfb.XORKeyStream(plainText, cipherText)
85c85
< 	InputString = strings.Replace(InputString, '\n', "", -1)
---
> 	InputString = strings.Replace(InputString, "\n", "", -1)
89c89,91
< 		go handleBase64String(InputString)
---
> 		handleBase64String(InputString)
> 	} else {
> 		handleNormalString(InputString)
91d92
< 	handleNormalString(InputString)
```

Et c'est validé !