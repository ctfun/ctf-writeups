# ASM ère

## Description

Nous avons besoin de sécuriser des données sur un très vieux système.

Heureusement, nous avons déjà de nombreux programmes très sophistiqués pour le faire.

Nous avons cependant égaré l'interpreteur du langage de programmation interne à notre organisation.

Veuillez programmer l'interpreteur ASMera en Python, suivant les entrées et sorties d'exemple ci-dessous.

Le programme prend pour seul argument le fichier à executer et affiche le résultat en sortie standard.

Le test final peut contenir des opérations "==" "!=" "<" ">" "<=" et/ou ">="

**Fichiers joints**

- [ASMera.py](input/ASMera.py)
- [example_incrementer_input.txt](input/example_incrementer_input.txt)
- [example_incrementer_output.txt](input/example_incrementer_output.txt)
- [example_output.txt](input/example_output.txt)
- [example_input.txt](input/example_input.txt)

## Solution

Pas grand chose à dire ici : on a à écrire un interpréteur d'un langage bizarre, qui va devoir implémenter :
- un système de variables globales.
- des opérateurs de base pour les tests, l'affectation et quelques opérations.
- une gestion de fonctions avec passage de paramètres, supportant les appels récursifs.
- un système d'affichage.

C'est ce dernier point qui est particulièrement casse-pied : je n'ai pas réussi à faire quelque chose de propre, et je me demande réellement s'il est possible d'avoir une grammaire cohérente avec ce qui est fourni en exemple. C'est pour cela que mon code est un peu spaghetti à ce niveau, et comme la contrainte d'un CTF est le temps, pas de refactoring prévu dans l'immédiat !:)

Le code qui a permis de valider est [ici](ASMera.py).
