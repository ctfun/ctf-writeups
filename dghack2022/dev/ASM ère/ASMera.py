#!/usr/bin/env python3

import sys

# Ce dont on a besoin
functions = dict()
variables = dict()
call_stack = list()


# Load and parse the code
def load(filename):
    lines = open(filename, 'r').readlines()
    lines = [ x.rstrip('\n') for x in lines ]
    current_function = 'main'
    functions[current_function] = list()

    for line in lines:
        # Suppression des commentaires
        idx = line.find(';')
        if idx != -1:
            line = line[0:idx]

        # Suppression des lines vides
        if line == '':
            continue

        # Démarrage fonction
        if line[-1] == ':':
            current_function = line[:-1]
            functions[current_function] = list()
        # Fin fonction 
        elif line == 'retour':
            current_function = 'main'
        # instruction
        else:
            functions[current_function].append(line)

# Dump code
def dump():
    for function in functions:
        print(f'# {function}:')
        for line in functions[function]:
            print(f'\t{line}')

def exec_nombre(var, value):
    variables[var] = int(value)

def exec_incr(var, value):
    variables[var] += int(value)

LITTERAL = 'Litteral'
NON_LITTERAL = 'Non-litteral'
def exec_message(message):
    #print(f'#DEBUG: {message}')
    tokens = list()
    index = 0
    while index < len(message):
        if message[index] == '"':
            # Littéral
            fin = message.find('"', index+1)
            token = message[index+1:fin]
            #print(f'#\tLittéral :«{token}»')
            tokens.append([LITTERAL, token])
            index = fin +1
        else:
            # Non littéral
            next = message.find('"', index+1)
            if next == -1:
                next = len(message)
            token = message[index:next]
            #print(f'#\tNon-littéral : «{token}»')
            # On vire les espaces en trop
            while '  ' in token:
                token = token.replace('  ', ' ')
            # On évalue les variables (façon bourrin)
            for var, value in variables.items():
                token = token.replace(f'${var}', str(value))
            if token != '' and token != ' ':
                tokens.append([NON_LITTERAL, token])
            index = next
    
    # Putain mais qu'est-ce que je fous là, bordel de merde ?
    # Je suis à peu près certains que les exemples ne permettent
    # pas d'avoir une grammaire cohérente.
    string = ''
    for i in range(len(tokens)):
        type, token = tokens[i]
        if i != 0 and token.startswith(' ') and type == LITTERAL:
            ptype, ptoken = tokens[i-1]
            if ptype == NON_LITTERAL and ptoken[-1] == ' ':
                string = string.rstrip(' ')
        elif i != 0 and token.startswith(' ') and type == NON_LITTERAL:
            ptype, ptoken = tokens[i-1]
            if ptype == LITTERAL and ptoken[-1] == ' ':
                token = token.lstrip(' ')
        string += token

    print(string)


def _value_of(arg):
    if arg.startswith('$'):
        return variables[arg[1:]]
    else:
        return int(arg)


def eval_test(arg1, op, arg2):
    if op not in ['==', '!=', '<', '>', '<=', '>=']:
        print(f'ERR: operator {op} not supported')
    arg1, arg2 = [ _value_of(x) for x in [arg1, arg2] ]
    return eval(f'{arg1} {op} {arg2}')


def exec_function(function):
    rip = 0
    code = functions[function]
    while rip < len(code):
        #print(f'DEBUG: {code[rip]}')
        instr, *param = code[rip].split(' ')
        if instr == 'nombre':
            exec_nombre(*param)
        elif instr == 'incrementer':
            exec_incr(*param)
        elif instr == 'message':
            exec_message(code[rip][code[rip].find(' ')+1:])
        elif instr == 'appel':
            call_stack.append([function, rip+1])
            exec_function(*param)
        elif instr == 'retour':
            function, rip = call_stack.pop(-1)
        elif instr == 'si':
            if not eval_test(*param):
                while not code[rip].startswith('finsi'):
                    rip +=1
        elif instr == 'finsi':
            pass
        else:
            print(f'ERR: {instr} not implemented yet.')
        rip += 1

def main():
    if len(sys.argv) != 2:
        print(f"Usage: python3 {sys.argv[0]} <filename>")
        sys.exit(1)
    
    load(sys.argv[1])
    #dump()
    exec_function('main')


if __name__ == "__main__":
    main()