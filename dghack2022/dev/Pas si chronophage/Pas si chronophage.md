# Pas si chronophage

## Description

Cette application permet la connexion de l'utilisateur à son compte de box internet.

Lien :
- [Accéder à l'activité](http://passichronophage.chall.malicecyber.com/)


## Solution

On accède au site web qui affiche ceci:

![](web.png)

Il s'agit d'un formulaire de connexion, avec captcha maison qui représente une horloge
analogique et où il faut saisir l'heure affichée. Le champ `username` propose de saisir la valeur `admin`. 
On tente une connexion, qui échoue mais nous propose un lien vers un tutorial :

![](tuto.png)

On comprend alors qu'il va nous falloir bruteforcer le mot de passe qui est composé de 5 digits. Mais pour cela, il va falloir automatiser le passage du captcha.

Mon choix d'implémentation a été de parcourir deux cercles concentriques, de rayons différents, dans l'image de l'horloge founie :
- avec un grand rayon, on va détecter l'aiguille des minutes.
- avec un faible rayon, on détecte les deux aiguilles, on va ignorer la valeur récupérée à l'étape précédente.
- les angles récupérés permettent de déterminer les positions des aiguilles et d'en déduire l'heure.

L'implémentation est loin d'être parfaite ! Parfois, les angles ne sont pas tout à corrects, donc avec des erreurs +/- 1 heure et/ou minute. D'autre part, 
mon script est complètement perdu lorsque les deux aiguilles se chevauchent ! Je n'ai pas fait de statistiques, mais il semble avoir un taux de réussite aux alentours de 60%, ce qui est suffisant en ce qui me concerne. 

Le code de résolution est [ici](solve.py) et lorsque le bon mot de passe est trouvé, on se connecte sur l'interface et le flag nous est fourni :
`DGHACK{DontFeelBadAboutDumpingYourInternetProvider}`.