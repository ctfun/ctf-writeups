# Vivre pas cher

## Description

Notre serveur a été piraté. C'est une évidence.

Ils dévoilent notre code source sans arrêt, dès que nous le mettons à jour.

Vous devez trouver l'origine de cette backdoor dès que possible.

Fourni : une image disque.

## Solution

On commence par examiner l'image et monter le disque :
```console
$ sudo fdisk -l cheap-life.img 
Disque cheap-life.img : 1 GiB, 1073741824 octets, 2097152 secteurs
Unités : secteur de 1 × 512 = 512 octets
Taille de secteur (logique / physique) : 512 octets / 512 octets
taille d'E/S (minimale / optimale) : 512 octets / 512 octets
Type d'étiquette de disque : dos
Identifiant de disque : 0x5d8b75fc

Périphérique    Amorçage Début     Fin Secteurs Taille Id Type
cheap-life.img1 *         2048 2097151  2095104  1023M 83 Linux

$ sudo mount -o loop,offset=1048576 cheap-life.img tmp/
```

On examine le système en question et on tombe sur un service déclaré dans `systemd` qui comporte des mots intéressants. :)

```console
$ sudo cat etc/systemd/system/systembd.service 
[Unit]
Description=backdoor
After=network.target

[Service]
User=root
Type=simple
ExecStart=/root/exploit.py
Restart=on-failure
RestartSec=10s

[Install]
WantedBy=multi-user.target
```

Allons voir le script python associé :

```python
#!/usr/bin/env python3
import json

def exploit():
    print('exploit')

if __name__ == "__main__":
    exploit()
```

Inoffensif, à priori... Quoique : peut-on aller voir ce module json ?
D'autant plus bizarre, qu'il n'y a pas de python sur la machine. :/
Fausse piste, ou bien ?

Essayons de chercher ce mot-clef `backdoor` ailleurs pour voir :
```console
$ sudo grep -r backdoor 
etc/systemd/system/systembd.service:Description=backdoor
grep: var/lib/apt/lists/deb.debian.org_debian_dists_bullseye_main_binary-amd64_Packages.lz4 : fichiers binaires correspondent
grep: usr/lib64/libbackdoor.so : fichiers binaires correspondent
grep: usr/sbin/groupdel : fichiers binaires correspondent
```

On découvre le binaire `groupdel` qui se fait passer pour un binaire légitime mais qui en fait lance la backdoor en cas d'erreur dans les paramètres :

![](groupdel.png)

Il est lié à la librairie `libbackdoor.so`, regardons-là de plus près :

![](lib.png)

C'est visiblement elle qui installe le service persistant. Il y a un base64 intriguant. On le décode pour obtenir : `SystemDIsAFrenchExpressionAboutLivingOutOfResourcefulnessWithLittleMoney`. Et il se trouve que c'est le flag, même si je n'ai pas bien compris pourquoi.

`DGHACK{SystemDIsAFrenchExpressionAboutLivingOutOfResourcefulnessWithLittleMoney}`