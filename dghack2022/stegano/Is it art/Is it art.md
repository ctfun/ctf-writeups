# Is it art ?

## Description

Rendez-vous au stand DGA lors de l’European Cyber Week 2022 qui se tiendra à Rennes, un challenge se cache dans un des goodies qui vous seront offerts.

Saurez-vous le retrouver et le résoudre ?

![](barcode-png.png)


## Solution

On distingue deux codes barre, à gauche et à droite de l'image. On extrait et on reconstruit les codes barre et on fait une rotation à coup de [The Gimp](https://www.gimp.org/). 

![left](left.png)

![right](right.png)

On utilise ensuite le module python `pyzbar` pour les décoder :

```python
>>> from pyzbar.pyzbar import decode
>>> from PIL import Image
>>> decode(Image.open('work.png'))
[Decoded(data=b'4447417b', type='CODE128', rect=Rect(left=525, top=0, width=0, height=292), polygon=[Point(x=525, y=0), Point(x=525, y=292)], quality=147, orientation='DOWN')]
>>> decode(Image.open('right.png'))
[Decoded(data=b'2332327d', type='CODE128', rect=Rect(left=51, top=1, width=446, height=269), polygon=[Point(x=51, y=1), Point(x=51, y=269), Point(x=497, y=270), Point(x=497, y=2)], quality=239, orientation='UP')]
>>> bytes.fromhex('4447417b2332327d')
b'DGA{#22}'
```