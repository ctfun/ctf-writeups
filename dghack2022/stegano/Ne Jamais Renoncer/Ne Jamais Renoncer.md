# Ne Jamais Renoncer

## Description

Le logo officiel du DG'hAck a été repris par un artiste de talent, mais pourquoi ?

![](logo-from_an_artist.png)

## Solution

On se doute clairement qu'il y a quelque chose caché dans cette spirale colorée. On remarque que les couleurs utilisées sont spécifiques et comportent uniquement les composantes rouge, verte et bleue suivante : 0, 192, 255.

Après pas mal d'analyses statistiques pour essayer d'interpréter tout ça, on se rend compte en fait que ces valeurs sont caractéristiques du langage [Piet](https://esolangs.org/wiki/Piet), un langage ésotérique qui est représenté sous forme d'image.

On va pouvoir utiliser [un interpréteur](http://www.bertnase.de/npiet/), il va juste falloir convertir l'image au format PPM et réduire sa taille pour obtenir un pixel à la place de chacune des cases d'origine. Pour cela, on sort un petit script python (des utilitaires existent, mais on tendance à dénaturer les valeurs en faisant de l'anti-aliasing) :

```python
#!/usr/bin/env python3

from PIL import Image

im = Image.open('logo-from_an_artist.png')
px = im.load()

out = Image.new('RGB', (50, 50), (255, 255, 255))
pxout = out.load()

for i in range(50):
    for j in range(50):
        pxout[j, i] = px[j*96, i*96]

out.save('logo-from_an_artist.ppm')
```

On lance ensuite l'interpréteur Piet :
```console
$ ./npiet logo-from_an_artist.ppm 
DGHACK{P13T_IS_S0_FUN}_WWW.DGHACK.FR
```



