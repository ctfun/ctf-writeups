# 6 - Passing Notes

## Énoncé

Passing secret notes? That practically screams Valentine's Day to me! So, I've devised a super-secure way to encrypt a message so you can send it to that special someone! I used my program to encrypt a Valentine just for you! The only thing is... I don't remember the key. Ah, whatever! Here you go: `V4m\GDMHaDM3WKy6tACXaEuXumQgtJufGEyXTAtIuDm5GEHS`

The `valentine{...}` wrapper is included in the encrypted text.

Author: Manav (0xmmalik)

Fichier fourni : [passing_notes.py](passing_notes.py)

## Solution

À partir d'une clé secrète, on la dérive de manière plus ou moins obscure pour obtenir
la clé `mod_key`. Or `mod_key` est un élément de $GF(64)$. C'est un des 64 polynômes :

```
0 0
1 a
2 a^2
3 a^3
4 a^4
5 a^5
6 a^4 + a^3 + a + 1
7 a^5 + a^4 + a^2 + a
8 a^5 + a^4 + a^2 + a + 1
9 a^5 + a^4 + a^2 + 1
10 a^5 + a^4 + 1
11 a^5 + a^4 + a^3 + 1
12 a^5 + a^3 + 1
13 a^3 + 1
...
```

La ligne importante est : `encrypted += b64_alpha[field.index(field[b64_alpha.index(chr(char))] * mod_key)]`
Elle chiffre un caractère. Ce caractère est un des 64 de l'alphabet `base64`. Supposons que c'est la $x^e$ lettre de l'alphabet `base64`, on la remplace par le $x^e$ élément de $GF(64)$. Appelons $b$ cet élément. On calcule
$b \times modkey$, c'est encore un élément de $GF(64)$, disons le $y^e$. Le chiffré du caractère sera le $y^e$ élément de l'alphabet `base64`.

Comme on connaît le chiffré, et le début du message `valentine{`, on peut tester
tous les `mod_key` possibles, pour voir s'il y en a un qui fonctionne.

Autrement dit, connaissant le lettre à chiffrer et le chiffré, on teste tous les polynômes de $GF(64)$ pour voir lequel envoie
la lettre à chiffrer sur le chiffrer.

On trouve que le polynôme `mod_key` est $a^5 + a^4 + a^3 + a^2$.
C'est l'élément 44 de $GF(64)$.

On écrit ensuite une fonction de déchiffrement utilisant ce polynôme.

Le flag est : `valentine{th15_is_4_s3cret_m355age}`

La solution est dans [`passing_notes_mod.py`](passing_notes_mod.py) qui est adapté du fichier fourni.

Référence vidéo sur $GF(64)$ : 
<https://www.youtube.com/watch?v=oPjkfvuqqv4>
