from base64 import b64encode, b64decode
from random import choice
from sage.all import GF

# %% 
b64_alpha = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789\\="
field = list(GF(2**6, 'a'))

for k, v in enumerate(field):
    print(k, v)

# %%
def generate_secret_key(n):
  key = 1
  for _ in range(n):
    key *= choice(field)
    key += choice(field)
  return key

def encrypt(message, secret_key):
  message = b64encode(message)
  encrypted = ""
  mod_key = 6 * secret_key**6 + 3 * secret_key**4 + 7 * secret_key**3 + 15
  for char in message:
    encrypted += b64_alpha[field.index(field[b64_alpha.index(chr(char))] *
                                       mod_key)]
  return encrypted

# %%
def decrypt(message, mod_key):
  #message = b64encode(message)
  decrypted = ""
  # mod_key = 6 * secret_key**6 + 3 * secret_key**4 + 7 * secret_key**3 + 15
  for char in message:
    decrypted += b64_alpha[field.index(field[b64_alpha.index(chr(char))] /
                                       mod_key)]
  return b64decode(decrypted)

# %%
def search(m, c):
    res = []
    for a in field:
        if field.index(field[m] * a) == c:
            res.append(a)
    return res


# %%
s1 = r'dmFsZW50aW5l'
s2 = b'V4m\\GDMHaDM3WKy6tACXaEuXumQgtJufGEyXTAtIuDm5GEHS'

corres = []
for a, b in zip(s1, s2):
    corres.append((b64_alpha.index(a), b64_alpha.index(chr(b))))
    print(a, chr(b), search(*corres[-1]))

# %%
print(decrypt(s2, field[44]))