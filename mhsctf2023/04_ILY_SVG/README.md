# 04 ILY SVG

## Énoncé 

SRY for the title, I'm a big fan of three-letter intitialisms LOL! I wanted to make some Valentine cards, but I (as a nine-year-old) don't have enough money to get a professional to do it for me. Instead, I hired this super shady company, so I just need to make sure they didn't put anything weird in the file they sent me. Here's the file, let me know!

Remember to wrap your Valentine in valentine{...}!

Hint: look only at the x1 parameter of each cubic curve defined in the SVG

Author: Manav (0xmmalik)

![ily_svg.svg](ily_svg.svg)

# Solution

Réf : <https://medium.com/@bragg/cubic-bezier-curves-with-svg-paths-a326bb09616f>

Le x1 indiqué est l'abscisse du premier handle pour chaque tronçon de courbe.
C'est la coordonnée juste après la lettre `C` dans le SVG.

Son dernier chiffre est à 0 ou 1 pour les 153 premiers points.

[Code de résolution joint](solve.py)
