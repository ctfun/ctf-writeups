import re
from xml.dom import minidom
from snarktools import dataflow

def get_x1_b(stri):
    return re.findall("C [0-9\.]*([0-9])", stri)

doc = minidom.parse("ily_svg.svg") 
path_strings = [path.getAttribute('d') for path
                in doc.getElementsByTagName('path')]
doc.unlink()

lnum = [int(_) for _ in get_x1_b("".join(path_strings))]
# dataflow (truc perso) refait juste des paquets de 7 bits et reforme des octets
print(dataflow.bits_to_bytes(lnum[:153], 7))
