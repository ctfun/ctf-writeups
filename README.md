# ctf-writeups

Write-ups des CTF auxquels on participe.

- 2019
  - [FCS 2019](fcsc2019/README.md) (je mens, je fais ces challenges en 2024 grâce à [Hackropole](https://hackropole.fr/) ;-))
- 2020
  - [FCSC 2020](fcsc2020/README.md)
  - [Brigitte Friang (DGSE & ESIEE 2020)](brigittefriang/README.md)
- 2021
  - [FCSC 2021](fcsc2021/README.md)
  - [DG'hAck 2021](dghack2021/README.md)
- 2022
  - [FCSC 2022](fcsc2022/README.md)
  - [404CTF (DGSE)](404ctf/README.md)
  - [CTF INNN 2022 (équipe cup_team)](https://github.com/lsignac/ctf-innn-2022)
  - [DG'hAck 2022](dghack2022/README.md)
- 2023
  - [Mars@Hack 2023 (équipe cupteam)](https://gitlab.com/cupteam/marsathack2023)
  - [FCSC 2023](fcsc2023/README.md)
- 2024
  - [FCSC 2024](fcsc2024/README.md)