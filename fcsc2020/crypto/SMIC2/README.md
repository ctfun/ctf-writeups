# SMIC 2

**Description**

La sécurité du cryptosystème RSA repose sur un problème calculatoire bien connu.

On vous demande de déchiffrer le “message” chiffré c ci-dessous pour retrouver le “message” en clair m associé à partir de la clé publique `(n, e)`.

Valeurs :

- `e = 65537`
- `n = 632459103267572196107100983820469021721602147490918660274601`
- `c = 63775417045544543594281416329767355155835033510382720735973`

Le flag est FCSC{xxxx} où xxxx est remplacé par la valeur de m en écriture décimale.

**Solution**

En utilisant RsaCtfTools :

```
❯ python /opt/RsaCtfTool/RsaCtfTool.py  -n 632459103267572196107100983820469021721602147490918660274601 -e 65537 --private --decrypt 63775417045544543594281416329767355155835033510382720735973

[*] Testing key /tmp/tmptjknp5e0.
attack initialized...
attack initialized...
[*] Performing smallq attack on /tmp/tmptjknp5e0.
[+] Time elapsed: 0.2977 sec.
[*] Performing factordb attack on /tmp/tmptjknp5e0.
[*] Attack success with factordb method !
[+] Total time elapsed min,max,avg: 0.2977/0.2977/0.2977 sec.

Results for /tmp/tmptjknp5e0:

Private key :
-----BEGIN RSA PRIVATE KEY-----
MIGJAgEAAhlkwa0jykV6ApeOriyNhOiImX5kyYNlH3mpAgMBAAECGRrUqHKbbpzr
C8jnANjKiZoK2e3WlV51ZnECDQg2YZJK1/vnPdj4pl8CDQxEz4juEi+b/YmljPcC
DQNeMgOp+AvR2NZrmucCDQPL/9ZGp0YopmxE2b0CDQEUUT+TBmbKJ0AIDOg=
-----END RSA PRIVATE KEY-----

Decrypted data :
HEX : 0x59cd3f8c426ff5732a1bd084e5a599bd880a3ddd86486f7f93
INT (big endian) : 563694726501963824567957403529535003815080102246078401707923
INT (little endian) : 925858644864198762457006108771931749234749402401936087436633
STR : b'Y\xcd?\x8cBo\xf5s*\x1b\xd0\x84\xe5\xa5\x99\xbd\x88\n=\xdd\x86Ho\x7f\x93'
```

Le flag : `FCSC{563694726501963824567957403529535003815080102246078401707923}`