# Tarte Tatin (20 points)

**Challenge**

Votre mission : trouver le mot de passe qui affiche le flag.

SHA256(TarteTatin) = 16697d877a69f2d06f286e32a0c1e8fc01eff25c366ef200eee47cc5ab81f96e

<details>
    <summary>Indice</summary>
    Il faut désassembler le binaire fourni et analyser les instructions assembleurs, par exemple avec IDA, radare2, objdump -d, etc.
</details>

[TartTatin](https://france-cybersecurity-challenge.fr/files/bf78be49aad7635c912ffd1fcaad2413/TarteTatin)


**Solution**

Désolé, mais je vais faire plus rapide que désassembler le bazar. Je fais toujours un coup de `strings` sur un binaire avant de démarrer. Et il y a une chaîne qui saute aux yeux :
```console
$ strings TarteTatin
[...]
EBRBz72e30320b000/51c//2cc/102be713c55e66/`/ad02/4d1702e04cc654/2`80c|
NzTfdvs4Q4ttx1seGCC: (Ubuntu 7.5.0-3ubuntu1~18.04) 7.5.0
[...]
```

C'est amusant, c'est comme un flag FCSC, mais décalé de 1.

```python
>>> ''.join( [ chr(ord(c)+1) for c in 'EBRBz72e30320b000/51c//2cc/102be713c55e66/`/ad02/4d1702e04cc654/2`80c|' ])
'FCSC{83f41431c111062d003dd0213cf824d66f770a0be1305e2813f15dd76503a91d}'
```

Bon, si on veut vraiment désassembler, on peut regarder la fonction `transform` :
```nasm
000000000000077a <transform>:
 77a:	55                   	push   %rbp
 77b:	48 89 e5             	mov    %rsp,%rbp
 77e:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 782:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 786:	48 8d 50 01          	lea    0x1(%rax),%rdx
 78a:	48 89 55 f8          	mov    %rdx,-0x8(%rbp)
 78e:	0f b6 10             	movzbl (%rax),%edx
 791:	83 c2 01             	add    $0x1,%edx                // Décalage de 1
 794:	88 10                	mov    %dl,(%rax)
 796:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 79a:	0f b6 00             	movzbl (%rax),%eax
 79d:	84 c0                	test   %al,%al
 79f:	75 e1                	jne    782 <transform+0x8>
 7a1:	90                   	nop
 7a2:	5d                   	pop    %rbp
 7a3:	c3                   	retq   
```

Mais si on veut vraiment suivre le cheminement nominal, on note que le binaire attend un mot de passe, et si celui-ci est correct, il affichera le flag en clair. Le mot de passe est stocké avec le même décalage que le flag. Il se trouve juste en dessous du flag camouflé quand on fait un `strings` :

```python
>>> ''.join( [ chr(ord(c)-1) for c in 'NzTfdvs4Q4ttx1se' ])
'MySecur3P3ssw0rd'
```

Et alors :

```console
$ ./TarteTatin 
MySecur3P3ssw0rd
Well done! The flag is: FCSC{83f41431c111062d003dd0213cf824d66f770a0be1305e2813f15dd76503a91d}
```

Quel est votre objectif ? Valider rapidement pour passer à l'épreuve suivante ou comprendre le binaire ? ;-)
