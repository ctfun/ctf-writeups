# SuSHi (20 points)

**Challenge**

Connectez-vous à cette machine en SSH avec les identifiants mentionnés, et trouvez le flag.

    Adresse : challenges2.france-cybersecurity-challenge.fr
    Port : 6000
    Utilisateur : ctf
    Mot de passe : ctf

<details>
    <summary>Indice</summary>
    Pour vous connecter en SSH sur la machine depuis un poste Linux, utilisez la commande ssh -p[port] [utilisateur]@[adresse].

    Sur un poste Windows, un logiciel comme PuTTY permet de faire la connexion.
</details>

<details>
    <summary>Indice</summary>
    Une fois connecté en SSH, trouvez la commande pour lister les fichiers dans un répertoire (man ls peut être utile).
</details>


**Solution**

Straightforward :

```console
$ ssh -p 6000 ctf@challenges2.france-cybersecurity-challenge.fr 
ctf@challenges2.france-cybersecurity-challenge.fr's password: 
 __    __            _                 __           _     _   ___ 
/ / /\ \ \__ _ _ __ | |_      __ _    / _\_   _ ___| |__ (_) / _ \
\ \/  \/ / _` | '_ \| __|    / _` |   \ \| | | / __| '_ \| | \// /
 \  /\  / (_| | | | | |_    | (_| |   _\ \ |_| \__ \ | | | |   \/ 
  \/  \/ \__,_|_| |_|\__|    \__,_|   \__/\__,_|___/_| |_|_|   () 
ctf@SuSHi:~$ ls
ctf@SuSHi:~$ ls -al
total 24
drwxr-xr-x 1 ctf-admin ctf 4096 Apr 25 10:39 .
drwxr-xr-x 1 ctf-admin ctf 4096 Apr 25 10:38 ..
-rw-r--r-- 1 ctf-admin ctf  220 May 15  2017 .bash_logout
-rw-r--r-- 1 ctf-admin ctf 3526 May 15  2017 .bashrc
-r--r--r-- 1 ctf-admin ctf   71 Apr 25 10:38 .flag
-rw-r--r-- 1 ctf-admin ctf  675 May 15  2017 .profile
ctf@SuSHi:~$ cat .flag 
FCSC{ca10e42620c4e3be1b9d63eb31c9e8ffe60ea788d3f4a8ae4abeac3dccdf5b21}
```
