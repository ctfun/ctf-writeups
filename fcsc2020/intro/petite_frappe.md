# Petite frappe 1/3

**Challenge**

Lors de l’investigation d’un poste GNU/Linux, vous analysez un fichier qui semble être généré par un programme d’enregistrement de frappes de clavier (enregistrement de l’activité de chaque touche utilisée). Retrouvez ce qui a bien pu être écrit par l’utilisateur de ce poste à l’aide de ce fichier !

Note : Insérer le contenu tapé au clavier de ce poste entre FCSC{...} pour obtenir le flag.

Fichier: [petite_frappe_1.txt](petite_frappe_1.txt)


**Solution**

*Résolue en 2024 grâce à hackropole.fr*

Le fichier contient [une liste d'évènements](https://www.kernel.org/doc/html/latest/input/event-codes.html) générés lors de la frappe d'un clavier.

```
Event: time 1584656705.424839, -------------- SYN_REPORT ------------
Event: time 1584656706.404214, type 4 (EV_MSC), code 4 (MSC_SCAN), value 16
Event: time 1584656706.404214, type 1 (EV_KEY), code 22 (KEY_U), value 1
```

Ce qui va nous intéresser ici, c'est les symboles associées `KEY_x`. On scripte :

```
❯ grep -o KEY_. petite_frappe_1.txt  | uniq | sed "s/KEY_//" | xargs | sed "s/ //g"
UNEGENTILEINTRODUDUCTION
```

Il faut rectifier le résultat :
- le script élimine les doublons (qui existent je suppose parce qu'il y a un évènement d'appui, puis de relâche), 
- il va donc falloir ajouter un `L` car les 4 évènements ont été réduits à un seul.
- la frappe des touches `D` et `U` a probablement été très rapide car les évènements sont intriqués.

Le flag: `FCSC{UNEGENTILLEINTRODUCTION}`