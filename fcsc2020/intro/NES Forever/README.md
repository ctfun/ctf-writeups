# NES Forever

**Description**

Le bon vieux temps ! Pour trouver le flag, vous allez devoir inspecter un langage qui fait la base d’Internet.

Fichier: [docker-compose.yml](docker-compose.yml)

**Solution**

> Résolu en 2024 grâce à https://hackropole.fr/

On ouvre le code source de la page principale, et on trouve:

```html
  <!--
  FCSC{a1cec1710b5a2423ae927a12db174337508f07b470fc0a29bfc73461f131e0c2}
  -->
```