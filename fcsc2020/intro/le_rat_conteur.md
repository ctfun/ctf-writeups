# Le Rat Conteur (20 points)

**Challenge**

Le fichier suivant a été chiffré en AES-128 en mode CTR, avec la clé de 128 bits 00112233445566778899aabbccddeeff et un IV nul.

À vous de le déchiffrer pour obtenir le flag.

SHA256(flag.jpg.enc) = 3f326f481297b9eabf09b73d8cc178b14e1d2fbd614a20544fc632fcf15690ad

<details>
    <summary>Indice</summary>
    Explorer les options de la commande openssl pour déchiffrer le fichier.
</details>

<details>
    <summary>Indice</summary>
    La commande à trouver commence par : openssl enc -aes-128-ctr
</details>

[flag.jpg.enc](https://france-cybersecurity-challenge.fr/files/bcc7586fb86dfad03857f289c579b386/flag.jpg.enc)

**Solution**

Je n'aime définitivement pas openssl. La solution peut simplement s'obtenir avec [CyberChef](https://gchq.github.io/CyberChef/) :

![Le Rat Conteur 1](le_rat_conteur1.png)

Le résultat :

![Le Rat Conteur 2](le_rat_conteur2.jpg)
