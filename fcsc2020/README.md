# FCSC 2020

![](rank.png)


## intro

- [Babel Web](intro/babel_web.md)
- [Poney](intro/poney.md)
- [Cap ou Pcap](intro/cap_ou_pcap.md)
- [Le Rat Conteur](intro/le_rat_conteur.md)
- [Tarte Tatin](intro/tarte_tatin.md)
- [SuSHi](intro/shushi.md)
- [NES Forever](intro/NES%20Forever/README.md)
- [Petite frappe 1/3](intro/petite_frappe.md)

## misc

- [Clepsydre](misc/clepsydre.md)
- [Le Rustique](misc/le_rustique.md)
- [Mazopotamia](misc/mazopotamia.md)

## crypto

- [SMIC 1](crypto/SMIC1/README.md)
- [SMIC 2](crypto/SMIC2/README.md)

## hardware


## forensics

- [Find Me](forensics/find_me.md)
- [Académie de l'investigation - C'est la rentrée](forensics/rentree.md)
- [Académie de l'investigation - Porte dérobée](forensics/porte_derobee.md)
- [Académie de l'investigation - Rédaction](forensics/redaction.md)
- [Académie de l'investigation - Premiers artefacts](forensics/premiers_artefacts.md)
- [Chapardeur de mots de passe](forensics/chapardeur.md)
- [CryptoLocker V1](forensics/cryptolocker.md)
- [CryptoLocker V2](forensics/cryptolockerv2.md)
- [Petite frappe 2/3](forensics/petite_frappe_2.md)
- [Petite frappe 3/3](forensics/PetiteFrappe3/README.md)

## reverse

- [Ssecret](reverse/ssecret/README.md)
- [Infiltrate](reverse/infiltrate/infiltrate.md)
- [Serial Keyler](reverse/serial_keyler.md)

## pwn

- [Root Kitty](pwn/RootKitty/README.md)

## web

- [EnterTheDungeon](web/enter_dungeon/dungeon.md)
- [Lipogrammeurs](web/lipogrammeurs/lipogrammeurs.md)
- [Flag Checker](web/flag_checker/flag_checker.md)
- [Bestiary](web/bestiary/bestiary.md)
- [Rainbow Pages 1](web/rainbow_pages_1/README.md)
- [Revision](web/revision/README.md)
- [Shuffle Shop](web/shuffle_shop/README.md)


