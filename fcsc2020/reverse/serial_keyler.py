#!/usr/bin/env python3

from telnetlib import Telnet
import re

tn = Telnet('challenges2.france-cybersecurity-challenge.fr', 3001)
while True:
    q = tn.read_until(b'>>>').decode('utf-8')
    print(q, end='')
    search = re.search(r'What is a valid serial for username: (.*)', q)
    if not search:
        exit(0)
    username = search.group(1)
    serial = ''.join([ chr(ord(c) ^ 0x1f) for c in reversed(username) ])
    print(' {}'.format(serial))
    tn.write('{}\n'.format(serial).encode('utf-8'))