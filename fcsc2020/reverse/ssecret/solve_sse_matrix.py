import re
import struct
import base64
import z3

def fakepopcount( g ):
  r=0
  for j in range(0,128):
    r ^= (g>>j) & 1
  return r

def check(i, values):
  mask = 1 << 127
  res =0
  c = 0
  for v in values:
    c += 1
    a = fakepopcount( (x & v) )
    res |= mask * (a&1)
    mask = mask >>1
  return res

def solve(matrix, expected_res):
  res = []
  for i in range(0, 128):
    m=matrix[127-i] 
    e=expected_res[127-i]
    mask = 0
    for j in range(0,i):
      mask |= 1<<j
    tmp_res = 0
    z=0
    for r in res:
      tmp_res |= r<<z
      z+=1
    #print("%x & %x & %x (%x) == %x"%(m,mask,tmp_res,fakepopcount( m&mask&tmp_res ) ,e))
    if fakepopcount( m&mask&tmp_res ) == e:
      res.append(0)
    else:
      res.append(1)
  tmp_res = 0
  z=0
  for r in res:
    tmp_res |= r<<z
    z+=1
  return tmp_res
    
     
def swapp(matrix, i , j):
  tmp=matrix[i]
  matrix[i] = matrix[j]
  matrix[j] = tmp
          
def stepped_matrix(matrix, expected_res):
  for i in range(0, 128):
    mask = 1<<(127-i)
    if not mask&matrix[i]:
      # Line not starting with a 1 -> need to swapp is another line...
      for j in range(i, 128):
        if mask&matrix[j]:
          swapp(matrix, i,j)
          swapp(expected_res, i,j)
          break
    if not mask&matrix[i]:
      raise Exception
    for j in range(i+1, 128):
      if mask&matrix[j]:
        matrix[j] = matrix[j] ^ matrix[i]
        expected_res[j] = expected_res[j] ^ expected_res[i]
  return (matrix, expected_res)

def load_matrix():
  with open("sse_values") as f:
      l=f.readlines()
  values=[]
  for line in l:
      m=re.findall(r"\(([x0-9a-f]+)\),([x0-9a-f]+)",line)
      m=m[0]
      v=int(m[0],16)+(int(m[1],16)<<64)
      values.append(v)
  return values
 
print("Load matrix from code")
matrix = load_matrix()
expected =  matrix.pop(-1)
print("Expected res %x"%expected)
expected_res = expected
expected_res = [ (expected_res>>(127-i))&1 for i in range(0,128) ]

tmp_matrix = [m for m in matrix]
tmp_expected_res = [r for r in expected_res]

(tmp_matrix, tmp_expected_res) = stepped_matrix(tmp_matrix, tmp_expected_res)
print("\nStepped matrix:")
print(tmp_matrix)

res = solve(tmp_matrix, tmp_expected_res)
print("\nSolution :")
print(hex(res))
x=res

print("\nDouble check:")
print(hex(check(res,  matrix)))
print("      ?=")
print(hex(expected))

res = ""
res += struct.pack("<Q", x & 0xffffffffffffffff)
res += struct.pack("<Q", (x>>64) & 0xffffffffffffffff)

print("\nBase64")
print(base64.b64encode(res))
