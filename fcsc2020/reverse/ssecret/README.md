# SSECRET

`ssecret` est un challenge de reverse engineering basé sur les extensions d'instructions SSE.

Le binaire s'execute en 3 phases:
 - Decodage de l'entrée base64
 - Vérification de la clé
 - Décryptage de la payload

La partie vérification de la clé m'a posé beaucoup de difficultés pendant le CTF. Et ce n'est qu'après que j'ai rés
olu le problème qui m'était présenter.

## Discalmer

Il existe déjà d'excellents write-ups sur ce challenge:
 - [https://s01den.github.io/WriteUp_SSEcret_S01den](https://s01den.github.io/WriteUp_SSEcret_S01den)
 - [https://blog.h25.io/FCSC-SSEcret/](https://blog.h25.io/FCSC-SSEcret/)

Ayant eu des difficultés sur la résolution du problème mathématique j'ai décidé de rédiger un write-up de ma solution, qui n'est peut-être pas très académique et un peu trop intuitive.

## Le problème

Après avoir reversé le code on comprend que le problème posé est de la forme suivante:

```C
var INPUT (128 bits)
cst MASK (128 bits)
cst ARRAY[128]

for each A in ARRAY:
  POPCNT( A & INPUT ) & 0x1 ?= MASK
```

Notez qu'il est possible de traduire le `POPCNT & 1` par un `XOR`.

Le calcul est donc de la forme suivante. 

![problem](problem.png)

Résoudre ce problème n'est pas possible avec `z3`.
Il m'a donc fallu trouver une méthode plus manuelle.

## Le math hack

Le problème posé est de la forme:

```C
 (a1 & i1) ^ (a2 & i2) ^ (a3 & i3) = m1
 (b1 & i1) ^ (b2 & i2) ^ (b3 & i3) = m2
 (c1 & i1) ^ (c2 & i2) ^ (c3 & i3) = m3
```

Pour le résoudre, mon astuce consiste à le faire passé sous la **forme échelonnée**:

```C
 (a'1 & i1) ^ (a'2 & i2) ^ (a'3 & i3) = m'1
              (b'2 & i2) ^ (b'3 & i3) = m'2
                           (c'3 & i3) = m'3
```

Ainsi on pourra calculer récursivement `i3` puis `i2` et enfin `i1`.

**Mais comment transformer en forme échelonnée?**

## Un exemple

Prenons un exemple, avec la matrice:

```C
 1 & i1 ^ 0 & i2 ^ 1 & i3 = 1
 0 & i1 ^ 1 & i2 ^ 1 & i3 = 0
 1 & i1 ^ 1 & i2 ^ 1 & i3 = 1
```

Pour n'avoir qu'un seul 1 sur la première colonne, nous allons xorer la ligne 3, avec la ligne 1 (ainsi que leurs résultats respectifs):


```C
 1 & i1 ^ 0 & i2 ^ 1 & i3 = 1
 0 & i1 ^ 1 & i2 ^ 1 & i3 = 0
 0 & i1 ^ 1 & i2 ^ 0 & i3 = 0
```

Nous avons ainsi effacé le 1 de la première colonne, sur la ligne 3. Nous pouvons maintenant reproduire cette mécanique sur la ligne 3 avec la colonne 2:

```C
 1 & i1 ^ 0 & i2 ^ 1 & i3 = 1
 0 & i1 ^ 1 & i2 ^ 1 & i3 = 0
 0 & i1 ^ 0 & i2 ^ 1 & i3 = 0
```

Nous avons maintenant une matrice échelonnée qui est facile à résoudre.
Ici, `i3=0`, `i2=0` et `i1=1`.

## The end

La suite du challenge consiste à être en mesure d'automatiser cette mécanique plusieurs fois, de manière à pouvoir résoudre plusieurs blocs 128 bits de la clé et ainsi décrypter entièrment la payload jusqu'à l'affichage du flag.

Je fournie un PoC qui effectue le calcul de la matrice échelonnée sur les premiers 128 bits à trouver.


