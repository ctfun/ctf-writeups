#!/usr/bin/env python3

from PIL import Image
import sys

im = Image.open('infiltrate.png', 'r')
print('Image : {}'.format(im.size))


WHITE=(255, 255, 255)
BLACK=(0, 0, 0)

pixels = im.load()
octet = ''
content = bytearray()
for j in range(im.height):
    for i in range(im.width):
        if pixels[i,j] == WHITE:
            octet += '1'
        elif pixels[i,j] == BLACK:
            octet += '0'
        else:
            print('C''est ni blanc, ni noir :-( : {}'.format(pixels[i,j]))
            exit(0)
        
        if len(octet) == 8:
            content.append(int(octet, 2))
            octet = ''

im.close()

bin = open(sys.argv[1], 'wb')
print('Writing to binary {}...'.format(sys.argv[1]))
bin.write(content)
bin.close()