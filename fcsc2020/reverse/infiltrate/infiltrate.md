# Infiltrate (25 points)

**Challenge**

Des agents ont réussi à exfiltrer un fichier en utilisant la LED du disque dur durant une copie de disque. Ils nous ont fourni l'image de la capture.

Retrouvez le flag.

SHA256(infiltrate.png) = 47c8b350989859c8b35d72f8b28f3f91df5766b350f49f7b817303309da1b77c.

[infiltrate.png](https://france-cybersecurity-challenge.fr/files/74a7dbd2362bbc60b950665742696e59/infiltrate.png)


**Solution**

Regardons cette image de plus près :

![infiltrate](infiltrate.png)

Du noir et blanc, une explication indiquant qu'on a capturé la LED du disque dur... On peut imaginer que l'image est le binaire du contenu du fichier, et qu'un pixel noir représente un zéro, et un blanc un un. Ça fait très production Hollywoodienne puisque dans la vraie vie, la LED d'un disque ne s'allume pas de cette façon, mais n'oublions pas qu'il s'agit d'une épreuve facile... ;-)

Avec un [court script python](infiltrate.py), on convertit cette image en binaire et on regarde son contenu :

```console
$ python3 infiltrate.py plop.bin
Image : (300, 350)
Writing to binary plop.bin...
$ file plop.bin 
plop.bin: data
$ od -t x1z  plop.bin  | head -10
0000000 04 51 81 91 55 50 89 d5 c0 e4 4d 3d 6a e7 1d ed  >.Q..UP....M=j...<
0000020 7f 45 4c 46 02 01 01 00 00 00 00 00 00 00 00 00  >.ELF............<
0000040 03 00 3e 00 01 00 00 00 90 07 00 00 00 00 00 00  >..>.............<
0000060 40 00 00 00 00 00 00 00 88 1a 00 00 00 00 00 00  >@...............<
0000100 00 00 00 00 40 00 38 00 09 00 40 00 1d 00 1c 00  >....@.8...@.....<
0000120 06 00 00 00 04 00 00 00 40 00 00 00 00 00 00 00  >........@.......<
0000140 40 00 00 00 00 00 00 00 40 00 00 00 00 00 00 00  >@.......@.......<
0000160 f8 01 00 00 00 00 00 00 f8 01 00 00 00 00 00 00  >................<
0000200 08 00 00 00 00 00 00 00 03 00 00 00 04 00 00 00  >................<
0000220 38 02 00 00 00 00 00 00 38 02 00 00 00 00 00 00  >8.......8.......<
```

Le fichier n'a pas l'air correct, mais le dump hexa montre un binaire ELF qui démarre après 16 octets. Essayons d'extraire cet exécutable et de le lancer :

```console
$ tail -c +17 plop.bin > infiltrate.bin 
$ file infiltrate.bin 
infiltrate.bin: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=de88640a6b950fce6f8529944f6c11083d967674, not stripped
$ chmod +x infiltrate.bin 
$ ./infiltrate.bin 
Hello! Entrez la clé 
toto
Mauvaise clé !
```

On va pouvoir commencer à reverser. Rapidement, c'est la fonction `doit` qui attire notre attention:

```nasm
gef➤  disass doit
Dump of assembler code for function doit:
   0x000000000000089a <+0>:	push   rbp
   0x000000000000089b <+1>:	mov    rbp,rsp
   0x000000000000089e <+4>:	sub    rsp,0x40
   0x00000000000008a2 <+8>:	mov    QWORD PTR [rbp-0x38],rdi
   0x00000000000008a6 <+12>:	mov    rax,QWORD PTR fs:0x28
   0x00000000000008af <+21>:	mov    QWORD PTR [rbp-0x8],rax
   0x00000000000008b3 <+25>:	xor    eax,eax
   0x00000000000008b5 <+27>:	mov    rax,QWORD PTR [rbp-0x38]
   0x00000000000008b9 <+31>:	mov    rdi,rax
   0x00000000000008bc <+34>:	call   0x750 <strlen@plt>
   0x00000000000008c1 <+39>:	mov    rcx,rax
   0x00000000000008c4 <+42>:	lea    rdx,[rbp-0x20]
   0x00000000000008c8 <+46>:	mov    rax,QWORD PTR [rbp-0x38]
   0x00000000000008cc <+50>:	mov    rsi,rcx
   0x00000000000008cf <+53>:	mov    rdi,rax
   0x00000000000008d2 <+56>:	call   0x760 <SHA1@plt>
   0x00000000000008d7 <+61>:	mov    DWORD PTR [rbp-0x24],0x0
   0x00000000000008de <+68>:	mov    DWORD PTR [rbp-0x28],0x0
   0x00000000000008e5 <+75>:	jmp    0x9a8 <doit+270>
   0x00000000000008ea <+80>:	movzx  eax,BYTE PTR [rbp-0x20]
   0x00000000000008ee <+84>:	cmp    al,0x58
   0x00000000000008f0 <+86>:	jne    0x9a0 <doit+262>
   0x00000000000008f6 <+92>:	movzx  eax,BYTE PTR [rbp-0x1f]
   0x00000000000008fa <+96>:	cmp    al,0x23
   0x00000000000008fc <+98>:	jne    0x9a0 <doit+262>
   0x0000000000000902 <+104>:	movzx  eax,BYTE PTR [rbp-0x16]
   0x0000000000000906 <+108>:	cmp    al,0xa3
   0x0000000000000908 <+110>:	jne    0x9a0 <doit+262>
   0x000000000000090e <+116>:	movzx  eax,BYTE PTR [rbp-0x1e]
   0x0000000000000912 <+120>:	cmp    al,0xdb
   0x0000000000000914 <+122>:	jne    0x9a0 <doit+262>
   0x000000000000091a <+128>:	movzx  eax,BYTE PTR [rbp-0x1d]
   0x000000000000091e <+132>:	cmp    al,0x97
   0x0000000000000920 <+134>:	jne    0x9a0 <doit+262>
   0x0000000000000922 <+136>:	movzx  eax,BYTE PTR [rbp-0x1a]
   0x0000000000000926 <+140>:	cmp    al,0xc4
   0x0000000000000928 <+142>:	jne    0x9a0 <doit+262>
   0x000000000000092a <+144>:	movzx  eax,BYTE PTR [rbp-0x1c]
   0x000000000000092e <+148>:	cmp    al,0x68
   0x0000000000000930 <+150>:	jne    0x9a0 <doit+262>
   0x0000000000000932 <+152>:	movzx  eax,BYTE PTR [rbp-0x1b]
   0x0000000000000936 <+156>:	cmp    al,0x1
   0x0000000000000938 <+158>:	jne    0x9a0 <doit+262>
   0x000000000000093a <+160>:	movzx  eax,BYTE PTR [rbp-0xe]
   0x000000000000093e <+164>:	cmp    al,0x26
   0x0000000000000940 <+166>:	jne    0x9a0 <doit+262>
   0x0000000000000942 <+168>:	movzx  eax,BYTE PTR [rbp-0x19]
   0x0000000000000946 <+172>:	cmp    al,0xa0
   0x0000000000000948 <+174>:	jne    0x9a0 <doit+262>
   0x000000000000094a <+176>:	movzx  eax,BYTE PTR [rbp-0x18]
   0x000000000000094e <+180>:	cmp    al,0xe2
   0x0000000000000950 <+182>:	jne    0x9a0 <doit+262>
   0x0000000000000952 <+184>:	movzx  eax,BYTE PTR [rbp-0x17]
   0x0000000000000956 <+188>:	cmp    al,0xd7
   0x0000000000000958 <+190>:	jne    0x9a0 <doit+262>
   0x000000000000095a <+192>:	movzx  eax,BYTE PTR [rbp-0xd]
   0x000000000000095e <+196>:	cmp    al,0x12
   0x0000000000000960 <+198>:	jne    0x9a0 <doit+262>
   0x0000000000000962 <+200>:	movzx  eax,BYTE PTR [rbp-0x15]
   0x0000000000000966 <+204>:	cmp    al,0x30
   0x0000000000000968 <+206>:	jne    0x9a0 <doit+262>
   0x000000000000096a <+208>:	movzx  eax,BYTE PTR [rbp-0x14]
   0x000000000000096e <+212>:	cmp    al,0xb2
   0x0000000000000970 <+214>:	jne    0x9a0 <doit+262>
   0x0000000000000972 <+216>:	movzx  eax,BYTE PTR [rbp-0x13]
   0x0000000000000976 <+220>:	cmp    al,0xbb
   0x0000000000000978 <+222>:	jne    0x9a0 <doit+262>
   0x000000000000097a <+224>:	movzx  eax,BYTE PTR [rbp-0x11]
   0x000000000000097e <+228>:	cmp    al,0xfe
   0x0000000000000980 <+230>:	jne    0x9a0 <doit+262>
   0x0000000000000982 <+232>:	movzx  eax,BYTE PTR [rbp-0x10]
   0x0000000000000986 <+236>:	cmp    al,0x27
   0x0000000000000988 <+238>:	jne    0x9a0 <doit+262>
   0x000000000000098a <+240>:	movzx  eax,BYTE PTR [rbp-0x12]
   0x000000000000098e <+244>:	cmp    al,0x82
   0x0000000000000990 <+246>:	jne    0x9a0 <doit+262>
   0x0000000000000992 <+248>:	movzx  eax,BYTE PTR [rbp-0xf]
   0x0000000000000996 <+252>:	cmp    al,0xcc
   0x0000000000000998 <+254>:	jne    0x9a0 <doit+262>
   0x000000000000099a <+256>:	add    DWORD PTR [rbp-0x24],0x1
   0x000000000000099e <+260>:	jmp    0x9a4 <doit+266>
   0x00000000000009a0 <+262>:	sub    DWORD PTR [rbp-0x24],0x1
   0x00000000000009a4 <+266>:	add    DWORD PTR [rbp-0x28],0x1
   0x00000000000009a8 <+270>:	cmp    DWORD PTR [rbp-0x28],0x13
   0x00000000000009ac <+274>:	jle    0x8ea <doit+80>
   0x00000000000009b2 <+280>:	cmp    DWORD PTR [rbp-0x24],0x14
   0x00000000000009b6 <+284>:	jne    0x9d2 <doit+312>
   0x00000000000009b8 <+286>:	mov    rax,QWORD PTR [rbp-0x38]
   0x00000000000009bc <+290>:	mov    rsi,rax
   0x00000000000009bf <+293>:	lea    rdi,[rip+0x12e]        # 0xaf4
   0x00000000000009c6 <+300>:	mov    eax,0x0
   0x00000000000009cb <+305>:	call   0x710 <printf@plt>
   0x00000000000009d0 <+310>:	jmp    0x9de <doit+324>
   0x00000000000009d2 <+312>:	lea    rdi,[rip+0x138]        # 0xb11
   0x00000000000009d9 <+319>:	call   0x720 <puts@plt>
   0x00000000000009de <+324>:	nop
   0x00000000000009df <+325>:	mov    rax,QWORD PTR [rbp-0x8]
   0x00000000000009e3 <+329>:	xor    rax,QWORD PTR fs:0x28
   0x00000000000009ec <+338>:	je     0x9f3 <doit+345>
   0x00000000000009ee <+340>:	call   0x770 <__stack_chk_fail@plt>
   0x00000000000009f3 <+345>:	leave  
   0x00000000000009f4 <+346>:	ret    
```

On voit que la chaîne saisie est hachée par `SHA1` et le résultat est comparé octet par octet dans une série de `cmp/jne`. On va donc reconstruire le hash attendu, en prenant garde à tout remettre dans le bon ordre.

`5823db976801c4a0e2d7a330b2bb82fe27cc2612`

Google est notre ami pour trouver une table de correspondance, mais dans le pire des cas un coup de brute-force est suffisant pour déterminer que ce hash est le SHA1 de `401445`.

```console
$ ./infiltrate.bin 
Hello! Entrez la clé 
401445
Bravo, le flag est FCSC{401445}
```