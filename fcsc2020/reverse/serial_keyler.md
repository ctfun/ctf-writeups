# Serial Keyler (25 points)

**Challenge**

On vous demande d'écrire un générateur d'entrées valides pour ce binaire, puis de le valider sur les entrées fournies par le service distant afin d'obtenir le flag.

Service : nc challenges2.france-cybersecurity-challenge.fr 3001

SHA256(SerialKeyler) : 75525de17bd6493782256061f096e4567e0a26825fba2103c4815eeb9c121e73

[SerialKeyler](https://france-cybersecurity-challenge.fr/files/b913cdbbf7b5644a76a6cdf4d7cea2f9/SerialKeyler)


**Solution**

On fait quelques essais avec le binaire récupéré :

```console
$ file SerialKeyler
SerialKeyler: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=b307e4dbb397126c0f411da59fd4c4d57304e42e, stripped

$ ./SerialKeyler 
[+] Username: groutchmeu
[+] Serial:   12345
[!] Incorrect serial.

$ strings SerialKeyler
[...]
[+] Username: 
%63s
[+] Serial:   
[>] Valid serial!
[>] Now connect to the remote server and generate serials for the given usernames.
[!] Incorrect serial.
[...]

$ ltrace ./SerialKeyler 
printf("[+] Username: ")                                                = 14
__isoc99_scanf(0x563a267b3a57, 0x7ffc72ec40d0, 0, 0[+] Username: plopl
)                    = 1
printf("[+] Serial:   ")                                                = 14
__isoc99_scanf(0x563a267b3a57, 0x7ffc72ec4110, 0, 0[+] Serial:   aaaaaa
)                    = 1
strlen("plopl")                                                         = 5
memset(0x7ffc72ec4070, '\0', 64)                                        = 0x7ffc72ec4070
strcmp("aaaaaa", "sopso")                                               = -18
puts("[!] Incorrect serial."[!] Incorrect serial.
)                                           = 22
+++ exited (status 0) +++
```

Le truc intéressant est le `strcmp` dans la dernière trace. On remarque que le serial fait la même taille que le username. On soupçonne aussi qu'il s'agit d'une monosubstitution, le `p` étant «chiffré» en `s` et le `l` en `o`. Jetons un oeil au code, et en particulier à la routine charge du «chiffrement» :

```nasm
 889:	48 8b 55 88          	mov    -0x78(%rbp),%rdx
 88d:	48 8b 45 a0          	mov    -0x60(%rbp),%rax
 891:	48 01 d0             	add    %rdx,%rax
 894:	0f b6 00             	movzbl (%rax),%eax
 897:	88 45 9f             	mov    %al,-0x61(%rbp)
 89a:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 89e:	48 2b 45 a0          	sub    -0x60(%rbp),%rax
 8a2:	48 83 e8 01          	sub    $0x1,%rax
 8a6:	0f b6 55 9f          	movzbl -0x61(%rbp),%edx
 8aa:	83 f2 1f             	xor    $0x1f,%edx
 8ad:	88 54 05 b0          	mov    %dl,-0x50(%rbp,%rax,1)
 8b1:	48 83 45 a0 01       	addq   $0x1,-0x60(%rbp)
 8b6:	48 8b 45 a0          	mov    -0x60(%rbp),%rax
 8ba:	48 3b 45 a8          	cmp    -0x58(%rbp),%rax
 8be:	72 c9                	jb     889 <__cxa_finalize@plt+0x169>
```

Il s'agirait d'un `XOR` avec 0x1F. Faisons le test :

```python
>>> ''.join([ chr(ord(c) ^ 0x1f) for c in 'plopl' ])
'ospos'
```

Tout simpl... ah non, tiens, c'est à l'envers ! ;-)

```python
>>> ''.join([ chr(ord(c) ^ 0x1f) for c in reversed('plopl') ])
'sopso'
```

Ne reste plus qu'à scripter le tout pour répondre au service, car on se rend vite compte qu'il va être laborieux de les passer à la main un par un. Donc on fait un [script python](serial_keyler.py) et voici le résultat :

```console
$ python3 reverse/serial_keyler.py 
What is a valid serial for username: ecsc
>>> |l|z
 What is a valid serial for username: ANSSI
>>> VLLQ^
 What is a valid serial for username: HelloWorld
>>> {smpHpsszW
 What is a valid serial for username: TeamFrance
>>> z|q~mYr~zK
 What is a valid serial for username: #ECSC #TeamFR
>>> MYr~zK<?\L\Z<
 What is a valid serial for username: Dixie Muniz
>>> evqjR?zvgv[
 What is a valid serial for username: Regina Miller
>>> mzssvR?~qvxzM
 What is a valid serial for username: Jamie Fant
>>> kq~Y?zvr~U
 What is a valid serial for username: Anastacia Gosch
>>> w|lpX?~v|~kl~q^
 What is a valid serial for username: Andrew Ho
>>> pW?hzm{q^
 What is a valid serial for username: Lois Lewis
>>> lvhzS?lvpS
 What is a valid serial for username: David Baird
>>> {mv~]?{vi~[
 What is a valid serial for username: Jimmy Blake
>>> zt~s]?frrvU
 What is a valid serial for username: Seth Kanas
>>> l~q~T?wkzL
 What is a valid serial for username: Timothy Alfonso
>>> plqpys^?fwkprvK
 What is a valid serial for username: Dorothy Porter
>>> mzkmpO?fwkpmp[
 What is a valid serial for username: Ruth Schnieders
>>> lmz{zvqw|L?wkjM
 What is a valid serial for username: Robert Burch
>>> w|mj]?kmz}pM
 What is a valid serial for username: Elena Roberts
>>> lkmz}pM?~qzsZ
 What is a valid serial for username: Hector Crouch
>>> w|jpm\?mpk|zW
 What is a valid serial for username: Mack Patel
>>> szk~O?t|~R
 What is a valid serial for username: Richard Williams
>>> lr~vssvH?{m~w|vM
 What is a valid serial for username: Nicholas Ross
>>> llpM?l~spw|vQ
 What is a valid serial for username: Carlyn Botner
>>> mzqkp]?qfsm~\
 What is a valid serial for username: Nichole Thomas
>>> l~rpwK?zspw|vQ
 What is a valid serial for username: Josephine Wilkerson
>>> qplmztsvH?zqvwozlpU
 What is a valid serial for username: Samuel Carovski
>>> vtlipm~\?szjr~L
 What is a valid serial for username: Ariel Buggs
>>> lxxj]?szvm^
 What is a valid serial for username: Jennifer Ober
>>> mz}P?mzyvqqzU
 What is a valid serial for username: Dorothy Seveney
>>> fzqzizL?fwkpmp[
 What is a valid serial for username: Vnt4kasB8WpJhdvzMcf
>>> y|Rei{wUoH']l~t+kqI
 What is a valid serial for username: TPmAZWXyAl6ScNnU
>>> JqQ|L)s^fGHE^rOK
 What is a valid serial for username: LsHC5dCqsOCYQim
>>> rvNF\Pln\{*\WlS
 What is a valid serial for username: isAdAog4Pg0DeYIGzF0rCj
>>> u\m/YeXVFz[/xO+xp^{^lv
 What is a valid serial for username: ZivRMJxaxvKgA
>>> ^xTig~gURMivE
 What is a valid serial for username: Kxrd4we7o0Cnb
>>> }q\/p(zh+{mgT
 What is a valid serial for username: w2Drmjpmp5hGnQoaVrGCrk
>>> tm\XmI~pNqXw*orourm[-h
 What is a valid serial for username: Z95aK5sE9ujv2
>>> -iuj&Zl*T~*&E
 What is a valid serial for username: qWgf0zp2FT5emRgqwNkoL8
>>> 'SptQhnxMrz*KY-oe/yxHn
 What is a valid serial for username: eVQKs3wBwYtEEe
>>> zZZkFh]h,lTNIz
 What is a valid serial for username: ckp6zvkDprHkWdqD2p
>>> o-[n{HtWmo[tie)ot|
 What is a valid serial for username: hBEVkZ0mrT41TC7cQytYR0g
>>> x/MFkfN|(\K.+Kmr/EtIZ]w
 What is a valid serial for username: rbgRGzpExlXIebiDaQ
>>> N~[v}zVGsgZoeXMx}m
 What is a valid serial for username: 8Utv5uQrjubepWOyOvvw
>>> hiiPfPHoz}jumNj*ikJ'
 What is a valid serial for username: 30ywUjHunBr7K3b5c038HFTe
>>> zKYW',/|*},T(m]qjWuJhf/,
 What is a valid serial for username: 1TBmceYtVsyo
>>> pflIkFz|r]K.
 What is a valid serial for username: bDpktQjgxyrmQ
>>> NrmfgxuNkto[}
 What is a valid serial for username: gm203BnForlUkXIfKU8w
>>> h'JTyVGtJsmpYq],/-rx
 What is a valid serial for username: uq067eUcfjmOafsGTfQbhQ8
>>> 'Nw}NyKXly~Pruy|Jz()/nj
 What is a valid serial for username: EbLgbv9NpLAP
>>> O^SoQ&i}xS}Z
 What is a valid serial for username: AX98cQ5PivztEPaG
>>> X~OZkeivO*N|'&G^
 What is a valid serial for username: 7bxh3PHLAA9SKM
>>> RTL&^^SWO,wg}(
 What is a valid serial for username: BP2UEVqGaXeh4S
>>> L+wzG~XnIZJ-O]
 What is a valid serial for username: XCvnV4C5JpYmqvIH4RvLsuZw
>>> hEjlSiM+WVinrFoU*\+Iqi\G
 What is a valid serial for username: feIWhM9D3iDRdFjmdaZFjA
>>> ^uYE~{ruY{M[v,[&RwHVzy
 Well done! Here is the flag: FCSC{8f1018d0cfe395018a1c90dbff352e2ba4a6261336fb7c32454cdae4974d4333}
```
