# Clepsydre (138 points)

**Challenge**

À l'origine, la clepsydre est un instrument à eau qui permet de définir la durée d'un évènement, la durée d'un discours par exemple. On contraint la durée de l’évènement au temps de vidage d'une cuve contenant de l'eau qui s'écoule par un petit orifice. Dans l'exemple du discours, l'orateur doit s'arrêter quand le récipient est vide. La durée visualisée par ce moyen est indépendante d'un débit régulier du liquide ; le récipient peut avoir n'importe quelle forme. L'instrument n'est donc  pas une horloge hydraulique (Wikipedia).

Service : nc challenges2.france-cybersecurity-challenge.fr 6006

**Solution**

En se connectant sur le service distant, tout ce qu'on obtient est :
```
Entrez votre mot de passe :
```

Avec un peu d'expérience, vu la référence au temps, on est probablement en face d'un challenge demandant une attaque temporelle : le temps de vérification du mot de passe n'est pas constant, et en mesurant le temps de réponse du service, on doit pouvoir en déduire quels caractères composent le mot de passe.

On peut vérifier ça en essayant quelques mots de passe d'un caractère, et de voir comment le service répond. La majorité du temps, il répond  `Mot de passe incorrect !` immédiatement, sauf quand on saisit 'T'. Dans ce cas, il répond avec le même message, mais après une seconde.

On en déduit donc que le service examine les caractères du mot de passe un par un, et si le caractère est juste, il pause pendant une seconde. C'est largement suffisant pour déterminer quels sont les caractères justes ou non, et une simple attaque par force brute va permettre d'énumérer chaque caractère. Par contre, ça va être long.

La résolution peut donc se faire par [ce script python](clepsydre.py). La liste des caractères à tester aurait probablement pu être plus courte, mais le concepteur du challenge a été un peu vicieux. Heureusement, le mot de passe n'est pas très long.

```console
$ python clepsydre.py ""
 !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRST (T)
 !"#$%&'()*+,-./0123 (T3)
 !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklm (T3m)
 !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnop (T3mp)
 !"# (T3mp#)
 ! 

Félicitations vous avez su vaincre votre impatience : 

FCSC{6bdd5f185a5fda5ae37245d355f757eb0bbe888eea004cda16cf79b2c0d60d32}
```