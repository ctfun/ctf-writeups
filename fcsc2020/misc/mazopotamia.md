# Mazopotamia (170 points)

**Challenge**

Bienvenue en Mazopotamia ! Pour obtenir le flag, vous devrez vous échapper de plusieurs labyrinthes comme celui montré ci-dessous. 

![exemple](mazopotamia/mazopotamia.png)

Toutes les informations sont données ici :

nc challenges2.france-cybersecurity-challenge.fr 6002

**Solution**

L'énoncé est assez limpide : on est face à un labyrinthe, relativement simple si ce n'est qu'il semble y avoir des portes de couleur, et une notion de sens dans lequel on a le droit de les passer (ici, alternativement des portes bleues et rouge). Il ne semble pas nécessaire de fournir le chemin le plus court.

La connexion au serveur fournit des éléments supplémentaires : un nouvel exemple, qui nous est envoyé en base64, et une solution possible pour cet exemple (`NENNWWNWWSSSES`).

![exemple](mazopotamia/exemple.png)

À ce moment, on se rend compte d'une subtilité : là où je m'attendais à avancer de case en case, il s'avère que les portes semblent «pousser» le joueur (disons que c'est comme ça que je me le représente). Ainsi, faire un déplacement qui aboutit sur un porte nous déplace... à la case qui suit la porte. Je m'en suis rendu compte assez tard, ce qui explique pourquoi c'est un peu moche dans mon code de résolution. ;-)

Ma solution en python est disponible [ici](mazopotamia.py). Quelques remarques:
- mon code est moche (patch à l'arrache sans refacto), mais j'ai la flemme.
- j'ai codé l'interaction avec le serveur dans un autre script, qui n'a pas d'intérêt. C'était plus simple pour déboguer.
- c'est un simple parcours en profondeur, écrit en récursif avec une pile permettant d'éviter les boucles. C'est suffisant, d'autant qu'on ne fait que chercher un parcours qui fonctionne, sans autre contrainte.
- évidemment, il n'y a pas qu'un seul labyrinthe à résoudre. En fait, il y en a... 30. 
- évidemment, les contraintes sur les portes changent en cours de route (on va jusqu'à 4 types de portes différentes) et les tailles également. Il vaut mieux l'avoir prévu dès le début.

Au final, pour le fun, voici les 30 labyrinthes que mon script a traité :

|||||||
|---|---|---|---|---|---|
| [1](mazopotamia/maze01.png)  | [2](mazopotamia/maze02.png)  | [3](mazopotamia/maze03.png)  | [4](mazopotamia/maze04.png)  | [5](mazopotamia/maze05.png)  | [6](mazopotamia/maze06.png)  |
| [7](mazopotamia/maze07.png)  | [8](mazopotamia/maze08.png)  | [9](mazopotamia/maze09.png)  | [10](mazopotamia/maze10.png) | [11](mazopotamia/maze11.png) | [12](mazopotamia/maze12.png) | 
| [13](mazopotamia/maze13.png) | [14](mazopotamia/maze14.png) | [15](mazopotamia/maze15.png) | [16](mazopotamia/maze16.png) | [17](mazopotamia/maze17.png) | [18](mazopotamia/maze18.png) |
| [19](mazopotamia/maze19.png) | [20](mazopotamia/maze20.png) | [21](mazopotamia/maze21.png) | [22](mazopotamia/maze22.png) | [23](mazopotamia/maze23.png) | [24](mazopotamia/maze24.png) |
| [25](mazopotamia/maze25.png) | [26](mazopotamia/maze26.png) | [27](mazopotamia/maze27.png) | [28](mazopotamia/maze28.png) | [29](mazopotamia/maze29.png) | [30](mazopotamia/maze30.png) |
