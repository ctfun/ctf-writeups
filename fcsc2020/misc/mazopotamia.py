#!/usr/bin/env python3

from PIL import Image
import sys

color = {
    (0, 0, 0)       : "#",
    (255, 255, 255) : ' ',
    (0, 80, 239)    : 'b',      # bleu
    (229, 20, 0)    : 'r',      # rouge
    (250, 104, 0)   : 'o',      # orange
    (27, 161, 226)  : 'c',      # cyan
    (253, 253, 253) : 'S',      # flèche de sortie. ;-)
}

img = Image.open(sys.argv[1])
#print(img.size)
width, height = img.size
pixels = img.load()

# Carré de 64x64
# Premier en haut à gauche : 64x192
SIZE=64

maze = list()
found = ''
for i in range(SIZE*3, height-SIZE*2, SIZE):
    line = ''
    for j in range(SIZE, width-SIZE, SIZE):
        col = pixels[j+SIZE/2, i+SIZE/2]
        if col in color:
            c = color[col]
            if c not in found:
                found += c
        else:
            print('Couleur inconnue : {}'.format(col))
            c = '?'
        line += c
    maze.append(line)

cycle = 'br'
if 'o' in found:
    cycle = 'bro'
if 'c' in found:
    cycle = 'broc'
print('Cycle : {}'.format(cycle))

# Note : sur la dernière ligne, l'entrée est # et la sortie est ? ;-)
i = height-SIZE*2+SIZE/2
for j in range(1, width//SIZE-1):
    col = pixels[j*SIZE+SIZE/2, i]
    if col in color and color[col] == '#':
        entree = (len(maze)-1, j-1)
    elif col in color and color[col] == 'S':
        sortie = (len(maze), j-1)   # Parce que la porte va nous éjecter (cf. plus loin)

print('\n'.join(maze))
print('(entree, sortie)=({},{})'.format(entree, sortie))
height, width = len(maze), len(maze[0])

# Solve
deplact = {
    'N' : (-1, 0), 
    'E' : (0, +1), 
    'S' : (+1, 0), 
    'W' : (0, -1)
}
def next(pos, ppos, icycle, stack):
    print('Entering {}-{})'.format(pos, icycle))
    if pos == sortie:
        return (True, '')

    for d in deplact.keys():
        nicycle = icycle
        nx = pos[0] + deplact[d][0]
        ny = pos[1] + deplact[d][1]
        if (nx, ny) == ppos:
            continue
        if nx >= height or ny >= width: # Le reste on s'en fout : y'a des murs.
            continue 
        #print('Debug nx={}, ny={}'.format(nx, ny))
        c = maze[nx][ny]
        if c == '#':
            continue
        if c != ' ' and c != cycle[icycle]:
            continue
        if c == cycle[icycle]:
            nicycle = (icycle + 1) % len(cycle)
            # Fun fact (oupa), les portes t'éjectent (+1 déplacement gratuit)
            nx += deplact[d][0]
            ny += deplact[d][1]
        if ((nx, ny), nicycle) in stack:
            #print('Boucle !')
            continue
        nstack = stack.copy()
        nstack.append(((nx, ny), nicycle))
        found, trajet = next((nx, ny), pos, nicycle, nstack)
        if found:
            return True, d+trajet
    
    return False, None


def dump(pos):
    for i in range(height):
        for j in range(width):
            if (i, j) == pos:
                print("☺️", end='')
            else:
                print(maze[i][j], end='')
        print()


def dump_trajet(entree, trajet):
    pos = entree
    dump(pos)
    for m in trajet:
        print("Mouvement : {}".format(m))
        pos = ( pos[0] + deplact[m][0], pos[1] + deplact[m][1] )
        if maze[pos[0]][pos[1]] in cycle:
            pos = ( pos[0] + deplact[m][0], pos[1] + deplact[m][1] )
        dump(pos)
        #input()

_, trajet = next(entree, None, 1, [])
print(trajet)
dump_trajet(entree, trajet)
print(trajet)
