# Shuffle Shop

**Description**

Vous venez d’entrer dans un château…

Note : Cette épreuve avait été proposée lors de la finale du FCSC 2020.

Fichier: [docker-compose.yml](docker-compose.yml)


**Solution**

*Résolue en 2024 grâce à hackropole.fr*

On se connecte à l'IHM et:

![](UI1.png)

On clique sur le bouton:

![](UI2.png)

En arrière-plan, on voit deux appels vers le serveur :

```
GET
	http://localhost:8000/cart.php?refresh=

    {"total":0,"Health potion":0,"Stamina potion":0,"Strengh potion":0,"Clerverness potion":0,"Flag potion":0,"Breath potion":0,"Invisible potion":0,"Fly potion":0,"Love potion":0,"Fire-protection potion":0}
```

et

```
GET
	http://localhost:8000/cart.php?refreshEnc=

    4861d347d2f39fa406fb3b08f52982fe85d2927560d60fcf9d3c02e25e4f6f35f2df39dd23ed5845714a65ffb50a46e65150d1dec636fa71795100b5f07d4d7b05014d5e28fe8398ebbd11fe6b435b464d3cacf1a1c0d4e5b773c33c3c0f76b186d8f97992c0e811e680b8a2e7296fc82230982213a37ee2600f3293d47df45ceaa228e31358c18a1bd4079e9123a8a9e1d2e9a9fea06c969cf7a944df14394cde8a8f32d3cfd3c63a9f3e236b7d550de3f52fb891a0ed8ec7bf1e8d5af638099efe9cde6992f140ffb1258723c03760
```

À chaque fois qu'on appuie sur le bouton `add` d'une des potions, cela déclenche :

```
GET
	http://localhost:8000/cart.php?add=&idx=<identifiant de la potion>

    potion added
```

Ainsi que deux nouveaux appels à `refresh` et `refreshEnc`.

Après quelques clics sur `Shuffle !`, on voit apparaître des `Flag potion`, mais à 10000 FCSC-coins, on ne peut pas les acheter. De plus, lorsque l'on 
clique sur `Confirm cart`, on nous indique que le panier ne peut être validé s'il ne contient pas de `Flag potion`. On voit aussi, dans le js
de la page, que la validation du panier transmet ce que nous a retourné
`refreshEnc`, et qui semble être le panier sous forme chiffrée.

```
GET
	http://localhost:8000/cart.php?validate=&cart=4861d347d2f39fa406fb3b08f52982fe85d2927560d60fcf9d3c02e25e4f6f35f2df39dd23ed5845714a65ffb50a46e65150d1dec636fa71795100b5f07d4d7b05014d5e28fe8398ebbd11fe6b435b464d3cacf1a1c0d4e5b773c33c3c0f76b186d8f97992c0e811e680b8a2e7296fc82230982213a37ee2600f3293d47df45ceaa228e31358c18a1bd4079e9123a8a9e1d2e9a9fea06c969cf7a944df14394cde8a8f32d3cfd3c63a9f3e236b7d550de3f52fb891a0ed8ec7bf1e8d5af638099efe9cde6992f140ffb1258723c03760

    You should have the flag potion in your cart to win    
```


Donc, on suppose qu'il faut casser le chiffrement et forger un panier contenant des `Flag potion`. On remarque aussi qu'à chaque fois qu'on 
redémarre le process (en supprimant le cookie), on retombe sur la même valeur de chiffré. De plus, on arrive à générer des avertissements, mais
sans beaucoup de fuite d'information :

```
<br />
<b>Warning</b>:  hex2bin(): Hexadecimal input string must have an even length in <b>/var/www/html/libssl.php</b> on line <b>20</b><br />
You should have the flag potion in your cart to win 
```

On se creuse alors les méninges pour savoir à quel chiffrement on a
affaire. La payload chiffrée fait 208 octets, soit 13 blocs de 16. On
peut donc supposer de l'AES. Mais dans quel mode ? Décomposons  deux paniers proches en blocs de 16 :

Payload n°1:

```
01 {"total":10,"Hea    3fc02ee86c92c5429c1753f560b9e679
02 lth potion":0,"S    d04f5ef584029a718b779a05967bae0b
03 tamina potion":1    ee732993af1f1d3a6da01d7ce6f25947
04 ,"Strengh potion    e049440c06e50aec529fb14bdbbe81d4
05 ":0,"Clerverness    e0af636931ea0d18f030c8a9453679a1
06  potion":0,"Flag    1fb40b1f3f7881835386e2113d8b7777
07  potion":0,"Brea    3c7e45712b14444d470cfe46c6f9b27a
08 th potion":0,"In    53d2da2e395063a67047ecc49dec5aba
09 visible potion":    568c3df55964d7d3bab704e701831c34
10 0,"Fly potion":0    29e83a4493fd102cd9b49d2ada8b68d1
11 ,"Love potion":0    a04cb37d89749b3254e1933360286b8f
12 ,"Fire-protectio    a0ca8a3aa1e227b75d2a0e3baa91aac3
13 n potion":0}        5cc148da97c5cdd3cde4a35494765949
```

Payload n°2:

```
01 {"total":20,"Hea    a15320b18b2c290adf33f212ae175104    Différent
02 lth potion":0,"S    d04f5ef584029a718b779a05967bae0b
03 tamina potion":2    cb5998f5bd949f21ef8b0c84ba08735a    Différent
04 ,"Strengh potion    e049440c06e50aec529fb14bdbbe81d4
05 ":0,"Clerverness    e0af636931ea0d18f030c8a9453679a1
06  potion":0,"Flag    1fb40b1f3f7881835386e2113d8b7777
07  potion":0,"Brea    3c7e45712b14444d470cfe46c6f9b27a
08 th potion":0,"In    53d2da2e395063a67047ecc49dec5aba
09 visible potion":    568c3df55964d7d3bab704e701831c34
10 0,"Fly potion":0    29e83a4493fd102cd9b49d2ada8b68d1
11 ,"Love potion":0    a04cb37d89749b3254e1933360286b8f
12 ,"Fire-protectio    a0ca8a3aa1e227b75d2a0e3baa91aac3
13 n potion":0}        5cc148da97c5cdd3cde4a35494765949
```

On remarque que seul deux blocs diffèrent ! On est en présence d'ECB, 
chacun des blocs est chiffré/déchiffré indepdamment des autres !

On remarque aussi que les blocs 6 et 7 sont proches en contenu. Une idée possible est d'acheter
une potion Cleverness (tiens donc ?), de récupérer le bloc 6 correspondant, et de l'utiliser à la place du bloc 7.

Voici la payload n°3, après l'achat d'une Cleverness :

```
01 {"total":35,"Hea     6704961b408d4779b6cf7fd600ecded0
02 lth potion":0,"S     d04f5ef584029a718b779a05967bae0b
03 tamina potion":2     cb5998f5bd949f21ef8b0c84ba08735a
04 ,"Strengh potion     e049440c06e50aec529fb14bdbbe81d4
05 ":0,"Clerverness     e0af636931ea0d18f030c8a9453679a1
06  potion":1,"Flag     7fca8a0ff4f5cf1c34812e251a99f5ad    <===== Ici
07  potion":0,"Brea     3c7e45712b14444d470cfe46c6f9b27a
08 th potion":0,"In     53d2da2e395063a67047ecc49dec5aba
09 visible potion":     568c3df55964d7d3bab704e701831c34
10 0,"Fly potion":0     29e83a4493fd102cd9b49d2ada8b68d1
11 ,"Love potion":0     a04cb37d89749b3254e1933360286b8f
12 ,"Fire-protectio     a0ca8a3aa1e227b75d2a0e3baa91aac3
13 n potion":0}         5cc148da97c5cdd3cde4a35494765949
```

On reprend la payload n°2 et on change son bloc 7 par le bloc 6 de la payload n°3:

```
01 {"total":20,"Hea    a15320b18b2c290adf33f212ae175104
02 lth potion":0,"S    d04f5ef584029a718b779a05967bae0b
03 tamina potion":2    cb5998f5bd949f21ef8b0c84ba08735a
04 ,"Strengh potion    e049440c06e50aec529fb14bdbbe81d4
05 ":0,"Clerverness    e0af636931ea0d18f030c8a9453679a1
06  potion":0,"Flag    1fb40b1f3f7881835386e2113d8b7777
07  potion":1,"Flag    7fca8a0ff4f5cf1c34812e251a99f5ad        <===== Ici
08 th potion":0,"In    53d2da2e395063a67047ecc49dec5aba
09 visible potion":    568c3df55964d7d3bab704e701831c34
10 0,"Fly potion":0    29e83a4493fd102cd9b49d2ada8b68d1
11 ,"Love potion":0    a04cb37d89749b3254e1933360286b8f
12 ,"Fire-protectio    a0ca8a3aa1e227b75d2a0e3baa91aac3
13 n potion":0}        5cc148da97c5cdd3cde4a35494765949
```

On reconcatène tous les blocs et on tente de soumettre le panier :

```
❯ curl "http://localhost:8000/cart.php?validate&cart=a15320b18b2c290adf33f212ae175104d04f5ef584029a718b779a05967bae0bcb5998f5bd949f21ef8b0c84ba08735ae049440c06e50aec529fb14bdbbe81d4e0af636931ea0d18f030c8a9453679a11fb40b1f3f7881835386e2113d8b77777fca8a0ff4f5cf1c34812e251a99f5ad53d2da2e395063a67047ecc49dec5aba568c3df55964d7d3bab704e701831c3429e83a4493fd102cd9b49d2ada8b68d1a04cb37d89749b3254e1933360286b8fa0ca8a3aa1e227b75d2a0e3baa91aac35cc148da97c5cdd3cde4a35494765949"
You win the flag : FCSC{c9582322dfa2997384b7d38b73b5a80a69374d5f3f616c431e98823172f5c7df}
```