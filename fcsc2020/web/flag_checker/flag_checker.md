# Flag Checker (100 points)

**Challenge**

Voici un service qui permet simplement de vérifier si le flag est correct.

URL : http://challenges2.france-cybersecurity-challenge.fr:5005/


**Solution**

> Cette épreuve est une petite déception pour moi. En effet, elle a démarré avec un score de base de 200 points, je m'attendais donc à une difficulté moyenne (pour comparaison, c'était aussi le score de base de [Mazopotamia](../../misc/mazopotamia.md) qui demande un peu de boulot). Dans la réalité, comme on va le voir, elle peut être résolue en quelques minutes avec un peu d'intuition, sans même connaître la technologie.

La page se présente avec un champ de saisie permettant de valider un flag. On l'essaie de suite :

![Flag Checker 1](flag_checker1.png)

Immédiatement, on remarque que la vérification ne provoque pas de rechargement de la page ou de requête vers le serveur. La vérification est donc faite en local, et en regardant de plus près :

![Flag Checker 2](flag_checker2.png)

La page charge un fichier `index.wasm`. WASM est le diminutif de Web Assembly, une technologie récente permettant de développer des applications web avec de biens meilleures performances que le Javascript. Il s'agit d'un langage bas niveau, dont le bytecode est compilé en langage natif et exécuté par le navigateur.

La bonne nouvelle, c'est que ce standard est parfaitement intégré dans les outils de développement des navigateurs. Ainsi, le débogueur de Firefox nous affiche parfaitement le code, que je reproduis dans son intégralité :

```common_lisp {.line-numbers}
(module
  (type $type0 (func (param i32) (result i32)))
  (type $type1 (func))
  (type $type2 (func (param i32)))
  (type $type3 (func (result i32)))
  (import "a" "memory" (memory $memory0 256 256))
  (global $global0 (mut i32) (i32.const 5244480))
  (export "a" (func $func5))
  (export "b" (func $func4))
  (export "c" (func $func2))
  (export "d" (func $func1))
  (export "e" (func $func0))
  (func $func0 (param $var0 i32)
    get_local $var0
    set_global $global0
  )
  (func $func1 (param $var0 i32) (result i32)
    get_global $global0
    get_local $var0
    i32.sub
    i32.const -16
    i32.and
    tee_local $var0
    set_global $global0
    get_local $var0
  )
  (func $func2 (result i32)
    get_global $global0
  )
  (func $func3 (param $var0 i32) (result i32)
    (local $var1 i32) (local $var2 i32) (local $var3 i32) (local $var4 i32) (local $var5 i32)
    i32.const 70
    set_local $var3
    i32.const 1024
    set_local $var1
    block $label0
      get_local $var0
      i32.load8_u
      tee_local $var2
      i32.eqz
      br_if $label0
      loop $label2
        block $label1
          get_local $var2
          get_local $var1
          i32.load8_u
          tee_local $var4
          i32.ne
          br_if $label1
          get_local $var3
          i32.const -1
          i32.add
          tee_local $var3
          i32.eqz
          br_if $label1
          get_local $var4
          i32.eqz
          br_if $label1
          get_local $var1
          i32.const 1
          i32.add
          set_local $var1
          get_local $var0
          i32.load8_u offset=1
          set_local $var2
          get_local $var0
          i32.const 1
          i32.add
          set_local $var0
          get_local $var2
          br_if $label2
          br $label0
        end $label1
      end $label2
      get_local $var2
      set_local $var5
    end $label0
    get_local $var5
    i32.const 255
    i32.and
    get_local $var1
    i32.load8_u
    i32.sub
  )
  (func $func4 (param $var0 i32) (result i32)
    (local $var1 i32) (local $var2 i32)
    get_local $var0
    i32.load8_u
    tee_local $var2
    if
      get_local $var0
      set_local $var1
      loop $label0
        get_local $var1
        get_local $var2
        i32.const 3
        i32.xor
        i32.store8
        get_local $var1
        i32.load8_u offset=1
        set_local $var2
        get_local $var1
        i32.const 1
        i32.add
        set_local $var1
        get_local $var2
        br_if $label0
      end $label0
    end
    get_local $var0
    call $func3
    i32.eqz
  )
  (func $func5
    nop
  )
  (data (i32.const 1024)
    "E@P@x4f1g7f6ab:42`1g:f:7763133;e0e;03`6661`bee0:33fg732;b6fea44be34g0~"
  )
)
```

Cela pique les yeux ? Ne nous laissons pas impressionner, il n'est pas nécessaire de tout comprendre pour résoudre le challenge. La première chose à remarquer, c'est cette chaîne à la fin du code, dans ce qui pourrait ressembler à la fonction principale. Serait-ce le flag attendu ? Comparons :

```
E@P@x4f1g7f6ab:42`1g:f:7763133;e0e;03`6661`bee0:33fg732;b6fea44be34g0~
FCSC{je ne sais pas encore sa valeur mais ce ne peut être un hasard  }
```

Quand le même caractère est chiffré de la même manière à deux endroits différents, je pense tout de suite à une monosubstitution. Quelques essais m'ont vite fait abandonné l'idée. Il est plus rapide de revenir au code. En le lisant en diagonal, on remarque d'autres lignes intéressantes, dans la fonction `$func4` (ligne 94 à 98) :

```
        get_local $var1
        get_local $var2
        i32.const 3
        i32.xor
        i32.store8
```

Il n'est pas nécessaire de comprendre le bytecode WASM pour comprendre ce qui se passe ici : on a une boucle qui XORe avec l'entier 3. Serait-ce si simple ? Appelons python à la rescousse :

```python
>>> ''.join([ chr(ord(c)^3) for c in 'E@P@x4f1g7f6ab:42`1g:f:7763133;e0e;03`6661`bee0:33fg732;b6fea44be34g0~' ])
'FCSC{7e2d4e5ba971c2d9e944502008f3f830c5552caff3900ed4018a5efb77af07d3}'
```

Voilà, voilà... :/