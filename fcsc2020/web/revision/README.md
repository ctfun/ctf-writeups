# Revision

**Description**

La société Semper est spécialisée en archivage de documents électroniques. Afin de simplifier le travail des archivistes, un outil simple de suivi de modification a été mis en ligne. Depuis quelques temps néanmoins, cet outil dysfonctionne. Les salariés se plaignent de ne pas recevoir tous les documents et il n’est pas rare que le système plante. Le développeur de l’application pense avoir identifié l’origine du problème. Aidez-le à reproduire le bug.

**Note** : La taille totale des fichiers est limitée à 2Mo.

Fichiers: 
- [docker-compose.yml](docker-compose.yml)
- [comparator.py](comparator.py)


**Solution**

*Résolu en 2024 grâce à hackropole.fr*

La page web ne donne pas beaucoup d'info. On se rabat sur le script [comparator.py](comparator.py).
C'est la fonction `store` qui nous intéresse en particulier :

```python
    def store(self):
        self._reset_cursor()
        f1_hash = self._compute_sha1(self.f1)
        f2_hash = self._compute_sha1(self.f2)

        if self.db.document_exists(f1_hash) or self.db.document_exists(f2_hash):
            raise DatabaseError()

        attachments = set([f1_hash, f2_hash])
        # Debug debug...
        if len(attachments) < 2:
            raise StoreError([f1_hash, f2_hash], self._get_flag())
        else:
            self.m.send(attachments=attachments)
```

En effet, celle-ci calcule le hash `SHA1` de chacun des fichiers passés et les inclue
dans un ensemble avant de les envoyer. Si l'ensemble contient moins de deux éléments,
le flag est affiché. Il faut donc que les hash des deux documents soient identiques, et
on suppose que les documents doivent être différents, sinon ce ne serait pas rigolo.

Bref, on doit produire [une collision SHA1](https://shattered.io/) sur deux document PDF.
Il y a [un papier académique](https://shattered.io/static/shattered.pdf) sur le sujet mais 
c'est long et chiant. :) On peut plutôt réutiliser un outil existant comme 
https://github.com/nneonneo/sha1collider.

Attention, il nécessite quelques pré-requis. Sur une Ubuntu, l'installation des package `ghostscript` et `libjpeg-turbo-progs`.
Comme indiqué dans la documentation, on fournit deux PDF et on lance l'outil :

```
❯ python3 sha1collider/collide.py 1.pdf 2.pdf 
[...]
❯ sha1sum out-*
b2cc2f0b61aa935960653d4e6e2f13e7481cc4ac  out-1.pdf
b2cc2f0b61aa935960653d4e6e2f13e7481cc4ac  out-2.pdf
❯ md5sum out-*
cefee59c119b3b1da1492df52137d939  out-1.pdf
6c69940d4c207a0c2e59e7bfa1d115fe  out-2.pdf
```

On voit que les PDFs résultats sont bien différents (hash MD5) mais de même SHA1. On les soumet à l'UI et :

```
Bravo, vous avez trouvé une collision sha1 pour le fichier "b2cc2f0b61aa935960653d4e6e2f13e7481cc4ac" : FCSC{8f95b0fc1a793e102a65bae9c473e9a3c2893cf083a539636b082605c40c00c1} 
```