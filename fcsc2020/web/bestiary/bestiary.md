# Bestiary (100 points)

**Challenge**

On vous demande simplement de trouver le flag.

URL : http://challenges2.france-cybersecurity-challenge.fr:5004/

**Solution**

Rendons nous tout de suite sur la page :

![Bestiary1](bestiary1.png)

La liste déroulante permet de sélectionner un monstre, qui est passé en GET. Essayons de modifier cette valeur :

![Bestiary2](bestiary2.png)

Nous voilà en présence d'un classique, une injection dans un `include()` de PHP. Cette vulnérabilité peut potentiellement nous permettre de lire n'importe quel fichier sur le serveur, ou de faire exécuter du code (en incluant un fichier php distant). Cette dernière option n'est pas possible ici car les includes distant sont interdits (n'exagérons pas !).

On va donc chercher à lire le contenu du fichier `index.php`. Évidemment, si on tente d'inclure directement le fichier PHP, celui-ci sera interprété par PHP, et il inclura le fichier, qui sera interprété, qui inclura... Bref, une *Inception* qui ne nous aide pas. On va plutôt utiliser un [wrapper PHP](https://www.php.net/manual/fr/wrappers.php), et plus particulier le gestionnaire `php://filter` qui autorise une conversion en base64.  

On va donc passer le paramètre `monster` avec la valeur `php://filter/convert.base64-encode/resource=index.php`. Traduction : ouvre le fichier index.php et converti le en base64. Grâce à l'`include()`, le résultat apparaît dans la page :

![Bestiary3](bestiary3.png)

Un coup de décodage nous donne le code source de la page :

```php
<?php
	session_save_path("./sessions/");
	session_start();
	include_once('flag.php');
?>
<html>
<head>
	<title>Bestiary</title>
</head>
<body style="background-color:#3CB371;">
<center><h1>Bestiary</h1></center>
<script>
function show()
{
	var monster = document.getElementById("monster").value;
	document.location.href = "index.php?monster="+monster;
}
</script>

<p>
<?php
	$monster = NULL;

	if(isset($_SESSION['monster']) && !empty($_SESSION['monster']))
		$monster = $_SESSION['monster'];
	if(isset($_GET['monster']) && !empty($_GET['monster']))
	{
		$monster = $_GET['monster'];
		$_SESSION['monster'] = $monster;
	}

	if($monster !== NULL && strpos($monster, "flag") === False)
		include($monster);
	else
		echo "Select a monster to read his description.";
?>
</p>

[...]
```

Que peut-on tirer de tout ça ? 
- il existe un fichier flag.php qui contient probablement notre flag de validation.
- on ne peut pas utiliser la vulnérabilité que l'on connaît déjà pour lire son contenu.
- le répertoire de sessions PHP est situé dans le répertoire courant. Tiens, c'est pas banal.

Ce troisième point est également un grand classique de PHP. Il est fortement déconseillé de faire cela, car vous exposez le contenu des sessions des utilisateurs (et donc potentiellement de données sensibles). Ici, on va le tourner à notre avantage pour exécuter du code, mais faisons d'abord une rapide description du mécanisme de sessions PHP.

Une session PHP, côté serveur, est représentée par un fichier stocké dans le répertoire des sessions, dont le nom est `sess_<identifiant de session>`. notre identifiant de session est simple à retrouver, puisque c'est le contenu du cookie `PHPSESSID` dans notre navigateur. Illustrons cela sur le challenge : on sélectionne un monstre de la liste, puis on accède à l'URL de notre fichier de session puisque celui-ci est exposé par le serveur :

![Bestiary4](bestiary4.png)

On retrouve à l'intérieur le contenu de notre session. Il s'agit d'une sérialisation de la pseudo variable `$_SESSION`. Dans `index.php`, on voit que la variable `monster` contenant le monstre choisi est poussée dans la session, et cela se retrouve dans le fichier de session correspondant.

Résumons, la page nous permet :
- d'écrire arbitrairement dans notre fichier de session qui est exposé par le serveur.
- d'inclure et d'exécuter en PHP des fichiers arbitraires présents sur le serveur.
Par combinaison des deux, on a une exécution de code !

On va donc lire le contenu de `flag.php` à coup de `file_get_contents()`. On commence par écrire le code PHP, et on le passe dans la variable `monster` pour qu'il soit écrit dans le fichier de session. Attention à penser à remplacer les espaces par des  `+` :

![Bestiary5](bestiary5.png)

On jette un oeil à notre fichier de session sans l'exécuter pour vérifier notre injection :

![Bestiary6](bestiary6.png)

C'est bon, il nous suffit maintenant rappeler la page principale en demandant l'injection de notre fichier de session pour le code soit exécuté :

![Bestiary7](bestiary7.png)

Fun, bien qu'un peu dépassé en terme de technique...