# Académie de l'investigation - C'est la rentrée (25 points)

**Challenge**

Bienvenue à l'académie de l'investigation numérique ! Votre mission, valider un maximum d'étapes de cette série afin de démontrer votre dextérité en analyse mémoire GNU/Linux.

Première étape : retrouvez le HOSTNAME, le nom de l'utilisateur authentifié lors du dump et la version de Linux sur lequel le dump a été fait.

Format du flag : FCSC{hostname:user:x.x.x-x-amdxx}

Fichier : [dmp.tar.gz](https://hackropole.fr/filer/fcsc2020-forensics-academie-de-l-investigation/public_filer/dmp.mem.tar.xz)


**Solution**

Pour être honnête, je l'ai faite à coup de strings sur le dump... Mais pour la beauté, on la refait avec [volatility](https://github.com/volatilityfoundation/volatility).

Le premier point, c'est d'identifier le profil Linux dont on va avoir besoin. Et là, pour le coup, il nous faut le noyau, et effectivement ça va se faire à coup de `strings`. L'énoncé nous indique qu'il s'agit d'un Linux, et probablement en -amd64 vu le format de flag. Vérifions :

```console
$ strings dmp.mem | egrep "Linux [0-9]+\.[0-9]+\.[^C9]+-[0-9]+-amd[0-9]+" | head -1
Linux 5.4.0-4-amd64 Debian GNU/Linux bullseye/sid
```

Il est assez clair qu'on est en présence d'un noyau 5.4.0-4-amd64. On a même la distribution, Debian bullseyes. Je ne vais pas rentrer dans le détail de la création d'un profil Linux (c'est documenté dans [le wiki de Volatility](https://github.com/volatilityfoundation/volatility/wiki/Linux#Linux-Profiles)). Simplement, pour cette création, j'ai téléchargé et installé Bullseyes dans une machine virtuelle. À noter qu'au moment du CTF, le noyau avait déjà évolué, et il m'a fallu le downgrader, et bricoler à coup de google pour trouver les paquets linux-headers de la bonne version. Mais ça se fait, et j'obtiens donc un profil intitulé LinuxDebian-FCSCx64. Je me suis aussi fait un wrapper shell `vol` pour éviter une ligne de commande trop longue pour la suite.

```console
python2 /opt/volatility/vol.py -f dmp.mem --profile LinuxDebian-FCSCx64 $@
```

Le plus dur est fait, il est temps de répondre aux autres questions, à savoir le hostname et le nom de l'utilisateur.

Pour le hostname, ça se trouve dans les logs de démarrage :
```console
$ ./vol linux_dmesg | grep -i hostname
[2639205166.2] systemd[1]: Set hostname to <challenge.fcsc>.
```

Pour l'utilisateur, ça va être plus sioux. On va chercher la variable USER dans les environnements des process qui tournent :
```console
$ ./vol linux_psenv | egrep -o "USER=[^ ]+" | sort | uniq -c
     10 USER=Lesage
      1 USER=root
      1 USER=systemd-timesync
```

On a maintenant tout ce qu'il faut pour reconstituer le flag :

`FCSC{challenge.fcsc:Lesage:5.4.0-4-amd64}`