# Chapardeur de mots de passe (139 points)

**Challenge**

Un ami vous demande de l'aide pour déterminer si l'email qu'il vient d'ouvrir au sujet du Covid-19 était malveillant et si c'était le cas, ce qu'il risque.

Il prétend avoir essayé d'ouvrir le fichier joint à cet mail sans y parvenir. Peu de temps après, une fenêtre liée à l'anti-virus a indiqué, entre autre, le mot `KPOT v2.0` mais rien d'apparent n'est arrivé en dehors de cela.

Après une analyse préliminaire, votre ami vous informe qu'il est probable que ce malware ait été légèrement modifié, étant donné que le contenu potentiellement exfiltré (des parties du format de texte et de fichier avant chiffrement) ne semble plus prédictible. Il vous recommande donc de chercher d'autres éléments pour parvenir à l'aider.

Vous disposez d'une capture réseau de son trafic pour l'aider à déterminer si des données ont bien été volées et lui dire s'il doit rapidement changer ses mots de passe !

SHA256(pws.pcap) = 98e3b5f1fa4105ecdad4880cab6a7216c5bb3275d7367d1309ab0a0d7411475d - 463MB

[pws.pcap](https://france-cybersecurity-challenge.fr/files/77c4bb718972a6dbee9257d1adb5e1e3/pws.pcap)


**Solution**

Avant même d'ouvrir le fichier, on va commencer par se renseigner sur ce fameux `KPOT`. Il est notamment référencé sur [Malpedia](https://malpedia.caad.fkie.fraunhofer.de/details/win.kpot_stealer), ce qui nous fournit plusieurs liens décrivant le malware en question. Le plus intéressant en ce qui nous concerne, c'est celui de [Proofpoint](https://www.proofpoint.com/us/threat-insight/post/new-kpot-v20-stealer-brings-zero-persistence-and-memory-features-silently-steal), que je vous encourage à lire si ce n'est pas déjà fait.

> KPOT Stealer is a “stealer” malware that focuses on exfiltrating account information and other data from web browsers, instant messengers, email, VPN, RDP, FTP, cryptocurrency, and gaming software.


Quelques éléments qui nous intéressent :
- Le malware utilise HTTP pour communiquer avec son C&C, et dans le sample analysé, il attaque des URLs en gate.php.
- L'ensemble des communications est chiffré par XOR, la clef étant fixe dans le sample.
- On sait que le C&C envoie de demandes qui commencent par des zéros et des 1, et qui contiennent des motfs fixes, tel que `__DELIMM__` ou `__GRABBER__`.

On peut maintenant ouvrir le pcap, et on va directement chercher `http contains gate.php` :

![pcap](chapardeur_pcap.png)

Et en regardant le quatrième :

![pcap](chapardeur_pcap2.png)

On voit donc le malware contacter son C&C, recevoir une requête en base64, et répondre en binaire. On va supposer que le flag est dans ce dernier paquet, mais pour le moment, il va falloir trouver la clef du XOR pour déchiffrer tout ça, en commençant par la requête envoyée par le malware vers son C&C.

Ce qui va nous intéresser ici, c'est la commutativité de l'opération XOR. En effet, si on chiffre un message en clair par XOR, faire un XOR entre le clair et le chiffré va nous donner la clef. Or, comme on a une idée de quelques éléments présents dans la requête du C&C, on va pouvoir mener [une attaque par clair choisi](https://fr.wikipedia.org/wiki/Attaque_%C3%A0_texte_clair_connu).

> Note : pour la suite, ça va être un peu décousu, mélange de scripting et de manipulation à la main. Il y avait peut-être plus simple et direct, mais j'aime les phases telles que celle que je vais décrire.

Pour démarrer, on va prendre la requête en binaire du malware, et faire un XOR avec la clef `__DELIMM__`. Ça va nous donner une nouvelle clef, avec laquelle on va déchiffrer la requête complète. Évidemment, on ne sait pas exactement où `__DELIMM__` se trouve, donc on va itérer en décalant la position de la clef dans requête en espérant trouver quelque chose. Et ça tombe bien :

```
Décalage 16 => Clef tDlsdL5dv2 => 0110101110p.l.=..^.\A.ZM!26N-. .9.h.pa1).T.R._O)GVxO5I77...DEGRM.HMToLu&}.*Bf0,__GRABBER.xx@|J%An 5W.P@?BE.2Cm(hWx.i..G$.Od.o&])\rDv.e..'46}pj`W-of!.gH..mAdA.*.txt,__GR.e.d^e..o2uz)e}.ik"E@v}DcK>X..E$.R.[.M]FGhOm.j.....&[ZTM.^_3.E..jHS.____DELIMM.x
```

On a un début de clef `tDlsdL5dv2`, dont le résultat semble cadrer parfaitement avec ce qu'on attend : on a des zéros et des uns au début, du `GRABBER` et du `DELIM`. Coup de bol aussi, la clef est bien alignée, puisque le début du paquet est décodé. Maintenant, il va falloir trouver la bonne longueur. Pour ce faire, je recommence le déchiffrement en ajoutant des `0` (parce que pourquoi pas) à la clef. Jusqu'à ce que :

```python
>>> xor.decod(xor.binary, 'tDlsdL5dv2000000')
"01101011104b0Siv__DELIMM__7b9Liv8.149.373_Z.D...M__appdataZ.F0..BER__*.log)y/. 2,__GRABBERZ.$.(6data%__GRAG.D0..0__GRABBERZ.0Rjr__DELIMM__a6r.,)p_txt__GRAG.D0..*.txt,__GRD.C'.._%userprofl?dG..esktop__GRD.C'.._0__GRABBEW.^R..DELIMM____A.M+..____DELIMMZ."
```

La longueur de la clef semble idéale, avec un contenu qui semble de plus en plus explicite. Il ne reste qu'à trouver par quels caractères remplacer les `0`. Cette fois on va chercher des fragments de clairs connus que l'on saura compléter. Par exemple :

```python
>>> xor.decod(payload[:75], 'tDlsdL5dv2000000')
Décod : __DELIMM__7b9Liv8.149.373_Z.D...M__appdataZ.F0..BER__*.log)
Clef  : tDlsdL5dv2000000tDlsdL5dv2000000tDlsdL5dv2000000tDlsdL5dv20
```

On a la fin d'un `__GRABBER__`, et le début va nous donner d'un coup les 6 caractères manquants de la clef. On arrive donc à la clef `tDlsdL5dv25c1Rhv`. Voyons la requête complète en clair du C&C :

```
0110101110111110__DELIMM__218.108.149.373__DELIMM__appdata__GRABBER__*.log,*.txt,__GRABBER__%appdata%__GRABBER__0__GRABBER__1024__DELIMM__desktop_txt__GRABBER__*.txt,__GRABBER__%userprofile%\Desktop__GRABBER__0__GRABBER__0__DELIMM____DELIMM____DELIMM__
```

C'est parfait, la requête semble en phase avec ce que l'on sait du comportement du C&C. On va maintenant utiliser la clef découverte avec le paquet binaire que le malware retourne au C&C :

`_DRAPEAU_P|us2peurQue2M4l!  R4ssur3z-Votre-Am1-Et-vo1c1Votredr4peau_FCSC.{469e8168718996ec83a92acd6fe6b9c03c6ced2a3a7e7a2089b534baae97a7}._DRAPEAU_`