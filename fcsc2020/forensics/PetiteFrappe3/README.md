# Petite Frappe 3/3

**Énoncé**

Lors de l’investigation d’un serveur GNU/Linux en France, plusieurs fichiers inconnus de l’administrateur ont été retrouvés dont le fichier

```
-rw-r--r-- 1 root root 55K Mar 21 02:45 /tmp/input
```

Ce fichier est soupçonné d’être lié à une activité d’un enregistreur de frappe clavier mais aucun programme ne semble avoir été installé sur ce serveur à cette fin. Identifiez le format de ce fichier puis essayez de le décoder afin de trouver le mot de passe de `flag.gpg`.

Fichiers:
- [input](https://hackropole.fr/challenges/fcsc2020-forensics-petite-frappe/public/input)
- [flag.gpg](https://hackropole.fr/challenges/fcsc2020-forensics-petite-frappe/public/flag.gpg)


**Solution**

On ouvre le fichier `input` dans un éditeur hexa:

```
 00000000:  5f 22 76 5e 00 00 00 00  06 24 0b 00 00 00 00 00  _"v^.....$......
 00000010:  04 00 04 00 1c 00 00 00  5f 22 76 5e 00 00 00 00  ........_"v^....
 00000020:  06 24 0b 00 00 00 00 00  01 00 1c 00 00 00 00 00  .$..............
 00000030:  5f 22 76 5e 00 00 00 00  06 24 0b 00 00 00 00 00  _"v^.....$......
 00000040:  00 00 00 00 00 00 00 00  66 22 76 5e 00 00 00 00  ........f"v^....
 00000050:  1f f3 0e 00 00 00 00 00  04 00 04 00 17 00 00 00  ................
 00000060:  66 22 76 5e 00 00 00 00  1f f3 0e 00 00 00 00 00  f"v^............
 00000070:  01 00 17 00 01 00 00 00  66 22 76 5e 00 00 00 00  ........f"v^....
 00000080:  1f f3 0e 00 00 00 00 00  00 00 00 00 00 00 00 00  ................
 00000090:  67 22 76 5e 00 00 00 00  50 89 00 00 00 00 00 00  g"v^....P.......
 000000a0:  04 00 04 00 20 00 00 00  67 22 76 5e 00 00 00 00  .... ...g"v^....
 000000b0:  50 89 00 00 00 00 00 00  01 00 20 00 01 00 00 00  P......... .....
```

On remarque une espèce de pattern se répétant tous les 24 octets. Si on découpe sur cette taille, on obtient, déjà un compte rond sur la taille du fichier ;-), 
et :

```
5f 22 76 5e 00 00 00 00 06 24 0b 00 00 00 00 00 04 00 04 00 1c 00 00 00
5f 22 76 5e 00 00 00 00 06 24 0b 00 00 00 00 00 01 00 1c 00 00 00 00 00
5f 22 76 5e 00 00 00 00 06 24 0b 00 00 00 00 00 00 00 00 00 00 00 00 00
66 22 76 5e 00 00 00 00 1f f3 0e 00 00 00 00 00 04 00 04 00 17 00 00 00
66 22 76 5e 00 00 00 00 1f f3 0e 00 00 00 00 00 01 00 17 00 01 00 00 00
66 22 76 5e 00 00 00 00 1f f3 0e 00 00 00 00 00 00 00 00 00 00 00 00 00
67 22 76 5e 00 00 00 00 50 89 00 00 00 00 00 00 04 00 04 00 20 00 00 00
67 22 76 5e 00 00 00 00 50 89 00 00 00 00 00 00 01 00 20 00 01 00 00 00
67 22 76 5e 00 00 00 00 50 89 00 00 00 00 00 00 00 00 00 00 00 00 00 00
```

Les 4 premiers octets semblent être un nombre croissant, en little-endian. Un timestamp ? On se rappelle aussi que le fichier
provient de `/tmp/input`, ce qui ressemble à `/dev/input`. Une recherche rapide permet d'arriver à [cet article](https://thehackerdiary.wordpress.com/2017/04/21/exploring-devinput-1/). C'est prometteur, car il indique que le flux émis sur un `/dev/input/eventX` est une structure de 24 octets commençant par un timestamp:

```
struct input_event {
	struct timeval time;
	unsigned short type;
	unsigned short code;
	unsigned int value;
};
```

En suivant [le lien proposé par l'article](https://www.kernel.org/doc/Documentation/input/input.txt), on a plus d'informations :

>   `time` is the timestamp, it returns the time at which the event happened.
> Type is for example EV_REL for relative moment, EV_KEY for a keypress or
> release. More types are defined in include/uapi/linux/input-event-codes.h.
>
>  `code` is event code, for example REL_X or KEY_BACKSPACE, again a complete
> list is in include/uapi/linux/input-event-codes.h.
>
>  `value` is the value the event carries. Either a relative change for
> EV_REL, absolute new value for EV_ABS (joysticks ...), or 0 for EV_KEY for
> release, 1 for keypress and 2 for autorepeat.

On commence à décoder manuellement, et on trouve des commandes systèmes lancées :

```
id
groups
pwd
```

On note que le clavier semble AZERTY. On automatise en repartant du script  utilisé pour l'épreuve précédente. Cette fois-ci, on se
rend compte qu'il va falloir gérer `KEY_BACKSPACE`, et `KEY_RIGHTALT` ! On adapte (au minimum toujours, donc tout le mapping n'est pas complet) pour obtenir
[ce script](decod.py), et en le lançant :

```
id
groups
pwd
w
ping 298.125.250.42
sudo apt update
apt list --upgradable
man asc i 
  qcd 
ls
cd Docu Challs
git status
git rm flag.txt
vi /WQ
GIT ADD readme.md
git commit -m "Remove flag.txt"
git push
cd ..
ls
df -h
gpg --output flag --decrypt flag.gpg
Destination_Autriche_#TEAMFR
cat flag
exit
 c
```

On retrouve la commande utilisée pour décoder `flag.gpg` et le mot de passe utilisé. On utilise
la même et on obtient le fichier `flag` contenant:

```
    FCSC{0bec21052ae86baf149eb97ce52de0fec1d6b8e1ed827fe026f6197be07419c3}    

```