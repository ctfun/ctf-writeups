#!/usr/bin/env python3

#                   1         2          3         4         5         6         7         8         9         1
#        0123 456789012345678901234567 8901234567890123456789012345678901234567890123456789012345678901234567890123456789
lower = "  &é\"'(-è_çà)=  azertyuiop^$\n qsdfghjklmù* <wxcvbn,;:! *               789-456+1230.                        ? "
upper = "  1234567890°+  AZERTYUIOP¨£\n QSDFGHJKLM%µ >WXCVBN?./§ *               789-456+1230.                        ?"
altgr = "  ´~#{[|`\\^@]}                                                                       "

keycodes = {
    14:  "KEY_BACKSPACE",
    42:  "KEY_SHIFT",
    54:  "KEY_SHIFT",
    58:  "KEY_CAPSLOCK",
    100: "KEY_RIGHTALT",
}

KEYPRESSED = 1
RELEASE = 0

lower = { i:c for i, c in enumerate(lower) }
upper = { i:c for i, c in enumerate(upper) }
altgr = { i:c for i, c in enumerate(altgr) }

trace = open('input', 'rb').read()
l = len(trace)

s = ''
up = False
alt = False
for i in range(0, l, 24):
    t = trace[i:i+24]
    type, code, value = t[16:18], t[18:20], t[20:24]
    type, code, value = [ int.from_bytes(x, "little") for x in (type, code, value)]

    # Tout ce qui n'est pas clavier:
    if type != 1: # EV_KEY
        continue

    # Touches de contrôles
    if code in keycodes:
        if value == KEYPRESSED:
            if keycodes[code] == "KEY_SHIFT":
                up = True
            elif keycodes[code] == "KEY_BACKSPACE":
                s = s[:-1]
            elif keycodes[code] == "KEY_CAPSLOCK":
                up = not up
            elif keycodes[code] == "KEY_RIGHTALT":
                alt = True
        elif value == RELEASE:
            if keycodes[code] == "KEY_SHIFT":
                up = False
            elif keycodes[code] == "KEY_RIGHTALT":
                alt = False
        continue
    
    if value == KEYPRESSED:
        if code < len(lower):
            # print(f'{code=} ', end='')
            if not up and not alt:
                # print(lower[code])
                s += lower[code]
            elif up and not alt:
                # print(upper[code])
                s += upper[code]
            else:
                # print(altgr[code])
                s += altgr[code]
        else:
            print(f'Unknown {code=}')
            s += '?'

print(s)