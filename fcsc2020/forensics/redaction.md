# Académie de l'investigation - Rédaction (42 points)

**Challenge**

Le document note.docx vient d'être créé avec LibreOffice et enregistré avant le dump de la mémoire. Retrouvez son contenu !

Format du flag : FCSC{xxx}, où xxx est la chaîne qui vous sera indiquée à la lecture du contenu du document.

Le dump est identique au challenge [C'est la rentrée](rentree.md).

**Solutions**

J'ai pas mal galéré pour celui-là. Parmi les techniques qui ont foiré :
- dumper tous les fichiers en cache par `linux_recover_filesystem` : pas inintéressant, mais la plupart des fichiers sont vides.
- dumper la mémoire du process `soffice` (en supposant que le fichier était ouvert, ce que suggére l'énoncé).
- utiliser [scalpel](https://github.com/sleuthkit/scalpel) pour trouver des archives zip (on va revenir là-dessus) dans le dump (résultats non probant).

Tout ces échecs viennent du fait que le dump ne contient pas le fichier en intégralité. On va donc revenir à une technique plus terre à terre : qu'est-ce qu'un fichier DOCX ?

Je crée donc un fichier test.docx, contenant juste la chaîne «C'est un test», et je le sauvegarde. 

```console
$ file test.docx 
test.docx: Microsoft OOXML
```

Le format est OOXML ([Open Office XML](https://fr.wikipedia.org/wiki/Office_Open_XML)), norme documentée à des fins d'intéropérabilitié. On apprend notamment qu'en gros, on a affaire une archive zip remplit de fichiers XML et médias. 

```console
$ unzip -t test.docx
Archive:  test.docx
    testing: _rels/.rels              OK
    testing: docProps/app.xml         OK
    testing: docProps/core.xml        OK
    testing: word/_rels/document.xml.rels   OK
    testing: word/settings.xml        OK
    testing: word/fontTable.xml       OK
    testing: word/document.xml        OK
    testing: word/styles.xml          OK
    testing: [Content_Types].xml      OK
No errors detected in compressed data of test.docx.
```

Dans cette archive, on remarque en particulier le fichier word/document.xml. Faisons une extraction pour vérifier notre intuition :

```console
$ unzip test.docx word/document.xml
Archive:  test.docx
  inflating: word/document.xml       
$ cat word/document.xml 
```

> <w:document xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape" xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" mc:Ignorable="w14 wp14"><w:body><w:p><w:pPr><w:pStyle w:val="Normal"/><w:rPr></w:rPr></w:pPr><w:r><w:rPr></w:rPr><w:t>**C’est un test.**</w:t></w:r></w:p><w:sectPr><w:type w:val="nextPage"/><w:pgSz w:w="11906" w:h="16838"/><w:pgMar w:left="1134" w:right="1134" w:header="0" w:top="1134" w:footer="0" w:bottom="1134" w:gutter="0"/><w:pgNumType w:fmt="decimal"/><w:formProt w:val="false"/><w:textDirection w:val="lrTb"/></w:sectPr></w:body></w:document>p


C'est bien ça, le fichier contient la chaîne «C'est un test» de notre document. Allons donc maintenant chercher si le dump contient ce fichier XML, et en particulier la balise `<w:document `:

```console
$ strings dmp.mem | grep "<w:document"
```
> <w:document xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape" xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" mc:Ignorable="w14 wp14"><w:body><w:p><w:pPr><w:pStyle w:val="Normal"/><w:bidi w:val="0"/><w:jc w:val="left"/><w:rPr></w:rPr></w:pPr><w:r><w:rPr></w:rPr><w:t>Nouvelle note</w:t></w:r></w:p><w:p><w:pPr><w:pStyle w:val="Normal"/><w:bidi w:val="0"/><w:jc w:val="left"/><w:rPr></w:rPr></w:pPr><w:r><w:rPr></w:rPr></w:r></w:p><w:p><w:pPr><w:pStyle w:val="Normal"/><w:bidi w:val="0"/><w:jc w:val="left"/><w:rPr></w:rPr></w:pPr><w:r><w:rPr></w:rPr><w:t>**Preuve : PQHJRTSFYH-3467024-LSHRFLDFGA**</w:t></w:r></w:p><w:sectPr><w:type w:val="nextPage"/><w:pgSz w:w="12240" w:h="15840"/><w:pgMar w:left="1134" w:right="1134" w:header="0" w:top="1134" w:footer="0" w:bottom="1134" w:gutter="0"/><w:pgNumType w:fmt="decimal"/><w:formProt w:val="false"/><w:textDirection w:val="lrTb"/></w:sectPr></w:body></w:documen`

C'est trouvé, et notre flag est donc :

`FCSC{PQHJRTSFYH-3467024-LSHRFLDFGA}`