# Find Me (25 points)

**Challenge**

Vous avez accès à un fichier find_me qui semble renfermer un secret bien gardé, qui n'existe peut-être même plus. Retrouvez son contenu !

SHA256(find_me) = daacae76e8688b95fa4f447bb67e49185a17aadf7c92ffb22170acb7cab4e1a9.

[find_me](https://france-cybersecurity-challenge.fr/files/e899de9678ec4bf82124187be042d2d6/find_me)

**Solution**

On est en présence d'un fichier de type inconnu. On commence par l'identifier.

```console
$ file find_me
find_me: Linux rev 1.0 ext4 filesystem data, UUID=9c0d2dc5-184c-496a-ba8e-477309e521d9, volume name "find_me" (needs journal recovery) (extents) (64bit) (large files) (huge files)
```

Il s'agit d'un système de fichier, montons le :

```console
$ mkdir mount_point
$ sudo mount -o loop find_me mount_point/
[sudo] Mot de passe de ***: 
$ tree mount_point/
mount_point/
├── lost+found [error opening dir]
├── pass.b64
└── unlock_me

1 directory, 2 files
```

Et on recommence. Voyons ce qu'est unlock_me et ce que contient pass.b64 :
```console
$ file mount_point/unlock_me 
mount_point/unlock_me: LUKS encrypted file, ver 1 [aes, xts-plain64, sha256] UUID: 220745be-23df-4ef8-bff0-a36ab5cd1eff
$ cat mount_point/pass.b64 
nothing here. password splited!
```

Cette fois-ci, on a affaire à un système de fichier chiffré. On a donc besoin de trouver le mot de passe, qui n'est visiblement pas le contenu de pass.b64. Rappelons-nous de l'énoncé : « un secret bien gardé, qui n'existe peut-être même plus». Le mot de passe aurait-il été supprimé du système de fichier ext4, et serait-il alors récupérable ? Un bon moyen de confirmer est de regarder les chaînes du fichier find_me :
```console
$ strings find_me | head -30
find_me
/media/root/find_me
<{gi
lost+found
unlock_me
pass.b64
part00
part01
part02
part03
part04
part05
part06
part07
part08
part09
part0a
part0b
part0c
part0d
part0e
part0f
part10
part11
part12
part13
part14
,Az5
&?eo
=hLS
```

Il est clair qu'il existe (ou qu'il a existé) des fichiers part00 à part14. Comment les récupérer ?

J'ai essayé plusieurs solution, parmi lesquelles [extundelete](http://extundelete.sourceforge.net/) ou l'habituel [photorec](https://www.cgsecurity.org/wiki/PhotoRec_FR), mais sans succès. Jusqu'à ce que j'essaie le [Sleuth Kit](https://www.sleuthkit.org/).

On va utiliser `blkls`, une commande qui ouvre une image, et copie, par défauts, le contenu des blocs non alloués.

```console
$ blkls find_me
TWYtOVkyb01OWm5IWEtzak04cThuUlRUOHgzVWRZ
```

> *Note: je pense que ça va marcher parce qu'on a la chance de voir sortir les blocs dans le bon sens. Si on veut s'assurer de sortir les données dans le bon ordre (de part00 à part14), on peut aussi faire :*
>
> `for f in $(fls find_me | grep part | cut -f2); do fcat $f find_me; done`


Cool, ça ressemble à du base64. Voyons si la clef décodée, `Mf-9Y2oMNZnHXKsjM8q8nRTT8x3UdY`, permet d'ouvrir le conteneur chiffré :

```console
$ mkdir mount_point2
$ sudo /sbin/cryptsetup open mount_point/unlock_me conteneur
Saisissez la phrase secrète pour mount_point/unlock_me : 
$ sudo mount /dev/mapper/conteneur mount_point2
$ ls -al mount_point2
total 4
drwxr-xr-x 2 root root   92 avril  1 21:54 .
drwxr-xr-x 4 papy papy 4096 mai    7 21:11 ..
-r-------- 1 root root   70 avril  1 21:54 .you_found_me
$ sudo cat mount_point2/.you_found_me
FCSC{750322d61518672328c856ff72fac0a80220835b9864f60451c771ce6f9aeca1}
```