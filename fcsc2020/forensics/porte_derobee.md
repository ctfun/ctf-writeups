# Académie de l'investigation - Porte dérobée (30 points)

**Challenge**

Un poste distant est connecté au poste en cours d'analyse via une porte dérobée avec la capacité d'exécuter des commandes.

    Quel est le numéro de port à l'écoute de cette connexion ?
    Quelle est l'adresse IP distante connectée au moment du dump ?
    Quel est l'horodatage de la création du processus en UTC de cette porte dérobée ?

Format du flag : FCSC{port:IP:YYYY-MM-DD HH:MM:SS}

Le fichier de dump à analyser est identique au challenge [C'est la rentrée](rentree.md).

**Solution**

On résume : on a une porte dérobée, et un poste connecté dessus. En examinant les process et les connexions réseaux, on devrait s'en sortir. 

D'abord les process :
```console
$ ./vol linux_pstree
Name                 Pid             Uid            
[...]       
.x-terminal-emul     1503            1001           
..bash               1513            1001           
...ncat              1515            1001           
....sh               119511          1001           
...smbclient         119577          1001           
..bash               1519            1001           
...su                1522            1001           
....bash             1523                           
.....insmod          119822                         
..bash               119463          1001           
...ssh               119468          1001           
..bash               119707          1001           
...ncat              119711          1001
[...]
```

Du netcat qui spwane un shell ? C'est plutôt intrigant. Voyons de plus près ce process ncat de pid 1515, et ses connexions réseau :

```console
$ ./vol linux_netstat -p 1515
TCP      fd:6663:7363:1000:c10b:6374:25f:dc37:36280 fd:6663:7363:1000:55cf:b9c6:f41d:cc24:58014 ESTABLISHED                  ncat/1515 
```

Bingo. On a donc un netcat qui écoute sur le port 36280, sur lequel une IP s'est connectée et se retrouve avec un shell. Il nous reste juste à déterminer à quelle heure ce process a été démarré.

```console
$ ./vol linux_pslist -p 1515
Offset             Name                 Pid             PPid            Uid             Gid    DTB                Start Time
------------------ -------------------- --------------- --------------- --------------- ------ ------------------ ----------
0xffff9d72c014be00 ncat                 1515            1513            1001            1001   0x000000003e3d0000 2020-03-26 23:24:20 UTC+0000
```

On reconstitue les informations :
- le port d'écoute : 36280
- l'ip distante : fd:6663:7363:1000:55cf:b9c6:f41d:cc24
- l'heure de démarrage : 2020-03-26 23:24:20

Et le flag :

`FCSC{36280:fd:6663:7363:1000:55cf:b9c6:f41d:cc24:2020-03-26 23:24:20}`
