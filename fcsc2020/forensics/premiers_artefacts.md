# Académie de l'investigation - Premiers artéfacts (100 points)

**Challenge**

Pour avancer dans l'analyse, vous devez retrouver :

- Le nom de processus ayant le PID 1254.
- La commande exacte qui a été exécutée le 2020-03-26 23:29:19 UTC.
- Le nombre d'IP-DST unique en communications TCP établies (état ESTABLISHED) lors du dump.

Format du flag : FCSC{nom_du_processus:une_commande:n}

Le fichier de dump à analyser est identique au challenge [C'est la rentrée](rentree.md).

**Solution**

> *Note: au moment de rédiger ce writeup, je me rends compte que l'énoncé a été modifié en cours de route. Je me souviens très précisément qu'à l'origine, il était demandé de compter le nom de connexions établies en TCP et **UDP**. Évidemment, cela n'a pas trop de sens en UDP, mais j'avoue que je me suis posé des questions sur le moment.

On se retourne vers volatility pour répondre à toutes ces questions.

Pour le process, il y a une subtilité parce qu'il n'apparaît pas dans `linux_pslist` ou `linux_pstree`. Mon hypothèse est qu'il est arrêté. En revanche, il apparaît bien dans `linux_psxview` :

```console
$ ./vol linux_psxview | grep 1254
0x000000003fdccd80 pool-xfconfd           1254 False  True   False    False      False   False  
```

La commande à retrouver par sa date d'exécution est, elle, présente dans l'historique du bash :
```console
$ ./vol linux_bash | grep "2020-03-26 23:29:19"
    1523 bash                 2020-03-26 23:29:19 UTC+0000   nmap -sS -sV 10.42.42.0/24
```

Pour finir, le nombre d'IP destinations des connexions TCP établies :
```console
$ ./vol linux_netstat --output=greptext | grep ESTABLISHED | cut -d"|" -f5 | sort | uniq | wc -l
13
```

Je suis peut-être allé un peu vite ? On détaille :
- on affiche la liste des connexions réseau, dans un format scriptable (les attributs sont alors séparés par des `|`)
- on `grep` pour ne récupérer que les connexions établies.
- le `cut` permet de récupérer le 5ème champ (l'IP destination).
- `sort` trie les IP, `uniq` supprime les doublons et `wc` compte le tout.

Et voici notre flag :

`FCSC{pool-xfconfd:nmap -sS -sV 10.42.42.0/24:13}`


