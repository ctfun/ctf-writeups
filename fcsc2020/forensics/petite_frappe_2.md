# Petite frappe 2/3

**Description**

Lors de l’investigation d’un poste GNU/Linux, vous analysez un nouveau fichier qui semble être généré par un programme d’enregistrement de frappes de clavier. Retrouvez ce qui a bien pu être écrit par l’utilisateur de ce poste à l’aide de ce fichier !

Note : Insérer le contenu tapé au clavier de ce poste entre `FCSC{...}` pour obtenir le flag.

Fichier: [petite_frappe_2.txt](https://hackropole.fr/challenges/fcsc2020-forensics-petite-frappe/public/petite_frappe_2.txt)


**Solution**

> Résolue en 2024 grâce à hackropole.fr ;-)

On est en face de key codes remontés par le clavier. Je n'ai pas trouvé de méthode simple pour la conversion,
d'autant qu'il faut prendre en compte le mapping clavier. On va supposer qu'il s'agit d'un clavier AZERTY. 
J'utilise l'utilitaire `xev` pour récupérer les key codes de mon clavier et je construis des mappings :

```python
#!/usr/bin/env python3

#                  1         2            3          4         5         6         7          8         9
#        012345678901 23456789012  3456789012345 6789012345678901234567890123456789012345678901234567890123456789
lower = "..........&é\"'(-è_çà)=.	azertyuiop^$\n.qsdfghjklmùœ.*wxcvbn,;:!... ............................<"
upper = "..........1234567890°+.	AZERTYUIOP¨£\n.QSDFGHJKLM%µŒ.WXCVBN?./§... ............................>"
#        01234567890123456789012   3456789012345 6789012345678901234567890123456789012345678901234567890123456789
#                  1        2             3          4         5         6         7          8         9
keycodes = {
    22: "KEY_BACKSPACE",
    50: "KEY_SHIFT",
    66: "KEY_CAPSLOCK",
}
lower = { i:c for i, c in enumerate(lower) }
upper = { i:c for i, c in enumerate(upper) }
```

On verra si cela suffit ou s'il faut aller plus loin dans le mapping. Ensuite, je parse le fichier et je 
construis une chaîne via mes mappings. Les seules subtilités sont la gestion des touches `CAPSLOCK`, `SHIFT`
et `BACKSPACE` (pour rien, elles ne sont pas utilisées... ça servira probablement ailleurs...).

```python
actions = [ x.strip(' \n') for x in open('petite_frappe_2.txt', 'r').readlines() ]
str = ''
up = False
shift = False
for a in actions:

    if a in keycodes:
        if keycodes[a] == "KEY_SHIFT":
            shift = 'press' in a
        elif 'press' in a:
            if keycodes[a] == "KEY_CAPSLOCK":
                up = True
            elif keycodes[a] == "KEY_BACKSPACE":
                str = str[:-2]
            else:
                print(f'Unknown key code {a}')
    elif 'press' in a:
        code = int(a[-2:])
        if up or shift:
            str += upper[code]
        else:
            str += lower[code]

print(str)
```

Le résultat :

```shell
la solution avec xinput ne semble pas super pratique a decoder.; le flag est un_clavier_azerty_en_vaut_deux
```

Le flag est donc : `FCSC{un_clavier_azerty_en_vaut_deux}`
