# CryptoLocker v1

**Challenge**

Un de nos admins nous a appelé en urgence suite à un CryptoLocker qui s'est lancé sur un serveur ultra-sensible, juste après avoir appliqué une mise à jour fournie par notre prestataire informatique.

Ce malware vise spécifiquement un fichier pouvant faire perdre des millions d'euros à notre entreprise : il est très important de le retrouver !

L'administrateur nous a dit que pour éviter que le logiciel ne se propage, il a mis en pause le serveur virtualisé et a récupéré sa mémoire vive dès qu'il a détecté l'attaque.

Vous êtes notre seul espoir.

SHA256(memory1.dmp.gz) = 39e31ca61067e51f9566f0b669ac1ddbd09924a6f7720dcb08c9ac46cee186f5.

[memory1.dmp.gz](https://france-cybersecurity-challenge.fr/files/92b1d3116878bdeb5e9fb28ea31c8786/memory.dmp.gz)


**Solution**

Je passe rapidement sur la recherche du profil Windows à utiliser, et je me fais un wrapper pour simplifier l'appel à volatility :
```console
python2 /opt/volatility/vol.py -f memory.dmp --profile Win7SP1x86_23418 $@
```

L'énoncé donne pas mal d'informations. Notamment, il y a une corrélation entre la mise à jour du prestataire et l'infection. Volatility nous indique qu'il existe un process ressemblant à une mise à jour en cours :

```console
$ ./vol pstree | grep update
. 0x83de43a8:update_v0.5.ex                          3388   1432      2     61 2020-04-13 18:38:00 UTC+0000
```

En recherchant les fichiers ouverts, on ramène d'autres informations intéressantes :
```console
$ ./vol filescan | grep Desktop
Offset(P)            #Ptr   #Hnd Access Name
------------------ ------ ------ ------ ----
[...]
0x000000003ed13898      2      1 R--rw- \Device\HarddiskVolume1\Users\IEUser\Desktop\key.txt
0x000000003ed139f0      2      0 RW-rw- \Device\HarddiskVolume1\Users\IEUser\Desktop\flag.txt.enc
0x000000003ed66b60      6      0 R--r-d \Device\HarddiskVolume1\Users\IEUser\Desktop\update_v0.5.exe
[...]
```

Dans le même répertoire, on retrouve le binaire de mise à jour, un fichier contenant probablement notre flag chiffré, et un fichier de clef. Essayons d'extraire tout ça.

Le binaire est extrait par `procdump` :
```console
$ ./vol procdump -p 3388 -D .
Process(V) ImageBase  Name                 Result
---------- ---------- -------------------- ------
0x83de43a8 0x00400000 update_v0.5.ex       OK: executable.3388.exe
```

Un coup de `strings` sur l'exécutable confirme qu'il est louche, et le lien avec le flag et la clef :

```console
$ strings -e S executable.3388.exe
[...]
[info] entering the folder : %s
flag.txt
[info] file encryptable found : %s
	ENCRYPTOR v0.5
key.txt
[error] can't read the key-file :s
****Chiffrement terminée ! Envoyez l'argent !
[...]
```

Sortons maintenant la clef :
```console
$ ./vol dumpfiles -r key.txt -D . -n
DataSectionObject 0x84f13898   3388   \Device\HarddiskVolume1\Users\IEUser\Desktop\key.txt
SharedCacheMap 0x84f13898   3388   \Device\HarddiskVolume1\Users\IEUser\Desktop\key.txt
$ cat file.3388.0x84f13700.key.txt.vacb
0ba883a22afb84506c8d8fd9e42a5ce4e8eb1cc87c315a28dd
```

Pour le flag, la commande par défaut ne fonctionne pas. il va falloir être plus précis en passant en paramètre l'offset donné par `filescan` :
```console
$ ./vol dumpfiles -Q 0x000000003ed139f0 -D . -n
DataSectionObject 0x3ed139f0   None   \Device\HarddiskVolume1\Users\IEUser\Desktop\flag.txt.enc
$ od -t x1z file.None.0x855651e0.flag.txt.enc.dat
0000000 27 7b 6b 70 1a 01 00 55 05 07 5d 0c 53 55 05 55  >'{kp...U..].SU.U<
0000020 09 5d 59 5e 06 5c 04 02 06 54 07 51 00 55 01 5e  >.]Y^.\...T.Q.U.^<
0000040 55 57 52 5b 57 5c 51 54 50 07 51 07 0b 5e 55 51  >UWR[W\QTP.Q..^UQ<
0000060 55 56 02 59 5a 07 05 02 57 51 52 01 0f 03 57 02  >UV.YZ...WQR...W.<
0000100 06 01 5a 50 0f 1b 6e 00 00 00 00 00 00 00 00 00  >..ZP..n.........<
0000120 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  >................<
```

On a tout ce qu'il faut, maintenant il va falloir voir de plus près ce binaire. Quelques essais rapides dans une VM montrent que l'exécutable va chercher récursivement les fichiers `flag.txt` et les chiffrer pour les sauvegarder en `flag.txt.enc`. La boucle de chiffrement est la suivante :

![crypto_locker](cryptolocker.png)

Bref, on a affaire à un simple XOR. Du coup, fun fact, on n'a même pas besoin d'aller très loin pour effectuer le déchiffrement : en renommant le `flag.txt.enc` en `flag.txt` et en relançant l'exécutable, il va réappliquer le XOR et... nous déchiffrer le fichier. ;-)

```console
$ cp file.None.0x855651e0.flag.txt.enc.dat flag.txt && wine executable.3388.exe 
	ENCRYPTOR v0.5

[info] entering the folder : ./
[info] entering the folder : ./files/
[info] file encryptable found : ./flag.txt

****Chiffrement termin├®e ! Envoyez l'argent !


$ cat flag.txt.enc 
FCSC{324cee8fe3619a8bea64522eadf05c84df7c6df9f15e4cab4d0e04c77b20bb47}
```
