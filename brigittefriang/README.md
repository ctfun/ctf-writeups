# Challenge Brigitte Friang (DGSE & ESIEE 2020)

## Start

La page d'accueil du CTF mentionne : « Ouvrez bien les yeux, vous avez jusqu'au 11/11/2020 »

En regardant le code source de la page, on voit une référence à une page [message-secret.html](intro/message-secret.html).
Ça sent bon le décalage simple, et à tâton, on peut trouver que c'est du ROT7 :

```
Si vous parvenez a lire ce message, c'est que vous pouvez rejoindre l’operation «Brigitte Friang». Rejoignez-nous rapidement.

Brigitte Friang est une resistante, journaliste et ecrivaine francaise. Elle est nee le 23/01/1924 a Paris, elle a 19 ans sous l'occupation lorsqu'elle est recrutee puis formee comme secretaire/chiffreuse par un agent du BCRA, Jean-Francois Clouet des Perruches alias Galilee chef du Bureau des operations aeriennes (BOA) de la Region M (Cote du Nord, Finistere, Indre et Loire, Orne, Sarthe, Loire inferieure, Maine et Loire, Morbihan, Vendee). Brigitte Friang utilise parfois des foulards pour cacher des codes. Completez l’URL avec l’information qui est cachee dans ce message.

Suite a l’arrestation et la trahison de Pierre Manuel, Brigitte Friang est arretee par la Gestapo. Elle est blessee par balle en tentant de s’enfuir et est conduite a l’Hopital de la Pitie. Des resistants tenteront de la liberer mais sans succes. Elle est torturee et ne donnera pas d'informations. N’oubliez pas la barre oblique. Elle est ensuite envoyee dans le camp de Ravensbruck.

Apres son retour de deportation, elle participe a la creation du Rassemblement du peuple français (RPF). Elle integre la petite equipe, autour d'Andre Malraux, qui va preparer le discours fondateur de Strasbourg en 1947 et les elections legislatives de 1951.

Elle rentre a l'ORTF, et devient correspondante de guerre. Elle obtient son brevet de saut en parachute et accompagne des commandos de parachutistes en operation durant la guerre d’Indochine. Elle raconte son experience dans Les Fleurs du ciel (1955). D'autres agents sont sur le coup au moment ou je vous parle. Les meilleurs d'entre vous se donneront rendez-vous a l'European Cyberweek a Rennes pour une remise de prix. Resolvez le plus d'epreuves avant la fin de cette mission et tentez de gagner votre place parmi l'elite! Par la suite, elle couvre l’expedition de Suez, la guerre des Six Jours et la guerre du Viet Nam. Elle prend position en faveur d'une autonomie du journalisme dans le service public ce qui lui vaut d'etre licenciee de l'ORTF.

Elle ecrit plusieurs livres et temoigne de l'engagement des femmes dans la Resistance.
```

Mais en fait, il y a surtout certains caractères en gras dans la page HTML d'origine. En les regroupant, ça donne `joha`, ou en ROT7 : `chat`.

On utilise l'URL /chat et on tombe sur le portail d'introduction :

![](intro/portail.png)


## Intro

Ces parties constituaient l'intro du CTF. Les épreuves ne rapportent pas de point, mais
réussir une des catégories ci-dessous donne l'accès au board du réel CTF.

- [Alphonse Bertillon (Forensic)](intro/forensic/README.md)
- [Antoine Rossignol (Crypto)](intro/crypto/README.md)
- [Blaise Pascal (Algo)](intro/algo/README.md)
- [Jérémy Nitel (web)](intro/web/README.md)

## CTF

Les épreuves suivantes composent le CTF lui-même.

![](board.png)

Celles que j'ai résolues :
- [Sous l'océan (forensic, 50 points)](forensic/Sous%20L'océan/README.md)
- [ASCII UART (hardware, 100 points)](hardware/ASCII%20UART/README.md)
- [ChatBot (web, 100 points)](web/ChatBot/README.md)
- [Alone Muks (pwn, 100 points)](pwn/Alone%20Muks/README.md)
- [Keypad Sniffer (hardware, 150 points)](hardware/Keypad%20Sniffer/README.md)
- [Le discret Napier (crypto, 150 points)](crypto/Le%20discret%20Napier/README.md)
- [Le polyglotte (stégano, 150 points)](stegano/Le%20polyglotte/README.md)
- [Steganosaurus (forensic, 400 points)](forensic/Steganosaurus/README.md)