# Le polyglotte (stégano, 150 points)

## Énoncé

Nous avons intercepté un fichier top secret émanant d'Evil Country, il est très certainement en rapport avec leur programme nucléaire. Personne n'arrive à lire son contenu.

Pouvez-vous le faire pour nous ? Une archive était dans le même dossier, elle peut vous servir

Le flag est de la forme : DGSESIEE{x} avec x un hash que vous trouverez

[message.pdf](message.pdf) (SHA256=e5aa5c189d3f3397965238fbef5bc02c889de6d5eac713630e87377a5683967c)

[secrets.zip](secrets.zip) (SHA256=ae5877bb06ac9af5ad92c8cd40cd15785cbc7377c629ed8ec7443f251eeca91f)

## Solution

Pour ceux qui ne connaissent pas [Ange Albertini](https://github.com/angea), c'est le moment de vous
renseigner. Ses travaux sont très intéressants, et s'ils ne sont pas forcément très pratiques dans la
vraie vie, il y a une philosophie «hacking» passionnante. On va voir que cette épreuve repose sur
ses travaux.

Commençons par regarder le PDF :

![](message.png)

Pas très intéressant, ouvrons le plutôt avec un éditeur de texte :
```
<%PDF-1.2
<html>
<!DOCTYPE html>
<html>
	<head>
		<title>Flag</title>
		<meta charset="utf-8">
	</head>
	<body>
	<script>var flag = [91,48,93,97,97,57,51,56,97,49,54];</script>
<!--

2 0 obj
<< /Pages 4 0 R >>
endobj
[...]
9 0 obj
<< /Length 50 /MediaBox [0 0 20 20]
>>
stream
BT
 /F1 9 Tf
 30 600 Td
 1 0 0 700 0 0 Tm
 0 0 0 rg
 <43 65 20 64 6f 63 75 6d 65 6e 74 20 63 6f 6e 63 65 72 6e 65 20 6c 20 6f 70 65 72 61 74 69 6f 6e 20 73 6f 6c 65 69 6c 20 61 74 6f 6d 69 71 75 65 2e 0a 43 65 74 74 65 20 6f 70 65 72 61 74 69 6f 6e 20 65 73 74 20 73 74 72 69 63 74 65 6d 65 6e 74 20 63 6f 6e 66 69 64 65 6e 74 69 65 6c 6c 65 20 65 74 20 6e 65 20 64 6f 69 74 20 65 6e 20 61 75 63 75 6e 20 63 61 73 20 ea 74 72 65 20 64 65 76 6f 69 6c 65 65 2e 20 0a 4c 65 73 20 69 6e 66 6f 72 6d 61 74 69 6f 6e 73 20 73 75 72 20 6c 20 6f 70 65 72 61 74 69 6f 6e 20 73 6f 6e 74 20 64 69 73 73 65 6d 69 6e e9 65 73 20 64 61 6e 73 20 63 65 20 66 69 63 68 69 65 72 2e 0a 43 68 61 71 75 65 20 70 61 72 74 69 65 20 64 65 20 6c 20 69 6e 66 6f 72 6d 61 74 69 6f 6e 20 65 73 74 20 69 64 65 6e 74 69 66 69 65 65 20 70 61 72 20 75 6e 20 6e 6f 6d 62 72 65 20 70 61 72 20 65 78 20 3a 20 0a 5b 30 5d 61 65 37 62 63 61 38 65 20 63 6f 72 72 65 73 70 6f 6e 64 20 61 20 6c 61 20 70 72 65 6d 69 e8 72 65 20 70 61 72 74 69 65 20 64 65 20 6c 20 69 6e 66 6f 72 6d 61 74 69 6f 6e 20 71 75 20 69 6c 20 66 61 75 74 20 63 6f 6e 63 61 74 65 6e 65 72 20 61 75 20 72 65 73 74 65 2e> Tj
ET
endstream
endobj
[...]
--!>
		<script>for(i=0;i<flag.length;i++){flag[i] = flag[i]+4} alert(String.fromCharCode.apply(String, flag));</script>
	<body>
</html>
<!--
H�TMnh=�\���J�jo�X�}ә�VϪC<�_�$e����B�V��B��UX]Lҕ�_�7w���t�J�.#���7���4�C8>�C����{�$�����"��
[...]
```

On a donc une en-tête PDF, suivie de bouts de HTML et de PDF mélangés, et un gros blob binaire à la fin. Pour
qui connaît les dadas d'Ange, on se retrouve en face de deux techniques qu'il a développées. La première est qu'il
s'agit d'un fichier polyglotte : il peut être interprété de façons différentes suivant le logiciel utilisé. Ainsi, 
c'est un PDF, mais son ouverture dans un navigateur rend du HTML !

Intéressons nous au bloc contenant de l'hexadécimal dans l'un des streams PDF. Converti en ASCII, cela nous donne :
```
Ce document concerne l operation soleil atomique.
Cette operation est strictement confidentielle et ne doit en aucun cas être devoilee. 
Les informations sur l operation sont disseminées dans ce fichier.
Chaque partie de l information est identifiee par un nombre par ex : 
[0]ae7bca8e correspond a la première partie de l information qu il faut concatener au reste.
```

Il va donc falloir chercher des patterns de type `[X]YYYYYY` qui vont nous permettre de reconstruire le flag. Reprenons le début du fichier PDF :

`<script>var flag = [91,48,93,97,97,57,51,56,97,49,54];</script>`

Même motif, même punition, en ASCII cela donne : `[0]aa938a16`. Nous venons de trouver notre premier bout de flag.
Un peu plus loin dans le fichier, on trouve également :

`<5b 31 5d 34 64 38 36 32 64 35 61> Tj`, soit `[1]4d862d5a`, le deuxième fragment.

Regardons maintenant l'archive zip. Elle est protégée par un mot de passe, mais en bruteforçant en utilisant 
le [dictionnaire classique rockyou](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwj-04Gv7P_sAhWQy4UKHVcQC3UQFjAAegQIAhAC&url=https%3A%2F%2Fgithub.com%2Fbrannondorsey%2Fnaive-hashcat%2Freleases%2Fdownload%2Fdata%2Frockyou.txt&usg=AOvVaw3snAERl1mU6Ccr4WFEazBd) :

```console
$ fcrackzip -D -p rockyou.txt -u secrets.zip 
PASSWORD FOUND!!!!: pw == finenuke
```

Le zip contient un fichier texte qui contient un troisième bout du flag : 
```
Ange Albertini
key='\xce]`^+5w#\x96\xbbsa\x14\xa7\x0ei'
iv='\xc4\xa7\x1e\xa6\xc7\xe0\xfc\x82'
[3]4037402d4
```

Et une image :

![](hint.png)

Cette fois, Ange est clairement cité. La présence d'une clef et d'un IV nous oriente vers une autre de ses activités : 
[Angecryption](https://www.nolimitsecu.fr/ange-albertini-funky-file-formats/). Sans rentrer dans le détail (le lien précédant
pointe sur un podcast où il en parle, et comporte aussi un lien vers un talk qu'il a animé à ce sujet), l'idée est
de jouer sur le mode de chiffrement CBC et sur la technique polyglotte pour obtenir des fichiers d'un certain type (par exemple, PDF),
qui, lorsqu'on les chiffre ou déchiffre avec le bon couple clé/IV, donne un autre type (par ex : PNG). Ça ne marche pas dans tous 
les cas, c'est dépendant des formats de chacun et des librairies qui les parsent, mais il y a des résultats assez stables.

Si on revient au PDF de départ, la signature PDF en tête de fichier réduite et le blob binaire à la fin est caractéristique 
d'une Angecryption. Sachant que le fichier texte nous a donné une clef de 128 bits et un IV de 64 bits, reste à déterminer
l'algorithme de chiffrement par bloc utilisé. Revenons à l'image PNG : mais c'est bien sûr, on parle de [Blowfish](https://fr.wikipedia.org/wiki/Blowfish) !

Essayons donc de chiffrer le PDF avec Blowfish, la clef `\xce]``^+5w#\x96\xbbsa\x14\xa7\x0ei` et l'IV `\xc4\xa7\x1e\xa6\xc7\xe0\xfc\x82` :
```python
$ python
Python 3.8.5 (default, Jul 28 2020, 12:59:40) 
[GCC 9.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> from Crypto.Cipher import Blowfish
>>> algo = Blowfish.new(b'\xce]`^+5w#\x96\xbbsa\x14\xa7\x0ei', Blowfish.MODE_CBC, iv=b'\xc4\xa7\x1e\xa6\xc7\xe0\xfc\x82')
>>> data = open('message.pdf', "rb").read()
>>> data = algo.encrypt(data)
>>> open('encrypted', "wb").write(data)
89696
>>> 
$ file encrypted 
encrypted: JPEG image data
```

C'est très bon signe, on obtient une image JPG valide ! 

![](encrypted.jpg)

Mais ce n'est pas terminé. Rappelons nous des marottes d'Ange et analysons le fichier :
```console
$ binwalk -e encrypted.jpg 

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
72613         0x11BA5         ELF, 64-bit LSB shared object, AMD x86-64, version 1 (SYSV)
```

Un fichier ELF est concaténé au fichier JPG ! 

```console
$ tail -c +72614 encrypted.jpg > encrypted.elf
$ file encrypted.elf 
encrypted.elf: ELF 64-bit LSB shared object, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=dc9ae4a3811cf6a4acd62a1a5ab812bfd81fbe8d, for GNU/Linux 3.2.0, not stripped
$ chmod +x encrypted.elf 
$ ./encrypted.elf 
Operation Soleil Atomique
Entrez le mot de passe : qsdqsd
Mauvais mot de passe
```

On va passer rapidement sur l'examen du binaire : il y a une fausse piste avec une fonction `check_password` qui ne sert à rien, et la
vérification réelle du mot de passe se situe dans le main, qui XORe successivement les caractères du mot de passe entre eux, et 
vérifie à chaque étape le résultat avec des constantes. Pour ceux que ça intéressent, la fonction main annotée est [ici](main.asm).
On obtient un dernier fragment : `[2]e3c4d24`.

Reprenons tous nos fragments :
- `[0]aa938a16`
- `[1]4d862d5a`
- `[2]e3c4d24`
- `[3]4037402d4`

Le flag est donc `DGSESIEE{aa938a164d862d5ae3c4d244037402d4}`.
