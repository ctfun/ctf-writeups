#!/usr/bin/env python3

import ripyl
import ripyl.streaming as stream
import ripyl.protocol.uart as uart
import numpy
import sys

def read_samples():
    samples = list()
    with open(sys.argv[1], 'rb') as f:
        samples = numpy.fromfile(f, 'h') # h for signed short (car 8 bits signés dans l'énoncé)
    return samples

polarity = uart.UARTConfig.IdleHigh
stop_bits = 1
bits = 8
parity = 'even'
speed = int(sys.argv[2])
raw_samples = read_samples()
txd = stream.samples_to_sample_stream(raw_samples, 1/speed)
param_info = dict()
records_it = uart.uart_decode(txd, bits=bits, parity=parity, stop_bits=stop_bits, polarity=polarity, param_info=param_info, use_std_baud=True)
records = list(records_it) # This consumes the iterator and completes the decode

data = [rec.data for rec in records]
result = ''
for d in data:
    if d >31 and d < 127:
        result += chr(d)
    else:
        result += '?'
print(f'Bits={bits}, parity={parity}, stop_bits={stop_bits}, speed={speed} => baud_rate={param_info["baud_rate"]}\n\t{result}')