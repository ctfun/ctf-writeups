# ASCII UART (hardware, 100 points)

## Énoncé

Un informateur a intercepté un message binaire transmis sur un câble. Il a rapidement enregistré via la carte son d'un PC les données en 8 bits signés (ascii_uart.raw). Dans la précipitation, il a oublié de noter la fréquence d'échantillonnage. Retrouvez le message.

Le flag est de la forme DGSESIEE{X} avec X le message

[ascii_uart.raw](ascii_uart.raw) (SHA256=0421ace2bbacbb5a812868b0dbb38a23533cda67bf7f00b1031fdbd7a228c8a5)

## Solution

Le challenge semble (relativement) simple : il s'agit de décoder le flux UART en jouant avec 
les différents paramètres (nombre de bits, parité, vitesse, ...) jusqu'à trouver la bonne combinaison.
On peut par exemple s'appuyer sur [rypl](http://kevinpt.github.io/ripyl/) qui a l'air de faire ça très bien.
En tâtonnant à l'aide de [ce script python](decod_ascii_uart_v1.py), on obtient les caractéristiques : 96000 kHz, 
en 8N1.

```console
$ python decod_ascii_uart_v1.py  ascii_uart.raw 96000
Bits=8, parity=even, stop_bits=1, speed=96000 => baud_rate=300
	DGSESYIEE{ ?d[-_?-]b  \_(''/)_/  (^_-)   @}-;-=--     (*^_^*)  \o/ }
```

Problème, il semble y avoir des erreurs (cf. les ? dans le décodage, et le Y en trop dans le préfixe du flag).
On peut tenter de valider en retirant les ? et le Y  mais ça ne fonctionne pas. En creusant, on se rend compte 
qu'il y a effectivement des erreurs de transmission dans le flux, de deux types :
- Framing error : ok, on arrive à les récupérer, ce sont elles qui provoquent les ?.
- Parity error : c'est le drame, elles ne semblent pas remontées par ripyl !

Après une séance de debug, on parvient à [un patch de la librairie](ripyl.patch). On fait évoluer [le script python](decod_ascii_uart_v2.py)
également, afin de détecter les erreurs dans le flux, et retirer alors les caractères qui posent problème. Au final :

```console
$ python decod_ascii_uart_v2.py  ascii_uart.raw 96000
Bits=8, parity=even, stop_bits=1, speed=96000 => baud_rate=300
Result  : DGSESYIEE{ ?d[-_?-]b  \_(''/)_/  (^_-)   @}-;-=--     (*^_^*)  \o/ }
Errors  :      P     F    F                             P    P                
Cleaned : DGSESIEE{ d[-_-]b  \_(''/)_/  (^_-)   @}-;---    (*^_^*)  \o/ }
```