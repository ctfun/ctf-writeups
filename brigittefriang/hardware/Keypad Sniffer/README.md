# Keypad Sniffer (hardware, 150 points)

## Énoncé

Le code d'accès d'un centre militaire de télécommunications est saisi sur un clavier. Un agent a accédé au matériel (Cf. photos face avant et face arrière du clavier) et a inséré un dispositif pour enregister les données binaires qui transitent sur le connecteur du clavier. Le fichier joint (keypad_sniffer.txt) comprend les données binaires (échantillonnées à une fréquence de 15 kHz) au moment où une personne autorisée rentrait le code d'accès. Retrouvez le code d'accès.

Le flag est de la forme DGSESIEE{X} où X est le code saisi

[keypad_sniffer.txt](keypad_sniffer.txt) (SHA256=f5660a0b1c8877b67d7e5ce85087138cbd0c061b0b244afc516c489b39a7f79d)

[keypad_face.jpg](keypad_face.jpg) (SHA256=b39c0d732f645fc73f41f0955233bec3593008334a8796d2f1208346f927fef2)

[keypad_back.jpg](keypad_back.jpg) (SHA256=1f5d41c3521d04494779e43a4d5fae7cb14aad44e6e99cf36642ff4e88fab69f)

## Solution

C'est l'occasion de voir un peu comment fonctionne ce genre de chose. On commence par suivre les pistes sur les
deux photos qui nous sont données pour avoir une petite idée de ce qui sort par le câble. On arrive à quelque
chose comme ceci :

|||||||
|---|---|----|----|----|----|
| N | N | X1 | X2 | X3 | X4 |
| N | N | Y1 | Y2 | Y3 | Y4 |

Les deux pins de gauche de chaque ligne (N) sont raccordés entre eux et ne servent pas. Le reste des deux lignes
correspond probablement aux pins des lignes ou des colonnes, mais on ne sait pas si X représente les lignes et
Y les colonnes, ou l'inverse.

On peut aussi faire quelques statistiques sur le fichier de trace :
```console
$ sort keypad_sniffer.txt | uniq -c
   4140 100111100111
   5507 100111101011
   2855 100111101101
   1524 100111101110
   2008 101011100111
   5236 101011101011
   6727 101011101101
   2364 101011101110
   1204 101101100111
   2394 101101101011
   1144 101101101101
   3031 101110100111
   1416 101110101011
   1970 101110101101
   1702 101110101110
 166905 101111100111
 162736 101111101011
 164591 101111101101
 171692 101111101110
```

On peut remarquer :
- les bits 0, 1, 6, et 7 sont fixes, et correspondent donc aux connections neutres N qui ne servent pas.
- on peut poser l'hypothèse que les traces fournissent les données N N X1 X2 X3 X4 N N Y1 Y2 Y3 Y4 (ou l'inverse X/Y).
- la plupart du temps (4 dernières lignes des stats), les X restent au niveau haut, et les Y sont 
alternativement au niveau bas. C'est caractéristique de la technique de «matrix scanning» : on met un signal bas sur une 
ligne (resp. une colonne) et on regarde l'état des colonnes (resp. lignes). Si l'un des états passe également à zéro, 
alors le bouton à l'intersection (ligne, colonne) est en position appuyé.
- on a beaucoup de doublons dans le fichier, car la fréquence d'échantillonnage de la trace est supérieure à la fréquence
du circuit le scan.

Pour décoder, on peut imaginer alors l'algorithme suivant :
- on élimine les doublons.
- on découpe la trace en 4 lignes qui représentent un scan complet (finissant successivement par 0111, 1011, 1101 et 1110).
- sur chaque scan de 4 lignes, on détermine si une touche est appuyée ou non.
- on ne garde que les transitions up/down de chacune des touches.

Le tout est [écrit en python (encore !)](decode.py) et (coup de bol sur l'interprétation ligne/colonne), donne le résultat suivant :
```console
$ python decode.py 
Code  (1500 fois)
Code A (400 fois)
Code  (1468 fois)
Code E (312 fois)
Code  (667 fois)
Code 7 (159 fois)
Code  (452 fois)
Code 8 (316 fois)
Code  (355 fois)
Code F (202 fois)
Code  (459 fois)
Code 5 (314 fois)
Code  (411 fois)
Code 5 (377 fois)
Code  (390 fois)
Code C (225 fois)
Code  (582 fois)
Code 6 (325 fois)
Code  (1438 fois)
Code 6 (173 fois)
Code  (1378 fois)
Code 6 (390 fois)
Code  (1394 fois)
Code B (260 fois)
Code  (341 fois)
Code 2 (381 fois)
Code  (361 fois)
Code 3 (377 fois)
Code  (702 fois)
Code 0 (187 fois)
Code  (1349 fois)
Code 1 (152 fois)
Code  (527 fois)
Code 1 (395 fois)
Code  (1288 fois)
Code 9 (151 fois)
Code  (558 fois)
Code 2 (346 fois)
Code  (675 fois)
Code 4 (265 fois)
Code  (1400 fois)
DGSESIEE{AE78F55C666B23011924}, len=20
```