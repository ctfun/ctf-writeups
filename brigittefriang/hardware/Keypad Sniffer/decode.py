#!/usr/bin/env python3

codes = open('keypad_sniffer.txt', 'r').read().split('\n')

keymap = [  # k[ligne][colonne]
    [ '1', '2','3', 'F' ],
    [ '4', '5','6', 'E' ],
    [ '7', '8','9', 'D' ],
    [ 'A', '0','B', 'C' ],
]

def delete_dup(codes):
    # Supprime les lignes dupliquées
    new_codes = list()
    previous = ''
    for code in codes:
        if code != previous:
            new_codes.append(code)
            previous = code
    return new_codes

def decod_scan(lines):
    # On reçoit 4 lignes pour un scan.

    for line in lines:
        high = line[2:6]
        low = line[8:12]

        # Absence de frappe
        if high == '1111':
            continue

        # On choppe les 0 dans les deux et ça nous donne les coordonnées de la touche
        l = high.index('0')
        c = low.index('0')
        car = keymap[l][c]
        return car
    
    return ''

previous = ''
count = 0
pcar = ''
flag = ''
codes = delete_dup(codes)
while len(codes) > 3:
    scan = [ codes.pop(0) for _ in range(4) ]
    car = decod_scan(scan)
    if car == pcar:
        count += 1
    else:
        print(f'Code {pcar} ({count} fois)')
        flag += car
        pcar = car
        count = 1

print(f'Code {previous} ({count} fois)')

print(f'DGSESIEE{{{flag}}}, len={len(flag)}')