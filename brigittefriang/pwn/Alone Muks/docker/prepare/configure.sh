#!/bin/sh

# Pre-requisites OpenSSH
mkdir /var/run/sshd

# Users creation
for user in user globalSystem navigationSystem; do
    /usr/sbin/adduser "$user" --gecos "" --shell /bin/bash --disabled-password --force-badname
done
/usr/sbin/usermod -s /bin/rbash user
/bin/echo 'user:user' | /usr/sbin/chpasswd

# User environment
cp /prepare/login.py /home/user
cp /prepare/bash_profile /home/user/.bash_profile
mkdir /home/user/bin
ln -s /bin/date /home/user/bin
ln -s /usr/bin/python2 /home/user/bin
echo "source /home/user/.bash_profile" >> /home/user/.bashrc
chown -R user:user /home/user

# navigationSystem environment
cp /prepare/flag.txt /home/navigationSystem
chown -R navigationSystem:navigationSystem /home/navigationSystem
chmod 400 /home/navigationSystem/flag.txt

# globalSystem environment
cp /prepare/update /usr/bin/update
chown globalSystem:globalSystem /usr/bin/update
chmod 755 /usr/bin/update

# Sudo configuration
cp -f /prepare/sudoers /etc/sudoers
chmod 440 /etc/sudoers

# Remove motd
rm /etc/legal /etc/update-motd.d/*