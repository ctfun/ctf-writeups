# Image Docker Alone Muks

## Avertissement

Image Docker répliquant l'épreuve de Pwn du CTF Brigitte Friang 2020.

**Si vous souhaitez garder la surprise de la solution, ne consultez PAS les scripts de construction de l'image** !

## Usage

L'image est construite par docker build, par exemple :
```
docker build . -t alone_muks
```

Elle doit ensuite être lancée, en prévoyant la redirection du port SSh du container sur un port local :
```
docker run -p 127.0.0.1:1022:22 -d alone_muks
```