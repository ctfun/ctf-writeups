# Jérémy Nitel (Web)

## Partie 1

![](Jérémy%20Nitel%201.png)

![](Jérémy%20Nitel%202.png)

On se rend donc sur le premier site, celui de Stockos :

![](1/Stockos%201.png)

On se rappelle que Jérémy nous a indiqué que les mots de passe étaient pas terrible. On
fait quelques essais et on arrive sur un classique `admin/admin`. Une fois connecté, on
se balade dans les pages, et la plus intéressante est la page de gestion des stocks, qui
comporte un champ de recherche. Quelques manipulations montrent qu'une injection SQL est
présente. 

![](1/Stockos%202.png)

Une injection de `non-existent' union select table_name,1,1,1,1 from information_schema.tables;#`
fournit la liste des tables existantes dans la base de données. Au delà des classiques des bases
MySQL, on trouve celles de l'application : `customer`, `orders`, `section` et `supplier`.

Dans une deuxième étape, `non-existent' union SELECT GROUP_CONCAT(column_name),1,1,1,1 FROM information_schema.columns WHERE table_name = 'customer';#`
retourne la liste des colonnes de la table des clients : `delivery_address`, `email`, `id`, `name` et `signup_date`.
Une étape finalement assez inutile, puisqu'il n'y a que 5 colonnes, comme sur la requête
d'origine, donc pas besoin de sélectionner les colonnes voulues.

La troisième et dernière étape est de récupérer les données de la table des clients, par 
`non-existent' union SELECT * FROM customer;#`. 

![](1/Stockos%203.png)

Le client qui nous intéresse est probablement :

`12 	Evil Gouv 	2056 Evil Road, Evil City, Death State, Evil Country 	2015-10-21 00:00:00 	agent.malice@secret.evil.gov.ev`


On va maintenant sur le deuxième site, celui de réservation de vol. On tente la réservation demandée, mais il faut finir 
par s'authentifier. Qu'à cela ne tienne, on se crée son propre compte, mais la réservation échoue : notre profil n'est pas
autorisé à faire ce voyage.

![](1/Evil%20Air%201.png)

On joue alors avec les autres fonctionnalités, et la procédure de perte de mot de passe semble intéressante. En
effet, en l'utilisant, on reçoit par mail un lien comportant un token de reset qui ressemble à du base64. En décodant 
celui-ci, on se rend compte qu'il s'agit de notre adresse e-mail ! Ainsi, on peut réinitialiser le mot de passe de
n'importe quel compte en connaissant l'adresse e-mail de l'utilisateur.  

On lance donc la procédure de perte de mot de passe pour `agent.malice@secret.evil.gov.ev` :

![](1/Evil%20Air%202.png)

Puis on reconstruit un lien qu'il a dû recevoir dans sa boîte e-mail :
`http://challengecybersec.fr/35e334a1ef338faf064da9eb5f861d3c/reset/YWdlbnQubWFsaWNlQHNlY3JldC5ldmlsLmdvdi5ldg==`

![](1/Evil%20Air%203.png)

On se connecte sous cette identité, puis on récupère le QRcode dans les réservations.

![](1/Evil%20Air%204.png)

Le QR code nous donne le flag `DGSESIEE{2cd992f9b2319860ce3a35db6673a9b8}` que l'on retourne à Jérémy.

# Partie 2

![](Jérémy%20Nitel%203.png)

Le fichier fourni par Jérémy est [celui-ci](2/capture.pcap).

**TODO** À finir...