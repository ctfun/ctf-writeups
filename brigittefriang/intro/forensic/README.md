# Alphonse Bertillon (Forensic)

## Partie 1

![](Alphone%20Bertillon%201.png)

Le fichier de log en question est [ici](access.log).

On regarde les User-Agents présents dans le fichier :
```console
$ cut -d" " -f14- access.log  | sort | uniq -c
      1 "Evil Browser"
    189 "Mozilla/5.0 (Android; Linux armv7l; rv:5.0) Gecko/20110615 Firefox/5.0 Fennec/5.0"
    208 "Mozilla/5.0 (Android; Tablet; rv:19.0) Gecko/19.0 Firefox/19.0"
    205 "Mozilla/5.0 (iPhone; CPU iPhone OS 12_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.0 Mobile/15E148 Safari/604.1"
    210 "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36 Edge/12.246"
    198 "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.75 Safari/537.1"
    213 "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.2.149.27 Safari/525.13"
    220 "Mozilla/5.0 (Windows; U; Windows NT 6.1; fr; rv:1.8.1.20) Gecko/20081217 Firefox/2.0.0.20"
    206 "Mozilla/5.0 (X11; Linux x86_64; rv:2.0.1) Gecko/20100101 Firefox/4.0.1"
    191 "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0"
    177 "Mozilla/5.0 (X11; U; Linux i686; fr; rv:1.9b5) Gecko/2008041514 Firefox/3.0b5"
```

Celui qui n'est utilisé qu'une seule fois et qui a un nom correspondant à nos ennemis est probablement celui recherché.

```console
$ grep "Evil Browser" access.log 
179.97.58.61 - - [Nov 05 2020 16:22:20] "POST /login HTTP/1.1" 200 476 "-" "Evil Browser"
```

![](Alphone%20Bertillon%202.png)

## Partie 2

Le fichier en question est [récupérable ici](https://mega.nz/file/U44kDLhS#7wpaA9KDcj94pAiK9GVOVX8vA7gQcFvrjzCzCDH2cFA). On conviendra que pour un fichier jpg, 300 Mo, c'est intrigant. C'est le moment de sortir binwalk pour l'examiner :

```console
$ binwalk evil_country_landscape.jpg 

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             JPEG image data, JFIF standard 1.01
79798         0x137B6         Zip archive data, at least v2.0 to extract, uncompressed size: 173015040, name: part2.img
4775856       0x48DFB0        Zlib compressed data, best compression
34737670      0x2120E06       MySQL MISAM compressed data file Version 1
56164092      0x358FEFC       IMG0 (VxWorks) header, size: 257308484
128298187     0x7A5ACCB       Cisco IOS microcode, for "w%"
158637081     0x9749C19       Zip archive data, at least v2.0 to extract, uncompressed size: 173015040, name: part3.img
239002530     0xE3EE3A2       Zlib compressed data, best compression
317286906     0x12E969FA      End of Zip archive, footer length: 22
```

Ce qui va nous intéresser, ce sont les deux ZIP, le reste étant des faux positifs. On les extrait par -e, on dézippe, et on regarde ce que sont ces deux images :

```console
$ file *.img
part2.img: Linux Software RAID version 1.2 (1) UUID=dfaa645a:19afec72:60f1fa33:30d841da name=user-XPS-15-9570:6 level=5 disks=3
part3.img: Linux Software RAID version 1.2 (1) UUID=dfaa645a:19afec72:60f1fa33:30d841da name=user-XPS-15-9570:6 level=5 disks=3
```

Ok, ce sont des volumes RAID, mais encore ?

```console
$ mdadm --examine *.img
part2.img:
          Magic : a92b4efc
        Version : 1.2
    Feature Map : 0x0
     Array UUID : dfaa645a:19afec72:60f1fa33:30d841da
           Name : user-XPS-15-9570:6
  Creation Time : Tue Oct  6 11:29:56 2020
     Raid Level : raid5
   Raid Devices : 3

 Avail Dev Size : 333824 (163.00 MiB 170.92 MB)
     Array Size : 333824 (326.00 MiB 341.84 MB)
    Data Offset : 4096 sectors
   Super Offset : 8 sectors
   Unused Space : before=4016 sectors, after=0 sectors
          State : clean
    Device UUID : 666ea76a:5c750c31:b7c6c06b:1c9c7ee1

    Update Time : Tue Oct  6 14:38:08 2020
  Bad Block Log : 512 entries available at offset 16 sectors
       Checksum : 4fdf0461 - correct
         Events : 26

         Layout : left-symmetric
     Chunk Size : 512K

   Device Role : Active device 1
   Array State : .AA ('A' == active, '.' == missing, 'R' == replacing)
part3.img:
          Magic : a92b4efc
        Version : 1.2
    Feature Map : 0x0
     Array UUID : dfaa645a:19afec72:60f1fa33:30d841da
           Name : user-XPS-15-9570:6
  Creation Time : Tue Oct  6 11:29:56 2020
     Raid Level : raid5
   Raid Devices : 3

 Avail Dev Size : 333824 (163.00 MiB 170.92 MB)
     Array Size : 333824 (326.00 MiB 341.84 MB)
    Data Offset : 4096 sectors
   Super Offset : 8 sectors
   Unused Space : before=4016 sectors, after=0 sectors
          State : clean
    Device UUID : 8534de66:19c46d61:2668aa72:978c14a8

    Update Time : Tue Oct  6 14:38:08 2020
  Bad Block Log : 512 entries available at offset 16 sectors
       Checksum : 49f6ab29 - correct
         Events : 26

         Layout : left-symmetric
     Chunk Size : 512K

   Device Role : Active device 2
   Array State : .AA ('A' == active, '.' == missing, 'R' == replacing)
```

Donc on a deux images sur les trois qui composent ce volume RAID, mais son état est «propre», donc on peut le monter.

On commence par mapper les deux parties sur des dev loop :
```console
$ sudo losetup /dev/loop6 part2.img
$ sudo losetup /dev/loop7 part3.img
```

Sur mon système, les images sont alors immédiatement détectées comme un volume RAID :
```console
$ cat /proc/mdstat
Personalities : [raid6] [raid5] [raid4]
md127 : active (auto-read-only) raid5 loop7[3] loop6[1]
      333824 blocks super 1.2 level 5, 512k chunk, algorithm 2 [3/2] [_UU]

unused devices: <none>
```

On peut maintenant le monter et regarder ce qu'il contient :
```console
$ sudo mount /dev/md127 tmp/
$ ls -al tmp/
total 298292
drwxr-xr-x 3 root root      4096 oct.   6 11:35 .
drwxr-xr-x 4 papy papy      4096 nov.  12 21:34 ..
-rw-r--r-- 1 root root 305119359 oct.   6 11:35 dump.zip
drwx------ 2 root root     16384 oct.   6 11:31 lost+found
```

## Partie 3

On a donc un dump.zip, qui contient un fichier dump.vmem, qui est évidemment une image mémoire Windows.

**TODO** À finir ! 

