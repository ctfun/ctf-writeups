#!/usr/bin/env python3

from BitVector import BitVector

cipher = [int(x, 16) for x in 'AF 3A 5E 20 A6 3A D0'.split(' ')]

modulus = BitVector(bitstring='100011011')
clear = list()
for c in cipher:
    bv = BitVector(intVal = c, size=8)
    t = bv.gf_MI(modulus, 8)
    clear.append(t.int_val())
print(''.join( [chr(x) for x in clear]))