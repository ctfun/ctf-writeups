# Antoine Rossignol (Crypto)

## Partie 1

![](Antoine%20Rossignol%201.png)
![](Antoine%20Rossignol%202.png)

Les fichiers sont récupérables ci-dessous :
- [echange.txt](1/echange.txt)
- [archive_chiffree](1/archive_chiffree)
- [layout.pdf](1/layout.pdf)
- [compte_rendu_eve.pdf](1/compte_rendu_eve.pdf)

Comme indiqué, on commence par essayer de prendre contact avec Ève, ses coordonnées sont dans le PDF de compte-rendu. On commence par un vrai e-mail à eve.descartes@esiee.fr. La réponse est décevante :
« Je suis actuellement indisponible.
Pour tout problème même minuscule, n'hésitez pas à me contacter par téléphone. »

Ok, allons-y pour le téléphone, dont le numéro est également dans le PDF. Et là, surprise, du morse !

`.-. . ... .. ... - .- -. -.-. .` ou, dans notre alphabet, `resistance`.

C'est le code qui permet d'ouvrir le fichier layout.pdf. Celui-ci est intitulé «fusibles DES», montre des circuits, et on remarque rapidement que certaines liaisons sont coupées, alors que d'autres non. Ça ressemble à du binaire, et quand on regroupe les liaisons par paquets de 8 bits :

```
01000001 01000101
01010011 00100000
00110010 00110101
00110110 00100000
01000101 01000011
01000010 00100000
00100000 00100000
00100000 00100000
00100000 00100000
00100000 00100000
01000010 00100000
00100000 00100000
00100000 00100000
00100000 00100000
00100000 00100000
00100000 00100000
```

C'est de l'ASCII, et plus précisément : «AES 256 ECB         B           ». En fait, cela nous donne les informations nécessaires pour déchiffrer l'archive. 

![](1/Cyberchef.png)

Note: attention à bien conserver le nombre d'espaces dans la clef. De même, je me demande d'où sort le B en plein milieu : il faut le remplacer par un espace pour déchiffrer correctement.

## Partie 2

On obtient alors une nouvelle archive zip qui contient [un PDF avec mot de passe](2/code_acces.pdf) et [un autre lisible](2/message.pdf). Ce dernier nous dit :
![](2/equations.png)

Il s'agit d'un système d'équations modulaires, qui peut être résolu par [le théorème des restes chinois](https://fr.wikipedia.org/wiki/Th%C3%A9or%C3%A8me_des_restes_chinois), par exemple à l'aide de [ce script python](2/chinese_remainders.py).

Le résultat nous donné x³=177693901848, donc x=5622. On peut alors ouvrir le PDF avec ce code.

## Partie 3

Le PDF nous indique :

![](3/GF256.png)

Après quelques lectures sur le sujet, on tombe notamment sur [ceci](https://engineering.purdue.edu/kak/compsec/NewLectures/Lecture7.pdf) qui, non content de citer le polynôme en question (tiens, c'est le polynôme utilisé par AES ?), évoque aussi une fonction d'inversion fournie par le package python [BitVector](https://engineering.purdue.edu/kak/dist/) :

```python
def gf_MI(num, mod, n):
    ’’’Using the arithmetic of the Galois Field GF(2^n), this function returns the
    multiplicative inverse of the bit pattern ’num’ when the modulus polynomial
    is represented by the bit pattern ’mod’.’’’
```

Dans notre cas, n=8 (GF(256)) et le *bit pattern* donne les valeurs des termes du polynôme, soit dans notre cas 100011011. Encore [un script python](3/inv_mul.py) et :

```console
$ python inv_mul.py 
b a:e z
```

On fournit la solution à Antoine et cela clôt cette partie.

![](Antoine%20Rossignol%203.png)