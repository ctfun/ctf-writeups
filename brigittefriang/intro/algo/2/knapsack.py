#!/usr/bin/env python3

import sys
from functools import reduce

sys.setrecursionlimit(50000)

f = open(sys.argv[1], 'r')
capacity, nb = [int(x) for x in f.readline().strip('\n').split(' ')]
objects = [int(x) for x in f.readline().strip('\n').split(' ')]
f.close()

ids = dict()
for i, v in enumerate(objects):
    if v in ids:
        ids[v].append(i)
    else:
        ids[v] = list([i])
weights = sorted(objects, reverse=True)


def pickOne(remains, current, capacity):
    #print(f'DEBUG: current={current}, capacity={capacity}, remains={remains}')
    for r in remains:
        if current + r > capacity:
            continue
        if current + r == capacity:
            #print(f'Good with {r} !')
            return list([r])
        
        remains2 = list(remains)
        remains2.remove(r)
        current2 = current + r
        result = pickOne(remains2, current2, capacity) 
        if result is not None:
            result.append(r)
            return result

    return None

print(f'Capacity : {capacity}\nObjects : {objects}\nWeights : {weights}\nIds : {ids}')
result = pickOne(weights, 0, capacity)
print(f'Exact result : {result}')

rids = list()
for r in result:
    plop = ids[r]
    rids.append(plop[0])
    ids[r] = plop[1:]
print(f'Ids : {rids}')

f = open(f'solution_{sys.argv[1]}.txt', 'w')
f.write('{}\n{}\n'.format(len(rids), " ".join([ str(x) for x in rids])))
f.close()