# Blaise Pascal (algo)

## Partie 1

![](Blaise%20Pascal%201.png)

Les fichiers en question : [original.txt](1/original.txt) et [intercepte.txt](1/intercepte.txt).

On commence par examiner les différences, caractère par caractère. Le fichier intercepté 
contient des caractères supplémentaires. Les premiers que l'on peut voir sont : `base64:/9j/4AAQS...`. 
En fait, il s'agit d'une image JPG. On bricole donc [un script python](1/diff.py), et le résultat :

![](1/flag.jpg)

On transmet le hash `/22caeee05cb8b2a49133be134a5e9432` à Blaise, et lui nous indique de suivre une URL identique pour la suite :

![](Blaise%20Pascal%202.png)

## Partie 2

On arrive alors sur l'écran suivant :

![](2/mission1.png)

L'archive contient les fichiers suivants :
- [IMPORTANT.pdf](2/IMPORTANT.pdf)
- [exemple.in](2/exemple.in)
- [fichier_a_petit.in](2/fichier_a_petit.in)
- [fichier_b_moyen.in](2/fichier_b_moyen.in)
- [fichier_c_gros.in](2/fichier_c_gros.in)
- [fichier_d_gros.in](2/fichier_d_gros.in)

Le PDF est assez explicite sur ce qui est attendu, à part sur un point qui n'est pas clair, et qui ne se voit
pas dans l'exemple : il est demandé de remplir l'entrepôt au maximum si c'est possible. Là aussi, c'est affaire
d'[un script python](2/knapsack.py) pour la résolution de tout cela.

![](2/mission2.png)

## Partie 3

On arrive sur la page suivante :

![](3/mission.png)

La mission est clairement affichée. En regardant d'un peu plus près, on se rend compte que les billets de blog sont
accessible par un identifiant, qu'il suffit d'énumérer un à un. Nouveau [script python](3/browse.py).

Avec le bon hash final : 

![](3/solution.png)

## Partie 4

On arrive sur un portail de connexion : 

![](4/login.png)

On tente une connexion, et il retourne une erreur immédiatement sans appel réseau apparent. C'est louche, et en regardant
le code source, on constate la référence à un fichier [login.js](4/login.js) qui assure l'authentification. Assez étonnamment, 
le mot de passe n'est pas référencé, tout se base sur le login.

Évidemment, le javascript est obfusqué. On commence donc par le passer dans [de4js](https://lelinhtinh.github.io/de4js/), 
puis c'est un effort d'analyse pour comprendre ce qu'il fait petit à petit. Une version nettoyée est disponible [ici](4/login2.js). 
Globalement, les étapes sont les suivantes :
- on ajoute du padding sur le login pour qu'il atteigne une taille de 40.
- on fait des permutations sur les positions des caractères suivant le tableau fourni.
- on chiffre le résultat à coup de XOR avec la clef `d[cn?k+qje)/N|t.wkGr]rO+k9b=2y,}@Zyb:8pla2'6%dn)`.
- on compare le chiffré avec la valeur `7<0l<ni03<l<l<3>5<b``>dk>j;3n0>>o9n0``nk39`.

Toutes les opérations sont aisément réversibles, [ça se scripte](4/reverse_login.py), et on obtient le login 
`3f3939527e73ad93b73b070bb12cde1292bbcde5`. En testant ce login avec n'importe quel mot de passe, on voit s'afficher 
«identifiants valides», puis une redirection vers un sous-répertoire du même nom que le login et... c'est le drame d'une erreur 404. 

En reprenant le tableau de permutation, on se rend compte qu'elle n'est pas bijective. Les caractères 0 et 4 sont perdus dans la
permutation, et donc l'opération inverse ne peut pas les retrouver. Il s'agit alors de [brute-forcer](4/bf.py) les deux caractères manquant 
dans l'URL de redirection pour obtenir la bonne : `5f3949527e73ad93b73b070bb12cde1292bbcde5`.

On arrive sur la liste des opérations :

![](4/Opération.png)

Et en ouvrant l'opération Diablerie :

![](4/Diablerie.png)

Et c'est la fin de cette partie !
