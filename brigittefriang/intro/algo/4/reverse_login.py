#!/usr/bin/env python3

def encrypt(test):
    key = 'd[cn?k+qje)/N|t.wkGr]rO+k9b=2y,}@Zyb:8pla2\'6%dn)'
    clear = ''
    for i, c in enumerate(test):
        n = ord(c) ^ ord(key[i]) & 15
        clear += chr(n)
    return clear

def unpermut(clear):
    indices = [0, 21, 0, 34, 4, 9, 23, 30, 14, 5, 29, 4, 24, 22, 8, 20, 31, 17, 38, 35, 15, 1, 13, 6, 12, 26, 25, 27, 33, 10, 7, 16, 32, 28, 3, 19, 37, 36, 18, 39]
    #print(sorted(indices))
    arrLogin = [ x for x in clear ]
    login = ['*'] * 40
    for i in range(0, 40):
        login[i] = arrLogin[indices[i]]
    return ''.join(login)

def permut(login):
    indices = [0, 21, 0, 34, 4, 9, 23, 30, 14, 5, 29, 4, 24, 22, 8, 20, 31, 17, 38, 35, 15, 1, 13, 6, 12, 26, 25, 27, 33, 10, 7, 16, 32, 28, 3, 19, 37, 36, 18, 39]
    arrLogin = [ x for x in login ]
    clear = ['*'] * 40
    for i in range(0, 40):
        arrLogin[indices[i]] = login[i]
    return ''.join(arrLogin)

def pad(login):
    padding = '9o#jzQ$=W8sN>n?kIXuIM7sh6 WilbDPx`1&YF5z'
    i = 0
    while len(login) < 40:
        login += padding[i]
        i+=1
    return login

print(unpermut(encrypt('7<0l<ni03<l<l<3>5<b`>dk>j;3n0>>o9n0`nk39')))