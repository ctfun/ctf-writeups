#!/usr/bin/python3

import requests

for a in '0123456789abcdef':
    for b in '0123456789abcdef':
        url = f'https://challengecybersec.fr/1410e53b7550c466c76fc7268a8160ae/{a}f39{b}9527e73ad93b73b070bb12cde1292bbcde5'
        r = requests.get(url)
        print(f'Testing {url} : {r.status_code}')
