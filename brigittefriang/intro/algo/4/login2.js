  function DisplayValidity(strIn) {
      var str = document.getElementById(strIn).value;
      if(check(str) == 0){
          document.getElementById("out").innerHTML = '<div class="alert alert-danger">Identifiants invalides</div>';
      }
      else{
          document.getElementById("out").innerHTML = '<div class="alert alert-success">Identifiants valides !</div>';
          document.location.href = window.location.href + "/" + str;
      }
  }

  function check(login) {
    var result = encrypt(permut(login));
    if (result == '7<0l<ni03<l<l<3>5<b`>dk>j;3n0>>o9n0`nk39') {
        return 1;
    }
    return 0;
  }
  
  function permut(login) {
    var indices = [0, 21, 0, 34, 4, 9, 23, 30, 14, 5, 29, 4, 24, 22, 8, 20, 31, 17, 38, 35, 15, 1, 13, 6, 12, 26, 25, 27, 33, 10, 7, 16, 32, 28, 3, 19, 37, 36, 18, 39];
    var padding = '9o#jzQ$=W8sN>n?kIXuIM7sh6 WilbDPx`1&YF5z';
    var i = 0;
    var lpadding = padding.length;
    while (login.length < 40) {
        login += padding[i++];
        if (i >= lpadding) {
            i = 0;
        }
    }
    var arrlogin = login.split('');
    for (i = 0; i < arrlogin.length; i++) {
        arrlogin[indices[i]] = login.charAt(i);
    }
    return arrlogin.join('');
  }
  
  function encrypt(string) {
    var key = "d[cn?k+qje)/N|t.wkGr]rO+k9b=2y,}@Zyb:8pla2'6%dn)";
    var arrstring = string.split('');
    var code = 0;
    for (var i = 0; i < arrstring.length; i++) {
        code = string.charCodeAt(i) ^ key.charCodeAt(i) & 15;
        arrstring[i] = String.fromCharCode(code);
        if (code < 32 || code > 126) {}     // gni ?
    }
    return arrstring.join('');
  }