#!/usr/bin/env python3

import requests
import re
from hashlib import md5

base='https://challengecybersec.fr/9bcb53d26eab7e9e08cc9ffae4396b48/blog/post/'

def browse_post(id):
    r = requests.get(f'{base}{id}')
    if r.status_code != 200:
        #print(f'Blog post {id} : {r.status_code}')
        return None
    page = r.text

    m = re.search(r'message-digest: </b><span id="partial-proof">([a-z0-9]+)</span>', page)
    if m is None:
        md = None
    else:
        md = m.group(1)

    return md

h = md5()
md = ''
sum = ''
i = 1
while True:
    md = browse_post(i)
    if md is None:
        break
    h.update(md.encode('utf-8'))    
    print(f'Post {i} : {md} ({h.hexdigest()})')
    i+=1

print(h.hexdigest())
