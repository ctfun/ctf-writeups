#!/usr/bin/env python3

from base64 import b64decode

original = open('original.txt', 'r')
intercepte = open('intercepte.txt', 'r')

diff = ''
for l1 in original:
    l2 = intercepte.readline()
    i=0
    j=0
    while i < len(l1):
        while l1[i] != l2[j]:
            diff += l2[j]
            j+=1
        i+=1
        j+=1

binary = b64decode(diff.split(':')[1])
with open('flag.jpg', 'wb') as f:
    f.write(binary)