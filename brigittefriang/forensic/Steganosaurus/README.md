# Steganosaurus (forensic, 400 points)

## Énoncé

Nos agents ont trouvé dans le camion de livraison une clef USB. Nous vous transférons le filesystem de cette dernière et espérons que votre grande capacité de réflexion pemettra de révéler les secrets les plus sombres d'Evil Country !

Le flag est de la forme DGSEESIEE{x} avec x une chaine de caractères. (Attention au DGEESIEE, une erreur de typo s'est glissée dans le flag)

[message](https://mega.nz/file/Zs5GwKqT#doz3y9udZ6c7VG9DJ6P9g-r6lGlVQ9nzY88l1h-gymY) (SHA256=3889febebd6b1d35c057c3ba3f6f722798f029d6d0321b484305922a3d55d4d8)

## Solution

Quel est donc ce fichier ?

```console
$ file message
message: DOS/MBR boot sector, code offset 0x58+2, OEM-ID "mkfs.fat", Media descriptor 0xf8, sectors/track 32, heads 64, hidden sectors 7256064, sectors 266240 (volumes > 32 MB), FAT (32 bit), sectors/FAT 2048, reserved 0x1, serial number 0xccd8d7cd, unlabeled
```

Montons donc cette partition FAT.

```console
$ sudo mount -o loop message tmp/
 tree -a tmp/
tmp/
├── readme
├── steganausorus.apk
└── .Trash-1000
    ├── files
    │   └── flag.png
    └── info
        └── flag.png.trashinfo

3 directories, 4 files
```

Voici le readme :
```
Bonjour evilcollegue !
Je te laisse ici une note d'avancement sur mes travaux !
J'ai réussi à implémenter complétement l'algorithme que j'avais présenté au QG au sein d'une application.
Je te joins également discrétement mes premiers résultats avec de vraies données sensibles ! Ils sont bons pour la corbeille mais ça n'est que le début !
Je t'avertis, l'application souffre d'un serieux defaut de performance ! je m'en occuperai plus tard.
contente-toi de valider les résultats.
Merci d'avance

For the worst,

QASKAB
```

Et le flag :

![](flag.png)

Si son contenu n'est pas très lisible, il y a immédiatement quelque chose de louche en zoomant en haut à gauche :

![](zoom_flag.png)

Regardons d'un peu plus près l'APK, en l'analysant par `apktool d steganausorus.apk`. Première étape, le Manifest Android :
```xml
<?xml version="1.0" encoding="utf-8" standalone="no"?><manifest xmlns:android="http://schemas.android.com/apk/res/android" android:compileSdkVersion="28" android:compileSdkVersionCodename="9" package="com.example.stegapp" platformBuildVersionCode="28" platformBuildVersionName="9">
    <uses-permission android:name="android.permission.INTERNET"/>
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"/>
    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
    <application android:appComponentFactory="androidx.core.app.CoreComponentFactory" android:debuggable="true" android:icon="@mipmap/ic_launcher" android:label="stegapp" android:name="io.flutter.app.FlutterApplication">
        <activity android:configChanges="density|fontScale|keyboard|keyboardHidden|layoutDirection|locale|orientation|screenLayout|screenSize|smallestScreenSize|uiMode" android:hardwareAccelerated="true" android:launchMode="singleTop" android:name="com.example.stegapp.MainActivity" android:theme="@style/LaunchTheme" android:windowSoftInputMode="adjustResize">
            <meta-data android:name="io.flutter.embedding.android.NormalTheme" android:resource="@style/NormalTheme"/>
            <meta-data android:name="io.flutter.embedding.android.SplashScreenDrawable" android:resource="@drawable/launch_background"/>
            <intent-filter>
                <action android:name="android.intent.action.MAIN"/>
                <category android:name="android.intent.category.LAUNCHER"/>
            </intent-filter>
        </activity>
        <meta-data android:name="flutterEmbedding" android:value="2"/>
        <provider android:authorities="com.example.stegapp.flutter.image_provider" android:exported="false" android:grantUriPermissions="true" android:name="io.flutter.plugins.imagepicker.ImagePickerFileProvider">
            <meta-data android:name="android.support.FILE_PROVIDER_PATHS" android:resource="@xml/flutter_image_picker_file_paths"/>
        </provider>
    </application>
</manifest>
```

Enfin, on lance l'application dans un émulateur pour voir à quoi elle ressemble.

![](apk/emulateur.png)

On a donc affaire à une application qui permet de faire de la stéganographie. Elle est visiblement à sens unique et n'offre pas
de fonctionnalité pour extraire un message d'une image. Il s'agirait d'un prototype pas très abouti, notamment en terme de 
performances, et si on l'essaie... ben je ne sais pas, je n'ai pas été assez patient. Le flag trouvé dans la corbeille serait un essai.
Et pour terminer, il semble qu'elle soit écrite en Flutter.

On note dans le manifest `android:debuggable="true"`, or, si l'application Flutter est en mode debug, on peut retrouver son code source !
Plus précisément, il est intégré au fichier `kernel_blob.bin` extrait par apktool sous `assets/flutter_assets`. Ainsi, en passant un coup
de `strings` sur ce fichier, et en cherchant le nom du fichier principal `lib/main.dart`, on trouve le code de l'appli sous forme de 
[fichier dart](apk/main.dart).

Même sans comprendre dart, on se rend rapidement compte que le code est assez sale. Pas étonnant qu'il ait des problème de performances !
Pour pouvoir être plus à l'aise, modifions ce code pour pouvoir l'exécuter de façon autonome sur notre machine, sans émulateur (pour cela
il vous faut installer un environnement de développement dart). On élimine les référence à Flutter, les widgets Android, on ajoute quelques
traces et on aboutit à [ceci](apk/cli.dart). Ce code peut être exécuté sous la forme `dart cli.dart <input.png> <text-to-hide> <output.png>`.

En analysant le code et en faisant quelques essais, on détermine les actions suivantes :
- l'image d'entrée est redimensionnée à une largeur de 100 pixels (1000 dans le code original, mais c'est un des points influant les perfs).
- chaque pixel est décodé et les valeurs binaires des composants RGB sont extraites pour composer une énorme chaîne de 0 et de 1.
- des portions de la version binaire du message à cacher sont cherchées dans la chaîne précédente, selon un algo moche que je n'ai pas
cherché à comprendre car il ne nous intéresse pas pour le décodage. Il en ressort les offsets des portions dans le flux binaire, et la taille
des portions.
- on reconstruit une image de sortie, comportant dans ses premiers pixels les offsets calculés (ce sont les fameux éléments bizarres qui 
apparaissent en haut à gauche du flag) ainsi que quelques autres éléments techniques, puis l'image redimensionnée.

Le plus gros du boulot est fait ! Reste maintenant à décoder le flag par la méthode inverse à l'aide de [ce script](extract_steg.py) : 
- extraction des offsets et de leurs tailles du premier quart de l'image.
- récupération des portions correspondantes dans le flux binaire des 3 quarts suivant.
- concaténation des portions et conversion de binaire en ascii pour obtenir le message.

En lançant le script :
```console
$ python extract_steg.py flag.png 
30 first pixels in the image : [0, 0, 8, 0, 0, 8, 3, 149, 223, 19, 63, 155, 98, 24, 15, 103, 162, 26, 0, 1, 158, 18, 15, 222, 79, 22, 30, 50, 28, 24, 64, 4, 10, 22, 0, 70, 220, 13, 212, 210, 198, 163, 209, 196, 162, 207, 194, 160, 208, 195, 161, 210, 197, 163, 211, 198, 164, 212, 199, 165, 211, 198, 164, 206, 193, 159, 209, 196, 162, 209, 196, 162, 208, 195, 160, 207, 193, 159, 209, 196, 162, 210, 197, 163, 209, 196, 162, 209, 196, 163]
Size of offsets array : 8
Size of message : 8 bits (?!?)
Offsets : [(234975, 19), (4168546, 24), (1009570, 26), (414, 18), (1039951, 22), (1978908, 24), (4195338, 22), (18140, 13)]
Total : 168 bits
Data extraction : 0100010001000111010 100110100010101000101010 10011010010010100010101000 101011110110100011 0010011000011010001000 111010010010101001101001 0000011001101010010001 1001101111101 01000100 01000111 01010011 01000101 01000101 01010011 01001001 01000101 01000101 01111011 01000110 01001100 00110100 01000111 01001001 01010011 01001000 00110011 01010010 00110011 01111101 
Message : DGSEESIEE{FL4GISH3R3}
```

