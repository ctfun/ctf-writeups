import 'package:cli/cli.dart' as cli;

import 'dart:convert';
import 'dart:math';
import 'dart:io';
import 'package:image/image.dart' as A;


String MessageToBinaryString(String pMessage){
  String Result;
  Result="";
  List<int> bytes = utf8.encode(pMessage);
  bytes.forEach((item) { Result+=item.toRadixString(2).padLeft(8,'0');});
  return Result;
}


Future<void> steggapp(String imagePath, String pMessage, String outputPath) async {
  //Declaration
  String binaryStringmessage;
  String binaryStringImage;
  String binaryStringData;
  List<List<int>> DataList =  List<List<int> >();

  //Initialisation

  //get the two binary string from parameters
  binaryStringmessage= MessageToBinaryString(pMessage);
  File image =new File(imagePath);
  A.Image decodedImage = A.readPng(image.readAsBytesSync());
  A.Image resisedimage = A.copyResize(decodedImage,width:100);

  int offsetdatasize=resisedimage.length*8*3; // 514000*8*3 => 12336000
  print(offsetdatasize.toRadixString(2).length);

  String RRGGBBString;
  String RedBinString;
  String BlueBinString;
  String GreenBinString;
  String PixelString;
  String MegaString;

  print("Size : ${resisedimage.length}, ${resisedimage.width}x${resisedimage.height}");

  MegaString="";
  print(resisedimage[0].toRadixString(2).padLeft(32, '0').substring(8));
  print(resisedimage[0].toRadixString(16));
  for (int i = 0;i < resisedimage.length;i++){
    RRGGBBString=resisedimage[i].toRadixString(2).padLeft(32, '0').substring(8);
    PixelString=RRGGBBString.substring(16,24)+RRGGBBString.substring(8,16)+RRGGBBString.substring(0,8);
    MegaString+=PixelString;
  }

  int messaggelength=0;
  String messagetohide=binaryStringmessage;
  String substringtoFind;
  substringtoFind=messagetohide.substring(0,1);

  String Stringbuilttest="";
  var offsetarray = new List();
  int offsettostore;
  int lengthtostore;
  int offset;
  String Megastringtosearch= MegaString.substring((MegaString.length/4).round());
  String plop = Megastringtosearch;
  print('Megastringtosearch : ${Megastringtosearch.substring(0, 300)}');
  //print("performing data calculation");

  print("Etape 1");
  while(messaggelength < binaryStringmessage.length ) {
    offsettostore=Megastringtosearch.indexOf(substringtoFind);

    //print(Megastringtosearch.substring(offsettostore,offsettostore+substringtoFind.length));
    while(offsettostore !=-1 && substringtoFind.length<=messagetohide.length-1){
        lengthtostore = substringtoFind.length;
        offset = offsettostore;
        substringtoFind = messagetohide.substring(0, substringtoFind.length + 1);
        offsettostore = Megastringtosearch.indexOf(substringtoFind);
        if (offsettostore != -1) {
          print('Offsettostore : ${offsettostore}, substringtoFind :${substringtoFind}, plop[offsettostore]=${plop.substring(offsettostore, offsettostore + substringtoFind.length)}');
        }
      }


  if(substringtoFind.length == messagetohide.length  ){
    int lastoffsettostore=Megastringtosearch.indexOf(substringtoFind);
    if(lastoffsettostore==-1){
      print('1 offsetarrayy.add(${offset}, ${lengthtostore})');
      offsetarray.add([offset, lengthtostore]);
      print('2 offsetarrayy.add(${Megastringtosearch.indexOf(substringtoFind[-1])}, 1)');
      offsetarray.add([Megastringtosearch.indexOf(substringtoFind[-1]),1]);

      Stringbuilttest+=Megastringtosearch.substring(Megastringtosearch.indexOf(substringtoFind[-1]),(Megastringtosearch.indexOf(substringtoFind[-1])+lengthtostore));
    }
    else{
      print('3 offsetarrayy.add(${Megastringtosearch.indexOf(substringtoFind)}, ${substringtoFind.length})');
      offsetarray.add([Megastringtosearch.indexOf(substringtoFind),substringtoFind.length]);
      var lastitem=offsetarray.last;
      Stringbuilttest+=Megastringtosearch.substring(Megastringtosearch.indexOf(substringtoFind),(offsettostore+substringtoFind.length));
    }
    messaggelength+=substringtoFind.length;
  }
  else {
    messagetohide = messagetohide.substring(substringtoFind.length - 1);
    messaggelength += substringtoFind.length;
    Stringbuilttest +=
        Megastringtosearch.substring(offset, (offset + lengthtostore));
    print('4 offsetarrayy.add(${offset}, ${lengthtostore})');
    offsetarray.add([offset, lengthtostore]);

    offsettostore = 0;
    lengthtostore = 1;
    offset = 0;

    substringtoFind = messagetohide.substring(0, 1);
  }


}

print(offsetarray);
  print("Etape 2");


  //int offsetdatasize=resisedimage.length*8*3; // 514000*8*3 => 12336000

  int lenghtdatasize=binaryStringmessage.length;
  int lenghtsizebit=lenghtdatasize.toRadixString(2).length;;
  //int datasizebit= offsetdatasize.toRadixString(2).length; // 101111000011101110000000 => 24
  int datasizebit = 24;

  String stringtowrite="";

  stringtowrite+=offsetarray.length.toRadixString(2).padLeft(datasizebit,'0')+lenghtsizebit.toRadixString(2).padLeft(datasizebit,'0');

  offsetarray.forEach((listofdata){
//      listofdata.forEach((data){
//        print(data.toRadixString(2).padLeft(datasizebit,'0'));
      stringtowrite+=listofdata[0].toRadixString(2).padLeft(datasizebit,'0')+listofdata[1].toRadixString(2).padLeft(lenghtsizebit,'0');
    });
  int lengthofmodifiedstring=stringtowrite.length;
  List<int> pixelvalue= new List();
  int compteur = 0;
  int missingsize;

  print(stringtowrite);
  print("Etape 3");

  String finaleImageString;
  finaleImageString=stringtowrite+MegaString.substring(stringtowrite.length);
  int limit;
  limit=stringtowrite.length;
  while(compteur <limit){
    try {

      pixelvalue.add(int.parse(stringtowrite.substring(0, 8),radix: 2));
      stringtowrite=stringtowrite.substring(8);
      compteur+=8;

    }
    on RangeError {
      missingsize=8-stringtowrite.length;
      pixelvalue.add(int.parse(stringtowrite+finaleImageString.substring(compteur+stringtowrite.length,compteur+stringtowrite.length+missingsize),radix: 2));
      compteur+=8;
    }
  }

  print(pixelvalue);
  print("Etape 4");

  A.Image imagetosave;
  int compteurpixel;
  imagetosave= resisedimage.clone();
  compteurpixel =0;
  List<int> lastpixellist = new List();
  for(int iz=0;iz<pixelvalue.length;iz+=3){
    try{
      var testpixel=pixelvalue[iz+2];
      imagetosave.data[compteurpixel]=A.getColor(pixelvalue[iz],pixelvalue[iz+1], pixelvalue[iz+2]);
      compteurpixel+=1;
    }
    on RangeError{
      pixelvalue=pixelvalue.sublist(iz);
      var basixpixellist=imagetosave.data[compteurpixel].toRadixString(2).padLeft(32, '0').substring(8);
      int RedChannelint=int.parse(basixpixellist.substring(16,24),radix: 2);
      int GreenChannelint=int.parse(basixpixellist.substring(16,24),radix: 2);
      int BlueChannelint=int.parse(basixpixellist.substring(16,24),radix: 2);
      List<int> originalpixelvalue = [RedChannelint,GreenChannelint,BlueChannelint];
      for(int ze=0;ze<=2;ze++){
        if (ze > pixelvalue.length-1){
        lastpixellist.add(originalpixelvalue[ze]);
        }  else {
          lastpixellist.add(pixelvalue[ze]);
        }
      }
      imagetosave.data[compteurpixel]=A.getColor(lastpixellist[0],lastpixellist[1], lastpixellist[2]);
    }
  }

  new File(outputPath)..writeAsBytesSync(A.encodePng(imagetosave));

  //binaryStringImage =ImageToBinary(pImage);
//
//    //calculate the list [numberofbytestoread,[offset,len],[offset,len],...]
//
//    DataList=stegalg(binaryStringmessage,binaryStringImage);
//
//    //convert DataList to binary Stream
//
//    binaryStringData=ListToBin(DataList);
//
//    //Apply this binary List to pixel Array
//    pixelArray=incorporateData(binaryStringData,binaryStringImage);
//
//    //Generate and save the image
//    Targetimage= GenerateImage(pixelArray);

}


void main(List<String> arguments) {
  steggapp(arguments[0], arguments[1], arguments[2]);
}
