#!/usr/bin/env python3

from PIL import Image
import sys

img = Image.open(sys.argv[1])
px = img.load()

data = [ px[i,0][j] for i in range(30) for j in range(3)]
print(f'30 first pixels in the image : {data}')

offsetsize = data.pop(0) << 16 | data.pop(0) << 8 | data.pop(0)
print(f'Size of offsets array : {offsetsize}')

msgsize = data.pop(0) << 16 | data.pop(0) << 8 | data.pop(0)
print(f'Size of message : {msgsize} bits (?!?)')

total = 0
offsets = list()
for _ in range(offsetsize):
    offset = data.pop(0) << 16 | data.pop(0) << 8 | data.pop(0)
    size = data.pop(0)
    total += size
    offsets.append((offset, size))
print(f'Offsets : {offsets}')
print(f'Total : {total} bits') # Ça correspond bien à la taille du message \o/

def toBinary(x):
    return bin(x)[2:].zfill(8)

# Gros bourrin ?
megastring = ''
for j in range(img.height):
    for i in range(img.width):
        rgb = px[i, j]
        #print('%02x%02x%02x' % (rgb[2], rgb[1], rgb[0]))
        pixelstring = toBinary(rgb[0]) + toBinary(rgb[1]) + toBinary(rgb[2])
        #print(pixelstring)
        megastring += pixelstring
megastring = megastring[len(megastring)//4:]

# Extract offsets
print(f'Data extraction : ', end='')
msg = ''
for offset, size in offsets:
    frag = megastring[offset:offset+size]
    print(f'{frag} ', end='')
    msg += frag

# Dump message
nbytes = total // 8
message = ''
for i in range(nbytes):
    octet = msg[i*8:i*8+8]
    print(f'{octet} ', end='')
    c = int(octet, 2)
    if c > 31 or c < 127:
        message += chr(c)
    else:
        message += '?'
print()
print(f'Message : {message}')
