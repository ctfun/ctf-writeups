# Sous l'océan (forensic, 50 points)

# L'énoncé

Nous pensons avoir retrouvé la trace d'Eve Descartes. Nous avons reçu un fichier anonyme provenant d'un smartphone Android (probablement celui de son ravisseur). Retrouvez des informations dans son historique de position.

Le flag est de la forme DGSESIEE{x} avec x une chaine de caractères

[memdump.txt](memdump.txt.bz2) (SHA256=29c702ff8dc570319e5e8d05fc4cb96c1536b595b9a4e93d6205774f9afd2bff)


# Solution

Le dump contient plusieurs séries de positions. Celles-ci pointent toutes sur l'océan, 
ce qui ne semble pas être très intéressant. Peut-être peut-on les tracer pour avancer ?

On commence par écrire une jolie suite de `sed` pour obtenir [une liste de points](plot.csv) extraits
du dump : 
```console
sed "s/\t/ /g" location.txt | tr -s " "  | cut -d" " -f4,5 | sed "s/,/./g" | sed "s/^[^ ]*$//" > plot.csv
```

On utilise ensuite gnuplot pour tracer ceci :
```console
gnuplot> set style line 1 lc rgb "black" lw 1
gnuplot> plot "plot.csv" with lines linestyle 1
```

Le résultat obtenu est :

![](gnuplot.png)

Le flag est donc `DGSESIEE{OC34N}`.