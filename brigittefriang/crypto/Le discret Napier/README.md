# Le discret Napier (crypto, 150 points)

## Énoncé

Stockos a encore choisi un code d'accès qui est solution d'une énigme mathématique ! Retrouvez x tel que : 17^x ≡ 183512102249711162422426526694763570228 [207419578609033051199924683129295125643]

Le flag est de la forme : DGSESIEE{x} avec x la solution 

## Solution

Pas grand chose à dire : c'est une résolution de l'algorithme discret. Il suffit de trouver
la bonne fonction d'un outil mathématique pour le calculer (Meci à Olivier S. pour m'avoir
donné la fonction à utiliser sur GP/Pari).

```console 
$ echo "znlog(183512102249711162422426526694763570228, Mod(17, 207419578609033051199924683129295125643))" | gp -s1000M
Reading GPRC: /etc/gprc ...Done.

                  GP/PARI CALCULATOR Version 2.11.2 (released)
          amd64 running linux (x86-64/GMP-6.1.2 kernel) 64-bit version
        compiled: Jul 12 2019, gcc version 9.1.0 (Ubuntu 9.1.0-8ubuntu1)
                           threading engine: pthread
                (readline v8.0 disabled, extended help enabled)

                     Copyright (C) 2000-2018 The PARI Group

PARI/GP is free software, covered by the GNU General Public License, and comes 
WITHOUT ANY WARRANTY WHATSOEVER.

Type ? for help, \q to quit.
Type ?17 for how to get moral (and possibly technical) support.

parisize = 1000000000, primelimit = 500000, nbthreads = 4
%1 = 697873717765
Goodbye!
```

Le flag est donc `DGSESIEE{697873717765}`.